#include "os.h"
#include "cx.h"
#include <stdbool.h>

#include "os_io_seproxyhal.h"

	
#define OFFSET_CLA 0
#define OFFSET_INS 1
#define OFFSET_P1 2
#define OFFSET_P2 3
#define OFFSET_LC 4
#define OFFSET_CDATA 5

// V1 OFFSET STRUCTURE
#define OFFSET_PATH 5
#define OFFSET_MSG_LEN 25
#define OFFSET_MSG 26

/*
// V2 OFFSET STRUCTURE
#define OFFSET_PATH 5
#define OFFSET_TX_AMOUNT 25
#define OFFSET_MSG_LEN 33
#define OFFSET_MSG 34
*/

#define CLA 0x80
#define P1_FIRST 0x02
#define P2_MORE 0x02
#define P2_LAST 0x04


#define INS_SHOW_BALANCE 0x2C
#define INS_GET_VERSION 0x05
#define INS_GET_PUBLIC_KEY 0x06
#define INS_SIGN 0x03
#define INS_SIGN_BACKUP 0x07
#define INS_GET_ADDRESS 0x09


unsigned char G_io_seproxyhal_spi_buffer[IO_SEPROXYHAL_BUFFER_SIZE_B];


// Removed static	
unsigned int encode_base58(const void *in, unsigned int length, char *out, unsigned int maxoutlen);
static bool derive(void);
static void ui_idle(void);
static void ui_approval(void);
static void ui_show_balance(void);
static void ui_say_ready(void);
static void send_error_reply(void);
static void handleAppVersion(void);


static void get_path_BE(void);
static bool verify_path(void);
static bool verify_api_version(void);
static void ui_reset_captions(void);
static void ui_set_lineBuffer_from_apdu(void);


// ////////////////////////////////////////////////////////////////////////////////

ux_state_t ux;

enum UI_STATE { UI_IDLE, UI_TEXT, UI_APPROVAL };
enum UI_STATE uiState;

static const bagl_element_t* io_seproxyhal_touch_approve(const bagl_element_t *e);
static const bagl_element_t *io_seproxyhal_touch_deny(const bagl_element_t *e);

static const bagl_element_t *io_seproxyhal_touch_exit(const bagl_element_t *e);
static const bagl_element_t *io_seproxyhal_touch_next(const bagl_element_t *e);
static const bagl_element_t *io_seproxyhal_touch_auth(const bagl_element_t *e);


// ////////////////////////////////////////////////////////////////////////////////

// static char address[100];
static char address[];


#define MAX_CHARS_PER_LINE 49
#define DEFAULT_FONT BAGL_FONT_OPEN_SANS_LIGHT_16px | BAGL_FONT_ALIGNMENT_LEFT
#define TEXT_HEIGHT 15
#define TEXT_SPACE 4

int i_cursor = 0;
// Pointer the text part of the INS_NOTIF APDU
WIDE char *text;
static unsigned int current_text_pos; // parsing cursor in the text to display
static unsigned int text_y; // current location of the displayed text

static char lineBuffer[50];

static char status_current_str[120];
static char balance_current_str[120];
static char block_tr_amount_current_str[120];

static unsigned char hashTainted; // notification to restart the hash

// m / purpose' / coin_type' / account' / change / address_index
static unsigned int path[5];
uint32_t path_n[5];
uint32_t i;
uint32_t i_bis;

// Holds the path from the buffer
uint8_t *dataBuffer;

// Added
int msgLen;
#define MAX_MSG_LEN 200

// CONSTANTS

/** MAX_TX_TEXT_WIDTH in blanks, used for clearing a line of text */
static const char TXT_BLANK[] = "                 ";

// static const char status_current_str[] = "Not connected";
static const char welcome_to_str[] = "Welcome to";
static const char blocknet_str[] = "Blocknet";

static const char status_not_connected_str[] = "Not connected";
static const char status_connected_str[] = "Connected";
static const char status_balance_str[] = "Balance:";

static const char status_str[] = "Status:";
static const char status_ready_str[] = "Ready";


// Test variables - can be removed later on

static const char NOT_AVAILABLE[] = "Not available";
static char tmp[100];
static const char BLOCK_TXT[] = "Random text";
static const char ADDR_ERR_TXT[] = "Cannot generate address";


unsigned char privateKeyData[32];
cx_ecfp_public_key_t publicKey;
cx_ecfp_private_key_t privateKey;


// Added from https://github.com/LedgerHQ/blue-sample-apps/blob/master/blue-app-samplesign/src/main.c

// private key in flash. const and N_ variable name are mandatory here
// static const cx_ecfp_private_key_t N_privateKey;
// initialization marker in flash. const and N_ variable name are mandatory here
// static const unsigned char N_initialized;
static cx_sha256_t hash;

// Signature result
unsigned char result[32];


// ********************************************************************************
// Ledger Blue specific UI
// ********************************************************************************

#ifdef TARGET_BLUE

static const bagl_element_t const bagl_ui_sample_blue[] = {
    // {
    //     {type, userid, x, y, width, height, stroke, radius, fill, fgcolor,
    //      bgcolor, font_id, icon_id},
    //     text,
    //     touch_area_brim,
    //     overfgcolor,
    //     overbgcolor,
    //     tap,
    //     out,
    //     over,
    // },
    {
        {BAGL_RECTANGLE, 0x00, 0, 60, 320, 420, 0, 0, BAGL_FILL, 0xf9f9f9,
         0xf9f9f9, 0, 0},
        NULL,
        0,
        0,
        0,
        NULL,
        NULL,
        NULL,
    },
    {
        {BAGL_RECTANGLE, 0x00, 0, 0, 320, 60, 0, 0, BAGL_FILL, 0x1d2028,
         0x1d2028, 0, 0},
        NULL,
        0,
        0,
        0,
        NULL,
        NULL,
        NULL,
    },
    {
        {BAGL_LABEL, 0x00, 20, 0, 320, 60, 0, 0, BAGL_FILL, 0xFFFFFF, 0x1d2028,
         BAGL_FONT_OPEN_SANS_LIGHT_14px | BAGL_FONT_ALIGNMENT_MIDDLE, 0},
        "Welcome to Blocknet !",
        0,
        0,
        0,
        NULL,
        NULL,
        NULL,
    },
    {
        {BAGL_LABEL, 0x00, 20, 100, 320, 60, 0, 0, 0, 0, 0xF9F9F9F9,
         BAGL_FONT_OPEN_SANS_LIGHT_16px | BAGL_FONT_ALIGNMENT_CENTER, 0},
        // address,
        status_current_str,
        0,
        0,
        0,
        NULL,
        NULL,
        NULL,
    },
    {
        {BAGL_BUTTON | BAGL_FLAG_TOUCHABLE, 0x00, 165, 225, 120, 40, 0, 6,
         BAGL_FILL, 0x41ccb4, 0xF9F9F9, BAGL_FONT_OPEN_SANS_LIGHT_14px |
         BAGL_FONT_ALIGNMENT_CENTER | BAGL_FONT_ALIGNMENT_MIDDLE, 0},
        "EXIT",
        0,
        0x37ae99,
        0xF9F9F9,
        io_seproxyhal_touch_exit,
        NULL,
        NULL,
    },
    {
        {BAGL_BUTTON | BAGL_FLAG_TOUCHABLE, 0x00, 165, 280, 120, 40, 0, 6,
         BAGL_FILL, 0x41ccb4, 0xF9F9F9, BAGL_FONT_OPEN_SANS_LIGHT_14px |
         BAGL_FONT_ALIGNMENT_CENTER | BAGL_FONT_ALIGNMENT_MIDDLE, 0},
        "NEXT",
        0,
        0x37ae99,
        0xF9F9F9,
        io_seproxyhal_touch_next,
        NULL,
        NULL,
    },
    {
        {BAGL_BUTTON | BAGL_FLAG_TOUCHABLE, 0x00, 165, 335, 120, 40, 0, 6,
         BAGL_FILL, 0x41ccb4, 0xF9F9F9, BAGL_FONT_OPEN_SANS_LIGHT_14px |
         BAGL_FONT_ALIGNMENT_CENTER | BAGL_FONT_ALIGNMENT_MIDDLE, 0},
        "AUTH",
        0,
        0x37ae99,
        0xF9F9F9,
        io_seproxyhal_touch_auth,
        NULL,
        NULL,
    },
};

static unsigned int
bagl_ui_sample_blue_button(unsigned int button_mask,
                           unsigned int button_mask_counter) {
    return 0;
}

#endif

// ********************************************************************************
// Ledger Nano S specific UI
// ********************************************************************************

#ifdef TARGET_NANOS

/** UI struct for the idle screen **/

static const bagl_element_t bagl_ui_sample_nanos[] = {
    // {
    //     {type, userid, x, y, width, height, stroke, radius, fill, fgcolor,
    //      bgcolor, font_id, icon_id},
    //     text,
    //     touch_area_brim,
    //     overfgcolor,
    //     overbgcolor,
    //     tap,
    //     out,
    //     over,
    // },
    {
        {BAGL_RECTANGLE, 0x00, 0, 0, 128, 32, 0, 0, BAGL_FILL, 0x000000,
         0xFFFFFF, 0, 0},
        NULL,
        0,
        0,
        0,
        NULL,
        NULL,
        NULL,
    },
    // Initial y=12, y=23
    {
        {BAGL_LABELINE, 0x02, 0, 12, 128, 11, 0, 0, 0, 0xFFFFFF, 0x000000,
         BAGL_FONT_OPEN_SANS_REGULAR_11px | BAGL_FONT_ALIGNMENT_CENTER, 0},
        status_current_str,
        0,
        0,
        0,
        NULL,
        NULL,
        NULL,
    },
    {
        {BAGL_LABELINE, 0x02, 23, 26, 82, 11, 0x80 | 10, 0, 0, 0xFFFFFF,
         0x000000, BAGL_FONT_OPEN_SANS_EXTRABOLD_11px |
         BAGL_FONT_ALIGNMENT_CENTER, 26},
        // address,
		lineBuffer,
        0,
        0,
        0,
        NULL,
        NULL,
        NULL,
    },
    {
        {BAGL_ICON, 0x00, 3, 12, 7, 7, 0, 0, 0, 0xFFFFFF, 0x000000, 0,
         BAGL_GLYPH_ICON_CROSS},
        NULL,
        0,
        0,
        0,
        NULL,
        NULL,
        NULL,
    },
    {
        {BAGL_ICON, 0x00, 117, 13, 8, 6, 0, 0, 0, 0xFFFFFF, 0x000000, 0,
         BAGL_GLYPH_ICON_CHECK},
        NULL,
        0,
        0,
        0,
        NULL,
        NULL,
        NULL,
    },
};

static const bagl_element_t*
bagl_ui_sample_nanos_prepro(const bagl_element_t *element) {
    switch (element->component.userid) {
    case 2:
        io_seproxyhal_setup_ticker(
            MAX(3000, 1000 + bagl_label_roundtrip_duration_ms(element, 7)));
        break;
    }
    return element;
}

static unsigned int
bagl_ui_sample_nanos_button(unsigned int button_mask,
                            unsigned int button_mask_counter) {
    switch (button_mask) {
    case BUTTON_EVT_RELEASED | BUTTON_LEFT:
        io_seproxyhal_touch_auth(NULL);
        break;

    case BUTTON_EVT_RELEASED | BUTTON_RIGHT:
        io_seproxyhal_touch_next(NULL);
        break;

    case BUTTON_EVT_RELEASED | BUTTON_LEFT | BUTTON_RIGHT: // EXIT
        io_seproxyhal_touch_exit(NULL);
        break;
    }
    return 0;
}


// //////////////////////////////////////////////////////////////////////////////////


static const bagl_element_t bagl_ui_approval_nanos[] = {
    // {
    //     {type, userid, x, y, width, height, stroke, radius, fill, fgcolor,
    //      bgcolor, font_id, icon_id},
    //     text,
    //     touch_area_brim,
    //     overfgcolor,
    //     overbgcolor,
    //     tap,
    //     out,
    //     over,
    // },
    {
        {BAGL_RECTANGLE, 0x00, 0, 0, 128, 32, 0, 0, BAGL_FILL, 0x000000,
         0xFFFFFF, 0, 0},
        NULL,
        0,
        0,
        0,
        NULL,
        NULL,
        NULL,
    },
    /* first line of description of current screen */
    {
        {BAGL_LABELINE, 0x02, 0, 12, 128, 11, 0, 0, 0, 0xFFFFFF, 0x000000,
         BAGL_FONT_OPEN_SANS_REGULAR_11px | BAGL_FONT_ALIGNMENT_CENTER, 0},
        "Confirm",
        0,
        0,
        0,
        NULL,
        NULL,
        NULL,
    },
    {
        {BAGL_LABELINE, 0x02, 23, 26, 82, 11, 0x80 | 10, 0, 0, 0xFFFFFF,
         0x000000, BAGL_FONT_OPEN_SANS_EXTRABOLD_11px |
         BAGL_FONT_ALIGNMENT_CENTER, 26},
        // address,
		// lineBuffer,
        "transaction ?",
        0,
        0,
        0,
        NULL,
        NULL,
        NULL,
    },
    {
        {BAGL_ICON, 0x00, 3, 12, 7, 7, 0, 0, 0, 0xFFFFFF, 0x000000, 0,
         BAGL_GLYPH_ICON_CROSS},
        NULL,
        0,
        0,
        0,
        NULL,
        NULL,
        NULL,
    },
    {
        {BAGL_ICON, 0x00, 117, 13, 8, 6, 0, 0, 0, 0xFFFFFF, 0x000000, 0,
         BAGL_GLYPH_ICON_CHECK},
        NULL,
        0,
        0,
        0,
        NULL,
        NULL,
        NULL,
    },
};


static unsigned int
bagl_ui_approval_nanos_button(unsigned int button_mask,
                              unsigned int button_mask_counter) {
    switch (button_mask) {
    case BUTTON_EVT_RELEASED | BUTTON_RIGHT:
        io_seproxyhal_touch_approve(NULL);
        break;

    case BUTTON_EVT_RELEASED | BUTTON_LEFT:
        io_seproxyhal_touch_deny(NULL);
        // os_sched_exit(0);
        break;
    }
    return 0;
}

#endif


static const bagl_element_t*
io_seproxyhal_touch_approve(const bagl_element_t *e) {
    
    // Test only

    /*
    G_io_apdu_buffer[0] = 0x90;
    G_io_apdu_buffer[1] = 0x00;

    // Send back the response, do not restart the event loop
    io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, 2);

    */

    unsigned int tx = 0;
                
	// Hash is finalized, send back the signature
	cx_hash(&hash.header, CX_LAST, G_io_apdu_buffer, 0, result, sizeof(result));
	tx = cx_ecdsa_sign((void*) &privateKey, CX_RND_RFC6979 | CX_LAST,
					CX_SHA256, result, sizeof(result), G_io_apdu_buffer, sizeof(G_io_apdu_buffer), NULL);
	
	// &= clears a bit
	G_io_apdu_buffer[0] &= 0xF0; // discard the parity information
					
	// Append response codes
	G_io_apdu_buffer[tx++] = 0x90;
	G_io_apdu_buffer[tx++] = 0x00;
    
    // Send back the response, do not restart the event loop
    io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, tx);

	ui_say_ready();
	
    // Display back the original UX
    ui_idle();
    
	return 0; // do not redraw the widget

}


static const bagl_element_t *io_seproxyhal_touch_deny(const bagl_element_t *e) {
    
    // hashTainted = 1;
    
    G_io_apdu_buffer[0] = 0x69;
    G_io_apdu_buffer[1] = 0x85;
    
    // Send back the response, do not restart the event loop
    io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, 2);
	
	ui_say_ready();
	
    // Display back the original UX    
    ui_idle();
    
    return 0; // do not redraw the widget
    
}


// //////////////////////////////////////////////////////////////////////////////////


static const bagl_element_t *io_seproxyhal_touch_exit(const bagl_element_t *e) {
    // Go back to the dashboard
    os_sched_exit(0);
    return NULL;
}

static const bagl_element_t *io_seproxyhal_touch_next(const bagl_element_t *e) {
    /*
    path[4]++;
    if (!derive()) {
        path[4]--;
    }
    */
    ui_idle();
    return NULL;
}

static const bagl_element_t *io_seproxyhal_touch_auth(const bagl_element_t *e) {
    if (!os_global_pin_is_validated()) {
        bolos_ux_params_t params;
        os_memset(&params, 0, sizeof(params));
        params.ux_id = BOLOS_UX_VALIDATE_PIN;
        os_ux_blocking(&params);
        // derive();
        ui_idle();
    }
    return NULL;
}

unsigned short io_exchange_al(unsigned char channel, unsigned short tx_len) {
    switch (channel & ~(IO_FLAGS)) {
    case CHANNEL_KEYBOARD:
        break;

    // multiplexed io exchange over a SPI channel and TLV encapsulated protocol
    case CHANNEL_SPI:
        if (tx_len) {
            io_seproxyhal_spi_send(G_io_apdu_buffer, tx_len);

            if (channel & IO_RESET_AFTER_REPLIED) {
                reset();
            }
            return 0; // nothing received from the master so far (it's a tx
                      // transaction)
        } else {
            return io_seproxyhal_spi_recv(G_io_apdu_buffer,
                                          sizeof(G_io_apdu_buffer), 0);
        }

    default:
        THROW(INVALID_PARAMETER);
    }
    return 0;
}


static void ui_idle(void) {
#ifdef TARGET_BLUE
    UX_DISPLAY(bagl_ui_sample_blue, NULL);
#else
    UX_DISPLAY(bagl_ui_sample_nanos, bagl_ui_sample_nanos_prepro);
#endif
}


static void ui_approval(void) {
// #ifdef TARGET_BLUE
//    UX_DISPLAY(bagl_ui_sample_blue, NULL);
// #else
    UX_DISPLAY(bagl_ui_approval_nanos, NULL);
// #endif
}


static void blocknet_main(void) {
    volatile unsigned int rx = 0;
    volatile unsigned int tx = 0;
    volatile unsigned int flags = 0;

    // DESIGN NOTE: the bootloader ignores the way APDU are fetched. The only
    // goal is to retrieve APDU.
    // When APDU are to be fetched from multiple IOs, like NFC+USB+BLE, make
    // sure the io_event is called with a
    // switch event, before the apdu is replied to the bootloader. This avoid
    // APDU injection faults.
    for (;;) {
        volatile unsigned short sw = 0;

        BEGIN_TRY {
            TRY {
                rx = tx;
                tx = 0; // ensure no race in catch_other if io_exchange throws
                        // an error
                rx = io_exchange(CHANNEL_APDU | flags, rx);
                flags = 0;

                // no apdu received, well, reset the session, and reset the
                // bootloader configuration
                if (rx == 0) {
                    THROW(0x6982);
                }

                if (G_io_apdu_buffer[0] != 0x80) {
                    THROW(0x6E00);
                }

                // unauthenticated instruction
                switch (G_io_apdu_buffer[1]) {
                
                case 0x00: // reset
                    
					flags |= IO_RESET_AFTER_REPLIED;
                    THROW(0x9000);
                    break;

				case INS_SHOW_BALANCE:
					
					/* FIRST VERSION */
					
					/*
					
					// First UI line
                    // os_memmove(status_current_str, TXT_BLANK, sizeof(TXT_BLANK));
					os_memmove(status_current_str, status_connected_str, 9);

                    // Second UI line             
                    os_memmove(lineBuffer, TXT_BLANK, sizeof(TXT_BLANK));
					
                    text = (char*) G_io_apdu_buffer + 5;
					current_text_pos = 0;
					i_cursor = 0;
					while ((text[current_text_pos] != 0) && (text[current_text_pos] != '\n') &&
						(i_cursor < MAX_CHARS_PER_LINE)) {
						lineBuffer[i_cursor++] = text[current_text_pos];
						current_text_pos++;
					}
					
					if (text[current_text_pos] == '\n') {
						current_text_pos++;
					}
					
					lineBuffer[i_cursor] = '\0';
					
					*/
					
					/* TEST VERSION */
					ui_show_balance();
					
					THROW(0x9000);
					break;

                case INS_GET_VERSION:
					
					/* FIRST VERSION */
					
                    /*
					G_io_apdu_buffer[0] = 0x00;
	                G_io_apdu_buffer[1] = 0x01;
                    G_io_apdu_buffer[2] = 0x90;
	                G_io_apdu_buffer[3] = 0x00;
                    
                    io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, 4);
					*/
					
					handleAppVersion();

                    break;
				
				/*
					P1 = 	0x02 if this is the first text chunk.
							0x00 otherwise
							
					P2 = 	0x04 if the text chunk is the last one.
							0x02 otherwise
				
				*/
                case INS_SIGN:
					
					if ((G_io_apdu_buffer[2] != 0x00) && (G_io_apdu_buffer[2] != 0x02)) {
						THROW(0x6B00);
					}
					
					if ((G_io_apdu_buffer[3] != 0x02) && (G_io_apdu_buffer[3] != 0x04)) {
						THROW(0x6B00);
					}
					
					msgLen = G_io_apdu_buffer[OFFSET_MSG_LEN];
					if (msgLen < 1) {
						THROW(0x6700);
					}

                    // This is the first part of the message to sign
					if (G_io_apdu_buffer[2] == P1_FIRST) {

                        if (!verify_api_version() || !verify_path() ) {
                            send_error_reply();
                        }

						cx_sha256_init(&hash);
					    
						
                        /*
                        dataBuffer = G_io_apdu_buffer + OFFSET_PATH;
						for (i = 0; i < 5; i++) {
							
							path_n[i] = (dataBuffer[0] << 24) 
                                        | (dataBuffer[1] << 16) 
                                        | (dataBuffer[2] << 8) 
                                        | (dataBuffer[3]);

                            dataBuffer += 4;

                        }
                        */
                       
                        get_path_BE();

                        os_perso_derive_node_bip32(CX_CURVE_256K1, path_n, 5, privateKeyData, NULL);

                        cx_ecdsa_init_private_key(CX_CURVE_256K1, privateKeyData, 32, &privateKey);
                        cx_ecfp_generate_pair(CX_CURVE_256K1, &publicKey, &privateKey, 1);
                    
                    }

                    G_io_apdu_buffer[5 + G_io_apdu_buffer[OFFSET_LC]] = '\0';
                    
                    cx_hash(&hash.header, 0, G_io_apdu_buffer + OFFSET_MSG, G_io_apdu_buffer[OFFSET_MSG_LEN], NULL, 0);

                    if (G_io_apdu_buffer[3] == P2_LAST) {
                        
                        ui_approval();						                

                    } else {						
						
                        tx = 0;
                        THROW(0x9000);

                        // Send back the response, do not restart the event loop
                        io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, tx);
						
                    }
                    
                    flags |= IO_ASYNCH_REPLY;

					break;
				
				
				case INS_GET_PUBLIC_KEY:
        	        
                    /*

                    dataBuffer = G_io_apdu_buffer + 5;
                    
                    for (i = 0; i < 5; i++) {

                        path_n[i] = (dataBuffer[0] << 24) 
                                        | (dataBuffer[1] << 16) 
                                        | (dataBuffer[2] << 8) 
                                        | (dataBuffer[3]);

                        dataBuffer += 4;
                        
                    }
                    
                    */


                    if (!verify_path() ) {
                        send_error_reply();
                    }

                    get_path_BE();

                    os_perso_derive_node_bip32(CX_CURVE_256K1, path_n, 5, privateKeyData, NULL);

                    cx_ecdsa_init_private_key(CX_CURVE_256K1, privateKeyData, 32, &privateKey);
                    cx_ecfp_generate_pair(CX_CURVE_256K1, &publicKey, &privateKey, 1);

                    os_memmove(G_io_apdu_buffer, publicKey.W, 65);
                    tx = 65;
                    THROW(0x9000);

                    break;
								
				/*	
					[ Minimum 26 bytes ]
					APDU Header 			=> 5 bytes (CLA + INS + P1 + P2 + LC)
                    BIP32 path [Fixed] 		=> 20 bytes
					Message length 			=> 1 byte
					--------------------------------------------------------------
                    Total offset to DATA	=> 26 bytes				
				*/
				
				case INS_SIGN_BACKUP:
					
					if (G_io_apdu_buffer[2] == P1_FIRST) {
                        
						cx_sha256_init(&hash);
					    
                        dataBuffer = G_io_apdu_buffer + 5;
						for (i = 0; i < 5; i++) {
						    path_n[i] = (dataBuffer[0] << 24) 
                                        | (dataBuffer[1] << 16) 
                                        | (dataBuffer[2] << 8) 
                                        | (dataBuffer[3]);

                            dataBuffer += 4;
                        }

                        os_perso_derive_node_bip32(CX_CURVE_256K1, path_n, 5, privateKeyData, NULL);
                        cx_ecdsa_init_private_key(CX_CURVE_256K1, privateKeyData, 32, &privateKey);
                        cx_ecfp_generate_pair(CX_CURVE_256K1, &publicKey, &privateKey, 1);
                    
                    }

                    G_io_apdu_buffer[5 + G_io_apdu_buffer[OFFSET_LC]] = '\0';
                    
                    cx_hash(&hash.header, 0, G_io_apdu_buffer + OFFSET_MSG, G_io_apdu_buffer[OFFSET_MSG_LEN], NULL, 0);

                    if (G_io_apdu_buffer[3] == P2_LAST) {
												
						tx = 0;
                
                        // Hash is finalized, send back the signature
                        cx_hash(&hash.header, CX_LAST, G_io_apdu_buffer, 0, result, sizeof(result));
                        tx = cx_ecdsa_sign((void*) &privateKey, CX_RND_RFC6979 | CX_LAST,
                                        CX_SHA256, result, sizeof(result), G_io_apdu_buffer, sizeof(G_io_apdu_buffer), NULL);
                        
                        // Is this really required ?
                        // &= clears a bit
                        G_io_apdu_buffer[0] &= 0xF0; // discard the parity information
                                        
                        G_io_apdu_buffer[tx++] = 0x90;
                        G_io_apdu_buffer[tx++] = 0x00;
                                        
                    } else {
						
						tx = 0;
						THROW(0x9000);
						
                    }
					
                    // Send back the response, do not restart the event loop
                    io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, tx);
            
                    // THROW(0x9000);
                    flags |= IO_ASYNCH_REPLY;
                    
                    break;
				
				/* 
					INS_GET_ADDRESS is not necessary for the block client.
					
					It has been added for convenience for external clients that
					may want to read the block ledger wallet without having to derive
					a block address from the public key.
					
				*/
				
				case INS_GET_ADDRESS:
					
					tx = 0;

                    /*
                    dataBuffer = G_io_apdu_buffer + 5;
                    
                    for (i = 0; i < 5; i++) {
                        path_n[i] = (dataBuffer[0] << 24) 
                                        | (dataBuffer[1] << 16) 
                                        | (dataBuffer[2] << 8) 
                                        | (dataBuffer[3]);

                        dataBuffer += 4;                        
                    }
                    */

                    if (!verify_path() ) {
                        send_error_reply();
                    }

                    get_path_BE();

                    if (!derive()) {
                        send_error_reply();
                    }
                    

                    os_memmove(G_io_apdu_buffer, address, 34);
                    tx = 34;
					
					G_io_apdu_buffer[tx++] = 0x90;
                    G_io_apdu_buffer[tx++] = 0x00;
					
					io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, tx);
					flags |= IO_ASYNCH_REPLY;
					
					break;

                case 0xFF: // return to dashboard
                    goto return_to_dashboard;

                default:
                    THROW(0x6D00);
                    break;
                }
            }
            CATCH_OTHER(e) {
                switch (e & 0xF000) {
                case 0x6000:
                case 0x9000:
                    sw = e;
                    break;
                default:
                    sw = 0x6800 | (e & 0x7FF);
                    break;
                }
                // Unexpected exception => report
                G_io_apdu_buffer[tx] = sw >> 8;
                G_io_apdu_buffer[tx + 1] = sw;
                tx += 2;
            }
            FINALLY {
            }
        }
        END_TRY;
    }

return_to_dashboard:
    return;
}

void io_seproxyhal_display(const bagl_element_t *element) {
    io_seproxyhal_display_default((bagl_element_t *)element);
}

unsigned char io_event(unsigned char channel) {
    // nothing done with the event, throw an error on the transport layer if
    // needed

    // can't have more than one tag in the reply, not supported yet.
    switch (G_io_seproxyhal_spi_buffer[0]) {
    case SEPROXYHAL_TAG_FINGER_EVENT:
        UX_FINGER_EVENT(G_io_seproxyhal_spi_buffer);
        break;

    case SEPROXYHAL_TAG_BUTTON_PUSH_EVENT: // for Nano S
        UX_BUTTON_PUSH_EVENT(G_io_seproxyhal_spi_buffer);
        break;

    case SEPROXYHAL_TAG_DISPLAY_PROCESSED_EVENT:
        if (UX_DISPLAYED()) {
            // TODO perform actions after all screen elements have been
            // displayed
        } else {
            UX_DISPLAYED_EVENT();
        }
        break;

    case SEPROXYHAL_TAG_TICKER_EVENT:
        UX_REDISPLAY();
        break;

    // unknown events are acknowledged
    default:
        break;
    }

    // close the event if not done previously (by a display or whatever)
    if (!io_seproxyhal_spi_is_status_sent()) {
        io_seproxyhal_general_status();
    }

    // command has been processed, DO NOT reset the current APDU transport
    return 1;
}

static const char BASE58ALPHABET[] = {
    '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F',
    'G', 'H', 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
    'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};


// static	
unsigned int encode_base58(const void *in, unsigned int length,
                                  char *out, unsigned int maxoutlen) {
    char tmp[164];
    char buffer[164];
    unsigned char j;
    unsigned char startAt;
    unsigned char zeroCount = 0;
    if (length > sizeof(tmp)) {
        THROW(INVALID_PARAMETER);
    }
    os_memmove(tmp, in, length);
    while ((zeroCount < length) && (tmp[zeroCount] == 0)) {
        ++zeroCount;
    }
    j = 2 * length;
    startAt = zeroCount;
    while (startAt < length) {
        unsigned short remainder = 0;
        unsigned char divLoop;
        for (divLoop = startAt; divLoop < length; divLoop++) {
            unsigned short digit256 = (unsigned short)(tmp[divLoop] & 0xff);
            unsigned short tmpDiv = remainder * 256 + digit256;
            tmp[divLoop] = (unsigned char)(tmpDiv / 58);
            remainder = (tmpDiv % 58);
        }
        if (tmp[startAt] == 0) {
            ++startAt;
        }
        buffer[--j] = BASE58ALPHABET[remainder];
    }
    while ((j < (2 * length)) && (buffer[j] == BASE58ALPHABET[0])) {
        ++j;
    }
    while (zeroCount-- > 0) {
        buffer[--j] = BASE58ALPHABET[0];
    }
    length = 2 * length - j;
    if (maxoutlen < length) {
        THROW(EXCEPTION_OVERFLOW);
    }
    os_memmove(out, (buffer + j), length);
    return length;
}


static void handleAppVersion() {
    
	G_io_apdu_buffer[0] = 0x00;
    G_io_apdu_buffer[1] = LEDGER_MAJOR_VERSION;
    G_io_apdu_buffer[2] = LEDGER_MINOR_VERSION;
    G_io_apdu_buffer[3] = LEDGER_PATCH_VERSION;

    G_io_apdu_buffer[4] = 0x90;
    G_io_apdu_buffer[5] = 0x00;
    	
	io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, 6);
}


static bool verify_api_version() {

	#if CX_APILEVEL >= 8
        return true;
	#else        
		return false;
	#endif  

}


static void get_path_BE() {
	
	dataBuffer = G_io_apdu_buffer + 5;
	
	for (i = 0; i < 5; i++) {
		path_n[i] = (dataBuffer[0] << 24) 
					| (dataBuffer[1] << 16) 
					| (dataBuffer[2] << 8) 
					| (dataBuffer[3]);

		dataBuffer += 4;
	}

}


static bool verify_path() {
	
	// Verify hardened paths
	
	bool path_check1 = (path_n[0] & 0x80000000);
	bool path_check2 = (path_n[1] & 0x80000000);
	bool path_check3 = (path_n[2] & 0x80000000);
	
	// Verify coin registration number


	
	if (!path_check1 || !path_check2 || !path_check3) {
		return false;
	}
		
	return true;
	
}


static void ui_reset_captions() {
	
	// First UI line
	os_memmove(status_current_str, TXT_BLANK, sizeof(TXT_BLANK));
	
	// Second UI line             
	os_memmove(lineBuffer, TXT_BLANK, sizeof(TXT_BLANK));

}


static void ui_set_lineBuffer_from_apdu() {
	
	text = (char*) G_io_apdu_buffer + 5;
	current_text_pos = 0;
	i_cursor = 0;
	while ((text[current_text_pos] != 0) && (text[current_text_pos] != '\n') &&
		(i_cursor < MAX_CHARS_PER_LINE)) {
		lineBuffer[i_cursor++] = text[current_text_pos];
		current_text_pos++;
	}
	
	if (text[current_text_pos] == '\n') {
		current_text_pos++;
	}
	
	lineBuffer[i_cursor] = '\0';

}


static void ui_show_balance() {
			
	ui_reset_captions();

	os_memmove(status_current_str, status_balance_str, 9);
	ui_set_lineBuffer_from_apdu();
		
}


static void ui_say_ready() {
	
	ui_reset_captions();

	// Show status string
	os_memmove(status_current_str, status_str, 7);
	
	// Show ready string
	os_memmove(lineBuffer, status_ready_str, 5);

}


static void send_error_reply() {
	
	G_io_apdu_buffer[0] = 0x69;
    G_io_apdu_buffer[1] = 0x85;
	io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, 2);
	ui_idle();
	
}


// Returns a mainnet address only
static bool derive() {

    cx_ecfp_private_key_t privateKey;
    cx_ecfp_public_key_t publicKey;
    
	union {
        cx_sha256_t shasha;
        cx_ripemd160_t riprip;
    } u;
    
	unsigned char privateKeyData[32];
    unsigned char tmp[25];
    unsigned int length;
    
    if (!os_global_pin_is_validated()) {
        os_memmove(address, NOT_AVAILABLE, sizeof(NOT_AVAILABLE));
        return false;
    }

    os_perso_derive_node_bip32(CX_CURVE_256K1, path_n, 5, privateKeyData, NULL);
    cx_ecdsa_init_private_key(CX_CURVE_256K1, privateKeyData, 32, &privateKey);
    cx_ecfp_generate_pair(CX_CURVE_256K1, &publicKey, &privateKey, 1);
    
    publicKey.W[0] = ((publicKey.W[64] & 1) ? 0x03 : 0x02);
    
	cx_sha256_init(&u.shasha);
    cx_hash(&u.shasha.header, CX_LAST, publicKey.W, 33, privateKeyData, sizeof(privateKeyData));
    cx_ripemd160_init(&u.riprip);
    cx_hash(&u.riprip.header, CX_LAST, privateKeyData, 32, tmp + 1, 256);
    
	// Blocknet Prefix : 26 => 0x1A
	tmp[0] = 0x1A;
    
	cx_sha256_init(&u.shasha);
    cx_hash(&u.shasha.header, CX_LAST, tmp, 21, privateKeyData, sizeof(privateKeyData));
    cx_sha256_init(&u.shasha);
    cx_hash(&u.shasha.header, CX_LAST, privateKeyData, 32, privateKeyData, sizeof(privateKeyData));
    os_memmove(tmp + 21, privateKeyData, 4);
    
    length = encode_base58(tmp, sizeof(tmp), address, 34);
    
    address[length] = '\0';
	
    return true;
}

__attribute__((section(".boot"))) int main(void) {
    // exit critical section
    __asm volatile("cpsie i");

    // ensure exception will work as planned
    os_boot();

    UX_INIT();

    BEGIN_TRY {
        TRY {
            io_seproxyhal_init();

            // Invalidate the current authentication to demonstrate reauthentication
            // Reauthenticate with "Auth" (Blue) or left button (Nano S)
            // os_global_pin_invalidate();
            
            
#ifdef LISTEN_BLE
            if (os_seph_features() &
                SEPROXYHAL_TAG_SESSION_START_EVENT_FEATURE_BLE) {
                BLE_power(0, NULL);
                // restart IOs
                BLE_power(1, NULL);
            }
#endif

            USB_power(0);
            USB_power(1);
			
			// Welcome screen
			os_memmove(status_current_str, welcome_to_str, sizeof(welcome_to_str));
            os_memmove(lineBuffer, blocknet_str, sizeof(blocknet_str));
            
            ui_idle();
            blocknet_main();
			
        }
        CATCH_OTHER(e) {
        }
        FINALLY {
        }
    }
    END_TRY;
}