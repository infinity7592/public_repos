# Blocknet trial barebone Ledger Nano S


## Install overview

1. Install the ledger toolchain
2. Set up the dev rules to allow communication
3. cd to the application folder to send to the ledger.
4. *make load* to install the application
or *make delete* to remove it

Note: sometimes the make file has to be tweaked a little.


## APDU communication protocol

Messages are exchanged via packets of the following form:

**CLASS | INSTRUCTION | P1 | P2 | LC | CDATA**

**Remark:**: 
- *CLA** is fixed => 0x80
- *LC* is the length of CDATA
- *CDATA* cannot be more than 255 bytes.


## Paths

Paths on ledger have the following 

Purpose / Coin type / Account / Change / Address_index

In practice, something like that:

44 / 0 / 0 / 0 / 0


## Implemented commands

Preliminary remark:
CLA and INS bytes were arbitrarily selected, and can be changed easily if necessary.


### GET_PUBLIC_KEY

// CLA => 1 byte = 0x80
// INS => 1 byte = 0x06
// P1 => 1 byte = 0x00
// P2 => 1 byte = 0x00
// PATH => 5 * 4 bytes = 20 bytes


### SIGN


// CLA => 1 byte = 0x80
// INS => 1 byte = 0x03
// P1 => 1 byte = 0x00 or 0x02
// P2 => 1 byte = 0x02 or 0x04
// PATH => 5 * 4 bytes = 20 bytes
// MSG_LEN => 1 byte
// MSG => VARIABLE


Given path occupies 20 bytes in the current implementation, this leaves 235 bytes of message content for each packet.
Long messages need to be split up accordingly.
The path needs to be resent each time on each signing related APDU.


## Return messages

- 0x90 => success message
- Anything else, is an error message.


## Links

[Ledger's documentation](http://ledger.readthedocs.io) to get started.



