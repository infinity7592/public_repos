from ledgerblue.comm import getDongle
from ledgerblue.commException import CommException
from secp256k1 import PublicKey
import time
# from ledger import ledger_utils

dongle = getDongle(True)

import binascii

CLA = 0x80
P1 = 0x00
P2 = 0x00
Lc = 0x00


INS_SHOW_BALANCE = 0x2C
INS_GET_VERSION = 0x05
INS_GET_PUBLIC_KEY = 0x06
INS_SIGN = 0x03
INS_GET_ADDRESS = 0x09


# address_path = [44, 0, 0, 0, 0]
address_path = [44, 0, 0, 1, 3]

# https://github.com/LedgerHQ/btchip-python/blob/master/btchip/btchipHelpers.py

def parse_bip32_path_internal(path):
    if len(path) == 0:
        return []
    result = []
    elements = path.split('/')
    for pathElement in elements:
        element = pathElement.split('\'')
        if len(element) == 1:
            result.append(int(element[0]))
        else:
            result.append(0x80000000 | int(element[0]))
    return result


# print(parse_bip32_path_internal("44/0/0/0/0"))
# print(parse_bip32_path_internal("44'/47'/0'/1/3"))


def writeUint32BE(value, buffer, hardened=False):
    if not hardened:
      buffer.append((value >> 24) & 0xff)
    else:
      tmp = (value >> 24) & 0xff
      tmp = 0x80 | tmp
      buffer.append(tmp)
      # print(tmp)
    buffer.append((value >> 16) & 0xff)
    buffer.append((value >> 8) & 0xff)
    buffer.append(value & 0xff)
    return buffer


def writeUint32LE(value, buffer):
    buffer.append(value & 0xff)
    buffer.append((value >> 8) & 0xff)
    buffer.append((value >> 16) & 0xff)
    buffer.append((value >> 24) & 0xff)
    return buffer

    
"""
    The first three elements of the path are hardened
"""
def serialize_bip32_path_internal(path):
    result = []
    cycle = 1
    for pathElement in path:
        if cycle > 3:
          writeUint32BE(pathElement, result, hardened=False)
        else:
          writeUint32BE(pathElement, result, hardened=True)
        cycle += 1
    return bytearray(result)


# print(serialize_bip32_path_internal([44, 0, 0, 0, 0]))
# print(0x80000000 | int(44))

rst = serialize_bip32_path_internal([44, 47, 0, 1, 3])
# print(binascii.hexlify(rst))


"""
    - PART I - HELPER FUNCTIONS
"""    

def get_path_bytes(lst, VERBOSE=False):
    rst = serialize_bip32_path_internal(address_path)
    return bytes(rst)
    rst = []
    for path_elt in lst:
        path_elt_b = (path_elt).to_bytes(4, byteorder="little")
        rst.extend(path_elt_b)
    rst = bytearray(rst)
    rst = bytes(rst)
    if VERBOSE:
        print("rst bytes: %s" % rst)
        print("len: %s bytes" % len(rst))
    return rst

    
def get_notif_apdu(msg="", VERBOSE=True):
    msg = "99999"
    BASE_APDU = [CLA, INS_SHOW_BALANCE, P1, P2]
    apdu = bytearray(BASE_APDU)
    apdu = bytes(apdu)
    apdu += bytes([len(bytes(msg, "utf-8"))])
    apdu += bytes(msg, "utf-8")
    return apdu


def get_version_apdu(VERBOSE=True):
    BASE_APDU = [CLA, INS_GET_VERSION, P1, P2]
    apdu = bytearray(BASE_APDU)
    apdu = bytes(apdu)
    return apdu


def get_address_apdu(VERBOSE=True):
    BASE_APDU = [CLA, INS_GET_ADDRESS, P1, P2]
    apdu = bytearray(BASE_APDU)
    apdu = bytes(apdu)
    bip32_path = get_path_bytes(address_path)
    if VERBOSE:
        print(bip32_path)
    Lc = bytes([len(bip32_path)])
    # Lc should be fixed at 20 bytes = 5 * 4 bytes
    apdu += Lc
    apdu += bip32_path
    return apdu
    
        
def get_pubkey_apdu(VERBOSE=True):
    BASE_APDU = [CLA, INS_GET_PUBLIC_KEY, P1, P2]
    apdu = bytearray(BASE_APDU)
    apdu = bytes(apdu)
    bip32_path = get_path_bytes(address_path)
    if VERBOSE:
        print(bip32_path)
    Lc = bytes([len(bip32_path)])
    # Lc should be fixed at 20 bytes = 5 * 4 bytes
    apdu += Lc
    apdu += bip32_path
    return apdu


def get_short_msg_to_sign_apdu(msg, VERBOSE=True):
    if len(msg) < 1:
        return
    if len(bytes(msg, "utf-8")) > 255 - 20:
        exit(1)
    P1 = bytes.fromhex("02")
    P2 = bytes.fromhex("04")
    BASE_APDU = [CLA, INS_SIGN]
    apdu = bytearray(BASE_APDU)
    apdu = bytes(apdu)
    apdu += P1
    apdu += P2

    bip32_path = get_path_bytes(address_path)
    
    Lc = bytes([len(bip32_path) + 1 + len(bytes(msg, "utf-8"))])
    apdu += Lc

    apdu += bip32_path

    apdu += bytes([len(bytes(msg, "utf-8"))])
    apdu += bytes(msg, "utf-8")

    if VERBOSE:
        print("Lc: %s" % Lc)
        print("len of msg in bytes: %s" % len(bytes(msg, "utf-8")))
        print("final apdu:")
        print(apdu)

    return apdu

    
"""
    Generate a string that is more than 255 bytes
"""
def get_long_msg():
    rst = "a"
    for i in range(350):
        rst += "a"
    return rst

  
"""
    Generate a string that is more than 255 bytes
"""
def sign_long_msg(textToSign="", VERBOSE=True, DRYRUN=False):
    if textToSign == "":
        textToSign = get_long_msg()

    BASE_APDU = [CLA, INS_SIGN]
    bip32_path = get_path_bytes(address_path)

    offset = 0
    round = 0
    # p1 = P1_FIRST
    p1 = bytes.fromhex("02")
    # p2 = P2_LAST
    p2 = bytes.fromhex("04")
    
    """
        P1 = 0x02 if first
            0x00 otherwise
            
        P2 = 0x04 if last
            0x02 otherwise
    """

    while offset != len(textToSign):
        if (len(textToSign) - offset) > 200:
            chunk = textToSign[offset: offset + 200]
        else:
            chunk = textToSign[offset:]
        if (offset + len(chunk)) == len(textToSign):
            # Finished = LAST
            p2 = bytes.fromhex("04")
        else:
            # MORE
            p2 = bytes.fromhex("02")

        apdu = bytearray(BASE_APDU)
        apdu = bytes(apdu)
        apdu += p1
        apdu += p2

        Lc = bytes([len(bip32_path) + 1 + len(bytes(chunk, "utf-8"))])
        apdu += Lc

        apdu += bip32_path
        apdu += bytes([len(bytes(chunk, "utf-8"))])
        # print("bip32_path bytes: %s" % bip32_path)
        # print("bip32_path hex: %s" % binascii.hexlify(bip32_path))
        apdu += bytes(chunk, "utf-8")

        if not DRYRUN:
            signature = dongle.exchange(apdu)

        offset += len(chunk)
        p1 = bytes.fromhex("00")

        round += 1
        if VERBOSE:
            print("[sign_long_msg] step #%s - %s / %s bytes" % (round, len(bytes(chunk, "utf-8")), len(bytes(textToSign, "utf-8"))))
            print(apdu)
            print("")
    if not DRYRUN:
        return signature, textToSign


def run_apdu(apdu, logmsg="", VERBOSE=True):
    if VERBOSE:
        print("[%s] apdu to run in bytes: " % logmsg)
        print(apdu)
        print("[%s] apdu length: %s bytes" % (logmsg, len(apdu)))
        print("[%s] apdu in hex: ")
        print(binascii.hexlify(apdu))
    resp = dongle.exchange(apdu)
    return resp

    
"""
    - PART II - TEST FUNCTIONS
"""


def test_notif(VERBOSE=True):
    notif_apdu = get_notif_apdu()
    notif_resp = run_apdu(notif_apdu)
    if VERBOSE:
        print("test_notif] notif_resp: %s \n" % notif_resp)
    

def test_version(VERBOSE=True):
    version_apdu = get_version_apdu()
    version_resp = run_apdu(version_apdu)
    if VERBOSE:
        print("[test_version] version_resp: %s \n" % version_resp)


def test_getaddress(VERBOSE=True):
    address_apdu = get_address_apdu()
    address_resp = run_apdu(address_apdu)
    if VERBOSE:
        print("address: %s" % address_resp)


def test_get_publickey(VERBOSE=True):
    pubkey_apdu = get_pubkey_apdu()
    publicKey_resp = run_apdu(pubkey_apdu)
    if VERBOSE:
        publickey_hex = binascii.hexlify(publicKey_resp).decode("utf-8")
        print("publickey_hex: %s \n" % publickey_hex)
    return publicKey_resp
        

def test_sign_short_msg(VERBOSE=True):
    msg = "spam"
    short_msg_sig_apdu = get_short_msg_to_sign_apdu(msg)
    signature = run_apdu(short_msg_sig_apdu)
    if VERBOSE:
        print("=> signature response (bytes):")
        print(signature)
        signature_hex = binascii.hexlify(signature).decode("utf-8")
        print("=> signature response (hex):")
        print(signature_hex)
    signature = publicKey.ecdsa_deserialize(bytes(signature))
    print("\n==> verified " + str(publicKey.ecdsa_verify(bytes(msg, "utf-8"), signature)))
    

def test_sign_long_msg(VERBOSE=True):
    signature, textToSign = sign_long_msg()
    if VERBOSE:
        print("=> signature response (bytes):")
        print(signature)
        signature_hex = binascii.hexlify(signature).decode("utf-8")
        print("=> signature response (hex):")
        print(signature_hex)
    signature = publicKey.ecdsa_deserialize(bytes(signature))
    print("\n==> verified " + str(publicKey.ecdsa_verify(bytes(textToSign, "utf-8"), signature)))

   
# test_notif()
# exit(0)

# test_version()
# exit(0)

# test_getaddress()
# exit(0)

publicKey = PublicKey(bytes(test_get_publickey()), raw=True)

# time.sleep(1)
# test_sign_short_msg()
# exit(0)

time.sleep(1)
test_sign_long_msg()
