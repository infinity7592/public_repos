
bin/app.elf:     file format elf32-littlearm


Disassembly of section .text:

c0d00000 <main>:
    address[length] = '\0';
	
    return true;
}

__attribute__((section(".boot"))) int main(void) {
c0d00000:	b5b0      	push	{r4, r5, r7, lr}
c0d00002:	b08c      	sub	sp, #48	; 0x30
    // exit critical section
    __asm volatile("cpsie i");
c0d00004:	b662      	cpsie	i

    // ensure exception will work as planned
    os_boot();
c0d00006:	f000 ff41 	bl	c0d00e8c <os_boot>

    UX_INIT();
c0d0000a:	481f      	ldr	r0, [pc, #124]	; (c0d00088 <main+0x88>)
c0d0000c:	2400      	movs	r4, #0
c0d0000e:	22b0      	movs	r2, #176	; 0xb0
c0d00010:	4621      	mov	r1, r4
c0d00012:	f000 ffeb 	bl	c0d00fec <os_memset>
c0d00016:	ad01      	add	r5, sp, #4

    BEGIN_TRY {
        TRY {
c0d00018:	4628      	mov	r0, r5
c0d0001a:	f002 fe3b 	bl	c0d02c94 <setjmp>
c0d0001e:	8528      	strh	r0, [r5, #40]	; 0x28
c0d00020:	491a      	ldr	r1, [pc, #104]	; (c0d0008c <main+0x8c>)
c0d00022:	4208      	tst	r0, r1
c0d00024:	d002      	beq.n	c0d0002c <main+0x2c>
c0d00026:	a801      	add	r0, sp, #4
            
            ui_idle();
            blocknet_main();
			
        }
        CATCH_OTHER(e) {
c0d00028:	8504      	strh	r4, [r0, #40]	; 0x28
c0d0002a:	e01a      	b.n	c0d00062 <main+0x62>
c0d0002c:	a801      	add	r0, sp, #4
    os_boot();

    UX_INIT();

    BEGIN_TRY {
        TRY {
c0d0002e:	f000 ff30 	bl	c0d00e92 <try_context_set>
            io_seproxyhal_init();
c0d00032:	f001 f9b7 	bl	c0d013a4 <io_seproxyhal_init>
                // restart IOs
                BLE_power(1, NULL);
            }
#endif

            USB_power(0);
c0d00036:	2000      	movs	r0, #0
c0d00038:	f002 fcce 	bl	c0d029d8 <USB_power>
            USB_power(1);
c0d0003c:	2001      	movs	r0, #1
c0d0003e:	f002 fccb 	bl	c0d029d8 <USB_power>
			
			// Welcome screen
			os_memmove(status_current_str, welcome_to_str, sizeof(welcome_to_str));
c0d00042:	4813      	ldr	r0, [pc, #76]	; (c0d00090 <main+0x90>)
c0d00044:	4914      	ldr	r1, [pc, #80]	; (c0d00098 <main+0x98>)
c0d00046:	4479      	add	r1, pc
c0d00048:	220b      	movs	r2, #11
c0d0004a:	f000 ffd8 	bl	c0d00ffe <os_memmove>
            os_memmove(lineBuffer, blocknet_str, sizeof(blocknet_str));
c0d0004e:	4811      	ldr	r0, [pc, #68]	; (c0d00094 <main+0x94>)
c0d00050:	4912      	ldr	r1, [pc, #72]	; (c0d0009c <main+0x9c>)
c0d00052:	4479      	add	r1, pc
c0d00054:	2209      	movs	r2, #9
c0d00056:	f000 ffd2 	bl	c0d00ffe <os_memmove>
            
            ui_idle();
c0d0005a:	f000 faa5 	bl	c0d005a8 <ui_idle>
            blocknet_main();
c0d0005e:	f000 faf1 	bl	c0d00644 <blocknet_main>
			
        }
        CATCH_OTHER(e) {
        }
        FINALLY {
c0d00062:	f001 f86e 	bl	c0d01142 <try_context_get>
c0d00066:	a901      	add	r1, sp, #4
c0d00068:	4288      	cmp	r0, r1
c0d0006a:	d103      	bne.n	c0d00074 <main+0x74>
c0d0006c:	f001 f86b 	bl	c0d01146 <try_context_get_previous>
c0d00070:	f000 ff0f 	bl	c0d00e92 <try_context_set>
c0d00074:	a801      	add	r0, sp, #4
        }
    }
    END_TRY;
c0d00076:	8d00      	ldrh	r0, [r0, #40]	; 0x28
c0d00078:	2800      	cmp	r0, #0
c0d0007a:	d102      	bne.n	c0d00082 <main+0x82>
c0d0007c:	2000      	movs	r0, #0
c0d0007e:	b00c      	add	sp, #48	; 0x30
c0d00080:	bdb0      	pop	{r4, r5, r7, pc}
        CATCH_OTHER(e) {
        }
        FINALLY {
        }
    }
    END_TRY;
c0d00082:	f001 f859 	bl	c0d01138 <os_longjmp>
c0d00086:	46c0      	nop			; (mov r8, r8)
c0d00088:	20001884 	.word	0x20001884
c0d0008c:	0000ffff 	.word	0x0000ffff
c0d00090:	20001934 	.word	0x20001934
c0d00094:	200019ac 	.word	0x200019ac
c0d00098:	00002ce6 	.word	0x00002ce6
c0d0009c:	00002ce5 	.word	0x00002ce5

c0d000a0 <io_exchange_al>:
        ui_idle();
    }
    return NULL;
}

unsigned short io_exchange_al(unsigned char channel, unsigned short tx_len) {
c0d000a0:	b5b0      	push	{r4, r5, r7, lr}
c0d000a2:	4605      	mov	r5, r0
c0d000a4:	200f      	movs	r0, #15
    switch (channel & ~(IO_FLAGS)) {
c0d000a6:	4028      	ands	r0, r5
c0d000a8:	2400      	movs	r4, #0
c0d000aa:	2801      	cmp	r0, #1
c0d000ac:	d013      	beq.n	c0d000d6 <io_exchange_al+0x36>
c0d000ae:	2802      	cmp	r0, #2
c0d000b0:	d113      	bne.n	c0d000da <io_exchange_al+0x3a>
    case CHANNEL_KEYBOARD:
        break;

    // multiplexed io exchange over a SPI channel and TLV encapsulated protocol
    case CHANNEL_SPI:
        if (tx_len) {
c0d000b2:	2900      	cmp	r1, #0
c0d000b4:	d008      	beq.n	c0d000c8 <io_exchange_al+0x28>
            io_seproxyhal_spi_send(G_io_apdu_buffer, tx_len);
c0d000b6:	480a      	ldr	r0, [pc, #40]	; (c0d000e0 <io_exchange_al+0x40>)
c0d000b8:	f001 fde4 	bl	c0d01c84 <io_seproxyhal_spi_send>

            if (channel & IO_RESET_AFTER_REPLIED) {
c0d000bc:	b268      	sxtb	r0, r5
c0d000be:	2800      	cmp	r0, #0
c0d000c0:	da09      	bge.n	c0d000d6 <io_exchange_al+0x36>
                reset();
c0d000c2:	f001 fcc1 	bl	c0d01a48 <reset>
c0d000c6:	e006      	b.n	c0d000d6 <io_exchange_al+0x36>
            }
            return 0; // nothing received from the master so far (it's a tx
                      // transaction)
        } else {
            return io_seproxyhal_spi_recv(G_io_apdu_buffer,
c0d000c8:	21ff      	movs	r1, #255	; 0xff
c0d000ca:	3152      	adds	r1, #82	; 0x52
c0d000cc:	4804      	ldr	r0, [pc, #16]	; (c0d000e0 <io_exchange_al+0x40>)
c0d000ce:	2200      	movs	r2, #0
c0d000d0:	f001 fe04 	bl	c0d01cdc <io_seproxyhal_spi_recv>
c0d000d4:	4604      	mov	r4, r0

    default:
        THROW(INVALID_PARAMETER);
    }
    return 0;
}
c0d000d6:	4620      	mov	r0, r4
c0d000d8:	bdb0      	pop	{r4, r5, r7, pc}
            return io_seproxyhal_spi_recv(G_io_apdu_buffer,
                                          sizeof(G_io_apdu_buffer), 0);
        }

    default:
        THROW(INVALID_PARAMETER);
c0d000da:	2002      	movs	r0, #2
c0d000dc:	f001 f82c 	bl	c0d01138 <os_longjmp>
c0d000e0:	20001b38 	.word	0x20001b38

c0d000e4 <io_seproxyhal_display>:

return_to_dashboard:
    return;
}

void io_seproxyhal_display(const bagl_element_t *element) {
c0d000e4:	b580      	push	{r7, lr}
    io_seproxyhal_display_default((bagl_element_t *)element);
c0d000e6:	f001 facf 	bl	c0d01688 <io_seproxyhal_display_default>
}
c0d000ea:	bd80      	pop	{r7, pc}

c0d000ec <io_event>:

unsigned char io_event(unsigned char channel) {
c0d000ec:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d000ee:	b085      	sub	sp, #20
    // nothing done with the event, throw an error on the transport layer if
    // needed

    // can't have more than one tag in the reply, not supported yet.
    switch (G_io_seproxyhal_spi_buffer[0]) {
c0d000f0:	4dd9      	ldr	r5, [pc, #868]	; (c0d00458 <io_event+0x36c>)
c0d000f2:	7828      	ldrb	r0, [r5, #0]
c0d000f4:	280c      	cmp	r0, #12
c0d000f6:	dc29      	bgt.n	c0d0014c <io_event+0x60>
c0d000f8:	2805      	cmp	r0, #5
c0d000fa:	d05d      	beq.n	c0d001b8 <io_event+0xcc>
c0d000fc:	280c      	cmp	r0, #12
c0d000fe:	d000      	beq.n	c0d00102 <io_event+0x16>
c0d00100:	e175      	b.n	c0d003ee <io_event+0x302>
    case SEPROXYHAL_TAG_FINGER_EVENT:
        UX_FINGER_EVENT(G_io_seproxyhal_spi_buffer);
c0d00102:	4cd6      	ldr	r4, [pc, #856]	; (c0d0045c <io_event+0x370>)
c0d00104:	2001      	movs	r0, #1
c0d00106:	7620      	strb	r0, [r4, #24]
c0d00108:	2600      	movs	r6, #0
c0d0010a:	61e6      	str	r6, [r4, #28]
c0d0010c:	4620      	mov	r0, r4
c0d0010e:	3018      	adds	r0, #24
c0d00110:	f001 fda2 	bl	c0d01c58 <os_ux>
c0d00114:	61e0      	str	r0, [r4, #28]
c0d00116:	f001 fc67 	bl	c0d019e8 <ux_check_status_default>
c0d0011a:	69e0      	ldr	r0, [r4, #28]
c0d0011c:	49d0      	ldr	r1, [pc, #832]	; (c0d00460 <io_event+0x374>)
c0d0011e:	4288      	cmp	r0, r1
c0d00120:	d100      	bne.n	c0d00124 <io_event+0x38>
c0d00122:	e164      	b.n	c0d003ee <io_event+0x302>
c0d00124:	2800      	cmp	r0, #0
c0d00126:	d100      	bne.n	c0d0012a <io_event+0x3e>
c0d00128:	e161      	b.n	c0d003ee <io_event+0x302>
c0d0012a:	49ce      	ldr	r1, [pc, #824]	; (c0d00464 <io_event+0x378>)
c0d0012c:	4288      	cmp	r0, r1
c0d0012e:	d000      	beq.n	c0d00132 <io_event+0x46>
c0d00130:	e0f2      	b.n	c0d00318 <io_event+0x22c>
c0d00132:	f001 f963 	bl	c0d013fc <io_seproxyhal_init_ux>
c0d00136:	60a6      	str	r6, [r4, #8]
c0d00138:	6820      	ldr	r0, [r4, #0]
c0d0013a:	2800      	cmp	r0, #0
c0d0013c:	d100      	bne.n	c0d00140 <io_event+0x54>
c0d0013e:	e156      	b.n	c0d003ee <io_event+0x302>
c0d00140:	69e0      	ldr	r0, [r4, #28]
c0d00142:	49c7      	ldr	r1, [pc, #796]	; (c0d00460 <io_event+0x374>)
c0d00144:	4288      	cmp	r0, r1
c0d00146:	d000      	beq.n	c0d0014a <io_event+0x5e>
c0d00148:	e0c0      	b.n	c0d002cc <io_event+0x1e0>
c0d0014a:	e150      	b.n	c0d003ee <io_event+0x302>
c0d0014c:	280d      	cmp	r0, #13
c0d0014e:	d058      	beq.n	c0d00202 <io_event+0x116>
c0d00150:	280e      	cmp	r0, #14
c0d00152:	d000      	beq.n	c0d00156 <io_event+0x6a>
c0d00154:	e14b      	b.n	c0d003ee <io_event+0x302>
            UX_DISPLAYED_EVENT();
        }
        break;

    case SEPROXYHAL_TAG_TICKER_EVENT:
        UX_REDISPLAY();
c0d00156:	f001 f951 	bl	c0d013fc <io_seproxyhal_init_ux>
c0d0015a:	4cc0      	ldr	r4, [pc, #768]	; (c0d0045c <io_event+0x370>)
c0d0015c:	2000      	movs	r0, #0
c0d0015e:	60a0      	str	r0, [r4, #8]
c0d00160:	6821      	ldr	r1, [r4, #0]
c0d00162:	2900      	cmp	r1, #0
c0d00164:	d100      	bne.n	c0d00168 <io_event+0x7c>
c0d00166:	e142      	b.n	c0d003ee <io_event+0x302>
c0d00168:	69e1      	ldr	r1, [r4, #28]
c0d0016a:	4abd      	ldr	r2, [pc, #756]	; (c0d00460 <io_event+0x374>)
c0d0016c:	4291      	cmp	r1, r2
c0d0016e:	d120      	bne.n	c0d001b2 <io_event+0xc6>
c0d00170:	e13d      	b.n	c0d003ee <io_event+0x302>
c0d00172:	6861      	ldr	r1, [r4, #4]
c0d00174:	4288      	cmp	r0, r1
c0d00176:	d300      	bcc.n	c0d0017a <io_event+0x8e>
c0d00178:	e139      	b.n	c0d003ee <io_event+0x302>
c0d0017a:	f001 fd99 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d0017e:	2800      	cmp	r0, #0
c0d00180:	d000      	beq.n	c0d00184 <io_event+0x98>
c0d00182:	e134      	b.n	c0d003ee <io_event+0x302>
c0d00184:	68a0      	ldr	r0, [r4, #8]
c0d00186:	68e1      	ldr	r1, [r4, #12]
c0d00188:	2538      	movs	r5, #56	; 0x38
c0d0018a:	4368      	muls	r0, r5
c0d0018c:	6822      	ldr	r2, [r4, #0]
c0d0018e:	1810      	adds	r0, r2, r0
c0d00190:	2900      	cmp	r1, #0
c0d00192:	d002      	beq.n	c0d0019a <io_event+0xae>
c0d00194:	4788      	blx	r1
c0d00196:	2800      	cmp	r0, #0
c0d00198:	d007      	beq.n	c0d001aa <io_event+0xbe>
c0d0019a:	2801      	cmp	r0, #1
c0d0019c:	d103      	bne.n	c0d001a6 <io_event+0xba>
c0d0019e:	68a0      	ldr	r0, [r4, #8]
c0d001a0:	4345      	muls	r5, r0
c0d001a2:	6820      	ldr	r0, [r4, #0]
c0d001a4:	1940      	adds	r0, r0, r5
return_to_dashboard:
    return;
}

void io_seproxyhal_display(const bagl_element_t *element) {
    io_seproxyhal_display_default((bagl_element_t *)element);
c0d001a6:	f001 fa6f 	bl	c0d01688 <io_seproxyhal_display_default>
            UX_DISPLAYED_EVENT();
        }
        break;

    case SEPROXYHAL_TAG_TICKER_EVENT:
        UX_REDISPLAY();
c0d001aa:	68a0      	ldr	r0, [r4, #8]
c0d001ac:	1c40      	adds	r0, r0, #1
c0d001ae:	60a0      	str	r0, [r4, #8]
c0d001b0:	6821      	ldr	r1, [r4, #0]
c0d001b2:	2900      	cmp	r1, #0
c0d001b4:	d1dd      	bne.n	c0d00172 <io_event+0x86>
c0d001b6:	e11a      	b.n	c0d003ee <io_event+0x302>
    case SEPROXYHAL_TAG_FINGER_EVENT:
        UX_FINGER_EVENT(G_io_seproxyhal_spi_buffer);
        break;

    case SEPROXYHAL_TAG_BUTTON_PUSH_EVENT: // for Nano S
        UX_BUTTON_PUSH_EVENT(G_io_seproxyhal_spi_buffer);
c0d001b8:	4ca8      	ldr	r4, [pc, #672]	; (c0d0045c <io_event+0x370>)
c0d001ba:	2001      	movs	r0, #1
c0d001bc:	7620      	strb	r0, [r4, #24]
c0d001be:	2600      	movs	r6, #0
c0d001c0:	61e6      	str	r6, [r4, #28]
c0d001c2:	4620      	mov	r0, r4
c0d001c4:	3018      	adds	r0, #24
c0d001c6:	f001 fd47 	bl	c0d01c58 <os_ux>
c0d001ca:	61e0      	str	r0, [r4, #28]
c0d001cc:	f001 fc0c 	bl	c0d019e8 <ux_check_status_default>
c0d001d0:	69e0      	ldr	r0, [r4, #28]
c0d001d2:	49a3      	ldr	r1, [pc, #652]	; (c0d00460 <io_event+0x374>)
c0d001d4:	4288      	cmp	r0, r1
c0d001d6:	d100      	bne.n	c0d001da <io_event+0xee>
c0d001d8:	e109      	b.n	c0d003ee <io_event+0x302>
c0d001da:	2800      	cmp	r0, #0
c0d001dc:	d100      	bne.n	c0d001e0 <io_event+0xf4>
c0d001de:	e106      	b.n	c0d003ee <io_event+0x302>
c0d001e0:	49a0      	ldr	r1, [pc, #640]	; (c0d00464 <io_event+0x378>)
c0d001e2:	4288      	cmp	r0, r1
c0d001e4:	d000      	beq.n	c0d001e8 <io_event+0xfc>
c0d001e6:	e0d1      	b.n	c0d0038c <io_event+0x2a0>
c0d001e8:	f001 f908 	bl	c0d013fc <io_seproxyhal_init_ux>
c0d001ec:	60a6      	str	r6, [r4, #8]
c0d001ee:	6820      	ldr	r0, [r4, #0]
c0d001f0:	2800      	cmp	r0, #0
c0d001f2:	d100      	bne.n	c0d001f6 <io_event+0x10a>
c0d001f4:	e0fb      	b.n	c0d003ee <io_event+0x302>
c0d001f6:	69e0      	ldr	r0, [r4, #28]
c0d001f8:	4999      	ldr	r1, [pc, #612]	; (c0d00460 <io_event+0x374>)
c0d001fa:	4288      	cmp	r0, r1
c0d001fc:	d000      	beq.n	c0d00200 <io_event+0x114>
c0d001fe:	e088      	b.n	c0d00312 <io_event+0x226>
c0d00200:	e0f5      	b.n	c0d003ee <io_event+0x302>
        break;

    case SEPROXYHAL_TAG_DISPLAY_PROCESSED_EVENT:
        if (UX_DISPLAYED()) {
c0d00202:	4c96      	ldr	r4, [pc, #600]	; (c0d0045c <io_event+0x370>)
c0d00204:	6860      	ldr	r0, [r4, #4]
c0d00206:	68a1      	ldr	r1, [r4, #8]
c0d00208:	4281      	cmp	r1, r0
c0d0020a:	d300      	bcc.n	c0d0020e <io_event+0x122>
c0d0020c:	e0ef      	b.n	c0d003ee <io_event+0x302>
            // TODO perform actions after all screen elements have been
            // displayed
        } else {
            UX_DISPLAYED_EVENT();
c0d0020e:	2001      	movs	r0, #1
c0d00210:	7620      	strb	r0, [r4, #24]
c0d00212:	2500      	movs	r5, #0
c0d00214:	61e5      	str	r5, [r4, #28]
c0d00216:	4620      	mov	r0, r4
c0d00218:	3018      	adds	r0, #24
c0d0021a:	f001 fd1d 	bl	c0d01c58 <os_ux>
c0d0021e:	61e0      	str	r0, [r4, #28]
c0d00220:	f001 fbe2 	bl	c0d019e8 <ux_check_status_default>
c0d00224:	69e0      	ldr	r0, [r4, #28]
c0d00226:	498e      	ldr	r1, [pc, #568]	; (c0d00460 <io_event+0x374>)
c0d00228:	4288      	cmp	r0, r1
c0d0022a:	d100      	bne.n	c0d0022e <io_event+0x142>
c0d0022c:	e0df      	b.n	c0d003ee <io_event+0x302>
c0d0022e:	498d      	ldr	r1, [pc, #564]	; (c0d00464 <io_event+0x378>)
c0d00230:	4288      	cmp	r0, r1
c0d00232:	d100      	bne.n	c0d00236 <io_event+0x14a>
c0d00234:	e0e4      	b.n	c0d00400 <io_event+0x314>
c0d00236:	2800      	cmp	r0, #0
c0d00238:	d100      	bne.n	c0d0023c <io_event+0x150>
c0d0023a:	e0d8      	b.n	c0d003ee <io_event+0x302>
c0d0023c:	6820      	ldr	r0, [r4, #0]
c0d0023e:	2800      	cmp	r0, #0
c0d00240:	d100      	bne.n	c0d00244 <io_event+0x158>
c0d00242:	e0ce      	b.n	c0d003e2 <io_event+0x2f6>
c0d00244:	68a0      	ldr	r0, [r4, #8]
c0d00246:	6861      	ldr	r1, [r4, #4]
c0d00248:	4288      	cmp	r0, r1
c0d0024a:	d300      	bcc.n	c0d0024e <io_event+0x162>
c0d0024c:	e0c9      	b.n	c0d003e2 <io_event+0x2f6>
c0d0024e:	f001 fd2f 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d00252:	2800      	cmp	r0, #0
c0d00254:	d000      	beq.n	c0d00258 <io_event+0x16c>
c0d00256:	e0c4      	b.n	c0d003e2 <io_event+0x2f6>
c0d00258:	68a0      	ldr	r0, [r4, #8]
c0d0025a:	68e1      	ldr	r1, [r4, #12]
c0d0025c:	2538      	movs	r5, #56	; 0x38
c0d0025e:	4368      	muls	r0, r5
c0d00260:	6822      	ldr	r2, [r4, #0]
c0d00262:	1810      	adds	r0, r2, r0
c0d00264:	2900      	cmp	r1, #0
c0d00266:	d002      	beq.n	c0d0026e <io_event+0x182>
c0d00268:	4788      	blx	r1
c0d0026a:	2800      	cmp	r0, #0
c0d0026c:	d007      	beq.n	c0d0027e <io_event+0x192>
c0d0026e:	2801      	cmp	r0, #1
c0d00270:	d103      	bne.n	c0d0027a <io_event+0x18e>
c0d00272:	68a0      	ldr	r0, [r4, #8]
c0d00274:	4345      	muls	r5, r0
c0d00276:	6820      	ldr	r0, [r4, #0]
c0d00278:	1940      	adds	r0, r0, r5
return_to_dashboard:
    return;
}

void io_seproxyhal_display(const bagl_element_t *element) {
    io_seproxyhal_display_default((bagl_element_t *)element);
c0d0027a:	f001 fa05 	bl	c0d01688 <io_seproxyhal_display_default>
    case SEPROXYHAL_TAG_DISPLAY_PROCESSED_EVENT:
        if (UX_DISPLAYED()) {
            // TODO perform actions after all screen elements have been
            // displayed
        } else {
            UX_DISPLAYED_EVENT();
c0d0027e:	68a0      	ldr	r0, [r4, #8]
c0d00280:	1c40      	adds	r0, r0, #1
c0d00282:	60a0      	str	r0, [r4, #8]
c0d00284:	6821      	ldr	r1, [r4, #0]
c0d00286:	2900      	cmp	r1, #0
c0d00288:	d1dd      	bne.n	c0d00246 <io_event+0x15a>
c0d0028a:	e0aa      	b.n	c0d003e2 <io_event+0x2f6>
    // needed

    // can't have more than one tag in the reply, not supported yet.
    switch (G_io_seproxyhal_spi_buffer[0]) {
    case SEPROXYHAL_TAG_FINGER_EVENT:
        UX_FINGER_EVENT(G_io_seproxyhal_spi_buffer);
c0d0028c:	6860      	ldr	r0, [r4, #4]
c0d0028e:	4286      	cmp	r6, r0
c0d00290:	d300      	bcc.n	c0d00294 <io_event+0x1a8>
c0d00292:	e0ac      	b.n	c0d003ee <io_event+0x302>
c0d00294:	f001 fd0c 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d00298:	2800      	cmp	r0, #0
c0d0029a:	d000      	beq.n	c0d0029e <io_event+0x1b2>
c0d0029c:	e0a7      	b.n	c0d003ee <io_event+0x302>
c0d0029e:	68a0      	ldr	r0, [r4, #8]
c0d002a0:	68e1      	ldr	r1, [r4, #12]
c0d002a2:	2538      	movs	r5, #56	; 0x38
c0d002a4:	4368      	muls	r0, r5
c0d002a6:	6822      	ldr	r2, [r4, #0]
c0d002a8:	1810      	adds	r0, r2, r0
c0d002aa:	2900      	cmp	r1, #0
c0d002ac:	d002      	beq.n	c0d002b4 <io_event+0x1c8>
c0d002ae:	4788      	blx	r1
c0d002b0:	2800      	cmp	r0, #0
c0d002b2:	d007      	beq.n	c0d002c4 <io_event+0x1d8>
c0d002b4:	2801      	cmp	r0, #1
c0d002b6:	d103      	bne.n	c0d002c0 <io_event+0x1d4>
c0d002b8:	68a0      	ldr	r0, [r4, #8]
c0d002ba:	4345      	muls	r5, r0
c0d002bc:	6820      	ldr	r0, [r4, #0]
c0d002be:	1940      	adds	r0, r0, r5
return_to_dashboard:
    return;
}

void io_seproxyhal_display(const bagl_element_t *element) {
    io_seproxyhal_display_default((bagl_element_t *)element);
c0d002c0:	f001 f9e2 	bl	c0d01688 <io_seproxyhal_display_default>
    // needed

    // can't have more than one tag in the reply, not supported yet.
    switch (G_io_seproxyhal_spi_buffer[0]) {
    case SEPROXYHAL_TAG_FINGER_EVENT:
        UX_FINGER_EVENT(G_io_seproxyhal_spi_buffer);
c0d002c4:	68a0      	ldr	r0, [r4, #8]
c0d002c6:	1c46      	adds	r6, r0, #1
c0d002c8:	60a6      	str	r6, [r4, #8]
c0d002ca:	6820      	ldr	r0, [r4, #0]
c0d002cc:	2800      	cmp	r0, #0
c0d002ce:	d1dd      	bne.n	c0d0028c <io_event+0x1a0>
c0d002d0:	e08d      	b.n	c0d003ee <io_event+0x302>
        break;

    case SEPROXYHAL_TAG_BUTTON_PUSH_EVENT: // for Nano S
        UX_BUTTON_PUSH_EVENT(G_io_seproxyhal_spi_buffer);
c0d002d2:	6860      	ldr	r0, [r4, #4]
c0d002d4:	4286      	cmp	r6, r0
c0d002d6:	d300      	bcc.n	c0d002da <io_event+0x1ee>
c0d002d8:	e089      	b.n	c0d003ee <io_event+0x302>
c0d002da:	f001 fce9 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d002de:	2800      	cmp	r0, #0
c0d002e0:	d000      	beq.n	c0d002e4 <io_event+0x1f8>
c0d002e2:	e084      	b.n	c0d003ee <io_event+0x302>
c0d002e4:	68a0      	ldr	r0, [r4, #8]
c0d002e6:	68e1      	ldr	r1, [r4, #12]
c0d002e8:	2538      	movs	r5, #56	; 0x38
c0d002ea:	4368      	muls	r0, r5
c0d002ec:	6822      	ldr	r2, [r4, #0]
c0d002ee:	1810      	adds	r0, r2, r0
c0d002f0:	2900      	cmp	r1, #0
c0d002f2:	d002      	beq.n	c0d002fa <io_event+0x20e>
c0d002f4:	4788      	blx	r1
c0d002f6:	2800      	cmp	r0, #0
c0d002f8:	d007      	beq.n	c0d0030a <io_event+0x21e>
c0d002fa:	2801      	cmp	r0, #1
c0d002fc:	d103      	bne.n	c0d00306 <io_event+0x21a>
c0d002fe:	68a0      	ldr	r0, [r4, #8]
c0d00300:	4345      	muls	r5, r0
c0d00302:	6820      	ldr	r0, [r4, #0]
c0d00304:	1940      	adds	r0, r0, r5
return_to_dashboard:
    return;
}

void io_seproxyhal_display(const bagl_element_t *element) {
    io_seproxyhal_display_default((bagl_element_t *)element);
c0d00306:	f001 f9bf 	bl	c0d01688 <io_seproxyhal_display_default>
    case SEPROXYHAL_TAG_FINGER_EVENT:
        UX_FINGER_EVENT(G_io_seproxyhal_spi_buffer);
        break;

    case SEPROXYHAL_TAG_BUTTON_PUSH_EVENT: // for Nano S
        UX_BUTTON_PUSH_EVENT(G_io_seproxyhal_spi_buffer);
c0d0030a:	68a0      	ldr	r0, [r4, #8]
c0d0030c:	1c46      	adds	r6, r0, #1
c0d0030e:	60a6      	str	r6, [r4, #8]
c0d00310:	6820      	ldr	r0, [r4, #0]
c0d00312:	2800      	cmp	r0, #0
c0d00314:	d1dd      	bne.n	c0d002d2 <io_event+0x1e6>
c0d00316:	e06a      	b.n	c0d003ee <io_event+0x302>
    // needed

    // can't have more than one tag in the reply, not supported yet.
    switch (G_io_seproxyhal_spi_buffer[0]) {
    case SEPROXYHAL_TAG_FINGER_EVENT:
        UX_FINGER_EVENT(G_io_seproxyhal_spi_buffer);
c0d00318:	88a0      	ldrh	r0, [r4, #4]
c0d0031a:	9004      	str	r0, [sp, #16]
c0d0031c:	6820      	ldr	r0, [r4, #0]
c0d0031e:	9003      	str	r0, [sp, #12]
c0d00320:	79ee      	ldrb	r6, [r5, #7]
c0d00322:	79ab      	ldrb	r3, [r5, #6]
c0d00324:	796f      	ldrb	r7, [r5, #5]
c0d00326:	792a      	ldrb	r2, [r5, #4]
c0d00328:	78ed      	ldrb	r5, [r5, #3]
c0d0032a:	68e1      	ldr	r1, [r4, #12]
c0d0032c:	4668      	mov	r0, sp
c0d0032e:	6005      	str	r5, [r0, #0]
c0d00330:	6041      	str	r1, [r0, #4]
c0d00332:	0212      	lsls	r2, r2, #8
c0d00334:	433a      	orrs	r2, r7
c0d00336:	021b      	lsls	r3, r3, #8
c0d00338:	4333      	orrs	r3, r6
c0d0033a:	9803      	ldr	r0, [sp, #12]
c0d0033c:	9904      	ldr	r1, [sp, #16]
c0d0033e:	f001 f8d5 	bl	c0d014ec <io_seproxyhal_touch_element_callback>
c0d00342:	6820      	ldr	r0, [r4, #0]
c0d00344:	2800      	cmp	r0, #0
c0d00346:	d04c      	beq.n	c0d003e2 <io_event+0x2f6>
c0d00348:	68a0      	ldr	r0, [r4, #8]
c0d0034a:	6861      	ldr	r1, [r4, #4]
c0d0034c:	4288      	cmp	r0, r1
c0d0034e:	d248      	bcs.n	c0d003e2 <io_event+0x2f6>
c0d00350:	f001 fcae 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d00354:	2800      	cmp	r0, #0
c0d00356:	d144      	bne.n	c0d003e2 <io_event+0x2f6>
c0d00358:	68a0      	ldr	r0, [r4, #8]
c0d0035a:	68e1      	ldr	r1, [r4, #12]
c0d0035c:	2538      	movs	r5, #56	; 0x38
c0d0035e:	4368      	muls	r0, r5
c0d00360:	6822      	ldr	r2, [r4, #0]
c0d00362:	1810      	adds	r0, r2, r0
c0d00364:	2900      	cmp	r1, #0
c0d00366:	d002      	beq.n	c0d0036e <io_event+0x282>
c0d00368:	4788      	blx	r1
c0d0036a:	2800      	cmp	r0, #0
c0d0036c:	d007      	beq.n	c0d0037e <io_event+0x292>
c0d0036e:	2801      	cmp	r0, #1
c0d00370:	d103      	bne.n	c0d0037a <io_event+0x28e>
c0d00372:	68a0      	ldr	r0, [r4, #8]
c0d00374:	4345      	muls	r5, r0
c0d00376:	6820      	ldr	r0, [r4, #0]
c0d00378:	1940      	adds	r0, r0, r5
return_to_dashboard:
    return;
}

void io_seproxyhal_display(const bagl_element_t *element) {
    io_seproxyhal_display_default((bagl_element_t *)element);
c0d0037a:	f001 f985 	bl	c0d01688 <io_seproxyhal_display_default>
    // needed

    // can't have more than one tag in the reply, not supported yet.
    switch (G_io_seproxyhal_spi_buffer[0]) {
    case SEPROXYHAL_TAG_FINGER_EVENT:
        UX_FINGER_EVENT(G_io_seproxyhal_spi_buffer);
c0d0037e:	68a0      	ldr	r0, [r4, #8]
c0d00380:	1c40      	adds	r0, r0, #1
c0d00382:	60a0      	str	r0, [r4, #8]
c0d00384:	6821      	ldr	r1, [r4, #0]
c0d00386:	2900      	cmp	r1, #0
c0d00388:	d1df      	bne.n	c0d0034a <io_event+0x25e>
c0d0038a:	e02a      	b.n	c0d003e2 <io_event+0x2f6>
        break;

    case SEPROXYHAL_TAG_BUTTON_PUSH_EVENT: // for Nano S
        UX_BUTTON_PUSH_EVENT(G_io_seproxyhal_spi_buffer);
c0d0038c:	6920      	ldr	r0, [r4, #16]
c0d0038e:	2800      	cmp	r0, #0
c0d00390:	d003      	beq.n	c0d0039a <io_event+0x2ae>
c0d00392:	78e9      	ldrb	r1, [r5, #3]
c0d00394:	0849      	lsrs	r1, r1, #1
c0d00396:	f001 f9f7 	bl	c0d01788 <io_seproxyhal_button_push>
c0d0039a:	6820      	ldr	r0, [r4, #0]
c0d0039c:	2800      	cmp	r0, #0
c0d0039e:	d020      	beq.n	c0d003e2 <io_event+0x2f6>
c0d003a0:	68a0      	ldr	r0, [r4, #8]
c0d003a2:	6861      	ldr	r1, [r4, #4]
c0d003a4:	4288      	cmp	r0, r1
c0d003a6:	d21c      	bcs.n	c0d003e2 <io_event+0x2f6>
c0d003a8:	f001 fc82 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d003ac:	2800      	cmp	r0, #0
c0d003ae:	d118      	bne.n	c0d003e2 <io_event+0x2f6>
c0d003b0:	68a0      	ldr	r0, [r4, #8]
c0d003b2:	68e1      	ldr	r1, [r4, #12]
c0d003b4:	2538      	movs	r5, #56	; 0x38
c0d003b6:	4368      	muls	r0, r5
c0d003b8:	6822      	ldr	r2, [r4, #0]
c0d003ba:	1810      	adds	r0, r2, r0
c0d003bc:	2900      	cmp	r1, #0
c0d003be:	d002      	beq.n	c0d003c6 <io_event+0x2da>
c0d003c0:	4788      	blx	r1
c0d003c2:	2800      	cmp	r0, #0
c0d003c4:	d007      	beq.n	c0d003d6 <io_event+0x2ea>
c0d003c6:	2801      	cmp	r0, #1
c0d003c8:	d103      	bne.n	c0d003d2 <io_event+0x2e6>
c0d003ca:	68a0      	ldr	r0, [r4, #8]
c0d003cc:	4345      	muls	r5, r0
c0d003ce:	6820      	ldr	r0, [r4, #0]
c0d003d0:	1940      	adds	r0, r0, r5
return_to_dashboard:
    return;
}

void io_seproxyhal_display(const bagl_element_t *element) {
    io_seproxyhal_display_default((bagl_element_t *)element);
c0d003d2:	f001 f959 	bl	c0d01688 <io_seproxyhal_display_default>
    case SEPROXYHAL_TAG_FINGER_EVENT:
        UX_FINGER_EVENT(G_io_seproxyhal_spi_buffer);
        break;

    case SEPROXYHAL_TAG_BUTTON_PUSH_EVENT: // for Nano S
        UX_BUTTON_PUSH_EVENT(G_io_seproxyhal_spi_buffer);
c0d003d6:	68a0      	ldr	r0, [r4, #8]
c0d003d8:	1c40      	adds	r0, r0, #1
c0d003da:	60a0      	str	r0, [r4, #8]
c0d003dc:	6821      	ldr	r1, [r4, #0]
c0d003de:	2900      	cmp	r1, #0
c0d003e0:	d1df      	bne.n	c0d003a2 <io_event+0x2b6>
c0d003e2:	6860      	ldr	r0, [r4, #4]
c0d003e4:	68a1      	ldr	r1, [r4, #8]
c0d003e6:	4281      	cmp	r1, r0
c0d003e8:	d301      	bcc.n	c0d003ee <io_event+0x302>
c0d003ea:	f001 fc61 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
    default:
        break;
    }

    // close the event if not done previously (by a display or whatever)
    if (!io_seproxyhal_spi_is_status_sent()) {
c0d003ee:	f001 fc5f 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d003f2:	2800      	cmp	r0, #0
c0d003f4:	d101      	bne.n	c0d003fa <io_event+0x30e>
        io_seproxyhal_general_status();
c0d003f6:	f000 fead 	bl	c0d01154 <io_seproxyhal_general_status>
    }

    // command has been processed, DO NOT reset the current APDU transport
    return 1;
c0d003fa:	2001      	movs	r0, #1
c0d003fc:	b005      	add	sp, #20
c0d003fe:	bdf0      	pop	{r4, r5, r6, r7, pc}
    case SEPROXYHAL_TAG_DISPLAY_PROCESSED_EVENT:
        if (UX_DISPLAYED()) {
            // TODO perform actions after all screen elements have been
            // displayed
        } else {
            UX_DISPLAYED_EVENT();
c0d00400:	f000 fffc 	bl	c0d013fc <io_seproxyhal_init_ux>
c0d00404:	60a5      	str	r5, [r4, #8]
c0d00406:	6820      	ldr	r0, [r4, #0]
c0d00408:	2800      	cmp	r0, #0
c0d0040a:	d0f0      	beq.n	c0d003ee <io_event+0x302>
c0d0040c:	69e0      	ldr	r0, [r4, #28]
c0d0040e:	4914      	ldr	r1, [pc, #80]	; (c0d00460 <io_event+0x374>)
c0d00410:	4288      	cmp	r0, r1
c0d00412:	d11e      	bne.n	c0d00452 <io_event+0x366>
c0d00414:	e7eb      	b.n	c0d003ee <io_event+0x302>
c0d00416:	6860      	ldr	r0, [r4, #4]
c0d00418:	4285      	cmp	r5, r0
c0d0041a:	d2e8      	bcs.n	c0d003ee <io_event+0x302>
c0d0041c:	f001 fc48 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d00420:	2800      	cmp	r0, #0
c0d00422:	d1e4      	bne.n	c0d003ee <io_event+0x302>
c0d00424:	68a0      	ldr	r0, [r4, #8]
c0d00426:	68e1      	ldr	r1, [r4, #12]
c0d00428:	2538      	movs	r5, #56	; 0x38
c0d0042a:	4368      	muls	r0, r5
c0d0042c:	6822      	ldr	r2, [r4, #0]
c0d0042e:	1810      	adds	r0, r2, r0
c0d00430:	2900      	cmp	r1, #0
c0d00432:	d002      	beq.n	c0d0043a <io_event+0x34e>
c0d00434:	4788      	blx	r1
c0d00436:	2800      	cmp	r0, #0
c0d00438:	d007      	beq.n	c0d0044a <io_event+0x35e>
c0d0043a:	2801      	cmp	r0, #1
c0d0043c:	d103      	bne.n	c0d00446 <io_event+0x35a>
c0d0043e:	68a0      	ldr	r0, [r4, #8]
c0d00440:	4345      	muls	r5, r0
c0d00442:	6820      	ldr	r0, [r4, #0]
c0d00444:	1940      	adds	r0, r0, r5
return_to_dashboard:
    return;
}

void io_seproxyhal_display(const bagl_element_t *element) {
    io_seproxyhal_display_default((bagl_element_t *)element);
c0d00446:	f001 f91f 	bl	c0d01688 <io_seproxyhal_display_default>
    case SEPROXYHAL_TAG_DISPLAY_PROCESSED_EVENT:
        if (UX_DISPLAYED()) {
            // TODO perform actions after all screen elements have been
            // displayed
        } else {
            UX_DISPLAYED_EVENT();
c0d0044a:	68a0      	ldr	r0, [r4, #8]
c0d0044c:	1c45      	adds	r5, r0, #1
c0d0044e:	60a5      	str	r5, [r4, #8]
c0d00450:	6820      	ldr	r0, [r4, #0]
c0d00452:	2800      	cmp	r0, #0
c0d00454:	d1df      	bne.n	c0d00416 <io_event+0x32a>
c0d00456:	e7ca      	b.n	c0d003ee <io_event+0x302>
c0d00458:	20001804 	.word	0x20001804
c0d0045c:	20001884 	.word	0x20001884
c0d00460:	b0105044 	.word	0xb0105044
c0d00464:	b0105055 	.word	0xb0105055

c0d00468 <encode_base58>:
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};


// static	
unsigned int encode_base58(const void *in, unsigned int length,
                                  char *out, unsigned int maxoutlen) {
c0d00468:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d0046a:	b0d9      	sub	sp, #356	; 0x164
c0d0046c:	461d      	mov	r5, r3
c0d0046e:	4614      	mov	r4, r2
c0d00470:	460e      	mov	r6, r1
c0d00472:	4601      	mov	r1, r0
    char tmp[164];
    char buffer[164];
    unsigned char j;
    unsigned char startAt;
    unsigned char zeroCount = 0;
    if (length > sizeof(tmp)) {
c0d00474:	2ea5      	cmp	r6, #165	; 0xa5
c0d00476:	d273      	bcs.n	c0d00560 <encode_base58+0xf8>
c0d00478:	a830      	add	r0, sp, #192	; 0xc0
        THROW(INVALID_PARAMETER);
    }
    os_memmove(tmp, in, length);
c0d0047a:	4632      	mov	r2, r6
c0d0047c:	f000 fdbf 	bl	c0d00ffe <os_memmove>
c0d00480:	2200      	movs	r2, #0
    while ((zeroCount < length) && (tmp[zeroCount] == 0)) {
c0d00482:	2e00      	cmp	r6, #0
c0d00484:	d009      	beq.n	c0d0049a <encode_base58+0x32>
c0d00486:	2000      	movs	r0, #0
c0d00488:	4602      	mov	r2, r0
c0d0048a:	a930      	add	r1, sp, #192	; 0xc0
c0d0048c:	5c08      	ldrb	r0, [r1, r0]
c0d0048e:	2800      	cmp	r0, #0
c0d00490:	d103      	bne.n	c0d0049a <encode_base58+0x32>
        ++zeroCount;
c0d00492:	1c52      	adds	r2, r2, #1
    unsigned char zeroCount = 0;
    if (length > sizeof(tmp)) {
        THROW(INVALID_PARAMETER);
    }
    os_memmove(tmp, in, length);
    while ((zeroCount < length) && (tmp[zeroCount] == 0)) {
c0d00494:	b2d0      	uxtb	r0, r2
c0d00496:	42b0      	cmp	r0, r6
c0d00498:	d3f7      	bcc.n	c0d0048a <encode_base58+0x22>
c0d0049a:	9502      	str	r5, [sp, #8]
c0d0049c:	9400      	str	r4, [sp, #0]
c0d0049e:	9203      	str	r2, [sp, #12]
        ++zeroCount;
    }
    j = 2 * length;
    startAt = zeroCount;
    while (startAt < length) {
c0d004a0:	b2d1      	uxtb	r1, r2
    }
    os_memmove(tmp, in, length);
    while ((zeroCount < length) && (tmp[zeroCount] == 0)) {
        ++zeroCount;
    }
    j = 2 * length;
c0d004a2:	0070      	lsls	r0, r6, #1
    startAt = zeroCount;
    while (startAt < length) {
c0d004a4:	42b1      	cmp	r1, r6
c0d004a6:	9001      	str	r0, [sp, #4]
c0d004a8:	4604      	mov	r4, r0
c0d004aa:	d228      	bcs.n	c0d004fe <encode_base58+0x96>
c0d004ac:	9c01      	ldr	r4, [sp, #4]
c0d004ae:	9803      	ldr	r0, [sp, #12]
c0d004b0:	9104      	str	r1, [sp, #16]
c0d004b2:	9405      	str	r4, [sp, #20]
c0d004b4:	9006      	str	r0, [sp, #24]
        unsigned short remainder = 0;
        unsigned char divLoop;
        for (divLoop = startAt; divLoop < length; divLoop++) {
c0d004b6:	b2c4      	uxtb	r4, r0
c0d004b8:	2100      	movs	r1, #0
c0d004ba:	42b4      	cmp	r4, r6
c0d004bc:	d20d      	bcs.n	c0d004da <encode_base58+0x72>
c0d004be:	2100      	movs	r1, #0
c0d004c0:	9f06      	ldr	r7, [sp, #24]
c0d004c2:	ad30      	add	r5, sp, #192	; 0xc0
            unsigned short digit256 = (unsigned short)(tmp[divLoop] & 0xff);
            unsigned short tmpDiv = remainder * 256 + digit256;
c0d004c4:	5d2a      	ldrb	r2, [r5, r4]
c0d004c6:	0208      	lsls	r0, r1, #8
            tmp[divLoop] = (unsigned char)(tmpDiv / 58);
c0d004c8:	4310      	orrs	r0, r2
c0d004ca:	213a      	movs	r1, #58	; 0x3a
            remainder = (tmpDiv % 58);
c0d004cc:	f002 fb88 	bl	c0d02be0 <__aeabi_uidivmod>
        unsigned short remainder = 0;
        unsigned char divLoop;
        for (divLoop = startAt; divLoop < length; divLoop++) {
            unsigned short digit256 = (unsigned short)(tmp[divLoop] & 0xff);
            unsigned short tmpDiv = remainder * 256 + digit256;
            tmp[divLoop] = (unsigned char)(tmpDiv / 58);
c0d004d0:	5528      	strb	r0, [r5, r4]
    j = 2 * length;
    startAt = zeroCount;
    while (startAt < length) {
        unsigned short remainder = 0;
        unsigned char divLoop;
        for (divLoop = startAt; divLoop < length; divLoop++) {
c0d004d2:	1c7f      	adds	r7, r7, #1
c0d004d4:	b2fc      	uxtb	r4, r7
c0d004d6:	42b4      	cmp	r4, r6
c0d004d8:	d3f3      	bcc.n	c0d004c2 <encode_base58+0x5a>
c0d004da:	a830      	add	r0, sp, #192	; 0xc0
            unsigned short digit256 = (unsigned short)(tmp[divLoop] & 0xff);
            unsigned short tmpDiv = remainder * 256 + digit256;
            tmp[divLoop] = (unsigned char)(tmpDiv / 58);
            remainder = (tmpDiv % 58);
        }
        if (tmp[startAt] == 0) {
c0d004dc:	9a04      	ldr	r2, [sp, #16]
c0d004de:	5c82      	ldrb	r2, [r0, r2]
            ++startAt;
        }
        buffer[--j] = BASE58ALPHABET[remainder];
c0d004e0:	a022      	add	r0, pc, #136	; (adr r0, c0d0056c <BASE58ALPHABET>)
c0d004e2:	5c40      	ldrb	r0, [r0, r1]
c0d004e4:	9c05      	ldr	r4, [sp, #20]
c0d004e6:	1e64      	subs	r4, r4, #1
c0d004e8:	b2e1      	uxtb	r1, r4
c0d004ea:	ab07      	add	r3, sp, #28
c0d004ec:	5458      	strb	r0, [r3, r1]
c0d004ee:	9906      	ldr	r1, [sp, #24]
            unsigned short digit256 = (unsigned short)(tmp[divLoop] & 0xff);
            unsigned short tmpDiv = remainder * 256 + digit256;
            tmp[divLoop] = (unsigned char)(tmpDiv / 58);
            remainder = (tmpDiv % 58);
        }
        if (tmp[startAt] == 0) {
c0d004f0:	1c48      	adds	r0, r1, #1
c0d004f2:	2a00      	cmp	r2, #0
c0d004f4:	d000      	beq.n	c0d004f8 <encode_base58+0x90>
c0d004f6:	4608      	mov	r0, r1
    while ((zeroCount < length) && (tmp[zeroCount] == 0)) {
        ++zeroCount;
    }
    j = 2 * length;
    startAt = zeroCount;
    while (startAt < length) {
c0d004f8:	b2c1      	uxtb	r1, r0
c0d004fa:	42b1      	cmp	r1, r6
c0d004fc:	d3d8      	bcc.n	c0d004b0 <encode_base58+0x48>
        if (tmp[startAt] == 0) {
            ++startAt;
        }
        buffer[--j] = BASE58ALPHABET[remainder];
    }
    while ((j < (2 * length)) && (buffer[j] == BASE58ALPHABET[0])) {
c0d004fe:	b2e2      	uxtb	r2, r4
c0d00500:	9e01      	ldr	r6, [sp, #4]
c0d00502:	42b2      	cmp	r2, r6
c0d00504:	d20b      	bcs.n	c0d0051e <encode_base58+0xb6>
c0d00506:	9800      	ldr	r0, [sp, #0]
c0d00508:	9d02      	ldr	r5, [sp, #8]
c0d0050a:	9b03      	ldr	r3, [sp, #12]
c0d0050c:	a907      	add	r1, sp, #28
c0d0050e:	5c89      	ldrb	r1, [r1, r2]
c0d00510:	2931      	cmp	r1, #49	; 0x31
c0d00512:	d107      	bne.n	c0d00524 <encode_base58+0xbc>
        ++j;
c0d00514:	1c64      	adds	r4, r4, #1
        if (tmp[startAt] == 0) {
            ++startAt;
        }
        buffer[--j] = BASE58ALPHABET[remainder];
    }
    while ((j < (2 * length)) && (buffer[j] == BASE58ALPHABET[0])) {
c0d00516:	b2e2      	uxtb	r2, r4
c0d00518:	42b2      	cmp	r2, r6
c0d0051a:	d3f7      	bcc.n	c0d0050c <encode_base58+0xa4>
c0d0051c:	e002      	b.n	c0d00524 <encode_base58+0xbc>
c0d0051e:	9800      	ldr	r0, [sp, #0]
c0d00520:	9d02      	ldr	r5, [sp, #8]
c0d00522:	9b03      	ldr	r3, [sp, #12]
        ++j;
    }
    while (zeroCount-- > 0) {
c0d00524:	0619      	lsls	r1, r3, #24
c0d00526:	d00f      	beq.n	c0d00548 <encode_base58+0xe0>
c0d00528:	4627      	mov	r7, r4
c0d0052a:	4625      	mov	r5, r4
c0d0052c:	4619      	mov	r1, r3
        buffer[--j] = BASE58ALPHABET[0];
c0d0052e:	1e6d      	subs	r5, r5, #1
c0d00530:	b2ea      	uxtb	r2, r5
c0d00532:	ab07      	add	r3, sp, #28
c0d00534:	2431      	movs	r4, #49	; 0x31
c0d00536:	549c      	strb	r4, [r3, r2]
        buffer[--j] = BASE58ALPHABET[remainder];
    }
    while ((j < (2 * length)) && (buffer[j] == BASE58ALPHABET[0])) {
        ++j;
    }
    while (zeroCount-- > 0) {
c0d00538:	1e49      	subs	r1, r1, #1
c0d0053a:	22ff      	movs	r2, #255	; 0xff
c0d0053c:	4211      	tst	r1, r2
c0d0053e:	d1f6      	bne.n	c0d0052e <encode_base58+0xc6>
c0d00540:	9903      	ldr	r1, [sp, #12]
c0d00542:	463c      	mov	r4, r7
c0d00544:	1a64      	subs	r4, r4, r1
c0d00546:	9d02      	ldr	r5, [sp, #8]
        buffer[--j] = BASE58ALPHABET[0];
    }
    length = 2 * length - j;
c0d00548:	b2e2      	uxtb	r2, r4
c0d0054a:	1ab6      	subs	r6, r6, r2
    if (maxoutlen < length) {
c0d0054c:	42ae      	cmp	r6, r5
c0d0054e:	d80a      	bhi.n	c0d00566 <encode_base58+0xfe>
c0d00550:	a907      	add	r1, sp, #28
        THROW(EXCEPTION_OVERFLOW);
    }
    os_memmove(out, (buffer + j), length);
c0d00552:	1889      	adds	r1, r1, r2
c0d00554:	4632      	mov	r2, r6
c0d00556:	f000 fd52 	bl	c0d00ffe <os_memmove>
    return length;
c0d0055a:	4630      	mov	r0, r6
c0d0055c:	b059      	add	sp, #356	; 0x164
c0d0055e:	bdf0      	pop	{r4, r5, r6, r7, pc}
    char buffer[164];
    unsigned char j;
    unsigned char startAt;
    unsigned char zeroCount = 0;
    if (length > sizeof(tmp)) {
        THROW(INVALID_PARAMETER);
c0d00560:	2002      	movs	r0, #2
c0d00562:	f000 fde9 	bl	c0d01138 <os_longjmp>
    while (zeroCount-- > 0) {
        buffer[--j] = BASE58ALPHABET[0];
    }
    length = 2 * length - j;
    if (maxoutlen < length) {
        THROW(EXCEPTION_OVERFLOW);
c0d00566:	2003      	movs	r0, #3
c0d00568:	f000 fde6 	bl	c0d01138 <os_longjmp>

c0d0056c <BASE58ALPHABET>:
c0d0056c:	34333231 	.word	0x34333231
c0d00570:	38373635 	.word	0x38373635
c0d00574:	43424139 	.word	0x43424139
c0d00578:	47464544 	.word	0x47464544
c0d0057c:	4c4b4a48 	.word	0x4c4b4a48
c0d00580:	51504e4d 	.word	0x51504e4d
c0d00584:	55545352 	.word	0x55545352
c0d00588:	59585756 	.word	0x59585756
c0d0058c:	6362615a 	.word	0x6362615a
c0d00590:	67666564 	.word	0x67666564
c0d00594:	6b6a6968 	.word	0x6b6a6968
c0d00598:	706f6e6d 	.word	0x706f6e6d
c0d0059c:	74737271 	.word	0x74737271
c0d005a0:	78777675 	.word	0x78777675
c0d005a4:	00007a79 	.word	0x00007a79

c0d005a8 <ui_idle>:
    }
    return 0;
}


static void ui_idle(void) {
c0d005a8:	b5b0      	push	{r4, r5, r7, lr}
#ifdef TARGET_BLUE
    UX_DISPLAY(bagl_ui_sample_blue, NULL);
#else
    UX_DISPLAY(bagl_ui_sample_nanos, bagl_ui_sample_nanos_prepro);
c0d005aa:	4c21      	ldr	r4, [pc, #132]	; (c0d00630 <ui_idle+0x88>)
c0d005ac:	4822      	ldr	r0, [pc, #136]	; (c0d00638 <ui_idle+0x90>)
c0d005ae:	4478      	add	r0, pc
c0d005b0:	6020      	str	r0, [r4, #0]
c0d005b2:	2005      	movs	r0, #5
c0d005b4:	6060      	str	r0, [r4, #4]
c0d005b6:	4821      	ldr	r0, [pc, #132]	; (c0d0063c <ui_idle+0x94>)
c0d005b8:	4478      	add	r0, pc
c0d005ba:	6120      	str	r0, [r4, #16]
c0d005bc:	4820      	ldr	r0, [pc, #128]	; (c0d00640 <ui_idle+0x98>)
c0d005be:	4478      	add	r0, pc
c0d005c0:	60e0      	str	r0, [r4, #12]
c0d005c2:	2003      	movs	r0, #3
c0d005c4:	7620      	strb	r0, [r4, #24]
c0d005c6:	2500      	movs	r5, #0
c0d005c8:	61e5      	str	r5, [r4, #28]
c0d005ca:	4620      	mov	r0, r4
c0d005cc:	3018      	adds	r0, #24
c0d005ce:	f001 fb43 	bl	c0d01c58 <os_ux>
c0d005d2:	61e0      	str	r0, [r4, #28]
c0d005d4:	f001 fa08 	bl	c0d019e8 <ux_check_status_default>
c0d005d8:	f000 ff10 	bl	c0d013fc <io_seproxyhal_init_ux>
c0d005dc:	60a5      	str	r5, [r4, #8]
c0d005de:	6820      	ldr	r0, [r4, #0]
c0d005e0:	2800      	cmp	r0, #0
c0d005e2:	d024      	beq.n	c0d0062e <ui_idle+0x86>
c0d005e4:	69e0      	ldr	r0, [r4, #28]
c0d005e6:	4913      	ldr	r1, [pc, #76]	; (c0d00634 <ui_idle+0x8c>)
c0d005e8:	4288      	cmp	r0, r1
c0d005ea:	d11e      	bne.n	c0d0062a <ui_idle+0x82>
c0d005ec:	e01f      	b.n	c0d0062e <ui_idle+0x86>
c0d005ee:	6860      	ldr	r0, [r4, #4]
c0d005f0:	4285      	cmp	r5, r0
c0d005f2:	d21c      	bcs.n	c0d0062e <ui_idle+0x86>
c0d005f4:	f001 fb5c 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d005f8:	2800      	cmp	r0, #0
c0d005fa:	d118      	bne.n	c0d0062e <ui_idle+0x86>
c0d005fc:	68a0      	ldr	r0, [r4, #8]
c0d005fe:	68e1      	ldr	r1, [r4, #12]
c0d00600:	2538      	movs	r5, #56	; 0x38
c0d00602:	4368      	muls	r0, r5
c0d00604:	6822      	ldr	r2, [r4, #0]
c0d00606:	1810      	adds	r0, r2, r0
c0d00608:	2900      	cmp	r1, #0
c0d0060a:	d002      	beq.n	c0d00612 <ui_idle+0x6a>
c0d0060c:	4788      	blx	r1
c0d0060e:	2800      	cmp	r0, #0
c0d00610:	d007      	beq.n	c0d00622 <ui_idle+0x7a>
c0d00612:	2801      	cmp	r0, #1
c0d00614:	d103      	bne.n	c0d0061e <ui_idle+0x76>
c0d00616:	68a0      	ldr	r0, [r4, #8]
c0d00618:	4345      	muls	r5, r0
c0d0061a:	6820      	ldr	r0, [r4, #0]
c0d0061c:	1940      	adds	r0, r0, r5
return_to_dashboard:
    return;
}

void io_seproxyhal_display(const bagl_element_t *element) {
    io_seproxyhal_display_default((bagl_element_t *)element);
c0d0061e:	f001 f833 	bl	c0d01688 <io_seproxyhal_display_default>

static void ui_idle(void) {
#ifdef TARGET_BLUE
    UX_DISPLAY(bagl_ui_sample_blue, NULL);
#else
    UX_DISPLAY(bagl_ui_sample_nanos, bagl_ui_sample_nanos_prepro);
c0d00622:	68a0      	ldr	r0, [r4, #8]
c0d00624:	1c45      	adds	r5, r0, #1
c0d00626:	60a5      	str	r5, [r4, #8]
c0d00628:	6820      	ldr	r0, [r4, #0]
c0d0062a:	2800      	cmp	r0, #0
c0d0062c:	d1df      	bne.n	c0d005ee <ui_idle+0x46>
#endif
}
c0d0062e:	bdb0      	pop	{r4, r5, r7, pc}
c0d00630:	20001884 	.word	0x20001884
c0d00634:	b0105044 	.word	0xb0105044
c0d00638:	00002792 	.word	0x00002792
c0d0063c:	00000711 	.word	0x00000711
c0d00640:	0000075f 	.word	0x0000075f

c0d00644 <blocknet_main>:
    UX_DISPLAY(bagl_ui_approval_nanos, NULL);
// #endif
}


static void blocknet_main(void) {
c0d00644:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d00646:	b0e9      	sub	sp, #420	; 0x1a4
c0d00648:	2600      	movs	r6, #0
    volatile unsigned int rx = 0;
c0d0064a:	9621      	str	r6, [sp, #132]	; 0x84
    volatile unsigned int tx = 0;
c0d0064c:	9620      	str	r6, [sp, #128]	; 0x80
    volatile unsigned int flags = 0;
c0d0064e:	961f      	str	r6, [sp, #124]	; 0x7c
c0d00650:	a822      	add	r0, sp, #136	; 0x88
c0d00652:	1c41      	adds	r1, r0, #1
c0d00654:	9110      	str	r1, [sp, #64]	; 0x40
c0d00656:	3015      	adds	r0, #21
c0d00658:	9011      	str	r0, [sp, #68]	; 0x44
c0d0065a:	a84c      	add	r0, sp, #304	; 0x130
c0d0065c:	4601      	mov	r1, r0
c0d0065e:	3108      	adds	r1, #8
c0d00660:	910e      	str	r1, [sp, #56]	; 0x38
c0d00662:	3048      	adds	r0, #72	; 0x48
c0d00664:	900f      	str	r0, [sp, #60]	; 0x3c
c0d00666:	4cc6      	ldr	r4, [pc, #792]	; (c0d00980 <blocknet_main+0x33c>)
c0d00668:	4fc6      	ldr	r7, [pc, #792]	; (c0d00984 <blocknet_main+0x340>)
c0d0066a:	9612      	str	r6, [sp, #72]	; 0x48
c0d0066c:	e16c      	b.n	c0d00948 <blocknet_main+0x304>
c0d0066e:	920b      	str	r2, [sp, #44]	; 0x2c
c0d00670:	930d      	str	r3, [sp, #52]	; 0x34
c0d00672:	2500      	movs	r5, #0
					
				*/
				
				case INS_GET_ADDRESS:
					
					tx = 0;
c0d00674:	9520      	str	r5, [sp, #128]	; 0x80
c0d00676:	48c4      	ldr	r0, [pc, #784]	; (c0d00988 <blocknet_main+0x344>)
c0d00678:	4602      	mov	r2, r0

static bool verify_path() {
	
	// Verify hardened paths
	
	bool path_check1 = (path_n[0] & 0x80000000);
c0d0067a:	ca07      	ldmia	r2, {r0, r1, r2}
	
	// Verify coin registration number


	
	if (!path_check1 || !path_check2 || !path_check3) {
c0d0067c:	4002      	ands	r2, r0
c0d0067e:	400a      	ands	r2, r1

                        dataBuffer += 4;                        
                    }
                    */

                    if (!verify_path() ) {
c0d00680:	2a00      	cmp	r2, #0
c0d00682:	db09      	blt.n	c0d00698 <blocknet_main+0x54>
}


static void send_error_reply() {
	
	G_io_apdu_buffer[0] = 0x69;
c0d00684:	2069      	movs	r0, #105	; 0x69
c0d00686:	7020      	strb	r0, [r4, #0]
    G_io_apdu_buffer[1] = 0x85;
c0d00688:	980d      	ldr	r0, [sp, #52]	; 0x34
c0d0068a:	7060      	strb	r0, [r4, #1]
	io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, 2);
c0d0068c:	2020      	movs	r0, #32
c0d0068e:	2102      	movs	r1, #2
c0d00690:	f001 f8b4 	bl	c0d017fc <io_exchange>
	ui_idle();
c0d00694:	f7ff ff88 	bl	c0d005a8 <ui_idle>

                    if (!verify_path() ) {
                        send_error_reply();
                    }

                    get_path_BE();
c0d00698:	f000 fb5c 	bl	c0d00d54 <get_path_BE>
    
	unsigned char privateKeyData[32];
    unsigned char tmp[25];
    unsigned int length;
    
    if (!os_global_pin_is_validated()) {
c0d0069c:	f001 fab0 	bl	c0d01c00 <os_global_pin_is_validated>
c0d006a0:	2800      	cmp	r0, #0
c0d006a2:	950a      	str	r5, [sp, #40]	; 0x28
c0d006a4:	d100      	bne.n	c0d006a8 <blocknet_main+0x64>
c0d006a6:	e122      	b.n	c0d008ee <blocknet_main+0x2aa>
        os_memmove(address, NOT_AVAILABLE, sizeof(NOT_AVAILABLE));
        return false;
    }

    os_perso_derive_node_bip32(CX_CURVE_256K1, path_n, 5, privateKeyData, NULL);
c0d006a8:	4668      	mov	r0, sp
c0d006aa:	6005      	str	r5, [r0, #0]
c0d006ac:	2021      	movs	r0, #33	; 0x21
c0d006ae:	2205      	movs	r2, #5
c0d006b0:	af29      	add	r7, sp, #164	; 0xa4
c0d006b2:	4605      	mov	r5, r0
c0d006b4:	49b4      	ldr	r1, [pc, #720]	; (c0d00988 <blocknet_main+0x344>)
c0d006b6:	463b      	mov	r3, r7
c0d006b8:	9707      	str	r7, [sp, #28]
c0d006ba:	f001 fa89 	bl	c0d01bd0 <os_perso_derive_node_bip32>
    cx_ecdsa_init_private_key(CX_CURVE_256K1, privateKeyData, 32, &privateKey);
c0d006be:	2220      	movs	r2, #32
c0d006c0:	9209      	str	r2, [sp, #36]	; 0x24
c0d006c2:	ab5f      	add	r3, sp, #380	; 0x17c
c0d006c4:	9304      	str	r3, [sp, #16]
c0d006c6:	9506      	str	r5, [sp, #24]
c0d006c8:	4628      	mov	r0, r5
c0d006ca:	4639      	mov	r1, r7
c0d006cc:	f001 fa30 	bl	c0d01b30 <cx_ecfp_init_private_key>
c0d006d0:	af4c      	add	r7, sp, #304	; 0x130
    cx_ecfp_generate_pair(CX_CURVE_256K1, &publicKey, &privateKey, 1);
c0d006d2:	4628      	mov	r0, r5
c0d006d4:	4639      	mov	r1, r7
c0d006d6:	9a04      	ldr	r2, [sp, #16]
c0d006d8:	4633      	mov	r3, r6
c0d006da:	f001 fa41 	bl	c0d01b60 <cx_ecfp_generate_pair>
    
    publicKey.W[0] = ((publicKey.W[64] & 1) ? 0x03 : 0x02);
c0d006de:	980f      	ldr	r0, [sp, #60]	; 0x3c
c0d006e0:	7800      	ldrb	r0, [r0, #0]
c0d006e2:	4030      	ands	r0, r6
c0d006e4:	2102      	movs	r1, #2
c0d006e6:	4301      	orrs	r1, r0
c0d006e8:	7239      	strb	r1, [r7, #8]
c0d006ea:	af31      	add	r7, sp, #196	; 0xc4
    
	cx_sha256_init(&u.shasha);
c0d006ec:	4638      	mov	r0, r7
c0d006ee:	f001 fa09 	bl	c0d01b04 <cx_sha256_init>
    cx_hash(&u.shasha.header, CX_LAST, publicKey.W, 33, privateKeyData, sizeof(privateKeyData));
c0d006f2:	4668      	mov	r0, sp
c0d006f4:	9d07      	ldr	r5, [sp, #28]
c0d006f6:	6005      	str	r5, [r0, #0]
c0d006f8:	9909      	ldr	r1, [sp, #36]	; 0x24
c0d006fa:	6041      	str	r1, [r0, #4]
c0d006fc:	4638      	mov	r0, r7
c0d006fe:	4631      	mov	r1, r6
c0d00700:	9a0e      	ldr	r2, [sp, #56]	; 0x38
c0d00702:	9b06      	ldr	r3, [sp, #24]
c0d00704:	f001 f9cc 	bl	c0d01aa0 <cx_hash>
    cx_ripemd160_init(&u.riprip);
c0d00708:	4638      	mov	r0, r7
c0d0070a:	f001 f9e5 	bl	c0d01ad8 <cx_ripemd160_init>
    cx_hash(&u.riprip.header, CX_LAST, privateKeyData, 32, tmp + 1, 256);
c0d0070e:	4668      	mov	r0, sp
c0d00710:	9910      	ldr	r1, [sp, #64]	; 0x40
c0d00712:	6001      	str	r1, [r0, #0]
c0d00714:	990b      	ldr	r1, [sp, #44]	; 0x2c
c0d00716:	6041      	str	r1, [r0, #4]
c0d00718:	4638      	mov	r0, r7
c0d0071a:	4631      	mov	r1, r6
c0d0071c:	462a      	mov	r2, r5
c0d0071e:	9d09      	ldr	r5, [sp, #36]	; 0x24
c0d00720:	462b      	mov	r3, r5
c0d00722:	f001 f9bd 	bl	c0d01aa0 <cx_hash>
c0d00726:	a922      	add	r1, sp, #136	; 0x88
    
	// Blocknet Prefix : 26 => 0x1A
	tmp[0] = 0x1A;
c0d00728:	9108      	str	r1, [sp, #32]
c0d0072a:	201a      	movs	r0, #26
c0d0072c:	7008      	strb	r0, [r1, #0]
    
	cx_sha256_init(&u.shasha);
c0d0072e:	4638      	mov	r0, r7
c0d00730:	f001 f9e8 	bl	c0d01b04 <cx_sha256_init>
    cx_hash(&u.shasha.header, CX_LAST, tmp, 21, privateKeyData, sizeof(privateKeyData));
c0d00734:	4668      	mov	r0, sp
c0d00736:	9907      	ldr	r1, [sp, #28]
c0d00738:	c022      	stmia	r0!, {r1, r5}
c0d0073a:	2315      	movs	r3, #21
c0d0073c:	4638      	mov	r0, r7
c0d0073e:	4631      	mov	r1, r6
c0d00740:	9a08      	ldr	r2, [sp, #32]
c0d00742:	f001 f9ad 	bl	c0d01aa0 <cx_hash>
    cx_sha256_init(&u.shasha);
c0d00746:	4638      	mov	r0, r7
c0d00748:	f001 f9dc 	bl	c0d01b04 <cx_sha256_init>
    cx_hash(&u.shasha.header, CX_LAST, privateKeyData, 32, privateKeyData, sizeof(privateKeyData));
c0d0074c:	4668      	mov	r0, sp
c0d0074e:	9a07      	ldr	r2, [sp, #28]
c0d00750:	6002      	str	r2, [r0, #0]
c0d00752:	462b      	mov	r3, r5
c0d00754:	6043      	str	r3, [r0, #4]
c0d00756:	4638      	mov	r0, r7
c0d00758:	4f8a      	ldr	r7, [pc, #552]	; (c0d00984 <blocknet_main+0x340>)
c0d0075a:	4631      	mov	r1, r6
c0d0075c:	4615      	mov	r5, r2
c0d0075e:	f001 f99f 	bl	c0d01aa0 <cx_hash>
c0d00762:	2204      	movs	r2, #4
    os_memmove(tmp + 21, privateKeyData, 4);
c0d00764:	9811      	ldr	r0, [sp, #68]	; 0x44
c0d00766:	4629      	mov	r1, r5
c0d00768:	f000 fc49 	bl	c0d00ffe <os_memmove>
    
    length = encode_base58(tmp, sizeof(tmp), address, 34);
c0d0076c:	2119      	movs	r1, #25
c0d0076e:	2322      	movs	r3, #34	; 0x22
c0d00770:	9808      	ldr	r0, [sp, #32]
c0d00772:	463a      	mov	r2, r7
c0d00774:	f7ff fe78 	bl	c0d00468 <encode_base58>
    
    address[length] = '\0';
c0d00778:	990a      	ldr	r1, [sp, #40]	; 0x28
c0d0077a:	5439      	strb	r1, [r7, r0]
c0d0077c:	9e12      	ldr	r6, [sp, #72]	; 0x48
c0d0077e:	9f0d      	ldr	r7, [sp, #52]	; 0x34
c0d00780:	e0c6      	b.n	c0d00910 <blocknet_main+0x2cc>
c0d00782:	910c      	str	r1, [sp, #48]	; 0x30
c0d00784:	2803      	cmp	r0, #3
c0d00786:	4d81      	ldr	r5, [pc, #516]	; (c0d0098c <blocknet_main+0x348>)
c0d00788:	d000      	beq.n	c0d0078c <blocknet_main+0x148>
c0d0078a:	e227      	b.n	c0d00bdc <blocknet_main+0x598>
							0x02 otherwise
				
				*/
                case INS_SIGN:
					
					if ((G_io_apdu_buffer[2] != 0x00) && (G_io_apdu_buffer[2] != 0x02)) {
c0d0078c:	78a1      	ldrb	r1, [r4, #2]
c0d0078e:	2202      	movs	r2, #2
c0d00790:	430a      	orrs	r2, r1
c0d00792:	206b      	movs	r0, #107	; 0x6b
c0d00794:	0200      	lsls	r0, r0, #8
c0d00796:	2a02      	cmp	r2, #2
c0d00798:	d000      	beq.n	c0d0079c <blocknet_main+0x158>
c0d0079a:	e21d      	b.n	c0d00bd8 <blocknet_main+0x594>
						THROW(0x6B00);
					}
					
					if ((G_io_apdu_buffer[3] != 0x02) && (G_io_apdu_buffer[3] != 0x04)) {
c0d0079c:	78e2      	ldrb	r2, [r4, #3]
c0d0079e:	2a02      	cmp	r2, #2
c0d007a0:	d002      	beq.n	c0d007a8 <blocknet_main+0x164>
c0d007a2:	2a04      	cmp	r2, #4
c0d007a4:	d000      	beq.n	c0d007a8 <blocknet_main+0x164>
c0d007a6:	e217      	b.n	c0d00bd8 <blocknet_main+0x594>
						THROW(0x6B00);
					}
					
					msgLen = G_io_apdu_buffer[OFFSET_MSG_LEN];
c0d007a8:	7e60      	ldrb	r0, [r4, #25]
c0d007aa:	4a79      	ldr	r2, [pc, #484]	; (c0d00990 <blocknet_main+0x34c>)
c0d007ac:	6010      	str	r0, [r2, #0]
					if (msgLen < 1) {
c0d007ae:	2800      	cmp	r0, #0
c0d007b0:	d100      	bne.n	c0d007b4 <blocknet_main+0x170>
c0d007b2:	e256      	b.n	c0d00c62 <blocknet_main+0x61e>
						THROW(0x6700);
					}

                    // This is the first part of the message to sign
					if (G_io_apdu_buffer[2] == P1_FIRST) {
c0d007b4:	2902      	cmp	r1, #2
c0d007b6:	d12d      	bne.n	c0d00814 <blocknet_main+0x1d0>
c0d007b8:	4873      	ldr	r0, [pc, #460]	; (c0d00988 <blocknet_main+0x344>)
c0d007ba:	4602      	mov	r2, r0

static bool verify_path() {
	
	// Verify hardened paths
	
	bool path_check1 = (path_n[0] & 0x80000000);
c0d007bc:	ca07      	ldmia	r2, {r0, r1, r2}
	
	// Verify coin registration number


	
	if (!path_check1 || !path_check2 || !path_check3) {
c0d007be:	4002      	ands	r2, r0
c0d007c0:	400a      	ands	r2, r1
					}

                    // This is the first part of the message to sign
					if (G_io_apdu_buffer[2] == P1_FIRST) {

                        if (!verify_api_version() || !verify_path() ) {
c0d007c2:	2a00      	cmp	r2, #0
c0d007c4:	db08      	blt.n	c0d007d8 <blocknet_main+0x194>
}


static void send_error_reply() {
	
	G_io_apdu_buffer[0] = 0x69;
c0d007c6:	2069      	movs	r0, #105	; 0x69
c0d007c8:	7020      	strb	r0, [r4, #0]
    G_io_apdu_buffer[1] = 0x85;
c0d007ca:	7063      	strb	r3, [r4, #1]
	io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, 2);
c0d007cc:	2020      	movs	r0, #32
c0d007ce:	2102      	movs	r1, #2
c0d007d0:	f001 f814 	bl	c0d017fc <io_exchange>
	ui_idle();
c0d007d4:	f7ff fee8 	bl	c0d005a8 <ui_idle>

                        if (!verify_api_version() || !verify_path() ) {
                            send_error_reply();
                        }

						cx_sha256_init(&hash);
c0d007d8:	4628      	mov	r0, r5
c0d007da:	f001 f993 	bl	c0d01b04 <cx_sha256_init>
                            dataBuffer += 4;

                        }
                        */
                       
                        get_path_BE();
c0d007de:	f000 fab9 	bl	c0d00d54 <get_path_BE>

                        os_perso_derive_node_bip32(CX_CURVE_256K1, path_n, 5, privateKeyData, NULL);
c0d007e2:	2000      	movs	r0, #0
c0d007e4:	4669      	mov	r1, sp
c0d007e6:	6008      	str	r0, [r1, #0]
c0d007e8:	2621      	movs	r6, #33	; 0x21
c0d007ea:	2205      	movs	r2, #5
c0d007ec:	4630      	mov	r0, r6
c0d007ee:	4966      	ldr	r1, [pc, #408]	; (c0d00988 <blocknet_main+0x344>)
c0d007f0:	4f68      	ldr	r7, [pc, #416]	; (c0d00994 <blocknet_main+0x350>)
c0d007f2:	463b      	mov	r3, r7
c0d007f4:	f001 f9ec 	bl	c0d01bd0 <os_perso_derive_node_bip32>

                        cx_ecdsa_init_private_key(CX_CURVE_256K1, privateKeyData, 32, &privateKey);
c0d007f8:	2220      	movs	r2, #32
c0d007fa:	4630      	mov	r0, r6
c0d007fc:	4639      	mov	r1, r7
c0d007fe:	4f66      	ldr	r7, [pc, #408]	; (c0d00998 <blocknet_main+0x354>)
c0d00800:	463b      	mov	r3, r7
c0d00802:	f001 f995 	bl	c0d01b30 <cx_ecfp_init_private_key>
                        cx_ecfp_generate_pair(CX_CURVE_256K1, &publicKey, &privateKey, 1);
c0d00806:	2301      	movs	r3, #1
c0d00808:	4630      	mov	r0, r6
c0d0080a:	4964      	ldr	r1, [pc, #400]	; (c0d0099c <blocknet_main+0x358>)
c0d0080c:	463a      	mov	r2, r7
c0d0080e:	f001 f9a7 	bl	c0d01b60 <cx_ecfp_generate_pair>
c0d00812:	4f5c      	ldr	r7, [pc, #368]	; (c0d00984 <blocknet_main+0x340>)
                    
                    }

                    G_io_apdu_buffer[5 + G_io_apdu_buffer[OFFSET_LC]] = '\0';
c0d00814:	7920      	ldrb	r0, [r4, #4]
c0d00816:	1820      	adds	r0, r4, r0
c0d00818:	2600      	movs	r6, #0
c0d0081a:	7146      	strb	r6, [r0, #5]
                    
                    cx_hash(&hash.header, 0, G_io_apdu_buffer + OFFSET_MSG, G_io_apdu_buffer[OFFSET_MSG_LEN], NULL, 0);
c0d0081c:	7e63      	ldrb	r3, [r4, #25]
c0d0081e:	4668      	mov	r0, sp
c0d00820:	6006      	str	r6, [r0, #0]
c0d00822:	6046      	str	r6, [r0, #4]
c0d00824:	4622      	mov	r2, r4
c0d00826:	321a      	adds	r2, #26
c0d00828:	4628      	mov	r0, r5
c0d0082a:	4631      	mov	r1, r6
c0d0082c:	f001 f938 	bl	c0d01aa0 <cx_hash>

                    if (G_io_apdu_buffer[3] == P2_LAST) {
c0d00830:	78e0      	ldrb	r0, [r4, #3]
c0d00832:	2804      	cmp	r0, #4
c0d00834:	d000      	beq.n	c0d00838 <blocknet_main+0x1f4>
c0d00836:	e218      	b.n	c0d00c6a <blocknet_main+0x626>

static void ui_approval(void) {
// #ifdef TARGET_BLUE
//    UX_DISPLAY(bagl_ui_sample_blue, NULL);
// #else
    UX_DISPLAY(bagl_ui_approval_nanos, NULL);
c0d00838:	4859      	ldr	r0, [pc, #356]	; (c0d009a0 <blocknet_main+0x35c>)
c0d0083a:	4478      	add	r0, pc
c0d0083c:	4d59      	ldr	r5, [pc, #356]	; (c0d009a4 <blocknet_main+0x360>)
c0d0083e:	6028      	str	r0, [r5, #0]
c0d00840:	2005      	movs	r0, #5
c0d00842:	6068      	str	r0, [r5, #4]
c0d00844:	4858      	ldr	r0, [pc, #352]	; (c0d009a8 <blocknet_main+0x364>)
c0d00846:	4478      	add	r0, pc
c0d00848:	6128      	str	r0, [r5, #16]
c0d0084a:	60ee      	str	r6, [r5, #12]
c0d0084c:	2003      	movs	r0, #3
c0d0084e:	7628      	strb	r0, [r5, #24]
c0d00850:	61ee      	str	r6, [r5, #28]
c0d00852:	4628      	mov	r0, r5
c0d00854:	3018      	adds	r0, #24
c0d00856:	f001 f9ff 	bl	c0d01c58 <os_ux>
c0d0085a:	61e8      	str	r0, [r5, #28]
c0d0085c:	f001 f8c4 	bl	c0d019e8 <ux_check_status_default>
c0d00860:	f000 fdcc 	bl	c0d013fc <io_seproxyhal_init_ux>
c0d00864:	60ae      	str	r6, [r5, #8]
c0d00866:	6828      	ldr	r0, [r5, #0]
c0d00868:	2800      	cmp	r0, #0
c0d0086a:	d02c      	beq.n	c0d008c6 <blocknet_main+0x282>
c0d0086c:	484d      	ldr	r0, [pc, #308]	; (c0d009a4 <blocknet_main+0x360>)
c0d0086e:	69c0      	ldr	r0, [r0, #28]
c0d00870:	494e      	ldr	r1, [pc, #312]	; (c0d009ac <blocknet_main+0x368>)
c0d00872:	4288      	cmp	r0, r1
c0d00874:	d11d      	bne.n	c0d008b2 <blocknet_main+0x26e>
c0d00876:	e026      	b.n	c0d008c6 <blocknet_main+0x282>
c0d00878:	484a      	ldr	r0, [pc, #296]	; (c0d009a4 <blocknet_main+0x360>)
c0d0087a:	4602      	mov	r2, r0
c0d0087c:	6890      	ldr	r0, [r2, #8]
c0d0087e:	68d1      	ldr	r1, [r2, #12]
c0d00880:	2538      	movs	r5, #56	; 0x38
c0d00882:	4368      	muls	r0, r5
c0d00884:	6812      	ldr	r2, [r2, #0]
c0d00886:	1810      	adds	r0, r2, r0
c0d00888:	2900      	cmp	r1, #0
c0d0088a:	d002      	beq.n	c0d00892 <blocknet_main+0x24e>
c0d0088c:	4788      	blx	r1
c0d0088e:	2800      	cmp	r0, #0
c0d00890:	d009      	beq.n	c0d008a6 <blocknet_main+0x262>
c0d00892:	2801      	cmp	r0, #1
c0d00894:	d105      	bne.n	c0d008a2 <blocknet_main+0x25e>
c0d00896:	4843      	ldr	r0, [pc, #268]	; (c0d009a4 <blocknet_main+0x360>)
c0d00898:	4601      	mov	r1, r0
c0d0089a:	6888      	ldr	r0, [r1, #8]
c0d0089c:	4345      	muls	r5, r0
c0d0089e:	6808      	ldr	r0, [r1, #0]
c0d008a0:	1940      	adds	r0, r0, r5
return_to_dashboard:
    return;
}

void io_seproxyhal_display(const bagl_element_t *element) {
    io_seproxyhal_display_default((bagl_element_t *)element);
c0d008a2:	f000 fef1 	bl	c0d01688 <io_seproxyhal_display_default>
c0d008a6:	483f      	ldr	r0, [pc, #252]	; (c0d009a4 <blocknet_main+0x360>)
c0d008a8:	4601      	mov	r1, r0

static void ui_approval(void) {
// #ifdef TARGET_BLUE
//    UX_DISPLAY(bagl_ui_sample_blue, NULL);
// #else
    UX_DISPLAY(bagl_ui_approval_nanos, NULL);
c0d008aa:	6888      	ldr	r0, [r1, #8]
c0d008ac:	1c46      	adds	r6, r0, #1
c0d008ae:	608e      	str	r6, [r1, #8]
c0d008b0:	6808      	ldr	r0, [r1, #0]
c0d008b2:	2800      	cmp	r0, #0
c0d008b4:	d007      	beq.n	c0d008c6 <blocknet_main+0x282>
c0d008b6:	48fb      	ldr	r0, [pc, #1004]	; (c0d00ca4 <blocknet_main+0x660>)
c0d008b8:	6840      	ldr	r0, [r0, #4]
c0d008ba:	4286      	cmp	r6, r0
c0d008bc:	d203      	bcs.n	c0d008c6 <blocknet_main+0x282>
c0d008be:	f001 f9f7 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d008c2:	2800      	cmp	r0, #0
c0d008c4:	d0d8      	beq.n	c0d00878 <blocknet_main+0x234>
                        // Send back the response, do not restart the event loop
                        io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, tx);
						
                    }
                    
                    flags |= IO_ASYNCH_REPLY;
c0d008c6:	2010      	movs	r0, #16
c0d008c8:	991f      	ldr	r1, [sp, #124]	; 0x7c
c0d008ca:	4301      	orrs	r1, r0
c0d008cc:	911f      	str	r1, [sp, #124]	; 0x7c
c0d008ce:	9e12      	ldr	r6, [sp, #72]	; 0x48
c0d008d0:	e132      	b.n	c0d00b38 <blocknet_main+0x4f4>
}


static void handleAppVersion() {
    
	G_io_apdu_buffer[0] = 0x00;
c0d008d2:	2000      	movs	r0, #0
c0d008d4:	7020      	strb	r0, [r4, #0]
    G_io_apdu_buffer[1] = LEDGER_MAJOR_VERSION;
c0d008d6:	7060      	strb	r0, [r4, #1]
    G_io_apdu_buffer[2] = LEDGER_MINOR_VERSION;
c0d008d8:	70a6      	strb	r6, [r4, #2]
    G_io_apdu_buffer[3] = LEDGER_PATCH_VERSION;
c0d008da:	70e0      	strb	r0, [r4, #3]

    G_io_apdu_buffer[4] = 0x90;
c0d008dc:	330b      	adds	r3, #11
c0d008de:	7123      	strb	r3, [r4, #4]
    G_io_apdu_buffer[5] = 0x00;
c0d008e0:	7160      	strb	r0, [r4, #5]
    	
	io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, 6);
c0d008e2:	2020      	movs	r0, #32
c0d008e4:	2106      	movs	r1, #6
c0d008e6:	f000 ff89 	bl	c0d017fc <io_exchange>
c0d008ea:	9e12      	ldr	r6, [sp, #72]	; 0x48
c0d008ec:	e124      	b.n	c0d00b38 <blocknet_main+0x4f4>
	unsigned char privateKeyData[32];
    unsigned char tmp[25];
    unsigned int length;
    
    if (!os_global_pin_is_validated()) {
        os_memmove(address, NOT_AVAILABLE, sizeof(NOT_AVAILABLE));
c0d008ee:	49f6      	ldr	r1, [pc, #984]	; (c0d00cc8 <blocknet_main+0x684>)
c0d008f0:	4479      	add	r1, pc
c0d008f2:	220e      	movs	r2, #14
c0d008f4:	4638      	mov	r0, r7
c0d008f6:	f000 fb82 	bl	c0d00ffe <os_memmove>
}


static void send_error_reply() {
	
	G_io_apdu_buffer[0] = 0x69;
c0d008fa:	2069      	movs	r0, #105	; 0x69
c0d008fc:	7020      	strb	r0, [r4, #0]
c0d008fe:	9f0d      	ldr	r7, [sp, #52]	; 0x34
    G_io_apdu_buffer[1] = 0x85;
c0d00900:	7067      	strb	r7, [r4, #1]
	io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, 2);
c0d00902:	2020      	movs	r0, #32
c0d00904:	2102      	movs	r1, #2
c0d00906:	f000 ff79 	bl	c0d017fc <io_exchange>
	ui_idle();
c0d0090a:	f7ff fe4d 	bl	c0d005a8 <ui_idle>
c0d0090e:	9e12      	ldr	r6, [sp, #72]	; 0x48
                    if (!derive()) {
                        send_error_reply();
                    }
                    

                    os_memmove(G_io_apdu_buffer, address, 34);
c0d00910:	2522      	movs	r5, #34	; 0x22
c0d00912:	4620      	mov	r0, r4
c0d00914:	49da      	ldr	r1, [pc, #872]	; (c0d00c80 <blocknet_main+0x63c>)
c0d00916:	462a      	mov	r2, r5
c0d00918:	f000 fb71 	bl	c0d00ffe <os_memmove>
                    tx = 34;
c0d0091c:	9520      	str	r5, [sp, #128]	; 0x80
					
					G_io_apdu_buffer[tx++] = 0x90;
c0d0091e:	9820      	ldr	r0, [sp, #128]	; 0x80
c0d00920:	1c41      	adds	r1, r0, #1
c0d00922:	9120      	str	r1, [sp, #128]	; 0x80
c0d00924:	370b      	adds	r7, #11
c0d00926:	5427      	strb	r7, [r4, r0]
                    G_io_apdu_buffer[tx++] = 0x00;
c0d00928:	9820      	ldr	r0, [sp, #128]	; 0x80
c0d0092a:	1c41      	adds	r1, r0, #1
c0d0092c:	9120      	str	r1, [sp, #128]	; 0x80
c0d0092e:	990a      	ldr	r1, [sp, #40]	; 0x28
c0d00930:	5421      	strb	r1, [r4, r0]
					
					io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, tx);
c0d00932:	9820      	ldr	r0, [sp, #128]	; 0x80
c0d00934:	b281      	uxth	r1, r0
c0d00936:	2020      	movs	r0, #32
c0d00938:	f000 ff60 	bl	c0d017fc <io_exchange>
					flags |= IO_ASYNCH_REPLY;
c0d0093c:	2010      	movs	r0, #16
c0d0093e:	991f      	ldr	r1, [sp, #124]	; 0x7c
c0d00940:	4301      	orrs	r1, r0
c0d00942:	911f      	str	r1, [sp, #124]	; 0x7c
c0d00944:	4fce      	ldr	r7, [pc, #824]	; (c0d00c80 <blocknet_main+0x63c>)
c0d00946:	e0f7      	b.n	c0d00b38 <blocknet_main+0x4f4>
c0d00948:	a81e      	add	r0, sp, #120	; 0x78
    // When APDU are to be fetched from multiple IOs, like NFC+USB+BLE, make
    // sure the io_event is called with a
    // switch event, before the apdu is replied to the bootloader. This avoid
    // APDU injection faults.
    for (;;) {
        volatile unsigned short sw = 0;
c0d0094a:	8006      	strh	r6, [r0, #0]
c0d0094c:	ad13      	add	r5, sp, #76	; 0x4c

        BEGIN_TRY {
            TRY {
c0d0094e:	4628      	mov	r0, r5
c0d00950:	f002 f9a0 	bl	c0d02c94 <setjmp>
c0d00954:	8528      	strh	r0, [r5, #40]	; 0x28
c0d00956:	49c6      	ldr	r1, [pc, #792]	; (c0d00c70 <blocknet_main+0x62c>)
c0d00958:	4208      	tst	r0, r1
c0d0095a:	d029      	beq.n	c0d009b0 <blocknet_main+0x36c>
c0d0095c:	a913      	add	r1, sp, #76	; 0x4c
                default:
                    THROW(0x6D00);
                    break;
                }
            }
            CATCH_OTHER(e) {
c0d0095e:	850e      	strh	r6, [r1, #40]	; 0x28
c0d00960:	210f      	movs	r1, #15
c0d00962:	0309      	lsls	r1, r1, #12
                switch (e & 0xF000) {
c0d00964:	4001      	ands	r1, r0
c0d00966:	2209      	movs	r2, #9
c0d00968:	0312      	lsls	r2, r2, #12
c0d0096a:	4291      	cmp	r1, r2
c0d0096c:	d004      	beq.n	c0d00978 <blocknet_main+0x334>
c0d0096e:	2203      	movs	r2, #3
c0d00970:	0352      	lsls	r2, r2, #13
c0d00972:	4291      	cmp	r1, r2
c0d00974:	d000      	beq.n	c0d00978 <blocknet_main+0x334>
c0d00976:	e0cd      	b.n	c0d00b14 <blocknet_main+0x4d0>
c0d00978:	a91e      	add	r1, sp, #120	; 0x78
                case 0x6000:
                case 0x9000:
                    sw = e;
c0d0097a:	8008      	strh	r0, [r1, #0]
c0d0097c:	e0d1      	b.n	c0d00b22 <blocknet_main+0x4de>
c0d0097e:	46c0      	nop			; (mov r8, r8)
c0d00980:	20001b38 	.word	0x20001b38
c0d00984:	20001b24 	.word	0x20001b24
c0d00988:	200019e4 	.word	0x200019e4
c0d0098c:	20001ab8 	.word	0x20001ab8
c0d00990:	20001a00 	.word	0x20001a00
c0d00994:	20001a04 	.word	0x20001a04
c0d00998:	20001a70 	.word	0x20001a70
c0d0099c:	20001a24 	.word	0x20001a24
c0d009a0:	0000263a 	.word	0x0000263a
c0d009a4:	20001884 	.word	0x20001884
c0d009a8:	00000557 	.word	0x00000557
c0d009ac:	b0105044 	.word	0xb0105044
c0d009b0:	a813      	add	r0, sp, #76	; 0x4c
    // APDU injection faults.
    for (;;) {
        volatile unsigned short sw = 0;

        BEGIN_TRY {
            TRY {
c0d009b2:	f000 fa6e 	bl	c0d00e92 <try_context_set>
                rx = tx;
c0d009b6:	9820      	ldr	r0, [sp, #128]	; 0x80
c0d009b8:	9021      	str	r0, [sp, #132]	; 0x84
c0d009ba:	2500      	movs	r5, #0
                tx = 0; // ensure no race in catch_other if io_exchange throws
c0d009bc:	9520      	str	r5, [sp, #128]	; 0x80
                        // an error
                rx = io_exchange(CHANNEL_APDU | flags, rx);
c0d009be:	981f      	ldr	r0, [sp, #124]	; 0x7c
c0d009c0:	9921      	ldr	r1, [sp, #132]	; 0x84
c0d009c2:	b2c0      	uxtb	r0, r0
c0d009c4:	b289      	uxth	r1, r1
c0d009c6:	f000 ff19 	bl	c0d017fc <io_exchange>
c0d009ca:	9021      	str	r0, [sp, #132]	; 0x84
                flags = 0;
c0d009cc:	951f      	str	r5, [sp, #124]	; 0x7c

                // no apdu received, well, reset the session, and reset the
                // bootloader configuration
                if (rx == 0) {
c0d009ce:	9821      	ldr	r0, [sp, #132]	; 0x84
c0d009d0:	2800      	cmp	r0, #0
c0d009d2:	d100      	bne.n	c0d009d6 <blocknet_main+0x392>
c0d009d4:	e0fb      	b.n	c0d00bce <blocknet_main+0x58a>
                    THROW(0x6982);
                }

                if (G_io_apdu_buffer[0] != 0x80) {
c0d009d6:	7820      	ldrb	r0, [r4, #0]
c0d009d8:	217a      	movs	r1, #122	; 0x7a
c0d009da:	43c9      	mvns	r1, r1
c0d009dc:	1f49      	subs	r1, r1, #5
c0d009de:	b2c9      	uxtb	r1, r1
                    

                    os_memmove(G_io_apdu_buffer, address, 34);
                    tx = 34;
					
					G_io_apdu_buffer[tx++] = 0x90;
c0d009e0:	2385      	movs	r3, #133	; 0x85
                // bootloader configuration
                if (rx == 0) {
                    THROW(0x6982);
                }

                if (G_io_apdu_buffer[0] != 0x80) {
c0d009e2:	4288      	cmp	r0, r1
c0d009e4:	d000      	beq.n	c0d009e8 <blocknet_main+0x3a4>
c0d009e6:	e0f5      	b.n	c0d00bd4 <blocknet_main+0x590>
c0d009e8:	7860      	ldrb	r0, [r4, #1]
c0d009ea:	2601      	movs	r6, #1
c0d009ec:	0232      	lsls	r2, r6, #8
c0d009ee:	2109      	movs	r1, #9
c0d009f0:	0309      	lsls	r1, r1, #12
                    THROW(0x6E00);
                }

                // unauthenticated instruction
                switch (G_io_apdu_buffer[1]) {
c0d009f2:	2808      	cmp	r0, #8
c0d009f4:	dd00      	ble.n	c0d009f8 <blocknet_main+0x3b4>
c0d009f6:	e0ad      	b.n	c0d00b54 <blocknet_main+0x510>
c0d009f8:	2804      	cmp	r0, #4
c0d009fa:	dc00      	bgt.n	c0d009fe <blocknet_main+0x3ba>
c0d009fc:	e6c1      	b.n	c0d00782 <blocknet_main+0x13e>
c0d009fe:	2805      	cmp	r0, #5
c0d00a00:	d100      	bne.n	c0d00a04 <blocknet_main+0x3c0>
c0d00a02:	e766      	b.n	c0d008d2 <blocknet_main+0x28e>
c0d00a04:	920b      	str	r2, [sp, #44]	; 0x2c
c0d00a06:	910c      	str	r1, [sp, #48]	; 0x30
c0d00a08:	930d      	str	r3, [sp, #52]	; 0x34
c0d00a0a:	2807      	cmp	r0, #7
c0d00a0c:	4d9d      	ldr	r5, [pc, #628]	; (c0d00c84 <blocknet_main+0x640>)
c0d00a0e:	d000      	beq.n	c0d00a12 <blocknet_main+0x3ce>
c0d00a10:	e0eb      	b.n	c0d00bea <blocknet_main+0x5a6>
                    Total offset to DATA	=> 26 bytes				
				*/
				
				case INS_SIGN_BACKUP:
					
					if (G_io_apdu_buffer[2] == P1_FIRST) {
c0d00a12:	78a0      	ldrb	r0, [r4, #2]
c0d00a14:	2802      	cmp	r0, #2
c0d00a16:	d137      	bne.n	c0d00a88 <blocknet_main+0x444>
                        
						cx_sha256_init(&hash);
c0d00a18:	4628      	mov	r0, r5
c0d00a1a:	f001 f873 	bl	c0d01b04 <cx_sha256_init>
					    
                        dataBuffer = G_io_apdu_buffer + 5;
c0d00a1e:	1d60      	adds	r0, r4, #5
c0d00a20:	499a      	ldr	r1, [pc, #616]	; (c0d00c8c <blocknet_main+0x648>)
c0d00a22:	4f99      	ldr	r7, [pc, #612]	; (c0d00c88 <blocknet_main+0x644>)
c0d00a24:	6038      	str	r0, [r7, #0]
c0d00a26:	2000      	movs	r0, #0
c0d00a28:	460f      	mov	r7, r1
c0d00a2a:	6008      	str	r0, [r1, #0]
c0d00a2c:	4e93      	ldr	r6, [pc, #588]	; (c0d00c7c <blocknet_main+0x638>)
						for (i = 0; i < 5; i++) {
						    path_n[i] = (dataBuffer[0] << 24) 
c0d00a2e:	0082      	lsls	r2, r0, #2
c0d00a30:	18a1      	adds	r1, r4, r2
c0d00a32:	794b      	ldrb	r3, [r1, #5]
c0d00a34:	061b      	lsls	r3, r3, #24
                                        | (dataBuffer[1] << 16) 
c0d00a36:	798d      	ldrb	r5, [r1, #6]
c0d00a38:	042d      	lsls	r5, r5, #16
c0d00a3a:	431d      	orrs	r5, r3
                                        | (dataBuffer[2] << 8) 
c0d00a3c:	79cb      	ldrb	r3, [r1, #7]
c0d00a3e:	021b      	lsls	r3, r3, #8
c0d00a40:	432b      	orrs	r3, r5
                                        | (dataBuffer[3]);
c0d00a42:	7a0d      	ldrb	r5, [r1, #8]
c0d00a44:	431d      	orrs	r5, r3
                        
						cx_sha256_init(&hash);
					    
                        dataBuffer = G_io_apdu_buffer + 5;
						for (i = 0; i < 5; i++) {
						    path_n[i] = (dataBuffer[0] << 24) 
c0d00a46:	50b5      	str	r5, [r6, r2]
                                        | (dataBuffer[1] << 16) 
                                        | (dataBuffer[2] << 8) 
                                        | (dataBuffer[3]);

                            dataBuffer += 4;
c0d00a48:	3109      	adds	r1, #9
c0d00a4a:	4a8f      	ldr	r2, [pc, #572]	; (c0d00c88 <blocknet_main+0x644>)
c0d00a4c:	6011      	str	r1, [r2, #0]
					if (G_io_apdu_buffer[2] == P1_FIRST) {
                        
						cx_sha256_init(&hash);
					    
                        dataBuffer = G_io_apdu_buffer + 5;
						for (i = 0; i < 5; i++) {
c0d00a4e:	1c40      	adds	r0, r0, #1
c0d00a50:	6038      	str	r0, [r7, #0]
c0d00a52:	2805      	cmp	r0, #5
c0d00a54:	d3eb      	bcc.n	c0d00a2e <blocknet_main+0x3ea>
                                        | (dataBuffer[3]);

                            dataBuffer += 4;
                        }

                        os_perso_derive_node_bip32(CX_CURVE_256K1, path_n, 5, privateKeyData, NULL);
c0d00a56:	2000      	movs	r0, #0
c0d00a58:	4669      	mov	r1, sp
c0d00a5a:	6008      	str	r0, [r1, #0]
c0d00a5c:	4631      	mov	r1, r6
c0d00a5e:	2621      	movs	r6, #33	; 0x21
c0d00a60:	2205      	movs	r2, #5
c0d00a62:	4630      	mov	r0, r6
c0d00a64:	4d8a      	ldr	r5, [pc, #552]	; (c0d00c90 <blocknet_main+0x64c>)
c0d00a66:	462b      	mov	r3, r5
c0d00a68:	f001 f8b2 	bl	c0d01bd0 <os_perso_derive_node_bip32>
                        cx_ecdsa_init_private_key(CX_CURVE_256K1, privateKeyData, 32, &privateKey);
c0d00a6c:	2220      	movs	r2, #32
c0d00a6e:	4630      	mov	r0, r6
c0d00a70:	4629      	mov	r1, r5
c0d00a72:	4d88      	ldr	r5, [pc, #544]	; (c0d00c94 <blocknet_main+0x650>)
c0d00a74:	462b      	mov	r3, r5
c0d00a76:	f001 f85b 	bl	c0d01b30 <cx_ecfp_init_private_key>
                        cx_ecfp_generate_pair(CX_CURVE_256K1, &publicKey, &privateKey, 1);
c0d00a7a:	2301      	movs	r3, #1
c0d00a7c:	4630      	mov	r0, r6
c0d00a7e:	4986      	ldr	r1, [pc, #536]	; (c0d00c98 <blocknet_main+0x654>)
c0d00a80:	462a      	mov	r2, r5
c0d00a82:	f001 f86d 	bl	c0d01b60 <cx_ecfp_generate_pair>
c0d00a86:	4d7f      	ldr	r5, [pc, #508]	; (c0d00c84 <blocknet_main+0x640>)
                    
                    }

                    G_io_apdu_buffer[5 + G_io_apdu_buffer[OFFSET_LC]] = '\0';
c0d00a88:	7920      	ldrb	r0, [r4, #4]
c0d00a8a:	1820      	adds	r0, r4, r0
c0d00a8c:	2600      	movs	r6, #0
c0d00a8e:	7146      	strb	r6, [r0, #5]
                    
                    cx_hash(&hash.header, 0, G_io_apdu_buffer + OFFSET_MSG, G_io_apdu_buffer[OFFSET_MSG_LEN], NULL, 0);
c0d00a90:	7e63      	ldrb	r3, [r4, #25]
c0d00a92:	4668      	mov	r0, sp
c0d00a94:	6006      	str	r6, [r0, #0]
c0d00a96:	6046      	str	r6, [r0, #4]
c0d00a98:	4622      	mov	r2, r4
c0d00a9a:	321a      	adds	r2, #26
c0d00a9c:	4628      	mov	r0, r5
c0d00a9e:	4631      	mov	r1, r6
c0d00aa0:	f000 fffe 	bl	c0d01aa0 <cx_hash>

                    if (G_io_apdu_buffer[3] == P2_LAST) {
c0d00aa4:	78e0      	ldrb	r0, [r4, #3]
c0d00aa6:	9620      	str	r6, [sp, #128]	; 0x80
c0d00aa8:	2804      	cmp	r0, #4
c0d00aaa:	4628      	mov	r0, r5
c0d00aac:	d000      	beq.n	c0d00ab0 <blocknet_main+0x46c>
c0d00aae:	e089      	b.n	c0d00bc4 <blocknet_main+0x580>
c0d00ab0:	2520      	movs	r5, #32
												
						tx = 0;
                
                        // Hash is finalized, send back the signature
                        cx_hash(&hash.header, CX_LAST, G_io_apdu_buffer, 0, result, sizeof(result));
c0d00ab2:	4669      	mov	r1, sp
c0d00ab4:	4f79      	ldr	r7, [pc, #484]	; (c0d00c9c <blocknet_main+0x658>)
c0d00ab6:	600f      	str	r7, [r1, #0]
c0d00ab8:	604d      	str	r5, [r1, #4]
c0d00aba:	2101      	movs	r1, #1
c0d00abc:	4622      	mov	r2, r4
c0d00abe:	4633      	mov	r3, r6
c0d00ac0:	f000 ffee 	bl	c0d01aa0 <cx_hash>
                        tx = cx_ecdsa_sign((void*) &privateKey, CX_RND_RFC6979 | CX_LAST,
c0d00ac4:	4668      	mov	r0, sp
c0d00ac6:	990b      	ldr	r1, [sp, #44]	; 0x2c
c0d00ac8:	3151      	adds	r1, #81	; 0x51
c0d00aca:	6005      	str	r5, [r0, #0]
c0d00acc:	6044      	str	r4, [r0, #4]
c0d00ace:	6081      	str	r1, [r0, #8]
c0d00ad0:	60c6      	str	r6, [r0, #12]
c0d00ad2:	4973      	ldr	r1, [pc, #460]	; (c0d00ca0 <blocknet_main+0x65c>)
c0d00ad4:	2203      	movs	r2, #3
c0d00ad6:	486f      	ldr	r0, [pc, #444]	; (c0d00c94 <blocknet_main+0x650>)
c0d00ad8:	463b      	mov	r3, r7
c0d00ada:	f001 f859 	bl	c0d01b90 <cx_ecdsa_sign>
c0d00ade:	9020      	str	r0, [sp, #128]	; 0x80
                                        CX_SHA256, result, sizeof(result), G_io_apdu_buffer, sizeof(G_io_apdu_buffer), NULL);
                        
                        // Is this really required ?
                        // &= clears a bit
                        G_io_apdu_buffer[0] &= 0xF0; // discard the parity information
c0d00ae0:	7820      	ldrb	r0, [r4, #0]
c0d00ae2:	21f0      	movs	r1, #240	; 0xf0
c0d00ae4:	4001      	ands	r1, r0
c0d00ae6:	7021      	strb	r1, [r4, #0]
                                        
                        G_io_apdu_buffer[tx++] = 0x90;
c0d00ae8:	9820      	ldr	r0, [sp, #128]	; 0x80
c0d00aea:	1c41      	adds	r1, r0, #1
c0d00aec:	9120      	str	r1, [sp, #128]	; 0x80
c0d00aee:	990d      	ldr	r1, [sp, #52]	; 0x34
c0d00af0:	310b      	adds	r1, #11
c0d00af2:	5421      	strb	r1, [r4, r0]
                        G_io_apdu_buffer[tx++] = 0x00;
c0d00af4:	9820      	ldr	r0, [sp, #128]	; 0x80
c0d00af6:	1c41      	adds	r1, r0, #1
c0d00af8:	9120      	str	r1, [sp, #128]	; 0x80
c0d00afa:	5426      	strb	r6, [r4, r0]
						THROW(0x9000);
						
                    }
					
                    // Send back the response, do not restart the event loop
                    io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, tx);
c0d00afc:	9820      	ldr	r0, [sp, #128]	; 0x80
c0d00afe:	b281      	uxth	r1, r0
c0d00b00:	4628      	mov	r0, r5
c0d00b02:	f000 fe7b 	bl	c0d017fc <io_exchange>
            
                    // THROW(0x9000);
                    flags |= IO_ASYNCH_REPLY;
c0d00b06:	2010      	movs	r0, #16
c0d00b08:	991f      	ldr	r1, [sp, #124]	; 0x7c
c0d00b0a:	4301      	orrs	r1, r0
c0d00b0c:	911f      	str	r1, [sp, #124]	; 0x7c
c0d00b0e:	9e12      	ldr	r6, [sp, #72]	; 0x48
c0d00b10:	4f5b      	ldr	r7, [pc, #364]	; (c0d00c80 <blocknet_main+0x63c>)
c0d00b12:	e011      	b.n	c0d00b38 <blocknet_main+0x4f4>
                case 0x6000:
                case 0x9000:
                    sw = e;
                    break;
                default:
                    sw = 0x6800 | (e & 0x7FF);
c0d00b14:	4957      	ldr	r1, [pc, #348]	; (c0d00c74 <blocknet_main+0x630>)
c0d00b16:	4008      	ands	r0, r1
c0d00b18:	210d      	movs	r1, #13
c0d00b1a:	02c9      	lsls	r1, r1, #11
c0d00b1c:	4301      	orrs	r1, r0
c0d00b1e:	a81e      	add	r0, sp, #120	; 0x78
c0d00b20:	8001      	strh	r1, [r0, #0]
                    break;
                }
                // Unexpected exception => report
                G_io_apdu_buffer[tx] = sw >> 8;
c0d00b22:	981e      	ldr	r0, [sp, #120]	; 0x78
c0d00b24:	0a00      	lsrs	r0, r0, #8
c0d00b26:	9920      	ldr	r1, [sp, #128]	; 0x80
c0d00b28:	5460      	strb	r0, [r4, r1]
                G_io_apdu_buffer[tx + 1] = sw;
c0d00b2a:	981e      	ldr	r0, [sp, #120]	; 0x78
c0d00b2c:	9920      	ldr	r1, [sp, #128]	; 0x80
                default:
                    sw = 0x6800 | (e & 0x7FF);
                    break;
                }
                // Unexpected exception => report
                G_io_apdu_buffer[tx] = sw >> 8;
c0d00b2e:	1861      	adds	r1, r4, r1
                G_io_apdu_buffer[tx + 1] = sw;
c0d00b30:	7048      	strb	r0, [r1, #1]
                tx += 2;
c0d00b32:	9820      	ldr	r0, [sp, #128]	; 0x80
c0d00b34:	1c80      	adds	r0, r0, #2
c0d00b36:	9020      	str	r0, [sp, #128]	; 0x80
            }
            FINALLY {
c0d00b38:	f000 fb03 	bl	c0d01142 <try_context_get>
c0d00b3c:	a913      	add	r1, sp, #76	; 0x4c
c0d00b3e:	4288      	cmp	r0, r1
c0d00b40:	d103      	bne.n	c0d00b4a <blocknet_main+0x506>
c0d00b42:	f000 fb00 	bl	c0d01146 <try_context_get_previous>
c0d00b46:	f000 f9a4 	bl	c0d00e92 <try_context_set>
c0d00b4a:	a813      	add	r0, sp, #76	; 0x4c
            }
        }
        END_TRY;
c0d00b4c:	8d00      	ldrh	r0, [r0, #40]	; 0x28
c0d00b4e:	2800      	cmp	r0, #0
c0d00b50:	d13b      	bne.n	c0d00bca <blocknet_main+0x586>
c0d00b52:	e6f9      	b.n	c0d00948 <blocknet_main+0x304>
c0d00b54:	2809      	cmp	r0, #9
c0d00b56:	d100      	bne.n	c0d00b5a <blocknet_main+0x516>
c0d00b58:	e589      	b.n	c0d0066e <blocknet_main+0x2a>
c0d00b5a:	282c      	cmp	r0, #44	; 0x2c
c0d00b5c:	d003      	beq.n	c0d00b66 <blocknet_main+0x522>
c0d00b5e:	28ff      	cmp	r0, #255	; 0xff
c0d00b60:	d17b      	bne.n	c0d00c5a <blocknet_main+0x616>
    }

return_to_dashboard:
    return;
}
c0d00b62:	b069      	add	sp, #420	; 0x1a4
c0d00b64:	bdf0      	pop	{r4, r5, r6, r7, pc}
c0d00b66:	910c      	str	r1, [sp, #48]	; 0x30


static void ui_reset_captions() {
	
	// First UI line
	os_memmove(status_current_str, TXT_BLANK, sizeof(TXT_BLANK));
c0d00b68:	484f      	ldr	r0, [pc, #316]	; (c0d00ca8 <blocknet_main+0x664>)
c0d00b6a:	4955      	ldr	r1, [pc, #340]	; (c0d00cc0 <blocknet_main+0x67c>)
c0d00b6c:	4479      	add	r1, pc
c0d00b6e:	9105      	str	r1, [sp, #20]
c0d00b70:	2712      	movs	r7, #18
c0d00b72:	463a      	mov	r2, r7
c0d00b74:	f000 fa43 	bl	c0d00ffe <os_memmove>
	
	// Second UI line             
	os_memmove(lineBuffer, TXT_BLANK, sizeof(TXT_BLANK));
c0d00b78:	4e4c      	ldr	r6, [pc, #304]	; (c0d00cac <blocknet_main+0x668>)
c0d00b7a:	4630      	mov	r0, r6
c0d00b7c:	9905      	ldr	r1, [sp, #20]
c0d00b7e:	463a      	mov	r2, r7
c0d00b80:	f000 fa3d 	bl	c0d00ffe <os_memmove>

static void ui_show_balance() {
			
	ui_reset_captions();

	os_memmove(status_current_str, status_balance_str, 9);
c0d00b84:	494f      	ldr	r1, [pc, #316]	; (c0d00cc4 <blocknet_main+0x680>)
c0d00b86:	4479      	add	r1, pc
c0d00b88:	2209      	movs	r2, #9
c0d00b8a:	4847      	ldr	r0, [pc, #284]	; (c0d00ca8 <blocknet_main+0x664>)
c0d00b8c:	f000 fa37 	bl	c0d00ffe <os_memmove>
}


static void ui_set_lineBuffer_from_apdu() {
	
	text = (char*) G_io_apdu_buffer + 5;
c0d00b90:	1d60      	adds	r0, r4, #5
c0d00b92:	4947      	ldr	r1, [pc, #284]	; (c0d00cb0 <blocknet_main+0x66c>)
c0d00b94:	6008      	str	r0, [r1, #0]
	current_text_pos = 0;
c0d00b96:	4847      	ldr	r0, [pc, #284]	; (c0d00cb4 <blocknet_main+0x670>)
c0d00b98:	6005      	str	r5, [r0, #0]
	i_cursor = 0;
c0d00b9a:	4947      	ldr	r1, [pc, #284]	; (c0d00cb8 <blocknet_main+0x674>)
c0d00b9c:	600d      	str	r5, [r1, #0]
c0d00b9e:	e004      	b.n	c0d00baa <blocknet_main+0x566>
	while ((text[current_text_pos] != 0) && (text[current_text_pos] != '\n') &&
		(i_cursor < MAX_CHARS_PER_LINE)) {
		lineBuffer[i_cursor++] = text[current_text_pos];
c0d00ba0:	1c6b      	adds	r3, r5, #1
c0d00ba2:	600b      	str	r3, [r1, #0]
c0d00ba4:	5572      	strb	r2, [r6, r5]
		current_text_pos++;
c0d00ba6:	6003      	str	r3, [r0, #0]
	text = (char*) G_io_apdu_buffer + 5;
	current_text_pos = 0;
	i_cursor = 0;
	while ((text[current_text_pos] != 0) && (text[current_text_pos] != '\n') &&
		(i_cursor < MAX_CHARS_PER_LINE)) {
		lineBuffer[i_cursor++] = text[current_text_pos];
c0d00ba8:	461d      	mov	r5, r3
static void ui_set_lineBuffer_from_apdu() {
	
	text = (char*) G_io_apdu_buffer + 5;
	current_text_pos = 0;
	i_cursor = 0;
	while ((text[current_text_pos] != 0) && (text[current_text_pos] != '\n') &&
c0d00baa:	1962      	adds	r2, r4, r5
c0d00bac:	7952      	ldrb	r2, [r2, #5]
c0d00bae:	2a00      	cmp	r2, #0
c0d00bb0:	d006      	beq.n	c0d00bc0 <blocknet_main+0x57c>
c0d00bb2:	2a0a      	cmp	r2, #10
c0d00bb4:	d002      	beq.n	c0d00bbc <blocknet_main+0x578>
c0d00bb6:	2d30      	cmp	r5, #48	; 0x30
c0d00bb8:	ddf2      	ble.n	c0d00ba0 <blocknet_main+0x55c>
c0d00bba:	e001      	b.n	c0d00bc0 <blocknet_main+0x57c>
		lineBuffer[i_cursor++] = text[current_text_pos];
		current_text_pos++;
	}
	
	if (text[current_text_pos] == '\n') {
		current_text_pos++;
c0d00bbc:	1c69      	adds	r1, r5, #1
c0d00bbe:	6001      	str	r1, [r0, #0]
	}
	
	lineBuffer[i_cursor] = '\0';
c0d00bc0:	2000      	movs	r0, #0
c0d00bc2:	5570      	strb	r0, [r6, r5]
c0d00bc4:	980c      	ldr	r0, [sp, #48]	; 0x30
c0d00bc6:	f000 fab7 	bl	c0d01138 <os_longjmp>
                tx += 2;
            }
            FINALLY {
            }
        }
        END_TRY;
c0d00bca:	f000 fab5 	bl	c0d01138 <os_longjmp>
                flags = 0;

                // no apdu received, well, reset the session, and reset the
                // bootloader configuration
                if (rx == 0) {
                    THROW(0x6982);
c0d00bce:	483b      	ldr	r0, [pc, #236]	; (c0d00cbc <blocknet_main+0x678>)
c0d00bd0:	f000 fab2 	bl	c0d01138 <os_longjmp>
                }

                if (G_io_apdu_buffer[0] != 0x80) {
                    THROW(0x6E00);
c0d00bd4:	2037      	movs	r0, #55	; 0x37
c0d00bd6:	0240      	lsls	r0, r0, #9
c0d00bd8:	f000 faae 	bl	c0d01138 <os_longjmp>
c0d00bdc:	2800      	cmp	r0, #0
c0d00bde:	d13c      	bne.n	c0d00c5a <blocknet_main+0x616>
                // unauthenticated instruction
                switch (G_io_apdu_buffer[1]) {
                
                case 0x00: // reset
                    
					flags |= IO_RESET_AFTER_REPLIED;
c0d00be0:	2080      	movs	r0, #128	; 0x80
c0d00be2:	991f      	ldr	r1, [sp, #124]	; 0x7c
c0d00be4:	4301      	orrs	r1, r0
c0d00be6:	911f      	str	r1, [sp, #124]	; 0x7c
c0d00be8:	e7ec      	b.n	c0d00bc4 <blocknet_main+0x580>
c0d00bea:	4f2b      	ldr	r7, [pc, #172]	; (c0d00c98 <blocknet_main+0x654>)
c0d00bec:	2806      	cmp	r0, #6
c0d00bee:	d134      	bne.n	c0d00c5a <blocknet_main+0x616>
c0d00bf0:	4822      	ldr	r0, [pc, #136]	; (c0d00c7c <blocknet_main+0x638>)
c0d00bf2:	4602      	mov	r2, r0

static bool verify_path() {
	
	// Verify hardened paths
	
	bool path_check1 = (path_n[0] & 0x80000000);
c0d00bf4:	ca07      	ldmia	r2, {r0, r1, r2}
	
	// Verify coin registration number


	
	if (!path_check1 || !path_check2 || !path_check3) {
c0d00bf6:	4002      	ands	r2, r0
c0d00bf8:	400a      	ands	r2, r1
                    }
                    
                    */


                    if (!verify_path() ) {
c0d00bfa:	2a00      	cmp	r2, #0
c0d00bfc:	db09      	blt.n	c0d00c12 <blocknet_main+0x5ce>
}


static void send_error_reply() {
	
	G_io_apdu_buffer[0] = 0x69;
c0d00bfe:	2069      	movs	r0, #105	; 0x69
c0d00c00:	7020      	strb	r0, [r4, #0]
    G_io_apdu_buffer[1] = 0x85;
c0d00c02:	980d      	ldr	r0, [sp, #52]	; 0x34
c0d00c04:	7060      	strb	r0, [r4, #1]
	io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, 2);
c0d00c06:	2020      	movs	r0, #32
c0d00c08:	2102      	movs	r1, #2
c0d00c0a:	f000 fdf7 	bl	c0d017fc <io_exchange>
	ui_idle();
c0d00c0e:	f7ff fccb 	bl	c0d005a8 <ui_idle>

                    if (!verify_path() ) {
                        send_error_reply();
                    }

                    get_path_BE();
c0d00c12:	f000 f89f 	bl	c0d00d54 <get_path_BE>
c0d00c16:	2000      	movs	r0, #0

                    os_perso_derive_node_bip32(CX_CURVE_256K1, path_n, 5, privateKeyData, NULL);
c0d00c18:	4669      	mov	r1, sp
c0d00c1a:	6008      	str	r0, [r1, #0]
c0d00c1c:	2421      	movs	r4, #33	; 0x21
c0d00c1e:	4917      	ldr	r1, [pc, #92]	; (c0d00c7c <blocknet_main+0x638>)
c0d00c20:	2205      	movs	r2, #5
c0d00c22:	4d1b      	ldr	r5, [pc, #108]	; (c0d00c90 <blocknet_main+0x64c>)
c0d00c24:	4620      	mov	r0, r4
c0d00c26:	462b      	mov	r3, r5
c0d00c28:	f000 ffd2 	bl	c0d01bd0 <os_perso_derive_node_bip32>

                    cx_ecdsa_init_private_key(CX_CURVE_256K1, privateKeyData, 32, &privateKey);
c0d00c2c:	2220      	movs	r2, #32
c0d00c2e:	4e19      	ldr	r6, [pc, #100]	; (c0d00c94 <blocknet_main+0x650>)
c0d00c30:	4620      	mov	r0, r4
c0d00c32:	4629      	mov	r1, r5
c0d00c34:	4633      	mov	r3, r6
c0d00c36:	f000 ff7b 	bl	c0d01b30 <cx_ecfp_init_private_key>
                    cx_ecfp_generate_pair(CX_CURVE_256K1, &publicKey, &privateKey, 1);
c0d00c3a:	2301      	movs	r3, #1
c0d00c3c:	4620      	mov	r0, r4
c0d00c3e:	463d      	mov	r5, r7
c0d00c40:	4629      	mov	r1, r5
c0d00c42:	4632      	mov	r2, r6
c0d00c44:	f000 ff8c 	bl	c0d01b60 <cx_ecfp_generate_pair>

                    os_memmove(G_io_apdu_buffer, publicKey.W, 65);
c0d00c48:	3508      	adds	r5, #8
c0d00c4a:	480b      	ldr	r0, [pc, #44]	; (c0d00c78 <blocknet_main+0x634>)
c0d00c4c:	2441      	movs	r4, #65	; 0x41
c0d00c4e:	4629      	mov	r1, r5
c0d00c50:	4622      	mov	r2, r4
c0d00c52:	f000 f9d4 	bl	c0d00ffe <os_memmove>
                    tx = 65;
c0d00c56:	9420      	str	r4, [sp, #128]	; 0x80
c0d00c58:	e7b4      	b.n	c0d00bc4 <blocknet_main+0x580>

                case 0xFF: // return to dashboard
                    goto return_to_dashboard;

                default:
                    THROW(0x6D00);
c0d00c5a:	206d      	movs	r0, #109	; 0x6d
c0d00c5c:	0200      	lsls	r0, r0, #8
c0d00c5e:	f000 fa6b 	bl	c0d01138 <os_longjmp>
						THROW(0x6B00);
					}
					
					msgLen = G_io_apdu_buffer[OFFSET_MSG_LEN];
					if (msgLen < 1) {
						THROW(0x6700);
c0d00c62:	2067      	movs	r0, #103	; 0x67
c0d00c64:	0200      	lsls	r0, r0, #8
c0d00c66:	f000 fa67 	bl	c0d01138 <os_longjmp>
                        
                        ui_approval();						                

                    } else {						
						
                        tx = 0;
c0d00c6a:	9620      	str	r6, [sp, #128]	; 0x80
c0d00c6c:	e7aa      	b.n	c0d00bc4 <blocknet_main+0x580>
c0d00c6e:	46c0      	nop			; (mov r8, r8)
c0d00c70:	0000ffff 	.word	0x0000ffff
c0d00c74:	000007ff 	.word	0x000007ff
c0d00c78:	20001b38 	.word	0x20001b38
c0d00c7c:	200019e4 	.word	0x200019e4
c0d00c80:	20001b24 	.word	0x20001b24
c0d00c84:	20001ab8 	.word	0x20001ab8
c0d00c88:	200019fc 	.word	0x200019fc
c0d00c8c:	200019f8 	.word	0x200019f8
c0d00c90:	20001a04 	.word	0x20001a04
c0d00c94:	20001a70 	.word	0x20001a70
c0d00c98:	20001a24 	.word	0x20001a24
c0d00c9c:	20001a98 	.word	0x20001a98
c0d00ca0:	00000601 	.word	0x00000601
c0d00ca4:	20001884 	.word	0x20001884
c0d00ca8:	20001934 	.word	0x20001934
c0d00cac:	200019ac 	.word	0x200019ac
c0d00cb0:	200019e0 	.word	0x200019e0
c0d00cb4:	20001b28 	.word	0x20001b28
c0d00cb8:	20001800 	.word	0x20001800
c0d00cbc:	00006982 	.word	0x00006982
c0d00cc0:	000022f5 	.word	0x000022f5
c0d00cc4:	000022d2 	.word	0x000022d2
c0d00cc8:	000027b6 	.word	0x000027b6

c0d00ccc <bagl_ui_sample_nanos_button>:
    return element;
}

static unsigned int
bagl_ui_sample_nanos_button(unsigned int button_mask,
                            unsigned int button_mask_counter) {
c0d00ccc:	b510      	push	{r4, lr}
c0d00cce:	b0a6      	sub	sp, #152	; 0x98
    switch (button_mask) {
c0d00cd0:	4910      	ldr	r1, [pc, #64]	; (c0d00d14 <bagl_ui_sample_nanos_button+0x48>)
c0d00cd2:	4288      	cmp	r0, r1
c0d00cd4:	d017      	beq.n	c0d00d06 <bagl_ui_sample_nanos_button+0x3a>
c0d00cd6:	4910      	ldr	r1, [pc, #64]	; (c0d00d18 <bagl_ui_sample_nanos_button+0x4c>)
c0d00cd8:	4288      	cmp	r0, r1
c0d00cda:	d011      	beq.n	c0d00d00 <bagl_ui_sample_nanos_button+0x34>
c0d00cdc:	490f      	ldr	r1, [pc, #60]	; (c0d00d1c <bagl_ui_sample_nanos_button+0x50>)
c0d00cde:	4288      	cmp	r0, r1
c0d00ce0:	d114      	bne.n	c0d00d0c <bagl_ui_sample_nanos_button+0x40>
    ui_idle();
    return NULL;
}

static const bagl_element_t *io_seproxyhal_touch_auth(const bagl_element_t *e) {
    if (!os_global_pin_is_validated()) {
c0d00ce2:	f000 ff8d 	bl	c0d01c00 <os_global_pin_is_validated>
c0d00ce6:	2800      	cmp	r0, #0
c0d00ce8:	d110      	bne.n	c0d00d0c <bagl_ui_sample_nanos_button+0x40>
c0d00cea:	466c      	mov	r4, sp
c0d00cec:	2100      	movs	r1, #0
        bolos_ux_params_t params;
        os_memset(&params, 0, sizeof(params));
c0d00cee:	2298      	movs	r2, #152	; 0x98
c0d00cf0:	4620      	mov	r0, r4
c0d00cf2:	f000 f97b 	bl	c0d00fec <os_memset>
        params.ux_id = BOLOS_UX_VALIDATE_PIN;
c0d00cf6:	200e      	movs	r0, #14
c0d00cf8:	7020      	strb	r0, [r4, #0]
        os_ux_blocking(&params);
c0d00cfa:	4620      	mov	r0, r4
c0d00cfc:	f000 fe46 	bl	c0d0198c <os_ux_blocking>
c0d00d00:	f7ff fc52 	bl	c0d005a8 <ui_idle>
c0d00d04:	e002      	b.n	c0d00d0c <bagl_ui_sample_nanos_button+0x40>
// //////////////////////////////////////////////////////////////////////////////////


static const bagl_element_t *io_seproxyhal_touch_exit(const bagl_element_t *e) {
    // Go back to the dashboard
    os_sched_exit(0);
c0d00d06:	2000      	movs	r0, #0
c0d00d08:	f000 ff90 	bl	c0d01c2c <os_sched_exit>

    case BUTTON_EVT_RELEASED | BUTTON_LEFT | BUTTON_RIGHT: // EXIT
        io_seproxyhal_touch_exit(NULL);
        break;
    }
    return 0;
c0d00d0c:	2000      	movs	r0, #0
c0d00d0e:	b026      	add	sp, #152	; 0x98
c0d00d10:	bd10      	pop	{r4, pc}
c0d00d12:	46c0      	nop			; (mov r8, r8)
c0d00d14:	80000003 	.word	0x80000003
c0d00d18:	80000002 	.word	0x80000002
c0d00d1c:	80000001 	.word	0x80000001

c0d00d20 <bagl_ui_sample_nanos_prepro>:
        NULL,
    },
};

static const bagl_element_t*
bagl_ui_sample_nanos_prepro(const bagl_element_t *element) {
c0d00d20:	b5b0      	push	{r4, r5, r7, lr}
c0d00d22:	4604      	mov	r4, r0
    switch (element->component.userid) {
c0d00d24:	7860      	ldrb	r0, [r4, #1]
c0d00d26:	2802      	cmp	r0, #2
c0d00d28:	d110      	bne.n	c0d00d4c <bagl_ui_sample_nanos_prepro+0x2c>
    case 2:
        io_seproxyhal_setup_ticker(
            MAX(3000, 1000 + bagl_label_roundtrip_duration_ms(element, 7)));
c0d00d2a:	2107      	movs	r1, #7
c0d00d2c:	4620      	mov	r0, r4
c0d00d2e:	f000 fced 	bl	c0d0170c <bagl_label_roundtrip_duration_ms>
c0d00d32:	217d      	movs	r1, #125	; 0x7d
c0d00d34:	00cd      	lsls	r5, r1, #3
c0d00d36:	1941      	adds	r1, r0, r5
c0d00d38:	4805      	ldr	r0, [pc, #20]	; (c0d00d50 <bagl_ui_sample_nanos_prepro+0x30>)
c0d00d3a:	4281      	cmp	r1, r0
c0d00d3c:	d304      	bcc.n	c0d00d48 <bagl_ui_sample_nanos_prepro+0x28>
c0d00d3e:	2107      	movs	r1, #7
c0d00d40:	4620      	mov	r0, r4
c0d00d42:	f000 fce3 	bl	c0d0170c <bagl_label_roundtrip_duration_ms>
c0d00d46:	1940      	adds	r0, r0, r5

static const bagl_element_t*
bagl_ui_sample_nanos_prepro(const bagl_element_t *element) {
    switch (element->component.userid) {
    case 2:
        io_seproxyhal_setup_ticker(
c0d00d48:	f000 fd0c 	bl	c0d01764 <io_seproxyhal_setup_ticker>
            MAX(3000, 1000 + bagl_label_roundtrip_duration_ms(element, 7)));
        break;
    }
    return element;
c0d00d4c:	4620      	mov	r0, r4
c0d00d4e:	bdb0      	pop	{r4, r5, r7, pc}
c0d00d50:	00000bb8 	.word	0x00000bb8

c0d00d54 <get_path_BE>:
	#endif  

}


static void get_path_BE() {
c0d00d54:	b5f0      	push	{r4, r5, r6, r7, lr}
	
	dataBuffer = G_io_apdu_buffer + 5;
c0d00d56:	480e      	ldr	r0, [pc, #56]	; (c0d00d90 <get_path_BE+0x3c>)
c0d00d58:	1d42      	adds	r2, r0, #5
c0d00d5a:	490e      	ldr	r1, [pc, #56]	; (c0d00d94 <get_path_BE+0x40>)
c0d00d5c:	600a      	str	r2, [r1, #0]
c0d00d5e:	4a0e      	ldr	r2, [pc, #56]	; (c0d00d98 <get_path_BE+0x44>)
c0d00d60:	2300      	movs	r3, #0
c0d00d62:	6013      	str	r3, [r2, #0]
c0d00d64:	4c0d      	ldr	r4, [pc, #52]	; (c0d00d9c <get_path_BE+0x48>)
	
	for (i = 0; i < 5; i++) {
		path_n[i] = (dataBuffer[0] << 24) 
c0d00d66:	009e      	lsls	r6, r3, #2
c0d00d68:	4809      	ldr	r0, [pc, #36]	; (c0d00d90 <get_path_BE+0x3c>)
c0d00d6a:	1985      	adds	r5, r0, r6
c0d00d6c:	796f      	ldrb	r7, [r5, #5]
c0d00d6e:	063f      	lsls	r7, r7, #24
					| (dataBuffer[1] << 16) 
c0d00d70:	79a8      	ldrb	r0, [r5, #6]
c0d00d72:	0400      	lsls	r0, r0, #16
c0d00d74:	4338      	orrs	r0, r7
					| (dataBuffer[2] << 8) 
c0d00d76:	79ef      	ldrb	r7, [r5, #7]
c0d00d78:	023f      	lsls	r7, r7, #8
c0d00d7a:	4307      	orrs	r7, r0
					| (dataBuffer[3]);
c0d00d7c:	7a28      	ldrb	r0, [r5, #8]
c0d00d7e:	4338      	orrs	r0, r7
static void get_path_BE() {
	
	dataBuffer = G_io_apdu_buffer + 5;
	
	for (i = 0; i < 5; i++) {
		path_n[i] = (dataBuffer[0] << 24) 
c0d00d80:	51a0      	str	r0, [r4, r6]
					| (dataBuffer[1] << 16) 
					| (dataBuffer[2] << 8) 
					| (dataBuffer[3]);

		dataBuffer += 4;
c0d00d82:	3509      	adds	r5, #9
c0d00d84:	600d      	str	r5, [r1, #0]

static void get_path_BE() {
	
	dataBuffer = G_io_apdu_buffer + 5;
	
	for (i = 0; i < 5; i++) {
c0d00d86:	1c5b      	adds	r3, r3, #1
c0d00d88:	6013      	str	r3, [r2, #0]
c0d00d8a:	2b05      	cmp	r3, #5
c0d00d8c:	d3eb      	bcc.n	c0d00d66 <get_path_BE+0x12>
					| (dataBuffer[3]);

		dataBuffer += 4;
	}

}
c0d00d8e:	bdf0      	pop	{r4, r5, r6, r7, pc}
c0d00d90:	20001b38 	.word	0x20001b38
c0d00d94:	200019fc 	.word	0x200019fc
c0d00d98:	200019f8 	.word	0x200019f8
c0d00d9c:	200019e4 	.word	0x200019e4

c0d00da0 <bagl_ui_approval_nanos_button>:
};


static unsigned int
bagl_ui_approval_nanos_button(unsigned int button_mask,
                              unsigned int button_mask_counter) {
c0d00da0:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d00da2:	b085      	sub	sp, #20
	
	// &= clears a bit
	G_io_apdu_buffer[0] &= 0xF0; // discard the parity information
					
	// Append response codes
	G_io_apdu_buffer[tx++] = 0x90;
c0d00da4:	2785      	movs	r7, #133	; 0x85


static unsigned int
bagl_ui_approval_nanos_button(unsigned int button_mask,
                              unsigned int button_mask_counter) {
    switch (button_mask) {
c0d00da6:	491d      	ldr	r1, [pc, #116]	; (c0d00e1c <bagl_ui_approval_nanos_button+0x7c>)
c0d00da8:	4288      	cmp	r0, r1
c0d00daa:	d027      	beq.n	c0d00dfc <bagl_ui_approval_nanos_button+0x5c>
c0d00dac:	491c      	ldr	r1, [pc, #112]	; (c0d00e20 <bagl_ui_approval_nanos_button+0x80>)
c0d00dae:	4288      	cmp	r0, r1
c0d00db0:	d130      	bne.n	c0d00e14 <bagl_ui_approval_nanos_button+0x74>
c0d00db2:	2420      	movs	r4, #32
    */

    unsigned int tx = 0;
                
	// Hash is finalized, send back the signature
	cx_hash(&hash.header, CX_LAST, G_io_apdu_buffer, 0, result, sizeof(result));
c0d00db4:	4668      	mov	r0, sp
c0d00db6:	6044      	str	r4, [r0, #4]
c0d00db8:	491b      	ldr	r1, [pc, #108]	; (c0d00e28 <bagl_ui_approval_nanos_button+0x88>)
c0d00dba:	6001      	str	r1, [r0, #0]
c0d00dbc:	481b      	ldr	r0, [pc, #108]	; (c0d00e2c <bagl_ui_approval_nanos_button+0x8c>)
c0d00dbe:	2101      	movs	r1, #1
c0d00dc0:	4d18      	ldr	r5, [pc, #96]	; (c0d00e24 <bagl_ui_approval_nanos_button+0x84>)
c0d00dc2:	2600      	movs	r6, #0
c0d00dc4:	462a      	mov	r2, r5
c0d00dc6:	4633      	mov	r3, r6
c0d00dc8:	f000 fe6a 	bl	c0d01aa0 <cx_hash>
	tx = cx_ecdsa_sign((void*) &privateKey, CX_RND_RFC6979 | CX_LAST,
c0d00dcc:	4668      	mov	r0, sp
c0d00dce:	21ff      	movs	r1, #255	; 0xff
c0d00dd0:	3152      	adds	r1, #82	; 0x52
c0d00dd2:	c030      	stmia	r0!, {r4, r5}
c0d00dd4:	6001      	str	r1, [r0, #0]
c0d00dd6:	6046      	str	r6, [r0, #4]
c0d00dd8:	4815      	ldr	r0, [pc, #84]	; (c0d00e30 <bagl_ui_approval_nanos_button+0x90>)
c0d00dda:	4916      	ldr	r1, [pc, #88]	; (c0d00e34 <bagl_ui_approval_nanos_button+0x94>)
c0d00ddc:	2203      	movs	r2, #3
c0d00dde:	4b12      	ldr	r3, [pc, #72]	; (c0d00e28 <bagl_ui_approval_nanos_button+0x88>)
c0d00de0:	f000 fed6 	bl	c0d01b90 <cx_ecdsa_sign>
					CX_SHA256, result, sizeof(result), G_io_apdu_buffer, sizeof(G_io_apdu_buffer), NULL);
	
	// &= clears a bit
	G_io_apdu_buffer[0] &= 0xF0; // discard the parity information
c0d00de4:	7829      	ldrb	r1, [r5, #0]
c0d00de6:	22f0      	movs	r2, #240	; 0xf0
c0d00de8:	400a      	ands	r2, r1
c0d00dea:	702a      	strb	r2, [r5, #0]
					
	// Append response codes
	G_io_apdu_buffer[tx++] = 0x90;
c0d00dec:	370b      	adds	r7, #11
c0d00dee:	542f      	strb	r7, [r5, r0]
    */

    unsigned int tx = 0;
                
	// Hash is finalized, send back the signature
	cx_hash(&hash.header, CX_LAST, G_io_apdu_buffer, 0, result, sizeof(result));
c0d00df0:	1829      	adds	r1, r5, r0
	// &= clears a bit
	G_io_apdu_buffer[0] &= 0xF0; // discard the parity information
					
	// Append response codes
	G_io_apdu_buffer[tx++] = 0x90;
	G_io_apdu_buffer[tx++] = 0x00;
c0d00df2:	704e      	strb	r6, [r1, #1]
c0d00df4:	1c80      	adds	r0, r0, #2
    
    // Send back the response, do not restart the event loop
    io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, tx);
c0d00df6:	b281      	uxth	r1, r0
c0d00df8:	4620      	mov	r0, r4
c0d00dfa:	e005      	b.n	c0d00e08 <bagl_ui_approval_nanos_button+0x68>

static const bagl_element_t *io_seproxyhal_touch_deny(const bagl_element_t *e) {
    
    // hashTainted = 1;
    
    G_io_apdu_buffer[0] = 0x69;
c0d00dfc:	4809      	ldr	r0, [pc, #36]	; (c0d00e24 <bagl_ui_approval_nanos_button+0x84>)
c0d00dfe:	2169      	movs	r1, #105	; 0x69
c0d00e00:	7001      	strb	r1, [r0, #0]
    G_io_apdu_buffer[1] = 0x85;
c0d00e02:	7047      	strb	r7, [r0, #1]
    
    // Send back the response, do not restart the event loop
    io_exchange(CHANNEL_APDU | IO_RETURN_AFTER_TX, 2);
c0d00e04:	2020      	movs	r0, #32
c0d00e06:	2102      	movs	r1, #2
c0d00e08:	f000 fcf8 	bl	c0d017fc <io_exchange>
c0d00e0c:	f000 f814 	bl	c0d00e38 <ui_say_ready>
c0d00e10:	f7ff fbca 	bl	c0d005a8 <ui_idle>
    case BUTTON_EVT_RELEASED | BUTTON_LEFT:
        io_seproxyhal_touch_deny(NULL);
        // os_sched_exit(0);
        break;
    }
    return 0;
c0d00e14:	2000      	movs	r0, #0
c0d00e16:	b005      	add	sp, #20
c0d00e18:	bdf0      	pop	{r4, r5, r6, r7, pc}
c0d00e1a:	46c0      	nop			; (mov r8, r8)
c0d00e1c:	80000001 	.word	0x80000001
c0d00e20:	80000002 	.word	0x80000002
c0d00e24:	20001b38 	.word	0x20001b38
c0d00e28:	20001a98 	.word	0x20001a98
c0d00e2c:	20001ab8 	.word	0x20001ab8
c0d00e30:	20001a70 	.word	0x20001a70
c0d00e34:	00000601 	.word	0x00000601

c0d00e38 <ui_say_ready>:
	ui_set_lineBuffer_from_apdu();
		
}


static void ui_say_ready() {
c0d00e38:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d00e3a:	b081      	sub	sp, #4


static void ui_reset_captions() {
	
	// First UI line
	os_memmove(status_current_str, TXT_BLANK, sizeof(TXT_BLANK));
c0d00e3c:	4c0e      	ldr	r4, [pc, #56]	; (c0d00e78 <ui_say_ready+0x40>)
c0d00e3e:	4e10      	ldr	r6, [pc, #64]	; (c0d00e80 <ui_say_ready+0x48>)
c0d00e40:	447e      	add	r6, pc
c0d00e42:	2712      	movs	r7, #18
c0d00e44:	4620      	mov	r0, r4
c0d00e46:	4631      	mov	r1, r6
c0d00e48:	463a      	mov	r2, r7
c0d00e4a:	f000 f8d8 	bl	c0d00ffe <os_memmove>
	
	// Second UI line             
	os_memmove(lineBuffer, TXT_BLANK, sizeof(TXT_BLANK));
c0d00e4e:	4d0b      	ldr	r5, [pc, #44]	; (c0d00e7c <ui_say_ready+0x44>)
c0d00e50:	4628      	mov	r0, r5
c0d00e52:	4631      	mov	r1, r6
c0d00e54:	463a      	mov	r2, r7
c0d00e56:	f000 f8d2 	bl	c0d00ffe <os_memmove>
static void ui_say_ready() {
	
	ui_reset_captions();

	// Show status string
	os_memmove(status_current_str, status_str, 7);
c0d00e5a:	490a      	ldr	r1, [pc, #40]	; (c0d00e84 <ui_say_ready+0x4c>)
c0d00e5c:	4479      	add	r1, pc
c0d00e5e:	2207      	movs	r2, #7
c0d00e60:	4620      	mov	r0, r4
c0d00e62:	f000 f8cc 	bl	c0d00ffe <os_memmove>
	
	// Show ready string
	os_memmove(lineBuffer, status_ready_str, 5);
c0d00e66:	4908      	ldr	r1, [pc, #32]	; (c0d00e88 <ui_say_ready+0x50>)
c0d00e68:	4479      	add	r1, pc
c0d00e6a:	2205      	movs	r2, #5
c0d00e6c:	4628      	mov	r0, r5
c0d00e6e:	f000 f8c6 	bl	c0d00ffe <os_memmove>

}
c0d00e72:	b001      	add	sp, #4
c0d00e74:	bdf0      	pop	{r4, r5, r6, r7, pc}
c0d00e76:	46c0      	nop			; (mov r8, r8)
c0d00e78:	20001934 	.word	0x20001934
c0d00e7c:	200019ac 	.word	0x200019ac
c0d00e80:	00002021 	.word	0x00002021
c0d00e84:	00002146 	.word	0x00002146
c0d00e88:	00002142 	.word	0x00002142

c0d00e8c <os_boot>:
  //                ^ platform register
  return (try_context_t*) current_ctx->jmp_buf[5];
}

void try_context_set(try_context_t* ctx) {
  __asm volatile ("mov r9, %0"::"r"(ctx));
c0d00e8c:	2000      	movs	r0, #0
c0d00e8e:	4681      	mov	r9, r0
void os_boot(void) {
  // TODO patch entry point when romming (f)

  // set the default try context to nothing
  try_context_set(NULL);
}
c0d00e90:	4770      	bx	lr

c0d00e92 <try_context_set>:
  //                ^ platform register
  return (try_context_t*) current_ctx->jmp_buf[5];
}

void try_context_set(try_context_t* ctx) {
  __asm volatile ("mov r9, %0"::"r"(ctx));
c0d00e92:	4681      	mov	r9, r0
}
c0d00e94:	4770      	bx	lr
	...

c0d00e98 <io_usb_hid_receive>:
volatile unsigned int   G_io_usb_hid_channel;
volatile unsigned int   G_io_usb_hid_remaining_length;
volatile unsigned int   G_io_usb_hid_sequence_number;
volatile unsigned char* G_io_usb_hid_current_buffer;

io_usb_hid_receive_status_t io_usb_hid_receive (io_send_t sndfct, unsigned char* buffer, unsigned short l) {
c0d00e98:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d00e9a:	b081      	sub	sp, #4
c0d00e9c:	9200      	str	r2, [sp, #0]
c0d00e9e:	460f      	mov	r7, r1
c0d00ea0:	4605      	mov	r5, r0
  // avoid over/under flows
  if (buffer != G_io_usb_ep_buffer) {
c0d00ea2:	4b49      	ldr	r3, [pc, #292]	; (c0d00fc8 <io_usb_hid_receive+0x130>)
c0d00ea4:	429f      	cmp	r7, r3
c0d00ea6:	d00f      	beq.n	c0d00ec8 <io_usb_hid_receive+0x30>
}

void os_memset(void * dst, unsigned char c, unsigned int length) {
#define DSTCHAR ((unsigned char *)dst)
  while(length--) {
    DSTCHAR[length] = c;
c0d00ea8:	4c47      	ldr	r4, [pc, #284]	; (c0d00fc8 <io_usb_hid_receive+0x130>)
c0d00eaa:	2640      	movs	r6, #64	; 0x40
c0d00eac:	4620      	mov	r0, r4
c0d00eae:	4631      	mov	r1, r6
c0d00eb0:	f001 fe9c 	bl	c0d02bec <__aeabi_memclr>
c0d00eb4:	9800      	ldr	r0, [sp, #0]

io_usb_hid_receive_status_t io_usb_hid_receive (io_send_t sndfct, unsigned char* buffer, unsigned short l) {
  // avoid over/under flows
  if (buffer != G_io_usb_ep_buffer) {
    os_memset(G_io_usb_ep_buffer, 0, sizeof(G_io_usb_ep_buffer));
    os_memmove(G_io_usb_ep_buffer, buffer, MIN(l, sizeof(G_io_usb_ep_buffer)));
c0d00eb6:	2840      	cmp	r0, #64	; 0x40
c0d00eb8:	4602      	mov	r2, r0
c0d00eba:	d300      	bcc.n	c0d00ebe <io_usb_hid_receive+0x26>
c0d00ebc:	4632      	mov	r2, r6
c0d00ebe:	4620      	mov	r0, r4
c0d00ec0:	4639      	mov	r1, r7
c0d00ec2:	f000 f89c 	bl	c0d00ffe <os_memmove>
c0d00ec6:	4b40      	ldr	r3, [pc, #256]	; (c0d00fc8 <io_usb_hid_receive+0x130>)
c0d00ec8:	7898      	ldrb	r0, [r3, #2]
  }

  // process the chunk content
  switch(G_io_usb_ep_buffer[2]) {
c0d00eca:	2801      	cmp	r0, #1
c0d00ecc:	dc0b      	bgt.n	c0d00ee6 <io_usb_hid_receive+0x4e>
c0d00ece:	2800      	cmp	r0, #0
c0d00ed0:	d02a      	beq.n	c0d00f28 <io_usb_hid_receive+0x90>
c0d00ed2:	2801      	cmp	r0, #1
c0d00ed4:	d16a      	bne.n	c0d00fac <io_usb_hid_receive+0x114>
    // await for the next chunk
    goto apdu_reset;

  case 0x01: // ALLOCATE CHANNEL
    // do not reset the current apdu reception if any
    cx_rng(G_io_usb_ep_buffer+3, 4);
c0d00ed6:	1cd8      	adds	r0, r3, #3
c0d00ed8:	2104      	movs	r1, #4
c0d00eda:	461c      	mov	r4, r3
c0d00edc:	f000 fdc8 	bl	c0d01a70 <cx_rng>
    // send the response
    sndfct(G_io_usb_ep_buffer, IO_HID_EP_LENGTH);
c0d00ee0:	2140      	movs	r1, #64	; 0x40
c0d00ee2:	4620      	mov	r0, r4
c0d00ee4:	e02b      	b.n	c0d00f3e <io_usb_hid_receive+0xa6>
c0d00ee6:	2802      	cmp	r0, #2
c0d00ee8:	d027      	beq.n	c0d00f3a <io_usb_hid_receive+0xa2>
c0d00eea:	2805      	cmp	r0, #5
c0d00eec:	d15e      	bne.n	c0d00fac <io_usb_hid_receive+0x114>

  // process the chunk content
  switch(G_io_usb_ep_buffer[2]) {
  case 0x05:
    // ensure sequence idx is 0 for the first chunk ! 
    if (U2BE(G_io_usb_ep_buffer, 3) != G_io_usb_hid_sequence_number) {
c0d00eee:	7918      	ldrb	r0, [r3, #4]
c0d00ef0:	78d9      	ldrb	r1, [r3, #3]
c0d00ef2:	0209      	lsls	r1, r1, #8
c0d00ef4:	4301      	orrs	r1, r0
c0d00ef6:	4a35      	ldr	r2, [pc, #212]	; (c0d00fcc <io_usb_hid_receive+0x134>)
c0d00ef8:	6810      	ldr	r0, [r2, #0]
c0d00efa:	2400      	movs	r4, #0
c0d00efc:	4281      	cmp	r1, r0
c0d00efe:	d15b      	bne.n	c0d00fb8 <io_usb_hid_receive+0x120>
c0d00f00:	4e33      	ldr	r6, [pc, #204]	; (c0d00fd0 <io_usb_hid_receive+0x138>)
      // ignore packet
      goto apdu_reset;
    }
    // cid, tag, seq
    l -= 2+1+2;
c0d00f02:	9800      	ldr	r0, [sp, #0]
c0d00f04:	1980      	adds	r0, r0, r6
c0d00f06:	1f07      	subs	r7, r0, #4
    
    // append the received chunk to the current command apdu
    if (G_io_usb_hid_sequence_number == 0) {
c0d00f08:	6810      	ldr	r0, [r2, #0]
c0d00f0a:	2800      	cmp	r0, #0
c0d00f0c:	d01a      	beq.n	c0d00f44 <io_usb_hid_receive+0xac>
      // copy data
      os_memmove((void*)G_io_usb_hid_current_buffer, G_io_usb_ep_buffer+7, l);
    }
    else {
      // check for invalid length encoding (more data in chunk that announced in the total apdu)
      if (l > G_io_usb_hid_remaining_length) {
c0d00f0e:	4639      	mov	r1, r7
c0d00f10:	4031      	ands	r1, r6
c0d00f12:	4830      	ldr	r0, [pc, #192]	; (c0d00fd4 <io_usb_hid_receive+0x13c>)
c0d00f14:	6802      	ldr	r2, [r0, #0]
c0d00f16:	4291      	cmp	r1, r2
c0d00f18:	d900      	bls.n	c0d00f1c <io_usb_hid_receive+0x84>
        l = G_io_usb_hid_remaining_length;
c0d00f1a:	6807      	ldr	r7, [r0, #0]
      }

      /// This is a following chunk
      // append content
      os_memmove((void*)G_io_usb_hid_current_buffer, G_io_usb_ep_buffer+5, l);
c0d00f1c:	463a      	mov	r2, r7
c0d00f1e:	4032      	ands	r2, r6
c0d00f20:	482d      	ldr	r0, [pc, #180]	; (c0d00fd8 <io_usb_hid_receive+0x140>)
c0d00f22:	6800      	ldr	r0, [r0, #0]
c0d00f24:	1d59      	adds	r1, r3, #5
c0d00f26:	e031      	b.n	c0d00f8c <io_usb_hid_receive+0xf4>
c0d00f28:	2400      	movs	r4, #0
}

void os_memset(void * dst, unsigned char c, unsigned int length) {
#define DSTCHAR ((unsigned char *)dst)
  while(length--) {
    DSTCHAR[length] = c;
c0d00f2a:	719c      	strb	r4, [r3, #6]
c0d00f2c:	715c      	strb	r4, [r3, #5]
c0d00f2e:	711c      	strb	r4, [r3, #4]
c0d00f30:	70dc      	strb	r4, [r3, #3]

  case 0x00: // get version ID
    // do not reset the current apdu reception if any
    os_memset(G_io_usb_ep_buffer+3, 0, 4); // PROTOCOL VERSION is 0
    // send the response
    sndfct(G_io_usb_ep_buffer, IO_HID_EP_LENGTH);
c0d00f32:	2140      	movs	r1, #64	; 0x40
c0d00f34:	4618      	mov	r0, r3
c0d00f36:	47a8      	blx	r5
c0d00f38:	e03e      	b.n	c0d00fb8 <io_usb_hid_receive+0x120>
    goto apdu_reset;

  case 0x02: // ECHO|PING
    // do not reset the current apdu reception if any
    // send the response
    sndfct(G_io_usb_ep_buffer, IO_HID_EP_LENGTH);
c0d00f3a:	4823      	ldr	r0, [pc, #140]	; (c0d00fc8 <io_usb_hid_receive+0x130>)
c0d00f3c:	2140      	movs	r1, #64	; 0x40
c0d00f3e:	47a8      	blx	r5
c0d00f40:	2400      	movs	r4, #0
c0d00f42:	e039      	b.n	c0d00fb8 <io_usb_hid_receive+0x120>
    
    // append the received chunk to the current command apdu
    if (G_io_usb_hid_sequence_number == 0) {
      /// This is the apdu first chunk
      // total apdu size to receive
      G_io_usb_hid_total_length = U2BE(G_io_usb_ep_buffer, 5); //(G_io_usb_ep_buffer[5]<<8)+(G_io_usb_ep_buffer[6]&0xFF);
c0d00f44:	7998      	ldrb	r0, [r3, #6]
c0d00f46:	7959      	ldrb	r1, [r3, #5]
c0d00f48:	0209      	lsls	r1, r1, #8
c0d00f4a:	4301      	orrs	r1, r0
c0d00f4c:	4823      	ldr	r0, [pc, #140]	; (c0d00fdc <io_usb_hid_receive+0x144>)
c0d00f4e:	6001      	str	r1, [r0, #0]
      // check for invalid length encoding (more data in chunk that announced in the total apdu)
      if (G_io_usb_hid_total_length > sizeof(G_io_apdu_buffer)) {
c0d00f50:	6801      	ldr	r1, [r0, #0]
c0d00f52:	2241      	movs	r2, #65	; 0x41
c0d00f54:	0092      	lsls	r2, r2, #2
c0d00f56:	4291      	cmp	r1, r2
c0d00f58:	d82e      	bhi.n	c0d00fb8 <io_usb_hid_receive+0x120>
        goto apdu_reset;
      }
      // seq and total length
      l -= 2;
      // compute remaining size to receive
      G_io_usb_hid_remaining_length = G_io_usb_hid_total_length;
c0d00f5a:	6801      	ldr	r1, [r0, #0]
c0d00f5c:	481d      	ldr	r0, [pc, #116]	; (c0d00fd4 <io_usb_hid_receive+0x13c>)
c0d00f5e:	6001      	str	r1, [r0, #0]
      G_io_usb_hid_current_buffer = G_io_apdu_buffer;
c0d00f60:	491d      	ldr	r1, [pc, #116]	; (c0d00fd8 <io_usb_hid_receive+0x140>)
c0d00f62:	4a1f      	ldr	r2, [pc, #124]	; (c0d00fe0 <io_usb_hid_receive+0x148>)
c0d00f64:	600a      	str	r2, [r1, #0]

      // retain the channel id to use for the reply
      G_io_usb_hid_channel = U2BE(G_io_usb_ep_buffer, 0);
c0d00f66:	7859      	ldrb	r1, [r3, #1]
c0d00f68:	781a      	ldrb	r2, [r3, #0]
c0d00f6a:	0212      	lsls	r2, r2, #8
c0d00f6c:	430a      	orrs	r2, r1
c0d00f6e:	491d      	ldr	r1, [pc, #116]	; (c0d00fe4 <io_usb_hid_receive+0x14c>)
c0d00f70:	600a      	str	r2, [r1, #0]
      // check for invalid length encoding (more data in chunk that announced in the total apdu)
      if (G_io_usb_hid_total_length > sizeof(G_io_apdu_buffer)) {
        goto apdu_reset;
      }
      // seq and total length
      l -= 2;
c0d00f72:	491d      	ldr	r1, [pc, #116]	; (c0d00fe8 <io_usb_hid_receive+0x150>)
c0d00f74:	9a00      	ldr	r2, [sp, #0]
c0d00f76:	1857      	adds	r7, r2, r1
      G_io_usb_hid_current_buffer = G_io_apdu_buffer;

      // retain the channel id to use for the reply
      G_io_usb_hid_channel = U2BE(G_io_usb_ep_buffer, 0);

      if (l > G_io_usb_hid_remaining_length) {
c0d00f78:	4639      	mov	r1, r7
c0d00f7a:	4031      	ands	r1, r6
c0d00f7c:	6802      	ldr	r2, [r0, #0]
c0d00f7e:	4291      	cmp	r1, r2
c0d00f80:	d900      	bls.n	c0d00f84 <io_usb_hid_receive+0xec>
        l = G_io_usb_hid_remaining_length;
c0d00f82:	6807      	ldr	r7, [r0, #0]
      }
      // copy data
      os_memmove((void*)G_io_usb_hid_current_buffer, G_io_usb_ep_buffer+7, l);
c0d00f84:	463a      	mov	r2, r7
c0d00f86:	4032      	ands	r2, r6
c0d00f88:	1dd9      	adds	r1, r3, #7
c0d00f8a:	4815      	ldr	r0, [pc, #84]	; (c0d00fe0 <io_usb_hid_receive+0x148>)
c0d00f8c:	f000 f837 	bl	c0d00ffe <os_memmove>
      /// This is a following chunk
      // append content
      os_memmove((void*)G_io_usb_hid_current_buffer, G_io_usb_ep_buffer+5, l);
    }
    // factorize (f)
    G_io_usb_hid_current_buffer += l;
c0d00f90:	4037      	ands	r7, r6
c0d00f92:	4811      	ldr	r0, [pc, #68]	; (c0d00fd8 <io_usb_hid_receive+0x140>)
c0d00f94:	6801      	ldr	r1, [r0, #0]
c0d00f96:	19c9      	adds	r1, r1, r7
c0d00f98:	6001      	str	r1, [r0, #0]
    G_io_usb_hid_remaining_length -= l;
c0d00f9a:	480e      	ldr	r0, [pc, #56]	; (c0d00fd4 <io_usb_hid_receive+0x13c>)
c0d00f9c:	6801      	ldr	r1, [r0, #0]
c0d00f9e:	1bc9      	subs	r1, r1, r7
c0d00fa0:	6001      	str	r1, [r0, #0]
c0d00fa2:	480a      	ldr	r0, [pc, #40]	; (c0d00fcc <io_usb_hid_receive+0x134>)
c0d00fa4:	4601      	mov	r1, r0
    G_io_usb_hid_sequence_number++;
c0d00fa6:	6808      	ldr	r0, [r1, #0]
c0d00fa8:	1c40      	adds	r0, r0, #1
c0d00faa:	6008      	str	r0, [r1, #0]
    // await for the next chunk
    goto apdu_reset;
  }

  // if more data to be received, notify it
  if (G_io_usb_hid_remaining_length) {
c0d00fac:	4809      	ldr	r0, [pc, #36]	; (c0d00fd4 <io_usb_hid_receive+0x13c>)
c0d00fae:	6801      	ldr	r1, [r0, #0]
c0d00fb0:	2001      	movs	r0, #1
c0d00fb2:	2402      	movs	r4, #2
c0d00fb4:	2900      	cmp	r1, #0
c0d00fb6:	d103      	bne.n	c0d00fc0 <io_usb_hid_receive+0x128>
  io_usb_hid_init();
  return IO_USB_APDU_RESET;
}

void io_usb_hid_init(void) {
  G_io_usb_hid_sequence_number = 0; 
c0d00fb8:	4804      	ldr	r0, [pc, #16]	; (c0d00fcc <io_usb_hid_receive+0x134>)
c0d00fba:	2100      	movs	r1, #0
c0d00fbc:	6001      	str	r1, [r0, #0]
c0d00fbe:	4620      	mov	r0, r4
  return IO_USB_APDU_RECEIVED;

apdu_reset:
  io_usb_hid_init();
  return IO_USB_APDU_RESET;
}
c0d00fc0:	b2c0      	uxtb	r0, r0
c0d00fc2:	b001      	add	sp, #4
c0d00fc4:	bdf0      	pop	{r4, r5, r6, r7, pc}
c0d00fc6:	46c0      	nop			; (mov r8, r8)
c0d00fc8:	20001c68 	.word	0x20001c68
c0d00fcc:	20001b2c 	.word	0x20001b2c
c0d00fd0:	0000ffff 	.word	0x0000ffff
c0d00fd4:	20001b34 	.word	0x20001b34
c0d00fd8:	20001c3c 	.word	0x20001c3c
c0d00fdc:	20001b30 	.word	0x20001b30
c0d00fe0:	20001b38 	.word	0x20001b38
c0d00fe4:	20001c40 	.word	0x20001c40
c0d00fe8:	0001fff9 	.word	0x0001fff9

c0d00fec <os_memset>:
    }
  }
#undef DSTCHAR
}

void os_memset(void * dst, unsigned char c, unsigned int length) {
c0d00fec:	b580      	push	{r7, lr}
c0d00fee:	460b      	mov	r3, r1
#define DSTCHAR ((unsigned char *)dst)
  while(length--) {
c0d00ff0:	2a00      	cmp	r2, #0
c0d00ff2:	d003      	beq.n	c0d00ffc <os_memset+0x10>
    DSTCHAR[length] = c;
c0d00ff4:	4611      	mov	r1, r2
c0d00ff6:	461a      	mov	r2, r3
c0d00ff8:	f001 fdfe 	bl	c0d02bf8 <__aeabi_memset>
  }
#undef DSTCHAR
}
c0d00ffc:	bd80      	pop	{r7, pc}

c0d00ffe <os_memmove>:
    }
  }
}
#endif // HAVE_USB_APDU

REENTRANT(void os_memmove(void * dst, const void WIDE * src, unsigned int length)) {
c0d00ffe:	b5b0      	push	{r4, r5, r7, lr}
#define DSTCHAR ((unsigned char *)dst)
#define SRCCHAR ((unsigned char WIDE *)src)
  if (dst > src) {
c0d01000:	4288      	cmp	r0, r1
c0d01002:	d90d      	bls.n	c0d01020 <os_memmove+0x22>
    while(length--) {
c0d01004:	2a00      	cmp	r2, #0
c0d01006:	d014      	beq.n	c0d01032 <os_memmove+0x34>
c0d01008:	1e49      	subs	r1, r1, #1
c0d0100a:	4252      	negs	r2, r2
c0d0100c:	1e40      	subs	r0, r0, #1
c0d0100e:	2300      	movs	r3, #0
c0d01010:	43db      	mvns	r3, r3
      DSTCHAR[length] = SRCCHAR[length];
c0d01012:	461c      	mov	r4, r3
c0d01014:	4354      	muls	r4, r2
c0d01016:	5d0d      	ldrb	r5, [r1, r4]
c0d01018:	5505      	strb	r5, [r0, r4]

REENTRANT(void os_memmove(void * dst, const void WIDE * src, unsigned int length)) {
#define DSTCHAR ((unsigned char *)dst)
#define SRCCHAR ((unsigned char WIDE *)src)
  if (dst > src) {
    while(length--) {
c0d0101a:	1c52      	adds	r2, r2, #1
c0d0101c:	d1f9      	bne.n	c0d01012 <os_memmove+0x14>
c0d0101e:	e008      	b.n	c0d01032 <os_memmove+0x34>
      DSTCHAR[length] = SRCCHAR[length];
    }
  }
  else {
    unsigned short l = 0;
    while (length--) {
c0d01020:	2a00      	cmp	r2, #0
c0d01022:	d006      	beq.n	c0d01032 <os_memmove+0x34>
c0d01024:	2300      	movs	r3, #0
      DSTCHAR[l] = SRCCHAR[l];
c0d01026:	b29c      	uxth	r4, r3
c0d01028:	5d0d      	ldrb	r5, [r1, r4]
c0d0102a:	5505      	strb	r5, [r0, r4]
      l++;
c0d0102c:	1c5b      	adds	r3, r3, #1
      DSTCHAR[length] = SRCCHAR[length];
    }
  }
  else {
    unsigned short l = 0;
    while (length--) {
c0d0102e:	1e52      	subs	r2, r2, #1
c0d01030:	d1f9      	bne.n	c0d01026 <os_memmove+0x28>
      DSTCHAR[l] = SRCCHAR[l];
      l++;
    }
  }
#undef DSTCHAR
}
c0d01032:	bdb0      	pop	{r4, r5, r7, pc}

c0d01034 <io_usb_hid_init>:
  io_usb_hid_init();
  return IO_USB_APDU_RESET;
}

void io_usb_hid_init(void) {
  G_io_usb_hid_sequence_number = 0; 
c0d01034:	4801      	ldr	r0, [pc, #4]	; (c0d0103c <io_usb_hid_init+0x8>)
c0d01036:	2100      	movs	r1, #0
c0d01038:	6001      	str	r1, [r0, #0]
  //G_io_usb_hid_remaining_length = 0; // not really needed
  //G_io_usb_hid_total_length = 0; // not really needed
  //G_io_usb_hid_current_buffer = G_io_apdu_buffer; // not really needed
}
c0d0103a:	4770      	bx	lr
c0d0103c:	20001b2c 	.word	0x20001b2c

c0d01040 <io_usb_hid_exchange>:

unsigned short io_usb_hid_exchange(io_send_t sndfct, unsigned short sndlength,
                                   io_recv_t rcvfct,
                                   unsigned char flags) {
c0d01040:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d01042:	b085      	sub	sp, #20
c0d01044:	9301      	str	r3, [sp, #4]
c0d01046:	9200      	str	r2, [sp, #0]
c0d01048:	460e      	mov	r6, r1
c0d0104a:	9003      	str	r0, [sp, #12]
  unsigned char l;

  // perform send
  if (sndlength) {
c0d0104c:	2e00      	cmp	r6, #0
c0d0104e:	d047      	beq.n	c0d010e0 <io_usb_hid_exchange+0xa0>
    G_io_usb_hid_sequence_number = 0; 
c0d01050:	4c32      	ldr	r4, [pc, #200]	; (c0d0111c <io_usb_hid_exchange+0xdc>)
c0d01052:	2000      	movs	r0, #0
c0d01054:	6020      	str	r0, [r4, #0]
    G_io_usb_hid_current_buffer = G_io_apdu_buffer;
c0d01056:	4932      	ldr	r1, [pc, #200]	; (c0d01120 <io_usb_hid_exchange+0xe0>)
c0d01058:	4832      	ldr	r0, [pc, #200]	; (c0d01124 <io_usb_hid_exchange+0xe4>)
c0d0105a:	6008      	str	r0, [r1, #0]
c0d0105c:	4f32      	ldr	r7, [pc, #200]	; (c0d01128 <io_usb_hid_exchange+0xe8>)
}

void os_memset(void * dst, unsigned char c, unsigned int length) {
#define DSTCHAR ((unsigned char *)dst)
  while(length--) {
    DSTCHAR[length] = c;
c0d0105e:	1d78      	adds	r0, r7, #5
c0d01060:	2539      	movs	r5, #57	; 0x39
c0d01062:	9002      	str	r0, [sp, #8]
c0d01064:	4629      	mov	r1, r5
c0d01066:	f001 fdc1 	bl	c0d02bec <__aeabi_memclr>
c0d0106a:	4830      	ldr	r0, [pc, #192]	; (c0d0112c <io_usb_hid_exchange+0xec>)
c0d0106c:	4601      	mov	r1, r0

    // fill the chunk
    os_memset(G_io_usb_ep_buffer, 0, IO_HID_EP_LENGTH-2);

    // keep the channel identifier
    G_io_usb_ep_buffer[0] = (G_io_usb_hid_channel>>8)&0xFF;
c0d0106e:	6808      	ldr	r0, [r1, #0]
c0d01070:	0a00      	lsrs	r0, r0, #8
c0d01072:	7038      	strb	r0, [r7, #0]
    G_io_usb_ep_buffer[1] = G_io_usb_hid_channel&0xFF;
c0d01074:	6808      	ldr	r0, [r1, #0]
c0d01076:	7078      	strb	r0, [r7, #1]
c0d01078:	2005      	movs	r0, #5
    G_io_usb_ep_buffer[2] = 0x05;
c0d0107a:	70b8      	strb	r0, [r7, #2]
    G_io_usb_ep_buffer[3] = G_io_usb_hid_sequence_number>>8;
c0d0107c:	6820      	ldr	r0, [r4, #0]
c0d0107e:	0a00      	lsrs	r0, r0, #8
c0d01080:	70f8      	strb	r0, [r7, #3]
    G_io_usb_ep_buffer[4] = G_io_usb_hid_sequence_number;
c0d01082:	6820      	ldr	r0, [r4, #0]
c0d01084:	7138      	strb	r0, [r7, #4]
c0d01086:	b2b1      	uxth	r1, r6

    if (G_io_usb_hid_sequence_number == 0) {
c0d01088:	6820      	ldr	r0, [r4, #0]
c0d0108a:	2800      	cmp	r0, #0
c0d0108c:	9104      	str	r1, [sp, #16]
c0d0108e:	d00a      	beq.n	c0d010a6 <io_usb_hid_exchange+0x66>
      G_io_usb_hid_current_buffer += l;
      sndlength -= l;
      l += 7;
    }
    else {
      l = ((sndlength>IO_HID_EP_LENGTH-5) ? IO_HID_EP_LENGTH-5 : sndlength);
c0d01090:	203b      	movs	r0, #59	; 0x3b
c0d01092:	293b      	cmp	r1, #59	; 0x3b
c0d01094:	460e      	mov	r6, r1
c0d01096:	d300      	bcc.n	c0d0109a <io_usb_hid_exchange+0x5a>
c0d01098:	4606      	mov	r6, r0
c0d0109a:	4821      	ldr	r0, [pc, #132]	; (c0d01120 <io_usb_hid_exchange+0xe0>)
c0d0109c:	4602      	mov	r2, r0
      os_memmove(G_io_usb_ep_buffer+5, (const void*)G_io_usb_hid_current_buffer, l);
c0d0109e:	6811      	ldr	r1, [r2, #0]
c0d010a0:	9802      	ldr	r0, [sp, #8]
c0d010a2:	4615      	mov	r5, r2
c0d010a4:	e009      	b.n	c0d010ba <io_usb_hid_exchange+0x7a>
    G_io_usb_ep_buffer[3] = G_io_usb_hid_sequence_number>>8;
    G_io_usb_ep_buffer[4] = G_io_usb_hid_sequence_number;

    if (G_io_usb_hid_sequence_number == 0) {
      l = ((sndlength>IO_HID_EP_LENGTH-7) ? IO_HID_EP_LENGTH-7 : sndlength);
      G_io_usb_ep_buffer[5] = sndlength>>8;
c0d010a6:	0a30      	lsrs	r0, r6, #8
c0d010a8:	7178      	strb	r0, [r7, #5]
      G_io_usb_ep_buffer[6] = sndlength;
c0d010aa:	71be      	strb	r6, [r7, #6]
    G_io_usb_ep_buffer[2] = 0x05;
    G_io_usb_ep_buffer[3] = G_io_usb_hid_sequence_number>>8;
    G_io_usb_ep_buffer[4] = G_io_usb_hid_sequence_number;

    if (G_io_usb_hid_sequence_number == 0) {
      l = ((sndlength>IO_HID_EP_LENGTH-7) ? IO_HID_EP_LENGTH-7 : sndlength);
c0d010ac:	2939      	cmp	r1, #57	; 0x39
c0d010ae:	460e      	mov	r6, r1
c0d010b0:	d300      	bcc.n	c0d010b4 <io_usb_hid_exchange+0x74>
c0d010b2:	462e      	mov	r6, r5
c0d010b4:	4d1a      	ldr	r5, [pc, #104]	; (c0d01120 <io_usb_hid_exchange+0xe0>)
      G_io_usb_ep_buffer[5] = sndlength>>8;
      G_io_usb_ep_buffer[6] = sndlength;
      os_memmove(G_io_usb_ep_buffer+7, (const void*)G_io_usb_hid_current_buffer, l);
c0d010b6:	6829      	ldr	r1, [r5, #0]
c0d010b8:	1df8      	adds	r0, r7, #7
c0d010ba:	4632      	mov	r2, r6
c0d010bc:	f7ff ff9f 	bl	c0d00ffe <os_memmove>
c0d010c0:	4c16      	ldr	r4, [pc, #88]	; (c0d0111c <io_usb_hid_exchange+0xdc>)
c0d010c2:	6828      	ldr	r0, [r5, #0]
c0d010c4:	1980      	adds	r0, r0, r6
      G_io_usb_hid_current_buffer += l;
c0d010c6:	6028      	str	r0, [r5, #0]
      G_io_usb_hid_current_buffer += l;
      sndlength -= l;
      l += 5;
    }
    // prepare next chunk numbering
    G_io_usb_hid_sequence_number++;
c0d010c8:	6820      	ldr	r0, [r4, #0]
c0d010ca:	1c40      	adds	r0, r0, #1
c0d010cc:	6020      	str	r0, [r4, #0]
    // send the chunk
    // always pad :)
    sndfct(G_io_usb_ep_buffer, sizeof(G_io_usb_ep_buffer));
c0d010ce:	2140      	movs	r1, #64	; 0x40
c0d010d0:	4638      	mov	r0, r7
c0d010d2:	9a03      	ldr	r2, [sp, #12]
c0d010d4:	4790      	blx	r2
c0d010d6:	9804      	ldr	r0, [sp, #16]
c0d010d8:	1b86      	subs	r6, r0, r6
c0d010da:	4815      	ldr	r0, [pc, #84]	; (c0d01130 <io_usb_hid_exchange+0xf0>)
  // perform send
  if (sndlength) {
    G_io_usb_hid_sequence_number = 0; 
    G_io_usb_hid_current_buffer = G_io_apdu_buffer;
  }
  while(sndlength) {
c0d010dc:	4206      	tst	r6, r0
c0d010de:	d1be      	bne.n	c0d0105e <io_usb_hid_exchange+0x1e>
  io_usb_hid_init();
  return IO_USB_APDU_RESET;
}

void io_usb_hid_init(void) {
  G_io_usb_hid_sequence_number = 0; 
c0d010e0:	480e      	ldr	r0, [pc, #56]	; (c0d0111c <io_usb_hid_exchange+0xdc>)
c0d010e2:	2400      	movs	r4, #0
c0d010e4:	6004      	str	r4, [r0, #0]
  }

  // prepare for next apdu
  io_usb_hid_init();

  if (flags & IO_RESET_AFTER_REPLIED) {
c0d010e6:	2080      	movs	r0, #128	; 0x80
c0d010e8:	9d01      	ldr	r5, [sp, #4]
c0d010ea:	4205      	tst	r5, r0
c0d010ec:	d001      	beq.n	c0d010f2 <io_usb_hid_exchange+0xb2>
    reset();
c0d010ee:	f000 fcab 	bl	c0d01a48 <reset>
  }

  if (flags & IO_RETURN_AFTER_TX ) {
c0d010f2:	06a8      	lsls	r0, r5, #26
c0d010f4:	d40f      	bmi.n	c0d01116 <io_usb_hid_exchange+0xd6>
c0d010f6:	4c0c      	ldr	r4, [pc, #48]	; (c0d01128 <io_usb_hid_exchange+0xe8>)
c0d010f8:	9d00      	ldr	r5, [sp, #0]
  }

  // receive the next command
  for(;;) {
    // receive a hid chunk
    l = rcvfct(G_io_usb_ep_buffer, sizeof(G_io_usb_ep_buffer));
c0d010fa:	2140      	movs	r1, #64	; 0x40
c0d010fc:	4620      	mov	r0, r4
c0d010fe:	47a8      	blx	r5
    // check for wrongly sized tlvs
    if (l > sizeof(G_io_usb_ep_buffer)) {
c0d01100:	b2c2      	uxtb	r2, r0
c0d01102:	2a40      	cmp	r2, #64	; 0x40
c0d01104:	d8f9      	bhi.n	c0d010fa <io_usb_hid_exchange+0xba>
      continue;
    }

    // call the chunk reception
    switch(io_usb_hid_receive(sndfct, G_io_usb_ep_buffer, l)) {
c0d01106:	9803      	ldr	r0, [sp, #12]
c0d01108:	4621      	mov	r1, r4
c0d0110a:	f7ff fec5 	bl	c0d00e98 <io_usb_hid_receive>
c0d0110e:	2802      	cmp	r0, #2
c0d01110:	d1f3      	bne.n	c0d010fa <io_usb_hid_exchange+0xba>
      default:
        continue;

      case IO_USB_APDU_RECEIVED:

        return G_io_usb_hid_total_length;
c0d01112:	4808      	ldr	r0, [pc, #32]	; (c0d01134 <io_usb_hid_exchange+0xf4>)
c0d01114:	6804      	ldr	r4, [r0, #0]
    }
  }
}
c0d01116:	b2a0      	uxth	r0, r4
c0d01118:	b005      	add	sp, #20
c0d0111a:	bdf0      	pop	{r4, r5, r6, r7, pc}
c0d0111c:	20001b2c 	.word	0x20001b2c
c0d01120:	20001c3c 	.word	0x20001c3c
c0d01124:	20001b38 	.word	0x20001b38
c0d01128:	20001c68 	.word	0x20001c68
c0d0112c:	20001c40 	.word	0x20001c40
c0d01130:	0000ffff 	.word	0x0000ffff
c0d01134:	20001b30 	.word	0x20001b30

c0d01138 <os_longjmp>:
void try_context_set(try_context_t* ctx) {
  __asm volatile ("mov r9, %0"::"r"(ctx));
}

#ifndef HAVE_BOLOS
void os_longjmp(unsigned int exception) {
c0d01138:	b580      	push	{r7, lr}
c0d0113a:	4601      	mov	r1, r0
  return xoracc;
}

try_context_t* try_context_get(void) {
  try_context_t* current_ctx;
  __asm volatile ("mov %0, r9":"=r"(current_ctx));
c0d0113c:	4648      	mov	r0, r9
  __asm volatile ("mov r9, %0"::"r"(ctx));
}

#ifndef HAVE_BOLOS
void os_longjmp(unsigned int exception) {
  longjmp(try_context_get()->jmp_buf, exception);
c0d0113e:	f001 fdb5 	bl	c0d02cac <longjmp>

c0d01142 <try_context_get>:
  return xoracc;
}

try_context_t* try_context_get(void) {
  try_context_t* current_ctx;
  __asm volatile ("mov %0, r9":"=r"(current_ctx));
c0d01142:	4648      	mov	r0, r9
  return current_ctx;
c0d01144:	4770      	bx	lr

c0d01146 <try_context_get_previous>:
}

try_context_t* try_context_get_previous(void) {
c0d01146:	2000      	movs	r0, #0
  try_context_t* current_ctx;
  __asm volatile ("mov %0, r9":"=r"(current_ctx));
c0d01148:	4649      	mov	r1, r9

  // first context reached ?
  if (current_ctx == NULL) {
c0d0114a:	2900      	cmp	r1, #0
c0d0114c:	d000      	beq.n	c0d01150 <try_context_get_previous+0xa>
  }

  // return r9 content saved on the current context. It links to the previous context.
  // r4 r5 r6 r7 r8 r9 r10 r11 sp lr
  //                ^ platform register
  return (try_context_t*) current_ctx->jmp_buf[5];
c0d0114e:	6948      	ldr	r0, [r1, #20]
}
c0d01150:	4770      	bx	lr
	...

c0d01154 <io_seproxyhal_general_status>:
  if (G_io_timeout) {
    G_io_timeout = timeout_ms;
  }
}

void io_seproxyhal_general_status(void) {
c0d01154:	b580      	push	{r7, lr}
  // avoid troubles
  if (io_seproxyhal_spi_is_status_sent()) {
c0d01156:	f000 fdab 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d0115a:	2800      	cmp	r0, #0
c0d0115c:	d10b      	bne.n	c0d01176 <io_seproxyhal_general_status+0x22>
    return;
  }
  // send the general status
  G_io_seproxyhal_spi_buffer[0] = SEPROXYHAL_TAG_GENERAL_STATUS;
c0d0115e:	4806      	ldr	r0, [pc, #24]	; (c0d01178 <io_seproxyhal_general_status+0x24>)
c0d01160:	2160      	movs	r1, #96	; 0x60
c0d01162:	7001      	strb	r1, [r0, #0]
  G_io_seproxyhal_spi_buffer[1] = 0;
c0d01164:	2100      	movs	r1, #0
c0d01166:	7041      	strb	r1, [r0, #1]
  G_io_seproxyhal_spi_buffer[2] = 2;
c0d01168:	2202      	movs	r2, #2
c0d0116a:	7082      	strb	r2, [r0, #2]
  G_io_seproxyhal_spi_buffer[3] = SEPROXYHAL_TAG_GENERAL_STATUS_LAST_COMMAND>>8;
c0d0116c:	70c1      	strb	r1, [r0, #3]
  G_io_seproxyhal_spi_buffer[4] = SEPROXYHAL_TAG_GENERAL_STATUS_LAST_COMMAND;
c0d0116e:	7101      	strb	r1, [r0, #4]
  io_seproxyhal_spi_send(G_io_seproxyhal_spi_buffer, 5);
c0d01170:	2105      	movs	r1, #5
c0d01172:	f000 fd87 	bl	c0d01c84 <io_seproxyhal_spi_send>
}
c0d01176:	bd80      	pop	{r7, pc}
c0d01178:	20001804 	.word	0x20001804

c0d0117c <io_seproxyhal_handle_usb_event>:
static volatile unsigned char G_io_usb_ep_xfer_len[IO_USB_MAX_ENDPOINTS];
#include "usbd_def.h"
#include "usbd_core.h"
extern USBD_HandleTypeDef USBD_Device;

void io_seproxyhal_handle_usb_event(void) {
c0d0117c:	b510      	push	{r4, lr}
  switch(G_io_seproxyhal_spi_buffer[3]) {
c0d0117e:	4813      	ldr	r0, [pc, #76]	; (c0d011cc <io_seproxyhal_handle_usb_event+0x50>)
c0d01180:	78c0      	ldrb	r0, [r0, #3]
c0d01182:	2803      	cmp	r0, #3
c0d01184:	dc07      	bgt.n	c0d01196 <io_seproxyhal_handle_usb_event+0x1a>
c0d01186:	2801      	cmp	r0, #1
c0d01188:	d00d      	beq.n	c0d011a6 <io_seproxyhal_handle_usb_event+0x2a>
c0d0118a:	2802      	cmp	r0, #2
c0d0118c:	d11d      	bne.n	c0d011ca <io_seproxyhal_handle_usb_event+0x4e>
      if (G_io_apdu_media != IO_APDU_MEDIA_NONE) {
        THROW(EXCEPTION_IO_RESET);
      }
      break;
    case SEPROXYHAL_TAG_USB_EVENT_SOF:
      USBD_LL_SOF(&USBD_Device);
c0d0118e:	4810      	ldr	r0, [pc, #64]	; (c0d011d0 <io_seproxyhal_handle_usb_event+0x54>)
c0d01190:	f001 f891 	bl	c0d022b6 <USBD_LL_SOF>
      break;
    case SEPROXYHAL_TAG_USB_EVENT_RESUMED:
      USBD_LL_Resume(&USBD_Device);
      break;
  }
}
c0d01194:	bd10      	pop	{r4, pc}
c0d01196:	2804      	cmp	r0, #4
c0d01198:	d014      	beq.n	c0d011c4 <io_seproxyhal_handle_usb_event+0x48>
c0d0119a:	2808      	cmp	r0, #8
c0d0119c:	d115      	bne.n	c0d011ca <io_seproxyhal_handle_usb_event+0x4e>
      break;
    case SEPROXYHAL_TAG_USB_EVENT_SUSPENDED:
      USBD_LL_Suspend(&USBD_Device);
      break;
    case SEPROXYHAL_TAG_USB_EVENT_RESUMED:
      USBD_LL_Resume(&USBD_Device);
c0d0119e:	480c      	ldr	r0, [pc, #48]	; (c0d011d0 <io_seproxyhal_handle_usb_event+0x54>)
c0d011a0:	f001 f887 	bl	c0d022b2 <USBD_LL_Resume>
      break;
  }
}
c0d011a4:	bd10      	pop	{r4, pc}
extern USBD_HandleTypeDef USBD_Device;

void io_seproxyhal_handle_usb_event(void) {
  switch(G_io_seproxyhal_spi_buffer[3]) {
    case SEPROXYHAL_TAG_USB_EVENT_RESET:
      USBD_LL_SetSpeed(&USBD_Device, USBD_SPEED_FULL);  
c0d011a6:	4c0a      	ldr	r4, [pc, #40]	; (c0d011d0 <io_seproxyhal_handle_usb_event+0x54>)
c0d011a8:	2101      	movs	r1, #1
c0d011aa:	4620      	mov	r0, r4
c0d011ac:	f001 f87c 	bl	c0d022a8 <USBD_LL_SetSpeed>
      USBD_LL_Reset(&USBD_Device);
c0d011b0:	4620      	mov	r0, r4
c0d011b2:	f001 f858 	bl	c0d02266 <USBD_LL_Reset>
      // ongoing APDU detected, throw a reset, even if not the media. to avoid potential troubles.
      if (G_io_apdu_media != IO_APDU_MEDIA_NONE) {
c0d011b6:	4807      	ldr	r0, [pc, #28]	; (c0d011d4 <io_seproxyhal_handle_usb_event+0x58>)
c0d011b8:	7800      	ldrb	r0, [r0, #0]
c0d011ba:	2800      	cmp	r0, #0
c0d011bc:	d005      	beq.n	c0d011ca <io_seproxyhal_handle_usb_event+0x4e>
        THROW(EXCEPTION_IO_RESET);
c0d011be:	2010      	movs	r0, #16
c0d011c0:	f7ff ffba 	bl	c0d01138 <os_longjmp>
      break;
    case SEPROXYHAL_TAG_USB_EVENT_SOF:
      USBD_LL_SOF(&USBD_Device);
      break;
    case SEPROXYHAL_TAG_USB_EVENT_SUSPENDED:
      USBD_LL_Suspend(&USBD_Device);
c0d011c4:	4802      	ldr	r0, [pc, #8]	; (c0d011d0 <io_seproxyhal_handle_usb_event+0x54>)
c0d011c6:	f001 f872 	bl	c0d022ae <USBD_LL_Suspend>
      break;
    case SEPROXYHAL_TAG_USB_EVENT_RESUMED:
      USBD_LL_Resume(&USBD_Device);
      break;
  }
}
c0d011ca:	bd10      	pop	{r4, pc}
c0d011cc:	20001804 	.word	0x20001804
c0d011d0:	20001cb0 	.word	0x20001cb0
c0d011d4:	20001c4c 	.word	0x20001c4c

c0d011d8 <io_seproxyhal_get_ep_rx_size>:

uint16_t io_seproxyhal_get_ep_rx_size(uint8_t epnum) {
  return G_io_usb_ep_xfer_len[epnum&0x7F];
c0d011d8:	217f      	movs	r1, #127	; 0x7f
c0d011da:	4001      	ands	r1, r0
c0d011dc:	4801      	ldr	r0, [pc, #4]	; (c0d011e4 <io_seproxyhal_get_ep_rx_size+0xc>)
c0d011de:	5c40      	ldrb	r0, [r0, r1]
c0d011e0:	4770      	bx	lr
c0d011e2:	46c0      	nop			; (mov r8, r8)
c0d011e4:	20001c4d 	.word	0x20001c4d

c0d011e8 <io_seproxyhal_handle_usb_ep_xfer_event>:
}

void io_seproxyhal_handle_usb_ep_xfer_event(void) {
c0d011e8:	b580      	push	{r7, lr}
  switch(G_io_seproxyhal_spi_buffer[4]) {
c0d011ea:	4810      	ldr	r0, [pc, #64]	; (c0d0122c <io_seproxyhal_handle_usb_ep_xfer_event+0x44>)
c0d011ec:	7901      	ldrb	r1, [r0, #4]
c0d011ee:	2904      	cmp	r1, #4
c0d011f0:	d008      	beq.n	c0d01204 <io_seproxyhal_handle_usb_ep_xfer_event+0x1c>
c0d011f2:	2902      	cmp	r1, #2
c0d011f4:	d011      	beq.n	c0d0121a <io_seproxyhal_handle_usb_ep_xfer_event+0x32>
c0d011f6:	2901      	cmp	r1, #1
c0d011f8:	d10e      	bne.n	c0d01218 <io_seproxyhal_handle_usb_ep_xfer_event+0x30>
    /* This event is received when a new SETUP token had been received on a control endpoint */
    case SEPROXYHAL_TAG_USB_EP_XFER_SETUP:
      // assume length of setup packet, and that it is on endpoint 0
      USBD_LL_SetupStage(&USBD_Device, &G_io_seproxyhal_spi_buffer[6]);
c0d011fa:	1d81      	adds	r1, r0, #6
c0d011fc:	480d      	ldr	r0, [pc, #52]	; (c0d01234 <io_seproxyhal_handle_usb_ep_xfer_event+0x4c>)
c0d011fe:	f000 ff2b 	bl	c0d02058 <USBD_LL_SetupStage>
      // saved just in case it is needed ...
      G_io_usb_ep_xfer_len[G_io_seproxyhal_spi_buffer[3]&0x7F] = G_io_seproxyhal_spi_buffer[5];
      USBD_LL_DataOutStage(&USBD_Device, G_io_seproxyhal_spi_buffer[3]&0x7F, &G_io_seproxyhal_spi_buffer[6]);
      break;
  }
}
c0d01202:	bd80      	pop	{r7, pc}
      break;

    /* This event is received when a new DATA token is received on an endpoint */
    case SEPROXYHAL_TAG_USB_EP_XFER_OUT:
      // saved just in case it is needed ...
      G_io_usb_ep_xfer_len[G_io_seproxyhal_spi_buffer[3]&0x7F] = G_io_seproxyhal_spi_buffer[5];
c0d01204:	78c2      	ldrb	r2, [r0, #3]
c0d01206:	217f      	movs	r1, #127	; 0x7f
c0d01208:	4011      	ands	r1, r2
c0d0120a:	7942      	ldrb	r2, [r0, #5]
c0d0120c:	4b08      	ldr	r3, [pc, #32]	; (c0d01230 <io_seproxyhal_handle_usb_ep_xfer_event+0x48>)
c0d0120e:	545a      	strb	r2, [r3, r1]
      USBD_LL_DataOutStage(&USBD_Device, G_io_seproxyhal_spi_buffer[3]&0x7F, &G_io_seproxyhal_spi_buffer[6]);
c0d01210:	1d82      	adds	r2, r0, #6
c0d01212:	4808      	ldr	r0, [pc, #32]	; (c0d01234 <io_seproxyhal_handle_usb_ep_xfer_event+0x4c>)
c0d01214:	f000 ff4f 	bl	c0d020b6 <USBD_LL_DataOutStage>
      break;
  }
}
c0d01218:	bd80      	pop	{r7, pc}
      USBD_LL_SetupStage(&USBD_Device, &G_io_seproxyhal_spi_buffer[6]);
      break;

    /* This event is received after the prepare data packet has been flushed to the usb host */
    case SEPROXYHAL_TAG_USB_EP_XFER_IN:
      USBD_LL_DataInStage(&USBD_Device, G_io_seproxyhal_spi_buffer[3]&0x7F, &G_io_seproxyhal_spi_buffer[6]);
c0d0121a:	78c2      	ldrb	r2, [r0, #3]
c0d0121c:	217f      	movs	r1, #127	; 0x7f
c0d0121e:	4011      	ands	r1, r2
c0d01220:	1d82      	adds	r2, r0, #6
c0d01222:	4804      	ldr	r0, [pc, #16]	; (c0d01234 <io_seproxyhal_handle_usb_ep_xfer_event+0x4c>)
c0d01224:	f000 ffa6 	bl	c0d02174 <USBD_LL_DataInStage>
      // saved just in case it is needed ...
      G_io_usb_ep_xfer_len[G_io_seproxyhal_spi_buffer[3]&0x7F] = G_io_seproxyhal_spi_buffer[5];
      USBD_LL_DataOutStage(&USBD_Device, G_io_seproxyhal_spi_buffer[3]&0x7F, &G_io_seproxyhal_spi_buffer[6]);
      break;
  }
}
c0d01228:	bd80      	pop	{r7, pc}
c0d0122a:	46c0      	nop			; (mov r8, r8)
c0d0122c:	20001804 	.word	0x20001804
c0d01230:	20001c4d 	.word	0x20001c4d
c0d01234:	20001cb0 	.word	0x20001cb0

c0d01238 <io_usb_send_ep>:
}

#endif // HAVE_L4_USBLIB

// TODO, refactor this using the USB DataIn event like for the U2F tunnel
void io_usb_send_ep(unsigned int ep, unsigned char* buffer, unsigned short length, unsigned int timeout) {
c0d01238:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d0123a:	b081      	sub	sp, #4
c0d0123c:	4614      	mov	r4, r2
c0d0123e:	4605      	mov	r5, r0
  if (timeout) {
    timeout++;
  }

  // won't send if overflowing seproxyhal buffer format
  if (length > 255) {
c0d01240:	2cff      	cmp	r4, #255	; 0xff
c0d01242:	d83a      	bhi.n	c0d012ba <io_usb_send_ep+0x82>
    return;
  }
  
  G_io_seproxyhal_spi_buffer[0] = SEPROXYHAL_TAG_USB_EP_PREPARE;
c0d01244:	4e1f      	ldr	r6, [pc, #124]	; (c0d012c4 <io_usb_send_ep+0x8c>)
c0d01246:	2050      	movs	r0, #80	; 0x50
c0d01248:	7030      	strb	r0, [r6, #0]
  G_io_seproxyhal_spi_buffer[1] = (3+length)>>8;
c0d0124a:	1ce0      	adds	r0, r4, #3
c0d0124c:	9100      	str	r1, [sp, #0]
c0d0124e:	0a01      	lsrs	r1, r0, #8
c0d01250:	7071      	strb	r1, [r6, #1]
  G_io_seproxyhal_spi_buffer[2] = (3+length);
c0d01252:	70b0      	strb	r0, [r6, #2]
  G_io_seproxyhal_spi_buffer[3] = ep|0x80;
c0d01254:	2080      	movs	r0, #128	; 0x80
c0d01256:	4305      	orrs	r5, r0
c0d01258:	70f5      	strb	r5, [r6, #3]
  G_io_seproxyhal_spi_buffer[4] = SEPROXYHAL_TAG_USB_EP_PREPARE_DIR_IN;
c0d0125a:	2020      	movs	r0, #32
c0d0125c:	7130      	strb	r0, [r6, #4]
  G_io_seproxyhal_spi_buffer[5] = length;
c0d0125e:	7174      	strb	r4, [r6, #5]
  io_seproxyhal_spi_send(G_io_seproxyhal_spi_buffer, 6);
c0d01260:	2106      	movs	r1, #6
c0d01262:	4630      	mov	r0, r6
c0d01264:	461f      	mov	r7, r3
c0d01266:	f000 fd0d 	bl	c0d01c84 <io_seproxyhal_spi_send>
  io_seproxyhal_spi_send(buffer, length);
c0d0126a:	9800      	ldr	r0, [sp, #0]
c0d0126c:	4621      	mov	r1, r4
c0d0126e:	f000 fd09 	bl	c0d01c84 <io_seproxyhal_spi_send>

  // if timeout is requested
  if(timeout) {
c0d01272:	1c78      	adds	r0, r7, #1
c0d01274:	2802      	cmp	r0, #2
c0d01276:	d320      	bcc.n	c0d012ba <io_usb_send_ep+0x82>
c0d01278:	e006      	b.n	c0d01288 <io_usb_send_ep+0x50>
          THROW(EXCEPTION_IO_RESET);
        }
        */

        // link disconnected ?
        if(G_io_seproxyhal_spi_buffer[0] == SEPROXYHAL_TAG_STATUS_EVENT) {
c0d0127a:	2915      	cmp	r1, #21
c0d0127c:	d102      	bne.n	c0d01284 <io_usb_send_ep+0x4c>
          if (!(U4BE(G_io_seproxyhal_spi_buffer, 3) & SEPROXYHAL_TAG_STATUS_EVENT_FLAG_USB_POWERED)) {
c0d0127e:	79b0      	ldrb	r0, [r6, #6]
c0d01280:	0700      	lsls	r0, r0, #28
c0d01282:	d51c      	bpl.n	c0d012be <io_usb_send_ep+0x86>
        
        // usb reset ?
        //io_seproxyhal_handle_usb_event();
        // also process other transfer requests if any (useful for HID keyboard while playing with CAPS lock key, side effect on LED status)
        // also handle IO timeout in a centralized and configurable way
        io_seproxyhal_handle_event();
c0d01284:	f000 f820 	bl	c0d012c8 <io_seproxyhal_handle_event>
  io_seproxyhal_spi_send(buffer, length);

  // if timeout is requested
  if(timeout) {
    for (;;) {
      if (!io_seproxyhal_spi_is_status_sent()) {
c0d01288:	f000 fd12 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d0128c:	2800      	cmp	r0, #0
c0d0128e:	d101      	bne.n	c0d01294 <io_usb_send_ep+0x5c>
        io_seproxyhal_general_status();
c0d01290:	f7ff ff60 	bl	c0d01154 <io_seproxyhal_general_status>
      }

      rx_len = io_seproxyhal_spi_recv(G_io_seproxyhal_spi_buffer, sizeof(G_io_seproxyhal_spi_buffer), 0);
c0d01294:	2180      	movs	r1, #128	; 0x80
c0d01296:	2200      	movs	r2, #0
c0d01298:	4630      	mov	r0, r6
c0d0129a:	f000 fd1f 	bl	c0d01cdc <io_seproxyhal_spi_recv>

      // wait for ack of the seproxyhal
      // discard if not an acknowledgment
      if (G_io_seproxyhal_spi_buffer[0] != SEPROXYHAL_TAG_USB_EP_XFER_EVENT
c0d0129e:	7831      	ldrb	r1, [r6, #0]
        || rx_len != 6 
c0d012a0:	2806      	cmp	r0, #6
c0d012a2:	d1ea      	bne.n	c0d0127a <io_usb_send_ep+0x42>
c0d012a4:	2910      	cmp	r1, #16
c0d012a6:	d1e8      	bne.n	c0d0127a <io_usb_send_ep+0x42>
        || G_io_seproxyhal_spi_buffer[3] != (ep|0x80)
c0d012a8:	78f0      	ldrb	r0, [r6, #3]
        || G_io_seproxyhal_spi_buffer[4] != SEPROXYHAL_TAG_USB_EP_XFER_IN
c0d012aa:	42a8      	cmp	r0, r5
c0d012ac:	d1e5      	bne.n	c0d0127a <io_usb_send_ep+0x42>
c0d012ae:	7930      	ldrb	r0, [r6, #4]
c0d012b0:	2802      	cmp	r0, #2
c0d012b2:	d1e2      	bne.n	c0d0127a <io_usb_send_ep+0x42>
        || G_io_seproxyhal_spi_buffer[5] != length) {
c0d012b4:	7970      	ldrb	r0, [r6, #5]

      rx_len = io_seproxyhal_spi_recv(G_io_seproxyhal_spi_buffer, sizeof(G_io_seproxyhal_spi_buffer), 0);

      // wait for ack of the seproxyhal
      // discard if not an acknowledgment
      if (G_io_seproxyhal_spi_buffer[0] != SEPROXYHAL_TAG_USB_EP_XFER_EVENT
c0d012b6:	42a0      	cmp	r0, r4
c0d012b8:	d1df      	bne.n	c0d0127a <io_usb_send_ep+0x42>

      // chunk sending succeeded
      break;
    }
  }
}
c0d012ba:	b001      	add	sp, #4
c0d012bc:	bdf0      	pop	{r4, r5, r6, r7, pc}
        */

        // link disconnected ?
        if(G_io_seproxyhal_spi_buffer[0] == SEPROXYHAL_TAG_STATUS_EVENT) {
          if (!(U4BE(G_io_seproxyhal_spi_buffer, 3) & SEPROXYHAL_TAG_STATUS_EVENT_FLAG_USB_POWERED)) {
           THROW(EXCEPTION_IO_RESET);
c0d012be:	2010      	movs	r0, #16
c0d012c0:	f7ff ff3a 	bl	c0d01138 <os_longjmp>
c0d012c4:	20001804 	.word	0x20001804

c0d012c8 <io_seproxyhal_handle_event>:
    // copy apdu to apdu buffer
    os_memmove(G_io_apdu_buffer, G_io_seproxyhal_spi_buffer+3, G_io_apdu_length);
  }
}

unsigned int io_seproxyhal_handle_event(void) {
c0d012c8:	b580      	push	{r7, lr}
  unsigned int rx_len = U2BE(G_io_seproxyhal_spi_buffer, 1);
c0d012ca:	481e      	ldr	r0, [pc, #120]	; (c0d01344 <io_seproxyhal_handle_event+0x7c>)
c0d012cc:	7882      	ldrb	r2, [r0, #2]
c0d012ce:	7841      	ldrb	r1, [r0, #1]
c0d012d0:	0209      	lsls	r1, r1, #8
c0d012d2:	4311      	orrs	r1, r2
c0d012d4:	7800      	ldrb	r0, [r0, #0]

  switch(G_io_seproxyhal_spi_buffer[0]) {
c0d012d6:	280f      	cmp	r0, #15
c0d012d8:	dc0a      	bgt.n	c0d012f0 <io_seproxyhal_handle_event+0x28>
c0d012da:	280e      	cmp	r0, #14
c0d012dc:	d010      	beq.n	c0d01300 <io_seproxyhal_handle_event+0x38>
c0d012de:	280f      	cmp	r0, #15
c0d012e0:	d11d      	bne.n	c0d0131e <io_seproxyhal_handle_event+0x56>
c0d012e2:	2000      	movs	r0, #0
  #ifdef HAVE_IO_USB
    case SEPROXYHAL_TAG_USB_EVENT:
      if (rx_len != 3+1) {
c0d012e4:	2904      	cmp	r1, #4
c0d012e6:	d121      	bne.n	c0d0132c <io_seproxyhal_handle_event+0x64>
        return 0;
      }
      io_seproxyhal_handle_usb_event();
c0d012e8:	f7ff ff48 	bl	c0d0117c <io_seproxyhal_handle_usb_event>
c0d012ec:	2001      	movs	r0, #1
    default:
      return io_event(CHANNEL_SPI);
  }
  // defaulty return as not processed
  return 0;
}
c0d012ee:	bd80      	pop	{r7, pc}
c0d012f0:	2810      	cmp	r0, #16
c0d012f2:	d018      	beq.n	c0d01326 <io_seproxyhal_handle_event+0x5e>
c0d012f4:	2816      	cmp	r0, #22
c0d012f6:	d112      	bne.n	c0d0131e <io_seproxyhal_handle_event+0x56>
      io_seproxyhal_handle_bluenrg_event();
      return 1;
  #endif // HAVE_BLE

    case SEPROXYHAL_TAG_CAPDU_EVENT:
      io_seproxyhal_handle_capdu_event();
c0d012f8:	f000 f832 	bl	c0d01360 <io_seproxyhal_handle_capdu_event>
c0d012fc:	2001      	movs	r0, #1
    default:
      return io_event(CHANNEL_SPI);
  }
  // defaulty return as not processed
  return 0;
}
c0d012fe:	bd80      	pop	{r7, pc}
      return 1;

      // ask the user if not processed here
    case SEPROXYHAL_TAG_TICKER_EVENT:
      // process ticker events to timeout the IO transfers, and forward to the user io_event function too
      if(G_io_timeout) {
c0d01300:	4811      	ldr	r0, [pc, #68]	; (c0d01348 <io_seproxyhal_handle_event+0x80>)
c0d01302:	6801      	ldr	r1, [r0, #0]
c0d01304:	2900      	cmp	r1, #0
c0d01306:	d00a      	beq.n	c0d0131e <io_seproxyhal_handle_event+0x56>
        G_io_timeout-=MIN(G_io_timeout, 100);
c0d01308:	6802      	ldr	r2, [r0, #0]
c0d0130a:	2164      	movs	r1, #100	; 0x64
c0d0130c:	2a63      	cmp	r2, #99	; 0x63
c0d0130e:	d800      	bhi.n	c0d01312 <io_seproxyhal_handle_event+0x4a>
c0d01310:	6801      	ldr	r1, [r0, #0]
c0d01312:	6802      	ldr	r2, [r0, #0]
c0d01314:	1a51      	subs	r1, r2, r1
c0d01316:	6001      	str	r1, [r0, #0]
        #warning TODO use real ticker event interval here instead of the x100ms multiplier
        if (!G_io_timeout) {
c0d01318:	6800      	ldr	r0, [r0, #0]
c0d0131a:	2800      	cmp	r0, #0
c0d0131c:	d00b      	beq.n	c0d01336 <io_seproxyhal_handle_event+0x6e>
          G_io_apdu_state = APDU_IDLE;
          THROW(EXCEPTION_IO_RESET);
        }
      }
    default:
      return io_event(CHANNEL_SPI);
c0d0131e:	2002      	movs	r0, #2
c0d01320:	f7fe fee4 	bl	c0d000ec <io_event>
  }
  // defaulty return as not processed
  return 0;
}
c0d01324:	bd80      	pop	{r7, pc}
c0d01326:	2000      	movs	r0, #0
      }
      io_seproxyhal_handle_usb_event();
      return 1;

    case SEPROXYHAL_TAG_USB_EP_XFER_EVENT:
      if (rx_len < 3+3) {
c0d01328:	2906      	cmp	r1, #6
c0d0132a:	d200      	bcs.n	c0d0132e <io_seproxyhal_handle_event+0x66>
    default:
      return io_event(CHANNEL_SPI);
  }
  // defaulty return as not processed
  return 0;
}
c0d0132c:	bd80      	pop	{r7, pc}
    case SEPROXYHAL_TAG_USB_EP_XFER_EVENT:
      if (rx_len < 3+3) {
        // error !
        return 0;
      }
      io_seproxyhal_handle_usb_ep_xfer_event();
c0d0132e:	f7ff ff5b 	bl	c0d011e8 <io_seproxyhal_handle_usb_ep_xfer_event>
c0d01332:	2001      	movs	r0, #1
    default:
      return io_event(CHANNEL_SPI);
  }
  // defaulty return as not processed
  return 0;
}
c0d01334:	bd80      	pop	{r7, pc}
      if(G_io_timeout) {
        G_io_timeout-=MIN(G_io_timeout, 100);
        #warning TODO use real ticker event interval here instead of the x100ms multiplier
        if (!G_io_timeout) {
          // timeout !
          G_io_apdu_state = APDU_IDLE;
c0d01336:	4805      	ldr	r0, [pc, #20]	; (c0d0134c <io_seproxyhal_handle_event+0x84>)
c0d01338:	2100      	movs	r1, #0
c0d0133a:	7001      	strb	r1, [r0, #0]
          THROW(EXCEPTION_IO_RESET);
c0d0133c:	2010      	movs	r0, #16
c0d0133e:	f7ff fefb 	bl	c0d01138 <os_longjmp>
c0d01342:	46c0      	nop			; (mov r8, r8)
c0d01344:	20001804 	.word	0x20001804
c0d01348:	20001c48 	.word	0x20001c48
c0d0134c:	20001c54 	.word	0x20001c54

c0d01350 <io_usb_send_apdu_data>:
      break;
    }
  }
}

void io_usb_send_apdu_data(unsigned char* buffer, unsigned short length) {
c0d01350:	b580      	push	{r7, lr}
c0d01352:	460a      	mov	r2, r1
c0d01354:	4601      	mov	r1, r0
  // wait for 20 events before hanging up and timeout (~2 seconds of timeout)
  io_usb_send_ep(0x82, buffer, length, 20);
c0d01356:	2082      	movs	r0, #130	; 0x82
c0d01358:	2314      	movs	r3, #20
c0d0135a:	f7ff ff6d 	bl	c0d01238 <io_usb_send_ep>
}
c0d0135e:	bd80      	pop	{r7, pc}

c0d01360 <io_seproxyhal_handle_capdu_event>:

}
#endif


void io_seproxyhal_handle_capdu_event(void) {
c0d01360:	b580      	push	{r7, lr}
  if(G_io_apdu_state == APDU_IDLE) 
c0d01362:	480b      	ldr	r0, [pc, #44]	; (c0d01390 <io_seproxyhal_handle_capdu_event+0x30>)
c0d01364:	7801      	ldrb	r1, [r0, #0]
c0d01366:	2900      	cmp	r1, #0
c0d01368:	d110      	bne.n	c0d0138c <io_seproxyhal_handle_capdu_event+0x2c>
  {
    G_io_apdu_media = IO_APDU_MEDIA_RAW; // for application code
c0d0136a:	490a      	ldr	r1, [pc, #40]	; (c0d01394 <io_seproxyhal_handle_capdu_event+0x34>)
c0d0136c:	2205      	movs	r2, #5
c0d0136e:	700a      	strb	r2, [r1, #0]
    G_io_apdu_state = APDU_RAW; // for next call to io_exchange
c0d01370:	210a      	movs	r1, #10
c0d01372:	7001      	strb	r1, [r0, #0]
    G_io_apdu_length = U2BE(G_io_seproxyhal_spi_buffer, 1);
c0d01374:	4808      	ldr	r0, [pc, #32]	; (c0d01398 <io_seproxyhal_handle_capdu_event+0x38>)
c0d01376:	7881      	ldrb	r1, [r0, #2]
c0d01378:	7842      	ldrb	r2, [r0, #1]
c0d0137a:	0212      	lsls	r2, r2, #8
c0d0137c:	430a      	orrs	r2, r1
c0d0137e:	4907      	ldr	r1, [pc, #28]	; (c0d0139c <io_seproxyhal_handle_capdu_event+0x3c>)
c0d01380:	800a      	strh	r2, [r1, #0]
    // copy apdu to apdu buffer
    os_memmove(G_io_apdu_buffer, G_io_seproxyhal_spi_buffer+3, G_io_apdu_length);
c0d01382:	880a      	ldrh	r2, [r1, #0]
c0d01384:	1cc1      	adds	r1, r0, #3
c0d01386:	4806      	ldr	r0, [pc, #24]	; (c0d013a0 <io_seproxyhal_handle_capdu_event+0x40>)
c0d01388:	f7ff fe39 	bl	c0d00ffe <os_memmove>
  }
}
c0d0138c:	bd80      	pop	{r7, pc}
c0d0138e:	46c0      	nop			; (mov r8, r8)
c0d01390:	20001c54 	.word	0x20001c54
c0d01394:	20001c4c 	.word	0x20001c4c
c0d01398:	20001804 	.word	0x20001804
c0d0139c:	20001c56 	.word	0x20001c56
c0d013a0:	20001b38 	.word	0x20001b38

c0d013a4 <io_seproxyhal_init>:
#ifdef HAVE_BOLOS_APP_STACK_CANARY
#define APP_STACK_CANARY_MAGIC 0xDEAD0031
extern unsigned int app_stack_canary;
#endif // HAVE_BOLOS_APP_STACK_CANARY

void io_seproxyhal_init(void) {
c0d013a4:	b510      	push	{r4, lr}
  // Enforce OS compatibility
  check_api_level(CX_COMPAT_APILEVEL);
c0d013a6:	2008      	movs	r0, #8
c0d013a8:	f000 fb38 	bl	c0d01a1c <check_api_level>

#ifdef HAVE_BOLOS_APP_STACK_CANARY
  app_stack_canary = APP_STACK_CANARY_MAGIC;
#endif // HAVE_BOLOS_APP_STACK_CANARY  

  G_io_apdu_state = APDU_IDLE;
c0d013ac:	480a      	ldr	r0, [pc, #40]	; (c0d013d8 <io_seproxyhal_init+0x34>)
c0d013ae:	2400      	movs	r4, #0
c0d013b0:	7004      	strb	r4, [r0, #0]
  G_io_apdu_offset = 0;
c0d013b2:	480a      	ldr	r0, [pc, #40]	; (c0d013dc <io_seproxyhal_init+0x38>)
c0d013b4:	8004      	strh	r4, [r0, #0]
  G_io_apdu_length = 0;
c0d013b6:	480a      	ldr	r0, [pc, #40]	; (c0d013e0 <io_seproxyhal_init+0x3c>)
c0d013b8:	8004      	strh	r4, [r0, #0]
  G_io_apdu_seq = 0;
c0d013ba:	480a      	ldr	r0, [pc, #40]	; (c0d013e4 <io_seproxyhal_init+0x40>)
c0d013bc:	8004      	strh	r4, [r0, #0]
  G_io_apdu_media = IO_APDU_MEDIA_NONE;
c0d013be:	480a      	ldr	r0, [pc, #40]	; (c0d013e8 <io_seproxyhal_init+0x44>)
c0d013c0:	7004      	strb	r4, [r0, #0]
  G_io_timeout_limit = NO_TIMEOUT;
c0d013c2:	480a      	ldr	r0, [pc, #40]	; (c0d013ec <io_seproxyhal_init+0x48>)
c0d013c4:	6004      	str	r4, [r0, #0]
  debug_apdus_offset = 0;
  #endif // DEBUG_APDU


  #ifdef HAVE_USB_APDU
  io_usb_hid_init();
c0d013c6:	f7ff fe35 	bl	c0d01034 <io_usb_hid_init>
  io_seproxyhal_init_button();
}

void io_seproxyhal_init_ux(void) {
  // initialize the touch part
  G_bagl_last_touched_not_released_component = NULL;
c0d013ca:	4809      	ldr	r0, [pc, #36]	; (c0d013f0 <io_seproxyhal_init+0x4c>)
c0d013cc:	6004      	str	r4, [r0, #0]

}

void io_seproxyhal_init_button(void) {
  // no button push so far
  G_button_mask = 0;
c0d013ce:	4809      	ldr	r0, [pc, #36]	; (c0d013f4 <io_seproxyhal_init+0x50>)
c0d013d0:	6004      	str	r4, [r0, #0]
  G_button_same_mask_counter = 0;
c0d013d2:	4809      	ldr	r0, [pc, #36]	; (c0d013f8 <io_seproxyhal_init+0x54>)
c0d013d4:	6004      	str	r4, [r0, #0]
  io_usb_hid_init();
  #endif // HAVE_USB_APDU

  io_seproxyhal_init_ux();
  io_seproxyhal_init_button();
}
c0d013d6:	bd10      	pop	{r4, pc}
c0d013d8:	20001c54 	.word	0x20001c54
c0d013dc:	20001c58 	.word	0x20001c58
c0d013e0:	20001c56 	.word	0x20001c56
c0d013e4:	20001c5a 	.word	0x20001c5a
c0d013e8:	20001c4c 	.word	0x20001c4c
c0d013ec:	20001c44 	.word	0x20001c44
c0d013f0:	20001c5c 	.word	0x20001c5c
c0d013f4:	20001c60 	.word	0x20001c60
c0d013f8:	20001c64 	.word	0x20001c64

c0d013fc <io_seproxyhal_init_ux>:

void io_seproxyhal_init_ux(void) {
  // initialize the touch part
  G_bagl_last_touched_not_released_component = NULL;
c0d013fc:	4801      	ldr	r0, [pc, #4]	; (c0d01404 <io_seproxyhal_init_ux+0x8>)
c0d013fe:	2100      	movs	r1, #0
c0d01400:	6001      	str	r1, [r0, #0]

}
c0d01402:	4770      	bx	lr
c0d01404:	20001c5c 	.word	0x20001c5c

c0d01408 <io_seproxyhal_touch_out>:
  G_button_same_mask_counter = 0;
}

#ifdef HAVE_BAGL

unsigned int io_seproxyhal_touch_out(const bagl_element_t* element, bagl_element_callback_t before_display) {
c0d01408:	b5b0      	push	{r4, r5, r7, lr}
c0d0140a:	460d      	mov	r5, r1
c0d0140c:	4604      	mov	r4, r0
  const bagl_element_t* el;
  if (element->out != NULL) {
c0d0140e:	6b20      	ldr	r0, [r4, #48]	; 0x30
c0d01410:	2800      	cmp	r0, #0
c0d01412:	d00c      	beq.n	c0d0142e <io_seproxyhal_touch_out+0x26>
    el = (const bagl_element_t*)PIC(((bagl_element_callback_t)PIC(element->out))(element));
c0d01414:	f000 faea 	bl	c0d019ec <pic>
c0d01418:	4601      	mov	r1, r0
c0d0141a:	4620      	mov	r0, r4
c0d0141c:	4788      	blx	r1
c0d0141e:	f000 fae5 	bl	c0d019ec <pic>
c0d01422:	2100      	movs	r1, #0
    // backward compatible with samples and such
    if (! el) {
c0d01424:	2800      	cmp	r0, #0
c0d01426:	d010      	beq.n	c0d0144a <io_seproxyhal_touch_out+0x42>
c0d01428:	2801      	cmp	r0, #1
c0d0142a:	d000      	beq.n	c0d0142e <io_seproxyhal_touch_out+0x26>
c0d0142c:	4604      	mov	r4, r0
      element = el;
    }
  }

  // out function might have triggered a draw of its own during a display callback
  if (before_display) {
c0d0142e:	2d00      	cmp	r5, #0
c0d01430:	d007      	beq.n	c0d01442 <io_seproxyhal_touch_out+0x3a>
    el = before_display(element);
c0d01432:	4620      	mov	r0, r4
c0d01434:	47a8      	blx	r5
c0d01436:	2100      	movs	r1, #0
    if (!el) {
c0d01438:	2800      	cmp	r0, #0
c0d0143a:	d006      	beq.n	c0d0144a <io_seproxyhal_touch_out+0x42>
c0d0143c:	2801      	cmp	r0, #1
c0d0143e:	d000      	beq.n	c0d01442 <io_seproxyhal_touch_out+0x3a>
c0d01440:	4604      	mov	r4, r0
    if ((unsigned int)el != 1) {
      element = el;
    }
  }

  io_seproxyhal_display(element);
c0d01442:	4620      	mov	r0, r4
c0d01444:	f7fe fe4e 	bl	c0d000e4 <io_seproxyhal_display>
c0d01448:	2101      	movs	r1, #1
  return 1;
}
c0d0144a:	4608      	mov	r0, r1
c0d0144c:	bdb0      	pop	{r4, r5, r7, pc}

c0d0144e <io_seproxyhal_touch_over>:

unsigned int io_seproxyhal_touch_over(const bagl_element_t* element, bagl_element_callback_t before_display) {
c0d0144e:	b5b0      	push	{r4, r5, r7, lr}
c0d01450:	b08e      	sub	sp, #56	; 0x38
c0d01452:	460c      	mov	r4, r1
c0d01454:	4605      	mov	r5, r0
  bagl_element_t e;
  const bagl_element_t* el;
  if (element->over != NULL) {
c0d01456:	6b68      	ldr	r0, [r5, #52]	; 0x34
c0d01458:	2800      	cmp	r0, #0
c0d0145a:	d00c      	beq.n	c0d01476 <io_seproxyhal_touch_over+0x28>
    el = (const bagl_element_t*)PIC(((bagl_element_callback_t)PIC(element->over))(element));
c0d0145c:	f000 fac6 	bl	c0d019ec <pic>
c0d01460:	4601      	mov	r1, r0
c0d01462:	4628      	mov	r0, r5
c0d01464:	4788      	blx	r1
c0d01466:	f000 fac1 	bl	c0d019ec <pic>
c0d0146a:	2100      	movs	r1, #0
    // backward compatible with samples and such
    if (!el) {
c0d0146c:	2800      	cmp	r0, #0
c0d0146e:	d016      	beq.n	c0d0149e <io_seproxyhal_touch_over+0x50>
c0d01470:	2801      	cmp	r0, #1
c0d01472:	d000      	beq.n	c0d01476 <io_seproxyhal_touch_over+0x28>
c0d01474:	4605      	mov	r5, r0
c0d01476:	4668      	mov	r0, sp
      element = el;
    }
  }

  // over function might have triggered a draw of its own during a display callback
  os_memmove(&e, (void*)element, sizeof(bagl_element_t));
c0d01478:	2238      	movs	r2, #56	; 0x38
c0d0147a:	4629      	mov	r1, r5
c0d0147c:	f7ff fdbf 	bl	c0d00ffe <os_memmove>
  e.component.fgcolor = element->overfgcolor;
c0d01480:	6a68      	ldr	r0, [r5, #36]	; 0x24
c0d01482:	9004      	str	r0, [sp, #16]
  e.component.bgcolor = element->overbgcolor;
c0d01484:	6aa8      	ldr	r0, [r5, #40]	; 0x28
c0d01486:	9005      	str	r0, [sp, #20]

  //element = &e; // for INARRAY checks, it disturbs a bit. avoid it

  if (before_display) {
c0d01488:	2c00      	cmp	r4, #0
c0d0148a:	d004      	beq.n	c0d01496 <io_seproxyhal_touch_over+0x48>
    el = before_display(element);
c0d0148c:	4628      	mov	r0, r5
c0d0148e:	47a0      	blx	r4
c0d01490:	2100      	movs	r1, #0
    element = &e;
    if (!el) {
c0d01492:	2800      	cmp	r0, #0
c0d01494:	d003      	beq.n	c0d0149e <io_seproxyhal_touch_over+0x50>
c0d01496:	4668      	mov	r0, sp
  //else 
  {
    element = &e;
  }

  io_seproxyhal_display(element);
c0d01498:	f7fe fe24 	bl	c0d000e4 <io_seproxyhal_display>
c0d0149c:	2101      	movs	r1, #1
  return 1;
}
c0d0149e:	4608      	mov	r0, r1
c0d014a0:	b00e      	add	sp, #56	; 0x38
c0d014a2:	bdb0      	pop	{r4, r5, r7, pc}

c0d014a4 <io_seproxyhal_touch_tap>:

unsigned int io_seproxyhal_touch_tap(const bagl_element_t* element, bagl_element_callback_t before_display) {
c0d014a4:	b5b0      	push	{r4, r5, r7, lr}
c0d014a6:	460d      	mov	r5, r1
c0d014a8:	4604      	mov	r4, r0
  const bagl_element_t* el;
  if (element->tap != NULL) {
c0d014aa:	6ae0      	ldr	r0, [r4, #44]	; 0x2c
c0d014ac:	2800      	cmp	r0, #0
c0d014ae:	d00c      	beq.n	c0d014ca <io_seproxyhal_touch_tap+0x26>
    el = (const bagl_element_t*)PIC(((bagl_element_callback_t)PIC(element->tap))(element));
c0d014b0:	f000 fa9c 	bl	c0d019ec <pic>
c0d014b4:	4601      	mov	r1, r0
c0d014b6:	4620      	mov	r0, r4
c0d014b8:	4788      	blx	r1
c0d014ba:	f000 fa97 	bl	c0d019ec <pic>
c0d014be:	2100      	movs	r1, #0
    // backward compatible with samples and such
    if (!el) {
c0d014c0:	2800      	cmp	r0, #0
c0d014c2:	d010      	beq.n	c0d014e6 <io_seproxyhal_touch_tap+0x42>
c0d014c4:	2801      	cmp	r0, #1
c0d014c6:	d000      	beq.n	c0d014ca <io_seproxyhal_touch_tap+0x26>
c0d014c8:	4604      	mov	r4, r0
      element = el;
    }
  }

  // tap function might have triggered a draw of its own during a display callback
  if (before_display) {
c0d014ca:	2d00      	cmp	r5, #0
c0d014cc:	d007      	beq.n	c0d014de <io_seproxyhal_touch_tap+0x3a>
    el = before_display(element);
c0d014ce:	4620      	mov	r0, r4
c0d014d0:	47a8      	blx	r5
c0d014d2:	2100      	movs	r1, #0
    if (!el) {
c0d014d4:	2800      	cmp	r0, #0
c0d014d6:	d006      	beq.n	c0d014e6 <io_seproxyhal_touch_tap+0x42>
c0d014d8:	2801      	cmp	r0, #1
c0d014da:	d000      	beq.n	c0d014de <io_seproxyhal_touch_tap+0x3a>
c0d014dc:	4604      	mov	r4, r0
    }
    if ((unsigned int)el != 1) {
      element = el;
    }
  }
  io_seproxyhal_display(element);
c0d014de:	4620      	mov	r0, r4
c0d014e0:	f7fe fe00 	bl	c0d000e4 <io_seproxyhal_display>
c0d014e4:	2101      	movs	r1, #1
  return 1;
}
c0d014e6:	4608      	mov	r0, r1
c0d014e8:	bdb0      	pop	{r4, r5, r7, pc}
	...

c0d014ec <io_seproxyhal_touch_element_callback>:
  io_seproxyhal_touch_element_callback(elements, element_count, x, y, event_kind, NULL);  
}

// browse all elements and until an element has changed state, continue browsing
// return if processed or not
void io_seproxyhal_touch_element_callback(const bagl_element_t* elements, unsigned short element_count, unsigned short x, unsigned short y, unsigned char event_kind, bagl_element_callback_t before_display) {
c0d014ec:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d014ee:	b087      	sub	sp, #28
c0d014f0:	9302      	str	r3, [sp, #8]
c0d014f2:	9203      	str	r2, [sp, #12]
c0d014f4:	9105      	str	r1, [sp, #20]
  unsigned char comp_idx;
  unsigned char last_touched_not_released_component_was_in_current_array = 0;

  // find the first empty entry
  for (comp_idx=0; comp_idx < element_count; comp_idx++) {
c0d014f6:	2900      	cmp	r1, #0
c0d014f8:	d077      	beq.n	c0d015ea <io_seproxyhal_touch_element_callback+0xfe>
c0d014fa:	9004      	str	r0, [sp, #16]
c0d014fc:	980d      	ldr	r0, [sp, #52]	; 0x34
c0d014fe:	9001      	str	r0, [sp, #4]
c0d01500:	980c      	ldr	r0, [sp, #48]	; 0x30
c0d01502:	9000      	str	r0, [sp, #0]
c0d01504:	2500      	movs	r5, #0
c0d01506:	4b3c      	ldr	r3, [pc, #240]	; (c0d015f8 <io_seproxyhal_touch_element_callback+0x10c>)
c0d01508:	9506      	str	r5, [sp, #24]
c0d0150a:	462f      	mov	r7, r5
c0d0150c:	461e      	mov	r6, r3
    // process all components matching the x/y/w/h (no break) => fishy for the released out of zone
    // continue processing only if a status has not been sent
    if (io_seproxyhal_spi_is_status_sent()) {
c0d0150e:	f000 fbcf 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d01512:	2800      	cmp	r0, #0
c0d01514:	d155      	bne.n	c0d015c2 <io_seproxyhal_touch_element_callback+0xd6>
      // continue instead of return to process all elemnts and therefore discard last touched element
      break;
    }

    // only perform out callback when element was in the current array, else, leave it be
    if (&elements[comp_idx] == G_bagl_last_touched_not_released_component) {
c0d01516:	2038      	movs	r0, #56	; 0x38
c0d01518:	4368      	muls	r0, r5
c0d0151a:	9c04      	ldr	r4, [sp, #16]
c0d0151c:	1825      	adds	r5, r4, r0
c0d0151e:	4633      	mov	r3, r6
c0d01520:	681a      	ldr	r2, [r3, #0]
c0d01522:	2101      	movs	r1, #1
c0d01524:	4295      	cmp	r5, r2
c0d01526:	d000      	beq.n	c0d0152a <io_seproxyhal_touch_element_callback+0x3e>
c0d01528:	9906      	ldr	r1, [sp, #24]
c0d0152a:	9106      	str	r1, [sp, #24]
      last_touched_not_released_component_was_in_current_array = 1;
    }

    // the first component drawn with a 
    if ((elements[comp_idx].component.type & BAGL_FLAG_TOUCHABLE) 
c0d0152c:	5620      	ldrsb	r0, [r4, r0]
        && elements[comp_idx].component.x-elements[comp_idx].touch_area_brim <= x && x<elements[comp_idx].component.x+elements[comp_idx].component.width+elements[comp_idx].touch_area_brim
c0d0152e:	2800      	cmp	r0, #0
c0d01530:	da41      	bge.n	c0d015b6 <io_seproxyhal_touch_element_callback+0xca>
c0d01532:	2020      	movs	r0, #32
c0d01534:	5c28      	ldrb	r0, [r5, r0]
c0d01536:	2102      	movs	r1, #2
c0d01538:	5e69      	ldrsh	r1, [r5, r1]
c0d0153a:	1a0a      	subs	r2, r1, r0
c0d0153c:	9c03      	ldr	r4, [sp, #12]
c0d0153e:	42a2      	cmp	r2, r4
c0d01540:	dc39      	bgt.n	c0d015b6 <io_seproxyhal_touch_element_callback+0xca>
c0d01542:	1841      	adds	r1, r0, r1
c0d01544:	88ea      	ldrh	r2, [r5, #6]
c0d01546:	1889      	adds	r1, r1, r2
        && elements[comp_idx].component.y-elements[comp_idx].touch_area_brim <= y && y<elements[comp_idx].component.y+elements[comp_idx].component.height+elements[comp_idx].touch_area_brim) {
c0d01548:	9a03      	ldr	r2, [sp, #12]
c0d0154a:	428a      	cmp	r2, r1
c0d0154c:	da33      	bge.n	c0d015b6 <io_seproxyhal_touch_element_callback+0xca>
c0d0154e:	2104      	movs	r1, #4
c0d01550:	5e6c      	ldrsh	r4, [r5, r1]
c0d01552:	1a22      	subs	r2, r4, r0
c0d01554:	9902      	ldr	r1, [sp, #8]
c0d01556:	428a      	cmp	r2, r1
c0d01558:	dc2d      	bgt.n	c0d015b6 <io_seproxyhal_touch_element_callback+0xca>
c0d0155a:	1820      	adds	r0, r4, r0
c0d0155c:	8929      	ldrh	r1, [r5, #8]
c0d0155e:	1840      	adds	r0, r0, r1
    if (&elements[comp_idx] == G_bagl_last_touched_not_released_component) {
      last_touched_not_released_component_was_in_current_array = 1;
    }

    // the first component drawn with a 
    if ((elements[comp_idx].component.type & BAGL_FLAG_TOUCHABLE) 
c0d01560:	9902      	ldr	r1, [sp, #8]
c0d01562:	4281      	cmp	r1, r0
c0d01564:	da27      	bge.n	c0d015b6 <io_seproxyhal_touch_element_callback+0xca>
        && elements[comp_idx].component.x-elements[comp_idx].touch_area_brim <= x && x<elements[comp_idx].component.x+elements[comp_idx].component.width+elements[comp_idx].touch_area_brim
        && elements[comp_idx].component.y-elements[comp_idx].touch_area_brim <= y && y<elements[comp_idx].component.y+elements[comp_idx].component.height+elements[comp_idx].touch_area_brim) {

      // outing the previous over'ed component
      if (&elements[comp_idx] != G_bagl_last_touched_not_released_component 
c0d01566:	6818      	ldr	r0, [r3, #0]
              && G_bagl_last_touched_not_released_component != NULL) {
c0d01568:	4285      	cmp	r5, r0
c0d0156a:	d010      	beq.n	c0d0158e <io_seproxyhal_touch_element_callback+0xa2>
c0d0156c:	6818      	ldr	r0, [r3, #0]
    if ((elements[comp_idx].component.type & BAGL_FLAG_TOUCHABLE) 
        && elements[comp_idx].component.x-elements[comp_idx].touch_area_brim <= x && x<elements[comp_idx].component.x+elements[comp_idx].component.width+elements[comp_idx].touch_area_brim
        && elements[comp_idx].component.y-elements[comp_idx].touch_area_brim <= y && y<elements[comp_idx].component.y+elements[comp_idx].component.height+elements[comp_idx].touch_area_brim) {

      // outing the previous over'ed component
      if (&elements[comp_idx] != G_bagl_last_touched_not_released_component 
c0d0156e:	2800      	cmp	r0, #0
c0d01570:	d00d      	beq.n	c0d0158e <io_seproxyhal_touch_element_callback+0xa2>
              && G_bagl_last_touched_not_released_component != NULL) {
        // only out the previous element if the newly matching will be displayed 
        if (!before_display || before_display(&elements[comp_idx])) {
c0d01572:	9801      	ldr	r0, [sp, #4]
c0d01574:	2800      	cmp	r0, #0
c0d01576:	d005      	beq.n	c0d01584 <io_seproxyhal_touch_element_callback+0x98>
c0d01578:	4628      	mov	r0, r5
c0d0157a:	9901      	ldr	r1, [sp, #4]
c0d0157c:	4788      	blx	r1
c0d0157e:	4633      	mov	r3, r6
c0d01580:	2800      	cmp	r0, #0
c0d01582:	d018      	beq.n	c0d015b6 <io_seproxyhal_touch_element_callback+0xca>
          if (io_seproxyhal_touch_out(G_bagl_last_touched_not_released_component, before_display)) {
c0d01584:	6818      	ldr	r0, [r3, #0]
c0d01586:	9901      	ldr	r1, [sp, #4]
c0d01588:	f7ff ff3e 	bl	c0d01408 <io_seproxyhal_touch_out>
c0d0158c:	e008      	b.n	c0d015a0 <io_seproxyhal_touch_element_callback+0xb4>
c0d0158e:	9800      	ldr	r0, [sp, #0]
        continue;
      }
      */
      
      // callback the hal to notify the component impacted by the user input
      else if (event_kind == SEPROXYHAL_TAG_FINGER_EVENT_RELEASE) {
c0d01590:	2801      	cmp	r0, #1
c0d01592:	d009      	beq.n	c0d015a8 <io_seproxyhal_touch_element_callback+0xbc>
c0d01594:	2802      	cmp	r0, #2
c0d01596:	d10e      	bne.n	c0d015b6 <io_seproxyhal_touch_element_callback+0xca>
        if (io_seproxyhal_touch_tap(&elements[comp_idx], before_display)) {
c0d01598:	4628      	mov	r0, r5
c0d0159a:	9901      	ldr	r1, [sp, #4]
c0d0159c:	f7ff ff82 	bl	c0d014a4 <io_seproxyhal_touch_tap>
c0d015a0:	4633      	mov	r3, r6
c0d015a2:	2800      	cmp	r0, #0
c0d015a4:	d007      	beq.n	c0d015b6 <io_seproxyhal_touch_element_callback+0xca>
c0d015a6:	e022      	b.n	c0d015ee <io_seproxyhal_touch_element_callback+0x102>
          return;
        }
      }
      else if (event_kind == SEPROXYHAL_TAG_FINGER_EVENT_TOUCH) {
        // ask for overing
        if (io_seproxyhal_touch_over(&elements[comp_idx], before_display)) {
c0d015a8:	4628      	mov	r0, r5
c0d015aa:	9901      	ldr	r1, [sp, #4]
c0d015ac:	f7ff ff4f 	bl	c0d0144e <io_seproxyhal_touch_over>
c0d015b0:	4633      	mov	r3, r6
c0d015b2:	2800      	cmp	r0, #0
c0d015b4:	d11e      	bne.n	c0d015f4 <io_seproxyhal_touch_element_callback+0x108>
void io_seproxyhal_touch_element_callback(const bagl_element_t* elements, unsigned short element_count, unsigned short x, unsigned short y, unsigned char event_kind, bagl_element_callback_t before_display) {
  unsigned char comp_idx;
  unsigned char last_touched_not_released_component_was_in_current_array = 0;

  // find the first empty entry
  for (comp_idx=0; comp_idx < element_count; comp_idx++) {
c0d015b6:	1c7f      	adds	r7, r7, #1
c0d015b8:	b2fd      	uxtb	r5, r7
c0d015ba:	9805      	ldr	r0, [sp, #20]
c0d015bc:	4285      	cmp	r5, r0
c0d015be:	d3a5      	bcc.n	c0d0150c <io_seproxyhal_touch_element_callback+0x20>
c0d015c0:	e000      	b.n	c0d015c4 <io_seproxyhal_touch_element_callback+0xd8>
c0d015c2:	4633      	mov	r3, r6
    }
  }

  // if overing out of component or over another component, the out event is sent after the over event of the previous component
  if(last_touched_not_released_component_was_in_current_array 
    && G_bagl_last_touched_not_released_component != NULL) {
c0d015c4:	9806      	ldr	r0, [sp, #24]
c0d015c6:	0600      	lsls	r0, r0, #24
c0d015c8:	d00f      	beq.n	c0d015ea <io_seproxyhal_touch_element_callback+0xfe>
c0d015ca:	6818      	ldr	r0, [r3, #0]
      }
    }
  }

  // if overing out of component or over another component, the out event is sent after the over event of the previous component
  if(last_touched_not_released_component_was_in_current_array 
c0d015cc:	2800      	cmp	r0, #0
c0d015ce:	d00c      	beq.n	c0d015ea <io_seproxyhal_touch_element_callback+0xfe>
    && G_bagl_last_touched_not_released_component != NULL) {

    // we won't be able to notify the out, don't do it, in case a diplay refused the dra of the relased element and the position matched another element of the array (in autocomplete for example)
    if (io_seproxyhal_spi_is_status_sent()) {
c0d015d0:	f000 fb6e 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d015d4:	4631      	mov	r1, r6
c0d015d6:	2800      	cmp	r0, #0
c0d015d8:	d107      	bne.n	c0d015ea <io_seproxyhal_touch_element_callback+0xfe>
      return;
    }
    
    if (io_seproxyhal_touch_out(G_bagl_last_touched_not_released_component, before_display)) {
c0d015da:	6808      	ldr	r0, [r1, #0]
c0d015dc:	9901      	ldr	r1, [sp, #4]
c0d015de:	f7ff ff13 	bl	c0d01408 <io_seproxyhal_touch_out>
c0d015e2:	2800      	cmp	r0, #0
c0d015e4:	d001      	beq.n	c0d015ea <io_seproxyhal_touch_element_callback+0xfe>
      // ok component out has been emitted
      G_bagl_last_touched_not_released_component = NULL;
c0d015e6:	2000      	movs	r0, #0
c0d015e8:	6030      	str	r0, [r6, #0]
    }
  }

  // not processed
}
c0d015ea:	b007      	add	sp, #28
c0d015ec:	bdf0      	pop	{r4, r5, r6, r7, pc}
c0d015ee:	2000      	movs	r0, #0
c0d015f0:	6018      	str	r0, [r3, #0]
c0d015f2:	e7fa      	b.n	c0d015ea <io_seproxyhal_touch_element_callback+0xfe>
      }
      else if (event_kind == SEPROXYHAL_TAG_FINGER_EVENT_TOUCH) {
        // ask for overing
        if (io_seproxyhal_touch_over(&elements[comp_idx], before_display)) {
          // remember the last touched component
          G_bagl_last_touched_not_released_component = (bagl_element_t*)&elements[comp_idx];
c0d015f4:	601d      	str	r5, [r3, #0]
c0d015f6:	e7f8      	b.n	c0d015ea <io_seproxyhal_touch_element_callback+0xfe>
c0d015f8:	20001c5c 	.word	0x20001c5c

c0d015fc <io_seproxyhal_display_icon>:
  // remaining length of bitmap bits to be displayed
  return len;
}
#endif // SEPROXYHAL_TAG_SCREEN_DISPLAY_RAW_STATUS

void io_seproxyhal_display_icon(bagl_component_t* icon_component, bagl_icon_details_t* icon_details) {
c0d015fc:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d015fe:	b089      	sub	sp, #36	; 0x24
c0d01600:	460c      	mov	r4, r1
c0d01602:	4601      	mov	r1, r0
c0d01604:	ad02      	add	r5, sp, #8
c0d01606:	221c      	movs	r2, #28
  bagl_component_t icon_component_mod;
  // ensure not being out of bounds in the icon component agianst the declared icon real size
  os_memmove(&icon_component_mod, icon_component, sizeof(bagl_component_t));
c0d01608:	4628      	mov	r0, r5
c0d0160a:	9201      	str	r2, [sp, #4]
c0d0160c:	f7ff fcf7 	bl	c0d00ffe <os_memmove>
  icon_component_mod.width = icon_details->width;
c0d01610:	6821      	ldr	r1, [r4, #0]
c0d01612:	80e9      	strh	r1, [r5, #6]
  icon_component_mod.height = icon_details->height;
c0d01614:	6862      	ldr	r2, [r4, #4]
c0d01616:	812a      	strh	r2, [r5, #8]
  // component type = ICON, provided bitmap
  // => bitmap transmitted


  // color index size
  unsigned int h = (1<<(icon_details->bpp))*sizeof(unsigned int); 
c0d01618:	68a0      	ldr	r0, [r4, #8]
  unsigned int w = ((icon_component->width*icon_component->height*icon_details->bpp)/8)+((icon_component->width*icon_component->height*icon_details->bpp)%8?1:0);
  unsigned short length = sizeof(bagl_component_t)
                          +1 /* bpp */
                          +h /* color index */
                          +w; /* image bitmap size */
  G_io_seproxyhal_spi_buffer[0] = SEPROXYHAL_TAG_SCREEN_DISPLAY_STATUS;
c0d0161a:	4f1a      	ldr	r7, [pc, #104]	; (c0d01684 <io_seproxyhal_display_icon+0x88>)
c0d0161c:	2365      	movs	r3, #101	; 0x65
c0d0161e:	703b      	strb	r3, [r7, #0]


  // color index size
  unsigned int h = (1<<(icon_details->bpp))*sizeof(unsigned int); 
  // bitmap size
  unsigned int w = ((icon_component->width*icon_component->height*icon_details->bpp)/8)+((icon_component->width*icon_component->height*icon_details->bpp)%8?1:0);
c0d01620:	b292      	uxth	r2, r2
c0d01622:	4342      	muls	r2, r0
c0d01624:	b28b      	uxth	r3, r1
c0d01626:	4353      	muls	r3, r2
c0d01628:	08d9      	lsrs	r1, r3, #3
c0d0162a:	1c4e      	adds	r6, r1, #1
c0d0162c:	2207      	movs	r2, #7
c0d0162e:	4213      	tst	r3, r2
c0d01630:	d100      	bne.n	c0d01634 <io_seproxyhal_display_icon+0x38>
c0d01632:	460e      	mov	r6, r1
c0d01634:	4631      	mov	r1, r6
c0d01636:	9100      	str	r1, [sp, #0]
c0d01638:	2604      	movs	r6, #4
  // component type = ICON, provided bitmap
  // => bitmap transmitted


  // color index size
  unsigned int h = (1<<(icon_details->bpp))*sizeof(unsigned int); 
c0d0163a:	4086      	lsls	r6, r0
  // bitmap size
  unsigned int w = ((icon_component->width*icon_component->height*icon_details->bpp)/8)+((icon_component->width*icon_component->height*icon_details->bpp)%8?1:0);
  unsigned short length = sizeof(bagl_component_t)
                          +1 /* bpp */
                          +h /* color index */
c0d0163c:	1870      	adds	r0, r6, r1
                          +w; /* image bitmap size */
c0d0163e:	301d      	adds	r0, #29
  G_io_seproxyhal_spi_buffer[0] = SEPROXYHAL_TAG_SCREEN_DISPLAY_STATUS;
  G_io_seproxyhal_spi_buffer[1] = length>>8;
c0d01640:	0a01      	lsrs	r1, r0, #8
c0d01642:	7079      	strb	r1, [r7, #1]
  G_io_seproxyhal_spi_buffer[2] = length;
c0d01644:	70b8      	strb	r0, [r7, #2]
c0d01646:	2103      	movs	r1, #3
  io_seproxyhal_spi_send(G_io_seproxyhal_spi_buffer, 3);
c0d01648:	4638      	mov	r0, r7
c0d0164a:	f000 fb1b 	bl	c0d01c84 <io_seproxyhal_spi_send>
  io_seproxyhal_spi_send((unsigned char*)icon_component, sizeof(bagl_component_t));
c0d0164e:	4628      	mov	r0, r5
c0d01650:	9901      	ldr	r1, [sp, #4]
c0d01652:	f000 fb17 	bl	c0d01c84 <io_seproxyhal_spi_send>
  G_io_seproxyhal_spi_buffer[0] = icon_details->bpp;
c0d01656:	68a0      	ldr	r0, [r4, #8]
c0d01658:	7038      	strb	r0, [r7, #0]
c0d0165a:	2101      	movs	r1, #1
  io_seproxyhal_spi_send(G_io_seproxyhal_spi_buffer, 1);
c0d0165c:	4638      	mov	r0, r7
c0d0165e:	f000 fb11 	bl	c0d01c84 <io_seproxyhal_spi_send>
  io_seproxyhal_spi_send((unsigned char*)PIC(icon_details->colors), h);
c0d01662:	68e0      	ldr	r0, [r4, #12]
c0d01664:	f000 f9c2 	bl	c0d019ec <pic>
c0d01668:	b2b1      	uxth	r1, r6
c0d0166a:	f000 fb0b 	bl	c0d01c84 <io_seproxyhal_spi_send>
  io_seproxyhal_spi_send((unsigned char*)PIC(icon_details->bitmap), w);
c0d0166e:	9800      	ldr	r0, [sp, #0]
c0d01670:	b285      	uxth	r5, r0
c0d01672:	6920      	ldr	r0, [r4, #16]
c0d01674:	f000 f9ba 	bl	c0d019ec <pic>
c0d01678:	4629      	mov	r1, r5
c0d0167a:	f000 fb03 	bl	c0d01c84 <io_seproxyhal_spi_send>
#endif // !SEPROXYHAL_TAG_SCREEN_DISPLAY_RAW_STATUS
}
c0d0167e:	b009      	add	sp, #36	; 0x24
c0d01680:	bdf0      	pop	{r4, r5, r6, r7, pc}
c0d01682:	46c0      	nop			; (mov r8, r8)
c0d01684:	20001804 	.word	0x20001804

c0d01688 <io_seproxyhal_display_default>:

void io_seproxyhal_display_default(const bagl_element_t * element) {
c0d01688:	b570      	push	{r4, r5, r6, lr}
c0d0168a:	4604      	mov	r4, r0
  // process automagically address from rom and from ram
  unsigned int type = (element->component.type & ~(BAGL_FLAG_TOUCHABLE));
c0d0168c:	7820      	ldrb	r0, [r4, #0]
c0d0168e:	267f      	movs	r6, #127	; 0x7f
c0d01690:	4006      	ands	r6, r0

  // avoid sending another status :), fixes a lot of bugs in the end
  if (io_seproxyhal_spi_is_status_sent()) {
c0d01692:	f000 fb0d 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d01696:	2800      	cmp	r0, #0
c0d01698:	d130      	bne.n	c0d016fc <io_seproxyhal_display_default+0x74>
c0d0169a:	2e00      	cmp	r6, #0
c0d0169c:	d02e      	beq.n	c0d016fc <io_seproxyhal_display_default+0x74>
    return;
  }

  if (type != BAGL_NONE) {
    if (element->text != NULL) {
c0d0169e:	69e0      	ldr	r0, [r4, #28]
c0d016a0:	2800      	cmp	r0, #0
c0d016a2:	d01d      	beq.n	c0d016e0 <io_seproxyhal_display_default+0x58>
      unsigned int text_adr = PIC((unsigned int)element->text);
c0d016a4:	f000 f9a2 	bl	c0d019ec <pic>
c0d016a8:	4605      	mov	r5, r0
      // consider an icon details descriptor is pointed by the context
      if (type == BAGL_ICON && element->component.icon_id == 0) {
c0d016aa:	2e05      	cmp	r6, #5
c0d016ac:	d102      	bne.n	c0d016b4 <io_seproxyhal_display_default+0x2c>
c0d016ae:	7ea0      	ldrb	r0, [r4, #26]
c0d016b0:	2800      	cmp	r0, #0
c0d016b2:	d024      	beq.n	c0d016fe <io_seproxyhal_display_default+0x76>
        io_seproxyhal_display_icon(&element->component, (bagl_icon_details_t*)text_adr);
      }
      else {
        unsigned short length = sizeof(bagl_component_t)+strlen((const char*)text_adr);
c0d016b4:	4628      	mov	r0, r5
c0d016b6:	f001 fb07 	bl	c0d02cc8 <strlen>
c0d016ba:	4606      	mov	r6, r0
        G_io_seproxyhal_spi_buffer[0] = SEPROXYHAL_TAG_SCREEN_DISPLAY_STATUS;
c0d016bc:	4812      	ldr	r0, [pc, #72]	; (c0d01708 <io_seproxyhal_display_default+0x80>)
c0d016be:	2165      	movs	r1, #101	; 0x65
c0d016c0:	7001      	strb	r1, [r0, #0]
      // consider an icon details descriptor is pointed by the context
      if (type == BAGL_ICON && element->component.icon_id == 0) {
        io_seproxyhal_display_icon(&element->component, (bagl_icon_details_t*)text_adr);
      }
      else {
        unsigned short length = sizeof(bagl_component_t)+strlen((const char*)text_adr);
c0d016c2:	4631      	mov	r1, r6
c0d016c4:	311c      	adds	r1, #28
        G_io_seproxyhal_spi_buffer[0] = SEPROXYHAL_TAG_SCREEN_DISPLAY_STATUS;
        G_io_seproxyhal_spi_buffer[1] = length>>8;
c0d016c6:	0a0a      	lsrs	r2, r1, #8
c0d016c8:	7042      	strb	r2, [r0, #1]
        G_io_seproxyhal_spi_buffer[2] = length;
c0d016ca:	7081      	strb	r1, [r0, #2]
        io_seproxyhal_spi_send(G_io_seproxyhal_spi_buffer, 3);
c0d016cc:	2103      	movs	r1, #3
c0d016ce:	f000 fad9 	bl	c0d01c84 <io_seproxyhal_spi_send>
c0d016d2:	211c      	movs	r1, #28
        io_seproxyhal_spi_send((unsigned char*)&element->component, sizeof(bagl_component_t));
c0d016d4:	4620      	mov	r0, r4
c0d016d6:	f000 fad5 	bl	c0d01c84 <io_seproxyhal_spi_send>
        io_seproxyhal_spi_send((unsigned char*)text_adr, length-sizeof(bagl_component_t));
c0d016da:	b2b1      	uxth	r1, r6
c0d016dc:	4628      	mov	r0, r5
c0d016de:	e00b      	b.n	c0d016f8 <io_seproxyhal_display_default+0x70>
      }
    }
    else {
      unsigned short length = sizeof(bagl_component_t);
      G_io_seproxyhal_spi_buffer[0] = SEPROXYHAL_TAG_SCREEN_DISPLAY_STATUS;
c0d016e0:	4809      	ldr	r0, [pc, #36]	; (c0d01708 <io_seproxyhal_display_default+0x80>)
c0d016e2:	2165      	movs	r1, #101	; 0x65
c0d016e4:	7001      	strb	r1, [r0, #0]
      G_io_seproxyhal_spi_buffer[1] = length>>8;
c0d016e6:	2100      	movs	r1, #0
c0d016e8:	7041      	strb	r1, [r0, #1]
c0d016ea:	251c      	movs	r5, #28
      G_io_seproxyhal_spi_buffer[2] = length;
c0d016ec:	7085      	strb	r5, [r0, #2]
      io_seproxyhal_spi_send(G_io_seproxyhal_spi_buffer, 3);
c0d016ee:	2103      	movs	r1, #3
c0d016f0:	f000 fac8 	bl	c0d01c84 <io_seproxyhal_spi_send>
      io_seproxyhal_spi_send((unsigned char*)&element->component, sizeof(bagl_component_t));
c0d016f4:	4620      	mov	r0, r4
c0d016f6:	4629      	mov	r1, r5
c0d016f8:	f000 fac4 	bl	c0d01c84 <io_seproxyhal_spi_send>
    }
  }
}
c0d016fc:	bd70      	pop	{r4, r5, r6, pc}
  if (type != BAGL_NONE) {
    if (element->text != NULL) {
      unsigned int text_adr = PIC((unsigned int)element->text);
      // consider an icon details descriptor is pointed by the context
      if (type == BAGL_ICON && element->component.icon_id == 0) {
        io_seproxyhal_display_icon(&element->component, (bagl_icon_details_t*)text_adr);
c0d016fe:	4620      	mov	r0, r4
c0d01700:	4629      	mov	r1, r5
c0d01702:	f7ff ff7b 	bl	c0d015fc <io_seproxyhal_display_icon>
      G_io_seproxyhal_spi_buffer[2] = length;
      io_seproxyhal_spi_send(G_io_seproxyhal_spi_buffer, 3);
      io_seproxyhal_spi_send((unsigned char*)&element->component, sizeof(bagl_component_t));
    }
  }
}
c0d01706:	bd70      	pop	{r4, r5, r6, pc}
c0d01708:	20001804 	.word	0x20001804

c0d0170c <bagl_label_roundtrip_duration_ms>:

unsigned int bagl_label_roundtrip_duration_ms(const bagl_element_t* e, unsigned int average_char_width) {
c0d0170c:	b580      	push	{r7, lr}
c0d0170e:	460a      	mov	r2, r1
  return bagl_label_roundtrip_duration_ms_buf(e, e->text, average_char_width);
c0d01710:	69c1      	ldr	r1, [r0, #28]
c0d01712:	f000 f801 	bl	c0d01718 <bagl_label_roundtrip_duration_ms_buf>
c0d01716:	bd80      	pop	{r7, pc}

c0d01718 <bagl_label_roundtrip_duration_ms_buf>:
}

unsigned int bagl_label_roundtrip_duration_ms_buf(const bagl_element_t* e, const char* str, unsigned int average_char_width) {
c0d01718:	b570      	push	{r4, r5, r6, lr}
c0d0171a:	4616      	mov	r6, r2
c0d0171c:	4604      	mov	r4, r0
c0d0171e:	2500      	movs	r5, #0
  // not a scrollable label
  if (e == NULL || (e->component.type != BAGL_LABEL && e->component.type != BAGL_LABELINE)) {
c0d01720:	2c00      	cmp	r4, #0
c0d01722:	d01c      	beq.n	c0d0175e <bagl_label_roundtrip_duration_ms_buf+0x46>
c0d01724:	7820      	ldrb	r0, [r4, #0]
c0d01726:	2807      	cmp	r0, #7
c0d01728:	d001      	beq.n	c0d0172e <bagl_label_roundtrip_duration_ms_buf+0x16>
c0d0172a:	2802      	cmp	r0, #2
c0d0172c:	d117      	bne.n	c0d0175e <bagl_label_roundtrip_duration_ms_buf+0x46>
    return 0;
  }
  
  unsigned int text_adr = PIC((unsigned int)str);
c0d0172e:	4608      	mov	r0, r1
c0d01730:	f000 f95c 	bl	c0d019ec <pic>
  unsigned int textlen = 0;
  
  // no delay, no text to display
  if (!text_adr) {
c0d01734:	2800      	cmp	r0, #0
c0d01736:	d012      	beq.n	c0d0175e <bagl_label_roundtrip_duration_ms_buf+0x46>
    return 0;
  }
  textlen = strlen((const char*)text_adr);
c0d01738:	f001 fac6 	bl	c0d02cc8 <strlen>
  
  // no delay, all text fits
  textlen = textlen * average_char_width;
c0d0173c:	4346      	muls	r6, r0
  if (textlen <= e->component.width) {
c0d0173e:	88e0      	ldrh	r0, [r4, #6]
c0d01740:	4286      	cmp	r6, r0
c0d01742:	d90c      	bls.n	c0d0175e <bagl_label_roundtrip_duration_ms_buf+0x46>
    return 0; 
  }
  
  // compute scrolled text length
  return 2*(textlen - e->component.width)*1000/e->component.icon_id + 2*(e->component.stroke & ~(0x80))*100;
c0d01744:	1a31      	subs	r1, r6, r0
c0d01746:	207d      	movs	r0, #125	; 0x7d
c0d01748:	0100      	lsls	r0, r0, #4
c0d0174a:	4348      	muls	r0, r1
c0d0174c:	7ea1      	ldrb	r1, [r4, #26]
c0d0174e:	f001 f9c1 	bl	c0d02ad4 <__aeabi_uidiv>
c0d01752:	7aa1      	ldrb	r1, [r4, #10]
c0d01754:	0049      	lsls	r1, r1, #1
c0d01756:	b2c9      	uxtb	r1, r1
c0d01758:	2264      	movs	r2, #100	; 0x64
c0d0175a:	434a      	muls	r2, r1
c0d0175c:	1815      	adds	r5, r2, r0
}
c0d0175e:	4628      	mov	r0, r5
c0d01760:	bd70      	pop	{r4, r5, r6, pc}
	...

c0d01764 <io_seproxyhal_setup_ticker>:

void io_seproxyhal_setup_ticker(unsigned int interval_ms) {
c0d01764:	b580      	push	{r7, lr}
  G_io_seproxyhal_spi_buffer[0] = SEPROXYHAL_TAG_SET_TICKER_INTERVAL;
c0d01766:	4a07      	ldr	r2, [pc, #28]	; (c0d01784 <io_seproxyhal_setup_ticker+0x20>)
c0d01768:	214e      	movs	r1, #78	; 0x4e
c0d0176a:	7011      	strb	r1, [r2, #0]
  G_io_seproxyhal_spi_buffer[1] = 0;
c0d0176c:	2100      	movs	r1, #0
c0d0176e:	7051      	strb	r1, [r2, #1]
  G_io_seproxyhal_spi_buffer[2] = 2;
c0d01770:	2102      	movs	r1, #2
c0d01772:	7091      	strb	r1, [r2, #2]
  G_io_seproxyhal_spi_buffer[3] = (interval_ms>>8)&0xff;
c0d01774:	0a01      	lsrs	r1, r0, #8
c0d01776:	70d1      	strb	r1, [r2, #3]
  G_io_seproxyhal_spi_buffer[4] = (interval_ms)&0xff;
c0d01778:	7110      	strb	r0, [r2, #4]
  io_seproxyhal_spi_send(G_io_seproxyhal_spi_buffer, 5);
c0d0177a:	2105      	movs	r1, #5
c0d0177c:	4610      	mov	r0, r2
c0d0177e:	f000 fa81 	bl	c0d01c84 <io_seproxyhal_spi_send>
}
c0d01782:	bd80      	pop	{r7, pc}
c0d01784:	20001804 	.word	0x20001804

c0d01788 <io_seproxyhal_button_push>:
  G_io_seproxyhal_spi_buffer[3] = (backlight_percentage?0x80:0)|(flags & 0x7F); // power on
  G_io_seproxyhal_spi_buffer[4] = backlight_percentage;
  io_seproxyhal_spi_send(G_io_seproxyhal_spi_buffer, 5);
}

void io_seproxyhal_button_push(button_push_callback_t button_callback, unsigned int new_button_mask) {
c0d01788:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d0178a:	b081      	sub	sp, #4
c0d0178c:	4604      	mov	r4, r0
  if (button_callback) {
c0d0178e:	2c00      	cmp	r4, #0
c0d01790:	d02b      	beq.n	c0d017ea <io_seproxyhal_button_push+0x62>
    unsigned int button_mask;
    unsigned int button_same_mask_counter;
    // enable speeded up long push
    if (new_button_mask == G_button_mask) {
c0d01792:	4817      	ldr	r0, [pc, #92]	; (c0d017f0 <io_seproxyhal_button_push+0x68>)
c0d01794:	6802      	ldr	r2, [r0, #0]
c0d01796:	428a      	cmp	r2, r1
c0d01798:	d103      	bne.n	c0d017a2 <io_seproxyhal_button_push+0x1a>
      // each 100ms ~
      G_button_same_mask_counter++;
c0d0179a:	4a16      	ldr	r2, [pc, #88]	; (c0d017f4 <io_seproxyhal_button_push+0x6c>)
c0d0179c:	6813      	ldr	r3, [r2, #0]
c0d0179e:	1c5b      	adds	r3, r3, #1
c0d017a0:	6013      	str	r3, [r2, #0]
    }

    // append the button mask
    button_mask = G_button_mask | new_button_mask;
c0d017a2:	6806      	ldr	r6, [r0, #0]
c0d017a4:	430e      	orrs	r6, r1

    // pre reset variable due to os_sched_exit
    button_same_mask_counter = G_button_same_mask_counter;
c0d017a6:	4a13      	ldr	r2, [pc, #76]	; (c0d017f4 <io_seproxyhal_button_push+0x6c>)
c0d017a8:	6815      	ldr	r5, [r2, #0]
c0d017aa:	4f13      	ldr	r7, [pc, #76]	; (c0d017f8 <io_seproxyhal_button_push+0x70>)

    // reset button mask
    if (new_button_mask == 0) {
c0d017ac:	2900      	cmp	r1, #0
c0d017ae:	d001      	beq.n	c0d017b4 <io_seproxyhal_button_push+0x2c>

      // notify button released event
      button_mask |= BUTTON_EVT_RELEASED;
    }
    else {
      G_button_mask = button_mask;
c0d017b0:	6006      	str	r6, [r0, #0]
c0d017b2:	e004      	b.n	c0d017be <io_seproxyhal_button_push+0x36>
c0d017b4:	2300      	movs	r3, #0
    button_same_mask_counter = G_button_same_mask_counter;

    // reset button mask
    if (new_button_mask == 0) {
      // reset next state when button are released
      G_button_mask = 0;
c0d017b6:	6003      	str	r3, [r0, #0]
      G_button_same_mask_counter=0;
c0d017b8:	6013      	str	r3, [r2, #0]

      // notify button released event
      button_mask |= BUTTON_EVT_RELEASED;
c0d017ba:	1c7b      	adds	r3, r7, #1
c0d017bc:	431e      	orrs	r6, r3
    else {
      G_button_mask = button_mask;
    }

    // reset counter when button mask changes
    if (new_button_mask != G_button_mask) {
c0d017be:	6800      	ldr	r0, [r0, #0]
c0d017c0:	4288      	cmp	r0, r1
c0d017c2:	d001      	beq.n	c0d017c8 <io_seproxyhal_button_push+0x40>
      G_button_same_mask_counter=0;
c0d017c4:	2000      	movs	r0, #0
c0d017c6:	6010      	str	r0, [r2, #0]
    }

    if (button_same_mask_counter >= BUTTON_FAST_THRESHOLD_CS) {
c0d017c8:	2d08      	cmp	r5, #8
c0d017ca:	d30b      	bcc.n	c0d017e4 <io_seproxyhal_button_push+0x5c>
      // fast bit when pressing and timing is right
      if ((button_same_mask_counter%BUTTON_FAST_ACTION_CS) == 0) {
c0d017cc:	2103      	movs	r1, #3
c0d017ce:	4628      	mov	r0, r5
c0d017d0:	f001 fa06 	bl	c0d02be0 <__aeabi_uidivmod>
        button_mask |= BUTTON_EVT_FAST;
c0d017d4:	2001      	movs	r0, #1
c0d017d6:	0780      	lsls	r0, r0, #30
c0d017d8:	4330      	orrs	r0, r6
      G_button_same_mask_counter=0;
    }

    if (button_same_mask_counter >= BUTTON_FAST_THRESHOLD_CS) {
      // fast bit when pressing and timing is right
      if ((button_same_mask_counter%BUTTON_FAST_ACTION_CS) == 0) {
c0d017da:	2900      	cmp	r1, #0
c0d017dc:	d000      	beq.n	c0d017e0 <io_seproxyhal_button_push+0x58>
c0d017de:	4630      	mov	r0, r6
      }
      */

      // discard the release event after a fastskip has been detected, to avoid strange at release behavior
      // and also to enable user to cancel an operation by starting triggering the fast skip
      button_mask &= ~BUTTON_EVT_RELEASED;
c0d017e0:	4038      	ands	r0, r7
c0d017e2:	e000      	b.n	c0d017e6 <io_seproxyhal_button_push+0x5e>
c0d017e4:	4630      	mov	r0, r6
    }

    // indicate if button have been released
    button_callback(button_mask, button_same_mask_counter);
c0d017e6:	4629      	mov	r1, r5
c0d017e8:	47a0      	blx	r4
  }
}
c0d017ea:	b001      	add	sp, #4
c0d017ec:	bdf0      	pop	{r4, r5, r6, r7, pc}
c0d017ee:	46c0      	nop			; (mov r8, r8)
c0d017f0:	20001c60 	.word	0x20001c60
c0d017f4:	20001c64 	.word	0x20001c64
c0d017f8:	7fffffff 	.word	0x7fffffff

c0d017fc <io_exchange>:

#ifdef HAVE_IO_U2F
u2f_service_t G_io_u2f;
#endif // HAVE_IO_U2F

unsigned short io_exchange(unsigned char channel, unsigned short tx_len) {
c0d017fc:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d017fe:	b081      	sub	sp, #4
c0d01800:	460d      	mov	r5, r1
c0d01802:	4604      	mov	r4, r0
    }
  }
  after_debug:
#endif // DEBUG_APDU

  switch(channel&~(IO_FLAGS)) {
c0d01804:	200f      	movs	r0, #15
c0d01806:	4204      	tst	r4, r0
c0d01808:	d007      	beq.n	c0d0181a <io_exchange+0x1e>
      }
    }
    break;

  default:
    return io_exchange_al(channel, tx_len);
c0d0180a:	4620      	mov	r0, r4
c0d0180c:	4629      	mov	r1, r5
c0d0180e:	f7fe fc47 	bl	c0d000a0 <io_exchange_al>
c0d01812:	4605      	mov	r5, r0
  }
}
c0d01814:	b2a8      	uxth	r0, r5
c0d01816:	b001      	add	sp, #4
c0d01818:	bdf0      	pop	{r4, r5, r6, r7, pc}

  switch(channel&~(IO_FLAGS)) {
  case CHANNEL_APDU:
    // TODO work up the spi state machine over the HAL proxy until an APDU is available

    if (tx_len && !(channel&IO_ASYNCH_REPLY)) {
c0d0181a:	2610      	movs	r6, #16
c0d0181c:	4026      	ands	r6, r4
c0d0181e:	2d00      	cmp	r5, #0
c0d01820:	d041      	beq.n	c0d018a6 <io_exchange+0xaa>
c0d01822:	2e00      	cmp	r6, #0
c0d01824:	d13f      	bne.n	c0d018a6 <io_exchange+0xaa>
      // prepare response timeout
      G_io_timeout = IO_RAPDU_TRANSMIT_TIMEOUT_MS;
c0d01826:	207d      	movs	r0, #125	; 0x7d
c0d01828:	0100      	lsls	r0, r0, #4
c0d0182a:	494d      	ldr	r1, [pc, #308]	; (c0d01960 <io_exchange+0x164>)
c0d0182c:	6008      	str	r0, [r1, #0]

      // until the whole RAPDU is transmitted, send chunks using the current mode for communication
      for (;;) {
        switch(G_io_apdu_state) {
c0d0182e:	4f4d      	ldr	r7, [pc, #308]	; (c0d01964 <io_exchange+0x168>)
c0d01830:	7838      	ldrb	r0, [r7, #0]
c0d01832:	2807      	cmp	r0, #7
c0d01834:	d00c      	beq.n	c0d01850 <io_exchange+0x54>
c0d01836:	280a      	cmp	r0, #10
c0d01838:	d012      	beq.n	c0d01860 <io_exchange+0x64>
c0d0183a:	2800      	cmp	r0, #0
c0d0183c:	d005      	beq.n	c0d0184a <io_exchange+0x4e>
          default: 
            // delegate to the hal in case of not generic transport mode (or asynch)
            if (io_exchange_al(channel, tx_len) == 0) {
c0d0183e:	4620      	mov	r0, r4
c0d01840:	4629      	mov	r1, r5
c0d01842:	f7fe fc2d 	bl	c0d000a0 <io_exchange_al>
c0d01846:	2800      	cmp	r0, #0
c0d01848:	d01b      	beq.n	c0d01882 <io_exchange+0x86>
              goto break_send;
            }
          case APDU_IDLE:
            LOG("invalid state for APDU reply\n");
            THROW(INVALID_STATE);
c0d0184a:	2009      	movs	r0, #9
c0d0184c:	f7ff fc74 	bl	c0d01138 <os_longjmp>
            goto break_send;

#ifdef HAVE_USB_APDU
          case APDU_USB_HID:
            // only send, don't perform synchronous reception of the next command (will be done later by the seproxyhal packet processing)
            io_usb_hid_exchange(io_usb_send_apdu_data, tx_len, NULL, IO_RETURN_AFTER_TX);
c0d01850:	484d      	ldr	r0, [pc, #308]	; (c0d01988 <io_exchange+0x18c>)
c0d01852:	4478      	add	r0, pc
c0d01854:	2200      	movs	r2, #0
c0d01856:	2320      	movs	r3, #32
c0d01858:	4629      	mov	r1, r5
c0d0185a:	f7ff fbf1 	bl	c0d01040 <io_usb_hid_exchange>
c0d0185e:	e010      	b.n	c0d01882 <io_exchange+0x86>
            LOG("invalid state for APDU reply\n");
            THROW(INVALID_STATE);
            break;

          case APDU_RAW:
            if (tx_len > sizeof(G_io_apdu_buffer)) {
c0d01860:	20ff      	movs	r0, #255	; 0xff
c0d01862:	3006      	adds	r0, #6
c0d01864:	4285      	cmp	r5, r0
c0d01866:	d278      	bcs.n	c0d0195a <io_exchange+0x15e>
              THROW(INVALID_PARAMETER);
            }
            // reply the RAW APDU over SEPROXYHAL protocol
            G_io_seproxyhal_spi_buffer[0]  = SEPROXYHAL_TAG_RAPDU;
c0d01868:	483f      	ldr	r0, [pc, #252]	; (c0d01968 <io_exchange+0x16c>)
c0d0186a:	2153      	movs	r1, #83	; 0x53
c0d0186c:	7001      	strb	r1, [r0, #0]
            G_io_seproxyhal_spi_buffer[1]  = (tx_len)>>8;
c0d0186e:	0a29      	lsrs	r1, r5, #8
c0d01870:	7041      	strb	r1, [r0, #1]
            G_io_seproxyhal_spi_buffer[2]  = (tx_len);
c0d01872:	7085      	strb	r5, [r0, #2]
            io_seproxyhal_spi_send(G_io_seproxyhal_spi_buffer, 3);
c0d01874:	2103      	movs	r1, #3
c0d01876:	f000 fa05 	bl	c0d01c84 <io_seproxyhal_spi_send>
            io_seproxyhal_spi_send(G_io_apdu_buffer, tx_len);
c0d0187a:	483c      	ldr	r0, [pc, #240]	; (c0d0196c <io_exchange+0x170>)
c0d0187c:	4629      	mov	r1, r5
c0d0187e:	f000 fa01 	bl	c0d01c84 <io_seproxyhal_spi_send>
c0d01882:	2500      	movs	r5, #0
        }
        continue;

      break_send:
        // reset apdu state
        G_io_apdu_state = APDU_IDLE;
c0d01884:	703d      	strb	r5, [r7, #0]
        G_io_apdu_offset = 0;
c0d01886:	483a      	ldr	r0, [pc, #232]	; (c0d01970 <io_exchange+0x174>)
c0d01888:	8005      	strh	r5, [r0, #0]
        G_io_apdu_length = 0;
c0d0188a:	483a      	ldr	r0, [pc, #232]	; (c0d01974 <io_exchange+0x178>)
c0d0188c:	8005      	strh	r5, [r0, #0]
        G_io_apdu_seq = 0;
c0d0188e:	483a      	ldr	r0, [pc, #232]	; (c0d01978 <io_exchange+0x17c>)
c0d01890:	8005      	strh	r5, [r0, #0]
        G_io_apdu_media = IO_APDU_MEDIA_NONE;
c0d01892:	483a      	ldr	r0, [pc, #232]	; (c0d0197c <io_exchange+0x180>)
c0d01894:	7005      	strb	r5, [r0, #0]

        // continue sending commands, don't issue status yet
        if (channel & IO_RETURN_AFTER_TX) {
c0d01896:	06a0      	lsls	r0, r4, #26
c0d01898:	d4bc      	bmi.n	c0d01814 <io_exchange+0x18>
          return 0;
        }
        // acknowledge the write request (general status OK) and no more command to follow (wait until another APDU container is received to continue unwrapping)
        io_seproxyhal_general_status();
c0d0189a:	f7ff fc5b 	bl	c0d01154 <io_seproxyhal_general_status>
        break;
      }

      // perform reset after io exchange
      if (channel & IO_RESET_AFTER_REPLIED) {
c0d0189e:	0620      	lsls	r0, r4, #24
c0d018a0:	d501      	bpl.n	c0d018a6 <io_exchange+0xaa>
        reset();
c0d018a2:	f000 f8d1 	bl	c0d01a48 <reset>
      }
    }

    if (!(channel&IO_ASYNCH_REPLY)) {
c0d018a6:	2e00      	cmp	r6, #0
c0d018a8:	d10c      	bne.n	c0d018c4 <io_exchange+0xc8>
      
      // already received the data of the apdu when received the whole apdu
      if ((channel & (CHANNEL_APDU|IO_RECEIVE_DATA)) == (CHANNEL_APDU|IO_RECEIVE_DATA)) {
c0d018aa:	0660      	lsls	r0, r4, #25
c0d018ac:	d450      	bmi.n	c0d01950 <io_exchange+0x154>
        // return apdu data - header
        return G_io_apdu_length-5;
      }

      // reply has ended, proceed to next apdu reception (reset status only after asynch reply)
      G_io_apdu_state = APDU_IDLE;
c0d018ae:	482d      	ldr	r0, [pc, #180]	; (c0d01964 <io_exchange+0x168>)
c0d018b0:	2100      	movs	r1, #0
c0d018b2:	7001      	strb	r1, [r0, #0]
      G_io_apdu_offset = 0;
c0d018b4:	482e      	ldr	r0, [pc, #184]	; (c0d01970 <io_exchange+0x174>)
c0d018b6:	8001      	strh	r1, [r0, #0]
      G_io_apdu_length = 0;
c0d018b8:	482e      	ldr	r0, [pc, #184]	; (c0d01974 <io_exchange+0x178>)
c0d018ba:	8001      	strh	r1, [r0, #0]
      G_io_apdu_seq = 0;
c0d018bc:	482e      	ldr	r0, [pc, #184]	; (c0d01978 <io_exchange+0x17c>)
c0d018be:	8001      	strh	r1, [r0, #0]
      G_io_apdu_media = IO_APDU_MEDIA_NONE;
c0d018c0:	482e      	ldr	r0, [pc, #184]	; (c0d0197c <io_exchange+0x180>)
c0d018c2:	7001      	strb	r1, [r0, #0]
c0d018c4:	4c28      	ldr	r4, [pc, #160]	; (c0d01968 <io_exchange+0x16c>)
c0d018c6:	4e2b      	ldr	r6, [pc, #172]	; (c0d01974 <io_exchange+0x178>)
c0d018c8:	4f2b      	ldr	r7, [pc, #172]	; (c0d01978 <io_exchange+0x17c>)
c0d018ca:	e002      	b.n	c0d018d2 <io_exchange+0xd6>
          break;
#endif // HAVE_IO_USB

        default:
          // tell the application that a non-apdu packet has been received
          io_event(CHANNEL_SPI);
c0d018cc:	2002      	movs	r0, #2
c0d018ce:	f7fe fc0d 	bl	c0d000ec <io_event>

    // ensure ready to receive an event (after an apdu processing with asynch flag, it may occur if the channel is not correctly managed)

    // until a new whole CAPDU is received
    for (;;) {
      if (!io_seproxyhal_spi_is_status_sent()) {
c0d018d2:	f000 f9ed 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d018d6:	2800      	cmp	r0, #0
c0d018d8:	d101      	bne.n	c0d018de <io_exchange+0xe2>
        io_seproxyhal_general_status();
c0d018da:	f7ff fc3b 	bl	c0d01154 <io_seproxyhal_general_status>
      }

      // wait until a SPI packet is available
      // NOTE: on ST31, dual wait ISO & RF (ISO instead of SPI)
      rx_len = io_seproxyhal_spi_recv(G_io_seproxyhal_spi_buffer, sizeof(G_io_seproxyhal_spi_buffer), 0);
c0d018de:	2180      	movs	r1, #128	; 0x80
c0d018e0:	2500      	movs	r5, #0
c0d018e2:	4620      	mov	r0, r4
c0d018e4:	462a      	mov	r2, r5
c0d018e6:	f000 f9f9 	bl	c0d01cdc <io_seproxyhal_spi_recv>

      // can't process split TLV, continue
      if (rx_len-3 != U2(G_io_seproxyhal_spi_buffer[1],G_io_seproxyhal_spi_buffer[2])) {
c0d018ea:	1ec1      	subs	r1, r0, #3
c0d018ec:	78a2      	ldrb	r2, [r4, #2]
c0d018ee:	7863      	ldrb	r3, [r4, #1]
c0d018f0:	021b      	lsls	r3, r3, #8
c0d018f2:	4313      	orrs	r3, r2
c0d018f4:	4299      	cmp	r1, r3
c0d018f6:	d115      	bne.n	c0d01924 <io_exchange+0x128>
      send_last_command:
        continue;
      }

      // if an apdu is already ongoing, then discard packet as a new packet
      if (G_io_apdu_media != IO_APDU_MEDIA_NONE) {
c0d018f8:	4920      	ldr	r1, [pc, #128]	; (c0d0197c <io_exchange+0x180>)
c0d018fa:	7809      	ldrb	r1, [r1, #0]
c0d018fc:	2900      	cmp	r1, #0
c0d018fe:	d002      	beq.n	c0d01906 <io_exchange+0x10a>
        io_seproxyhal_handle_event();
c0d01900:	f7ff fce2 	bl	c0d012c8 <io_seproxyhal_handle_event>
c0d01904:	e7e5      	b.n	c0d018d2 <io_exchange+0xd6>
        continue;
      }

      // depending on received TAG
      switch(G_io_seproxyhal_spi_buffer[0]) {
c0d01906:	7821      	ldrb	r1, [r4, #0]
c0d01908:	290f      	cmp	r1, #15
c0d0190a:	d006      	beq.n	c0d0191a <io_exchange+0x11e>
c0d0190c:	2910      	cmp	r1, #16
c0d0190e:	d011      	beq.n	c0d01934 <io_exchange+0x138>
c0d01910:	2916      	cmp	r1, #22
c0d01912:	d1db      	bne.n	c0d018cc <io_exchange+0xd0>

        case SEPROXYHAL_TAG_CAPDU_EVENT:
          io_seproxyhal_handle_capdu_event();
c0d01914:	f7ff fd24 	bl	c0d01360 <io_seproxyhal_handle_capdu_event>
c0d01918:	e011      	b.n	c0d0193e <io_exchange+0x142>
          goto send_last_command;
#endif // HAVE_BLE

#ifdef HAVE_IO_USB
        case SEPROXYHAL_TAG_USB_EVENT:
          if (rx_len != 3+1) {
c0d0191a:	2804      	cmp	r0, #4
c0d0191c:	d102      	bne.n	c0d01924 <io_exchange+0x128>
            // invalid length, not processable
            goto invalid_apdu_packet;
          }
          io_seproxyhal_handle_usb_event();
c0d0191e:	f7ff fc2d 	bl	c0d0117c <io_seproxyhal_handle_usb_event>
c0d01922:	e7d6      	b.n	c0d018d2 <io_exchange+0xd6>
c0d01924:	2000      	movs	r0, #0

      // can't process split TLV, continue
      if (rx_len-3 != U2(G_io_seproxyhal_spi_buffer[1],G_io_seproxyhal_spi_buffer[2])) {
        LOG("invalid TLV format\n");
      invalid_apdu_packet:
        G_io_apdu_state = APDU_IDLE;
c0d01926:	490f      	ldr	r1, [pc, #60]	; (c0d01964 <io_exchange+0x168>)
c0d01928:	7008      	strb	r0, [r1, #0]
        G_io_apdu_offset = 0;
c0d0192a:	4911      	ldr	r1, [pc, #68]	; (c0d01970 <io_exchange+0x174>)
c0d0192c:	8008      	strh	r0, [r1, #0]
        G_io_apdu_length = 0;
c0d0192e:	8030      	strh	r0, [r6, #0]
        G_io_apdu_seq = 0;
c0d01930:	8038      	strh	r0, [r7, #0]
c0d01932:	e7ce      	b.n	c0d018d2 <io_exchange+0xd6>

          // no state change, we're not dealing with an apdu yet
          goto send_last_command;

        case SEPROXYHAL_TAG_USB_EP_XFER_EVENT:
          if (rx_len < 3+3) {
c0d01934:	2806      	cmp	r0, #6
c0d01936:	d200      	bcs.n	c0d0193a <io_exchange+0x13e>
c0d01938:	e76c      	b.n	c0d01814 <io_exchange+0x18>
            // error !
            return 0;
          }
          io_seproxyhal_handle_usb_ep_xfer_event();
c0d0193a:	f7ff fc55 	bl	c0d011e8 <io_seproxyhal_handle_usb_ep_xfer_event>
c0d0193e:	8830      	ldrh	r0, [r6, #0]
c0d01940:	2800      	cmp	r0, #0
c0d01942:	d0c6      	beq.n	c0d018d2 <io_exchange+0xd6>
c0d01944:	480f      	ldr	r0, [pc, #60]	; (c0d01984 <io_exchange+0x188>)
c0d01946:	6800      	ldr	r0, [r0, #0]
c0d01948:	4905      	ldr	r1, [pc, #20]	; (c0d01960 <io_exchange+0x164>)
c0d0194a:	6008      	str	r0, [r1, #0]
c0d0194c:	8835      	ldrh	r5, [r6, #0]
c0d0194e:	e761      	b.n	c0d01814 <io_exchange+0x18>
    if (!(channel&IO_ASYNCH_REPLY)) {
      
      // already received the data of the apdu when received the whole apdu
      if ((channel & (CHANNEL_APDU|IO_RECEIVE_DATA)) == (CHANNEL_APDU|IO_RECEIVE_DATA)) {
        // return apdu data - header
        return G_io_apdu_length-5;
c0d01950:	4808      	ldr	r0, [pc, #32]	; (c0d01974 <io_exchange+0x178>)
c0d01952:	8800      	ldrh	r0, [r0, #0]
c0d01954:	490a      	ldr	r1, [pc, #40]	; (c0d01980 <io_exchange+0x184>)
c0d01956:	1845      	adds	r5, r0, r1
c0d01958:	e75c      	b.n	c0d01814 <io_exchange+0x18>
            THROW(INVALID_STATE);
            break;

          case APDU_RAW:
            if (tx_len > sizeof(G_io_apdu_buffer)) {
              THROW(INVALID_PARAMETER);
c0d0195a:	2002      	movs	r0, #2
c0d0195c:	f7ff fbec 	bl	c0d01138 <os_longjmp>
c0d01960:	20001c48 	.word	0x20001c48
c0d01964:	20001c54 	.word	0x20001c54
c0d01968:	20001804 	.word	0x20001804
c0d0196c:	20001b38 	.word	0x20001b38
c0d01970:	20001c58 	.word	0x20001c58
c0d01974:	20001c56 	.word	0x20001c56
c0d01978:	20001c5a 	.word	0x20001c5a
c0d0197c:	20001c4c 	.word	0x20001c4c
c0d01980:	0000fffb 	.word	0x0000fffb
c0d01984:	20001c44 	.word	0x20001c44
c0d01988:	fffffafb 	.word	0xfffffafb

c0d0198c <os_ux_blocking>:
  default:
    return io_exchange_al(channel, tx_len);
  }
}

unsigned int os_ux_blocking(bolos_ux_params_t* params) {
c0d0198c:	b570      	push	{r4, r5, r6, lr}
c0d0198e:	4604      	mov	r4, r0
c0d01990:	4d13      	ldr	r5, [pc, #76]	; (c0d019e0 <os_ux_blocking+0x54>)
c0d01992:	e01a      	b.n	c0d019ca <os_ux_blocking+0x3e>
     || ret == BOLOS_UX_CONTINUE) {

    // process events
    for (;;) {
      // send general status before receiving next event
      if (!io_seproxyhal_spi_is_status_sent()) {
c0d01994:	f000 f98c 	bl	c0d01cb0 <io_seproxyhal_spi_is_status_sent>
c0d01998:	2800      	cmp	r0, #0
c0d0199a:	d101      	bne.n	c0d019a0 <os_ux_blocking+0x14>
        io_seproxyhal_general_status();
c0d0199c:	f7ff fbda 	bl	c0d01154 <io_seproxyhal_general_status>
      }

      /*unsigned int rx_len = */io_seproxyhal_spi_recv(G_io_seproxyhal_spi_buffer, sizeof(G_io_seproxyhal_spi_buffer), 0);
c0d019a0:	2180      	movs	r1, #128	; 0x80
c0d019a2:	2600      	movs	r6, #0
c0d019a4:	4628      	mov	r0, r5
c0d019a6:	4632      	mov	r2, r6
c0d019a8:	f000 f998 	bl	c0d01cdc <io_seproxyhal_spi_recv>

      switch (G_io_seproxyhal_spi_buffer[0]) {
c0d019ac:	7828      	ldrb	r0, [r5, #0]
c0d019ae:	2815      	cmp	r0, #21
c0d019b0:	d804      	bhi.n	c0d019bc <os_ux_blocking+0x30>
c0d019b2:	2101      	movs	r1, #1
c0d019b4:	4081      	lsls	r1, r0
c0d019b6:	480b      	ldr	r0, [pc, #44]	; (c0d019e4 <os_ux_blocking+0x58>)
c0d019b8:	4201      	tst	r1, r0
c0d019ba:	d103      	bne.n	c0d019c4 <os_ux_blocking+0x38>
          // perform UX event on these ones, don't process as an IO event
          break;

        default:
          // if malformed, then a stall is likely to occur
          if (io_seproxyhal_handle_event()) {
c0d019bc:	f7ff fc84 	bl	c0d012c8 <io_seproxyhal_handle_event>
c0d019c0:	2800      	cmp	r0, #0
c0d019c2:	d1e7      	bne.n	c0d01994 <os_ux_blocking+0x8>

      // pass the packet to the ux
      break;
    }
    // prepare processing of the packet by the ux
    params->ux_id = BOLOS_UX_EVENT;
c0d019c4:	2001      	movs	r0, #1
c0d019c6:	7020      	strb	r0, [r4, #0]
    params->len = 0;
c0d019c8:	6066      	str	r6, [r4, #4]
    ret = os_ux(params);
c0d019ca:	4620      	mov	r0, r4
c0d019cc:	f000 f944 	bl	c0d01c58 <os_ux>
unsigned int os_ux_blocking(bolos_ux_params_t* params) {
  unsigned int ret;
  
  // until a real status is returned
  ret = os_ux(params);
  while(ret == BOLOS_UX_IGNORE 
c0d019d0:	2800      	cmp	r0, #0
c0d019d2:	d0df      	beq.n	c0d01994 <os_ux_blocking+0x8>
c0d019d4:	4901      	ldr	r1, [pc, #4]	; (c0d019dc <os_ux_blocking+0x50>)
c0d019d6:	4288      	cmp	r0, r1
c0d019d8:	d0dc      	beq.n	c0d01994 <os_ux_blocking+0x8>
    params->ux_id = BOLOS_UX_EVENT;
    params->len = 0;
    ret = os_ux(params);
  }

  return ret;
c0d019da:	bd70      	pop	{r4, r5, r6, pc}
c0d019dc:	b0105044 	.word	0xb0105044
c0d019e0:	20001804 	.word	0x20001804
c0d019e4:	00207020 	.word	0x00207020

c0d019e8 <ux_check_status_default>:
}

void ux_check_status_default(unsigned int status) {
  // nothing to be done here by default.
  UNUSED(status);
}
c0d019e8:	4770      	bx	lr
	...

c0d019ec <pic>:

// only apply PIC conversion if link_address is in linked code (over 0xC0D00000 in our example)
// this way, PIC call are armless if the address is not meant to be converted
extern unsigned int _nvram;
extern unsigned int _envram;
unsigned int pic(unsigned int link_address) {
c0d019ec:	b580      	push	{r7, lr}
//  screen_printf(" %08X", link_address);
	if (link_address >= ((unsigned int)&_nvram) && link_address < ((unsigned int)&_envram)) {
c0d019ee:	4904      	ldr	r1, [pc, #16]	; (c0d01a00 <pic+0x14>)
c0d019f0:	4288      	cmp	r0, r1
c0d019f2:	d304      	bcc.n	c0d019fe <pic+0x12>
c0d019f4:	4903      	ldr	r1, [pc, #12]	; (c0d01a04 <pic+0x18>)
c0d019f6:	4288      	cmp	r0, r1
c0d019f8:	d201      	bcs.n	c0d019fe <pic+0x12>
		link_address = pic_internal(link_address);
c0d019fa:	f000 f805 	bl	c0d01a08 <pic_internal>
//    screen_printf(" -> %08X\n", link_address);
  }
	return link_address;
c0d019fe:	bd80      	pop	{r7, pc}
c0d01a00:	c0d00000 	.word	0xc0d00000
c0d01a04:	c0d030c0 	.word	0xc0d030c0

c0d01a08 <pic_internal>:

unsigned int pic_internal(unsigned int link_address) __attribute__((naked));
unsigned int pic_internal(unsigned int link_address) 
{
  // compute the delta offset between LinkMemAddr & ExecMemAddr
  __asm volatile ("mov r2, pc\n");          // r2 = 0x109004
c0d01a08:	467a      	mov	r2, pc
  __asm volatile ("ldr r1, =pic_internal\n");        // r1 = 0xC0D00001
c0d01a0a:	4902      	ldr	r1, [pc, #8]	; (c0d01a14 <pic_internal+0xc>)
  __asm volatile ("adds r1, r1, #3\n");     // r1 = 0xC0D00004
c0d01a0c:	1cc9      	adds	r1, r1, #3
  __asm volatile ("subs r1, r1, r2\n");     // r1 = 0xC0BF7000 (delta between load and exec address)
c0d01a0e:	1a89      	subs	r1, r1, r2

  // adjust value of the given parameter
  __asm volatile ("subs r0, r0, r1\n");     // r0 = 0xC0D0C244 => r0 = 0x115244
c0d01a10:	1a40      	subs	r0, r0, r1
  __asm volatile ("bx lr\n");
c0d01a12:	4770      	bx	lr
c0d01a14:	c0d01a09 	.word	0xc0d01a09

c0d01a18 <SVC_Call>:
  // avoid a separate asm file, but avoid any intrusion from the compiler
  unsigned int SVC_Call(unsigned int syscall_id, unsigned int * parameters) __attribute__ ((naked));
  //                    r0                       r1
  unsigned int SVC_Call(unsigned int syscall_id, unsigned int * parameters) {
    // delegate svc
    asm volatile("svc #1":::"r0","r1");
c0d01a18:	df01      	svc	1
    // directly return R0 value
    asm volatile("bx  lr");
c0d01a1a:	4770      	bx	lr

c0d01a1c <check_api_level>:
  }
  void check_api_level ( unsigned int apiLevel ) 
{
c0d01a1c:	b580      	push	{r7, lr}
c0d01a1e:	b082      	sub	sp, #8
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0+1];
  parameters[0] = (unsigned int)apiLevel;
c0d01a20:	9000      	str	r0, [sp, #0]
  retid = SVC_Call(SYSCALL_check_api_level_ID_IN, parameters);
c0d01a22:	4807      	ldr	r0, [pc, #28]	; (c0d01a40 <check_api_level+0x24>)
c0d01a24:	4669      	mov	r1, sp
c0d01a26:	f7ff fff7 	bl	c0d01a18 <SVC_Call>
c0d01a2a:	aa01      	add	r2, sp, #4
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01a2c:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_check_api_level_ID_OUT) {
c0d01a2e:	4905      	ldr	r1, [pc, #20]	; (c0d01a44 <check_api_level+0x28>)
c0d01a30:	4288      	cmp	r0, r1
c0d01a32:	d101      	bne.n	c0d01a38 <check_api_level+0x1c>
    THROW(EXCEPTION_SECURITY);
  }
}
c0d01a34:	b002      	add	sp, #8
c0d01a36:	bd80      	pop	{r7, pc}
  unsigned int parameters [0+1];
  parameters[0] = (unsigned int)apiLevel;
  retid = SVC_Call(SYSCALL_check_api_level_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_check_api_level_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01a38:	2004      	movs	r0, #4
c0d01a3a:	f7ff fb7d 	bl	c0d01138 <os_longjmp>
c0d01a3e:	46c0      	nop			; (mov r8, r8)
c0d01a40:	60000137 	.word	0x60000137
c0d01a44:	900001c6 	.word	0x900001c6

c0d01a48 <reset>:
  }
}

void reset ( void ) 
{
c0d01a48:	b580      	push	{r7, lr}
c0d01a4a:	b082      	sub	sp, #8
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0];
  retid = SVC_Call(SYSCALL_reset_ID_IN, parameters);
c0d01a4c:	4806      	ldr	r0, [pc, #24]	; (c0d01a68 <reset+0x20>)
c0d01a4e:	a901      	add	r1, sp, #4
c0d01a50:	f7ff ffe2 	bl	c0d01a18 <SVC_Call>
c0d01a54:	466a      	mov	r2, sp
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01a56:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_reset_ID_OUT) {
c0d01a58:	4904      	ldr	r1, [pc, #16]	; (c0d01a6c <reset+0x24>)
c0d01a5a:	4288      	cmp	r0, r1
c0d01a5c:	d101      	bne.n	c0d01a62 <reset+0x1a>
    THROW(EXCEPTION_SECURITY);
  }
}
c0d01a5e:	b002      	add	sp, #8
c0d01a60:	bd80      	pop	{r7, pc}
  unsigned int retid;
  unsigned int parameters [0];
  retid = SVC_Call(SYSCALL_reset_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_reset_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01a62:	2004      	movs	r0, #4
c0d01a64:	f7ff fb68 	bl	c0d01138 <os_longjmp>
c0d01a68:	60000200 	.word	0x60000200
c0d01a6c:	900002f1 	.word	0x900002f1

c0d01a70 <cx_rng>:
  }
  return (unsigned char)ret;
}

unsigned char * cx_rng ( unsigned char * buffer, unsigned int len ) 
{
c0d01a70:	b580      	push	{r7, lr}
c0d01a72:	b084      	sub	sp, #16
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0+2];
  parameters[0] = (unsigned int)buffer;
c0d01a74:	9001      	str	r0, [sp, #4]
  parameters[1] = (unsigned int)len;
c0d01a76:	9102      	str	r1, [sp, #8]
  retid = SVC_Call(SYSCALL_cx_rng_ID_IN, parameters);
c0d01a78:	4807      	ldr	r0, [pc, #28]	; (c0d01a98 <cx_rng+0x28>)
c0d01a7a:	a901      	add	r1, sp, #4
c0d01a7c:	f7ff ffcc 	bl	c0d01a18 <SVC_Call>
c0d01a80:	aa03      	add	r2, sp, #12
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01a82:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_cx_rng_ID_OUT) {
c0d01a84:	4905      	ldr	r1, [pc, #20]	; (c0d01a9c <cx_rng+0x2c>)
c0d01a86:	4288      	cmp	r0, r1
c0d01a88:	d102      	bne.n	c0d01a90 <cx_rng+0x20>
    THROW(EXCEPTION_SECURITY);
  }
  return (unsigned char *)ret;
c0d01a8a:	9803      	ldr	r0, [sp, #12]
c0d01a8c:	b004      	add	sp, #16
c0d01a8e:	bd80      	pop	{r7, pc}
  parameters[0] = (unsigned int)buffer;
  parameters[1] = (unsigned int)len;
  retid = SVC_Call(SYSCALL_cx_rng_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_cx_rng_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01a90:	2004      	movs	r0, #4
c0d01a92:	f7ff fb51 	bl	c0d01138 <os_longjmp>
c0d01a96:	46c0      	nop			; (mov r8, r8)
c0d01a98:	6000052c 	.word	0x6000052c
c0d01a9c:	90000567 	.word	0x90000567

c0d01aa0 <cx_hash>:
  }
  return (int)ret;
}

int cx_hash ( cx_hash_t * hash, int mode, const unsigned char * in, unsigned int len, unsigned char * out, unsigned int out_len ) 
{
c0d01aa0:	b580      	push	{r7, lr}
c0d01aa2:	b088      	sub	sp, #32
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0+6];
  parameters[0] = (unsigned int)hash;
c0d01aa4:	af01      	add	r7, sp, #4
c0d01aa6:	c70f      	stmia	r7!, {r0, r1, r2, r3}
c0d01aa8:	980a      	ldr	r0, [sp, #40]	; 0x28
  parameters[1] = (unsigned int)mode;
  parameters[2] = (unsigned int)in;
  parameters[3] = (unsigned int)len;
  parameters[4] = (unsigned int)out;
c0d01aaa:	9005      	str	r0, [sp, #20]
c0d01aac:	980b      	ldr	r0, [sp, #44]	; 0x2c
  parameters[5] = (unsigned int)out_len;
c0d01aae:	9006      	str	r0, [sp, #24]
  retid = SVC_Call(SYSCALL_cx_hash_ID_IN, parameters);
c0d01ab0:	4807      	ldr	r0, [pc, #28]	; (c0d01ad0 <cx_hash+0x30>)
c0d01ab2:	a901      	add	r1, sp, #4
c0d01ab4:	f7ff ffb0 	bl	c0d01a18 <SVC_Call>
c0d01ab8:	aa07      	add	r2, sp, #28
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01aba:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_cx_hash_ID_OUT) {
c0d01abc:	4905      	ldr	r1, [pc, #20]	; (c0d01ad4 <cx_hash+0x34>)
c0d01abe:	4288      	cmp	r0, r1
c0d01ac0:	d102      	bne.n	c0d01ac8 <cx_hash+0x28>
    THROW(EXCEPTION_SECURITY);
  }
  return (int)ret;
c0d01ac2:	9807      	ldr	r0, [sp, #28]
c0d01ac4:	b008      	add	sp, #32
c0d01ac6:	bd80      	pop	{r7, pc}
  parameters[4] = (unsigned int)out;
  parameters[5] = (unsigned int)out_len;
  retid = SVC_Call(SYSCALL_cx_hash_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_cx_hash_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01ac8:	2004      	movs	r0, #4
c0d01aca:	f7ff fb35 	bl	c0d01138 <os_longjmp>
c0d01ace:	46c0      	nop			; (mov r8, r8)
c0d01ad0:	6000073b 	.word	0x6000073b
c0d01ad4:	900007ad 	.word	0x900007ad

c0d01ad8 <cx_ripemd160_init>:
  }
  return (int)ret;
}

int cx_ripemd160_init ( cx_ripemd160_t * hash ) 
{
c0d01ad8:	b580      	push	{r7, lr}
c0d01ada:	b082      	sub	sp, #8
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0+1];
  parameters[0] = (unsigned int)hash;
c0d01adc:	9000      	str	r0, [sp, #0]
  retid = SVC_Call(SYSCALL_cx_ripemd160_init_ID_IN, parameters);
c0d01ade:	4807      	ldr	r0, [pc, #28]	; (c0d01afc <cx_ripemd160_init+0x24>)
c0d01ae0:	4669      	mov	r1, sp
c0d01ae2:	f7ff ff99 	bl	c0d01a18 <SVC_Call>
c0d01ae6:	aa01      	add	r2, sp, #4
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01ae8:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_cx_ripemd160_init_ID_OUT) {
c0d01aea:	4905      	ldr	r1, [pc, #20]	; (c0d01b00 <cx_ripemd160_init+0x28>)
c0d01aec:	4288      	cmp	r0, r1
c0d01aee:	d102      	bne.n	c0d01af6 <cx_ripemd160_init+0x1e>
    THROW(EXCEPTION_SECURITY);
  }
  return (int)ret;
c0d01af0:	9801      	ldr	r0, [sp, #4]
c0d01af2:	b002      	add	sp, #8
c0d01af4:	bd80      	pop	{r7, pc}
  unsigned int parameters [0+1];
  parameters[0] = (unsigned int)hash;
  retid = SVC_Call(SYSCALL_cx_ripemd160_init_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_cx_ripemd160_init_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01af6:	2004      	movs	r0, #4
c0d01af8:	f7ff fb1e 	bl	c0d01138 <os_longjmp>
c0d01afc:	6000087f 	.word	0x6000087f
c0d01b00:	900008f8 	.word	0x900008f8

c0d01b04 <cx_sha256_init>:
  }
  return (int)ret;
}

int cx_sha256_init ( cx_sha256_t * hash ) 
{
c0d01b04:	b580      	push	{r7, lr}
c0d01b06:	b082      	sub	sp, #8
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0+1];
  parameters[0] = (unsigned int)hash;
c0d01b08:	9000      	str	r0, [sp, #0]
  retid = SVC_Call(SYSCALL_cx_sha256_init_ID_IN, parameters);
c0d01b0a:	4807      	ldr	r0, [pc, #28]	; (c0d01b28 <cx_sha256_init+0x24>)
c0d01b0c:	4669      	mov	r1, sp
c0d01b0e:	f7ff ff83 	bl	c0d01a18 <SVC_Call>
c0d01b12:	aa01      	add	r2, sp, #4
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01b14:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_cx_sha256_init_ID_OUT) {
c0d01b16:	4905      	ldr	r1, [pc, #20]	; (c0d01b2c <cx_sha256_init+0x28>)
c0d01b18:	4288      	cmp	r0, r1
c0d01b1a:	d102      	bne.n	c0d01b22 <cx_sha256_init+0x1e>
    THROW(EXCEPTION_SECURITY);
  }
  return (int)ret;
c0d01b1c:	9801      	ldr	r0, [sp, #4]
c0d01b1e:	b002      	add	sp, #8
c0d01b20:	bd80      	pop	{r7, pc}
  unsigned int parameters [0+1];
  parameters[0] = (unsigned int)hash;
  retid = SVC_Call(SYSCALL_cx_sha256_init_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_cx_sha256_init_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01b22:	2004      	movs	r0, #4
c0d01b24:	f7ff fb08 	bl	c0d01138 <os_longjmp>
c0d01b28:	60000adb 	.word	0x60000adb
c0d01b2c:	90000a64 	.word	0x90000a64

c0d01b30 <cx_ecfp_init_private_key>:
  }
  return (int)ret;
}

int cx_ecfp_init_private_key ( cx_curve_t curve, const unsigned char * rawkey, unsigned int key_len, cx_ecfp_private_key_t * pvkey ) 
{
c0d01b30:	b580      	push	{r7, lr}
c0d01b32:	b086      	sub	sp, #24
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0+4];
  parameters[0] = (unsigned int)curve;
c0d01b34:	af01      	add	r7, sp, #4
c0d01b36:	c70f      	stmia	r7!, {r0, r1, r2, r3}
  parameters[1] = (unsigned int)rawkey;
  parameters[2] = (unsigned int)key_len;
  parameters[3] = (unsigned int)pvkey;
  retid = SVC_Call(SYSCALL_cx_ecfp_init_private_key_ID_IN, parameters);
c0d01b38:	4807      	ldr	r0, [pc, #28]	; (c0d01b58 <cx_ecfp_init_private_key+0x28>)
c0d01b3a:	a901      	add	r1, sp, #4
c0d01b3c:	f7ff ff6c 	bl	c0d01a18 <SVC_Call>
c0d01b40:	aa05      	add	r2, sp, #20
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01b42:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_cx_ecfp_init_private_key_ID_OUT) {
c0d01b44:	4905      	ldr	r1, [pc, #20]	; (c0d01b5c <cx_ecfp_init_private_key+0x2c>)
c0d01b46:	4288      	cmp	r0, r1
c0d01b48:	d102      	bne.n	c0d01b50 <cx_ecfp_init_private_key+0x20>
    THROW(EXCEPTION_SECURITY);
  }
  return (int)ret;
c0d01b4a:	9805      	ldr	r0, [sp, #20]
c0d01b4c:	b006      	add	sp, #24
c0d01b4e:	bd80      	pop	{r7, pc}
  parameters[2] = (unsigned int)key_len;
  parameters[3] = (unsigned int)pvkey;
  retid = SVC_Call(SYSCALL_cx_ecfp_init_private_key_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_cx_ecfp_init_private_key_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01b50:	2004      	movs	r0, #4
c0d01b52:	f7ff faf1 	bl	c0d01138 <os_longjmp>
c0d01b56:	46c0      	nop			; (mov r8, r8)
c0d01b58:	60002bea 	.word	0x60002bea
c0d01b5c:	90002b63 	.word	0x90002b63

c0d01b60 <cx_ecfp_generate_pair>:
  }
  return (int)ret;
}

int cx_ecfp_generate_pair ( cx_curve_t curve, cx_ecfp_public_key_t * pubkey, cx_ecfp_private_key_t * privkey, int keepprivate ) 
{
c0d01b60:	b580      	push	{r7, lr}
c0d01b62:	b086      	sub	sp, #24
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0+4];
  parameters[0] = (unsigned int)curve;
c0d01b64:	af01      	add	r7, sp, #4
c0d01b66:	c70f      	stmia	r7!, {r0, r1, r2, r3}
  parameters[1] = (unsigned int)pubkey;
  parameters[2] = (unsigned int)privkey;
  parameters[3] = (unsigned int)keepprivate;
  retid = SVC_Call(SYSCALL_cx_ecfp_generate_pair_ID_IN, parameters);
c0d01b68:	4807      	ldr	r0, [pc, #28]	; (c0d01b88 <cx_ecfp_generate_pair+0x28>)
c0d01b6a:	a901      	add	r1, sp, #4
c0d01b6c:	f7ff ff54 	bl	c0d01a18 <SVC_Call>
c0d01b70:	aa05      	add	r2, sp, #20
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01b72:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_cx_ecfp_generate_pair_ID_OUT) {
c0d01b74:	4905      	ldr	r1, [pc, #20]	; (c0d01b8c <cx_ecfp_generate_pair+0x2c>)
c0d01b76:	4288      	cmp	r0, r1
c0d01b78:	d102      	bne.n	c0d01b80 <cx_ecfp_generate_pair+0x20>
    THROW(EXCEPTION_SECURITY);
  }
  return (int)ret;
c0d01b7a:	9805      	ldr	r0, [sp, #20]
c0d01b7c:	b006      	add	sp, #24
c0d01b7e:	bd80      	pop	{r7, pc}
  parameters[2] = (unsigned int)privkey;
  parameters[3] = (unsigned int)keepprivate;
  retid = SVC_Call(SYSCALL_cx_ecfp_generate_pair_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_cx_ecfp_generate_pair_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01b80:	2004      	movs	r0, #4
c0d01b82:	f7ff fad9 	bl	c0d01138 <os_longjmp>
c0d01b86:	46c0      	nop			; (mov r8, r8)
c0d01b88:	60002c2e 	.word	0x60002c2e
c0d01b8c:	90002c74 	.word	0x90002c74

c0d01b90 <cx_ecdsa_sign>:
  }
  return (int)ret;
}

int cx_ecdsa_sign ( const cx_ecfp_private_key_t * pvkey, int mode, cx_md_t hashID, const unsigned char * hash, unsigned int hash_len, unsigned char * sig, unsigned int sig_len, unsigned int * info ) 
{
c0d01b90:	b580      	push	{r7, lr}
c0d01b92:	b08a      	sub	sp, #40	; 0x28
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0+8];
  parameters[0] = (unsigned int)pvkey;
c0d01b94:	af01      	add	r7, sp, #4
c0d01b96:	c70f      	stmia	r7!, {r0, r1, r2, r3}
c0d01b98:	980c      	ldr	r0, [sp, #48]	; 0x30
  parameters[1] = (unsigned int)mode;
  parameters[2] = (unsigned int)hashID;
  parameters[3] = (unsigned int)hash;
  parameters[4] = (unsigned int)hash_len;
c0d01b9a:	9005      	str	r0, [sp, #20]
c0d01b9c:	980d      	ldr	r0, [sp, #52]	; 0x34
  parameters[5] = (unsigned int)sig;
c0d01b9e:	9006      	str	r0, [sp, #24]
c0d01ba0:	980e      	ldr	r0, [sp, #56]	; 0x38
  parameters[6] = (unsigned int)sig_len;
c0d01ba2:	9007      	str	r0, [sp, #28]
c0d01ba4:	980f      	ldr	r0, [sp, #60]	; 0x3c
  parameters[7] = (unsigned int)info;
c0d01ba6:	9008      	str	r0, [sp, #32]
  retid = SVC_Call(SYSCALL_cx_ecdsa_sign_ID_IN, parameters);
c0d01ba8:	4807      	ldr	r0, [pc, #28]	; (c0d01bc8 <cx_ecdsa_sign+0x38>)
c0d01baa:	a901      	add	r1, sp, #4
c0d01bac:	f7ff ff34 	bl	c0d01a18 <SVC_Call>
c0d01bb0:	aa09      	add	r2, sp, #36	; 0x24
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01bb2:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_cx_ecdsa_sign_ID_OUT) {
c0d01bb4:	4905      	ldr	r1, [pc, #20]	; (c0d01bcc <cx_ecdsa_sign+0x3c>)
c0d01bb6:	4288      	cmp	r0, r1
c0d01bb8:	d102      	bne.n	c0d01bc0 <cx_ecdsa_sign+0x30>
    THROW(EXCEPTION_SECURITY);
  }
  return (int)ret;
c0d01bba:	9809      	ldr	r0, [sp, #36]	; 0x24
c0d01bbc:	b00a      	add	sp, #40	; 0x28
c0d01bbe:	bd80      	pop	{r7, pc}
  parameters[6] = (unsigned int)sig_len;
  parameters[7] = (unsigned int)info;
  retid = SVC_Call(SYSCALL_cx_ecdsa_sign_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_cx_ecdsa_sign_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01bc0:	2004      	movs	r0, #4
c0d01bc2:	f7ff fab9 	bl	c0d01138 <os_longjmp>
c0d01bc6:	46c0      	nop			; (mov r8, r8)
c0d01bc8:	600035f3 	.word	0x600035f3
c0d01bcc:	90003576 	.word	0x90003576

c0d01bd0 <os_perso_derive_node_bip32>:
  }
  return (unsigned int)ret;
}

void os_perso_derive_node_bip32 ( cx_curve_t curve, const unsigned int * path, unsigned int pathLength, unsigned char * privateKey, unsigned char * chain ) 
{
c0d01bd0:	b580      	push	{r7, lr}
c0d01bd2:	b086      	sub	sp, #24
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0+5];
  parameters[0] = (unsigned int)curve;
c0d01bd4:	af00      	add	r7, sp, #0
c0d01bd6:	c70f      	stmia	r7!, {r0, r1, r2, r3}
c0d01bd8:	9808      	ldr	r0, [sp, #32]
  parameters[1] = (unsigned int)path;
  parameters[2] = (unsigned int)pathLength;
  parameters[3] = (unsigned int)privateKey;
  parameters[4] = (unsigned int)chain;
c0d01bda:	9004      	str	r0, [sp, #16]
  retid = SVC_Call(SYSCALL_os_perso_derive_node_bip32_ID_IN, parameters);
c0d01bdc:	4806      	ldr	r0, [pc, #24]	; (c0d01bf8 <os_perso_derive_node_bip32+0x28>)
c0d01bde:	4669      	mov	r1, sp
c0d01be0:	f7ff ff1a 	bl	c0d01a18 <SVC_Call>
c0d01be4:	aa05      	add	r2, sp, #20
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01be6:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_os_perso_derive_node_bip32_ID_OUT) {
c0d01be8:	4904      	ldr	r1, [pc, #16]	; (c0d01bfc <os_perso_derive_node_bip32+0x2c>)
c0d01bea:	4288      	cmp	r0, r1
c0d01bec:	d101      	bne.n	c0d01bf2 <os_perso_derive_node_bip32+0x22>
    THROW(EXCEPTION_SECURITY);
  }
}
c0d01bee:	b006      	add	sp, #24
c0d01bf0:	bd80      	pop	{r7, pc}
  parameters[3] = (unsigned int)privateKey;
  parameters[4] = (unsigned int)chain;
  retid = SVC_Call(SYSCALL_os_perso_derive_node_bip32_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_os_perso_derive_node_bip32_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01bf2:	2004      	movs	r0, #4
c0d01bf4:	f7ff faa0 	bl	c0d01138 <os_longjmp>
c0d01bf8:	600050ba 	.word	0x600050ba
c0d01bfc:	9000501e 	.word	0x9000501e

c0d01c00 <os_global_pin_is_validated>:
  }
  return (unsigned int)ret;
}

unsigned int os_global_pin_is_validated ( void ) 
{
c0d01c00:	b580      	push	{r7, lr}
c0d01c02:	b082      	sub	sp, #8
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0];
  retid = SVC_Call(SYSCALL_os_global_pin_is_validated_ID_IN, parameters);
c0d01c04:	4807      	ldr	r0, [pc, #28]	; (c0d01c24 <os_global_pin_is_validated+0x24>)
c0d01c06:	a901      	add	r1, sp, #4
c0d01c08:	f7ff ff06 	bl	c0d01a18 <SVC_Call>
c0d01c0c:	466a      	mov	r2, sp
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01c0e:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_os_global_pin_is_validated_ID_OUT) {
c0d01c10:	4905      	ldr	r1, [pc, #20]	; (c0d01c28 <os_global_pin_is_validated+0x28>)
c0d01c12:	4288      	cmp	r0, r1
c0d01c14:	d102      	bne.n	c0d01c1c <os_global_pin_is_validated+0x1c>
    THROW(EXCEPTION_SECURITY);
  }
  return (unsigned int)ret;
c0d01c16:	9800      	ldr	r0, [sp, #0]
c0d01c18:	b002      	add	sp, #8
c0d01c1a:	bd80      	pop	{r7, pc}
  unsigned int retid;
  unsigned int parameters [0];
  retid = SVC_Call(SYSCALL_os_global_pin_is_validated_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_os_global_pin_is_validated_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01c1c:	2004      	movs	r0, #4
c0d01c1e:	f7ff fa8b 	bl	c0d01138 <os_longjmp>
c0d01c22:	46c0      	nop			; (mov r8, r8)
c0d01c24:	60005889 	.word	0x60005889
c0d01c28:	90005845 	.word	0x90005845

c0d01c2c <os_sched_exit>:
  }
  return (unsigned int)ret;
}

void os_sched_exit ( unsigned int exit_code ) 
{
c0d01c2c:	b580      	push	{r7, lr}
c0d01c2e:	b082      	sub	sp, #8
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0+1];
  parameters[0] = (unsigned int)exit_code;
c0d01c30:	9000      	str	r0, [sp, #0]
  retid = SVC_Call(SYSCALL_os_sched_exit_ID_IN, parameters);
c0d01c32:	4807      	ldr	r0, [pc, #28]	; (c0d01c50 <os_sched_exit+0x24>)
c0d01c34:	4669      	mov	r1, sp
c0d01c36:	f7ff feef 	bl	c0d01a18 <SVC_Call>
c0d01c3a:	aa01      	add	r2, sp, #4
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01c3c:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_os_sched_exit_ID_OUT) {
c0d01c3e:	4905      	ldr	r1, [pc, #20]	; (c0d01c54 <os_sched_exit+0x28>)
c0d01c40:	4288      	cmp	r0, r1
c0d01c42:	d101      	bne.n	c0d01c48 <os_sched_exit+0x1c>
    THROW(EXCEPTION_SECURITY);
  }
}
c0d01c44:	b002      	add	sp, #8
c0d01c46:	bd80      	pop	{r7, pc}
  unsigned int parameters [0+1];
  parameters[0] = (unsigned int)exit_code;
  retid = SVC_Call(SYSCALL_os_sched_exit_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_os_sched_exit_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01c48:	2004      	movs	r0, #4
c0d01c4a:	f7ff fa75 	bl	c0d01138 <os_longjmp>
c0d01c4e:	46c0      	nop			; (mov r8, r8)
c0d01c50:	60005fe1 	.word	0x60005fe1
c0d01c54:	90005f6f 	.word	0x90005f6f

c0d01c58 <os_ux>:
    THROW(EXCEPTION_SECURITY);
  }
}

unsigned int os_ux ( bolos_ux_params_t * params ) 
{
c0d01c58:	b580      	push	{r7, lr}
c0d01c5a:	b082      	sub	sp, #8
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0+1];
  parameters[0] = (unsigned int)params;
c0d01c5c:	9000      	str	r0, [sp, #0]
  retid = SVC_Call(SYSCALL_os_ux_ID_IN, parameters);
c0d01c5e:	4807      	ldr	r0, [pc, #28]	; (c0d01c7c <os_ux+0x24>)
c0d01c60:	4669      	mov	r1, sp
c0d01c62:	f7ff fed9 	bl	c0d01a18 <SVC_Call>
c0d01c66:	aa01      	add	r2, sp, #4
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01c68:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_os_ux_ID_OUT) {
c0d01c6a:	4905      	ldr	r1, [pc, #20]	; (c0d01c80 <os_ux+0x28>)
c0d01c6c:	4288      	cmp	r0, r1
c0d01c6e:	d102      	bne.n	c0d01c76 <os_ux+0x1e>
    THROW(EXCEPTION_SECURITY);
  }
  return (unsigned int)ret;
c0d01c70:	9801      	ldr	r0, [sp, #4]
c0d01c72:	b002      	add	sp, #8
c0d01c74:	bd80      	pop	{r7, pc}
  unsigned int parameters [0+1];
  parameters[0] = (unsigned int)params;
  retid = SVC_Call(SYSCALL_os_ux_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_os_ux_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01c76:	2004      	movs	r0, #4
c0d01c78:	f7ff fa5e 	bl	c0d01138 <os_longjmp>
c0d01c7c:	60006158 	.word	0x60006158
c0d01c80:	9000611f 	.word	0x9000611f

c0d01c84 <io_seproxyhal_spi_send>:
  }
  return (unsigned int)ret;
}

void io_seproxyhal_spi_send ( const unsigned char * buffer, unsigned short length ) 
{
c0d01c84:	b580      	push	{r7, lr}
c0d01c86:	b084      	sub	sp, #16
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0+2];
  parameters[0] = (unsigned int)buffer;
c0d01c88:	9001      	str	r0, [sp, #4]
  parameters[1] = (unsigned int)length;
c0d01c8a:	9102      	str	r1, [sp, #8]
  retid = SVC_Call(SYSCALL_io_seproxyhal_spi_send_ID_IN, parameters);
c0d01c8c:	4806      	ldr	r0, [pc, #24]	; (c0d01ca8 <io_seproxyhal_spi_send+0x24>)
c0d01c8e:	a901      	add	r1, sp, #4
c0d01c90:	f7ff fec2 	bl	c0d01a18 <SVC_Call>
c0d01c94:	aa03      	add	r2, sp, #12
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01c96:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_io_seproxyhal_spi_send_ID_OUT) {
c0d01c98:	4904      	ldr	r1, [pc, #16]	; (c0d01cac <io_seproxyhal_spi_send+0x28>)
c0d01c9a:	4288      	cmp	r0, r1
c0d01c9c:	d101      	bne.n	c0d01ca2 <io_seproxyhal_spi_send+0x1e>
    THROW(EXCEPTION_SECURITY);
  }
}
c0d01c9e:	b004      	add	sp, #16
c0d01ca0:	bd80      	pop	{r7, pc}
  parameters[0] = (unsigned int)buffer;
  parameters[1] = (unsigned int)length;
  retid = SVC_Call(SYSCALL_io_seproxyhal_spi_send_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_io_seproxyhal_spi_send_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01ca2:	2004      	movs	r0, #4
c0d01ca4:	f7ff fa48 	bl	c0d01138 <os_longjmp>
c0d01ca8:	60006e1c 	.word	0x60006e1c
c0d01cac:	90006ef3 	.word	0x90006ef3

c0d01cb0 <io_seproxyhal_spi_is_status_sent>:
  }
}

unsigned int io_seproxyhal_spi_is_status_sent ( void ) 
{
c0d01cb0:	b580      	push	{r7, lr}
c0d01cb2:	b082      	sub	sp, #8
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0];
  retid = SVC_Call(SYSCALL_io_seproxyhal_spi_is_status_sent_ID_IN, parameters);
c0d01cb4:	4807      	ldr	r0, [pc, #28]	; (c0d01cd4 <io_seproxyhal_spi_is_status_sent+0x24>)
c0d01cb6:	a901      	add	r1, sp, #4
c0d01cb8:	f7ff feae 	bl	c0d01a18 <SVC_Call>
c0d01cbc:	466a      	mov	r2, sp
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01cbe:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_io_seproxyhal_spi_is_status_sent_ID_OUT) {
c0d01cc0:	4905      	ldr	r1, [pc, #20]	; (c0d01cd8 <io_seproxyhal_spi_is_status_sent+0x28>)
c0d01cc2:	4288      	cmp	r0, r1
c0d01cc4:	d102      	bne.n	c0d01ccc <io_seproxyhal_spi_is_status_sent+0x1c>
    THROW(EXCEPTION_SECURITY);
  }
  return (unsigned int)ret;
c0d01cc6:	9800      	ldr	r0, [sp, #0]
c0d01cc8:	b002      	add	sp, #8
c0d01cca:	bd80      	pop	{r7, pc}
  unsigned int retid;
  unsigned int parameters [0];
  retid = SVC_Call(SYSCALL_io_seproxyhal_spi_is_status_sent_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_io_seproxyhal_spi_is_status_sent_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01ccc:	2004      	movs	r0, #4
c0d01cce:	f7ff fa33 	bl	c0d01138 <os_longjmp>
c0d01cd2:	46c0      	nop			; (mov r8, r8)
c0d01cd4:	60006fcf 	.word	0x60006fcf
c0d01cd8:	90006f7f 	.word	0x90006f7f

c0d01cdc <io_seproxyhal_spi_recv>:
  }
  return (unsigned int)ret;
}

unsigned short io_seproxyhal_spi_recv ( unsigned char * buffer, unsigned short maxlength, unsigned int flags ) 
{
c0d01cdc:	b580      	push	{r7, lr}
c0d01cde:	b084      	sub	sp, #16
  unsigned int ret;
  unsigned int retid;
  unsigned int parameters [0+3];
  parameters[0] = (unsigned int)buffer;
c0d01ce0:	ab00      	add	r3, sp, #0
c0d01ce2:	c307      	stmia	r3!, {r0, r1, r2}
  parameters[1] = (unsigned int)maxlength;
  parameters[2] = (unsigned int)flags;
  retid = SVC_Call(SYSCALL_io_seproxyhal_spi_recv_ID_IN, parameters);
c0d01ce4:	4807      	ldr	r0, [pc, #28]	; (c0d01d04 <io_seproxyhal_spi_recv+0x28>)
c0d01ce6:	4669      	mov	r1, sp
c0d01ce8:	f7ff fe96 	bl	c0d01a18 <SVC_Call>
c0d01cec:	aa03      	add	r2, sp, #12
  asm volatile("str r1, %0":"=m"(ret)::"r1");
c0d01cee:	6011      	str	r1, [r2, #0]
  if (retid != SYSCALL_io_seproxyhal_spi_recv_ID_OUT) {
c0d01cf0:	4905      	ldr	r1, [pc, #20]	; (c0d01d08 <io_seproxyhal_spi_recv+0x2c>)
c0d01cf2:	4288      	cmp	r0, r1
c0d01cf4:	d103      	bne.n	c0d01cfe <io_seproxyhal_spi_recv+0x22>
c0d01cf6:	a803      	add	r0, sp, #12
    THROW(EXCEPTION_SECURITY);
  }
  return (unsigned short)ret;
c0d01cf8:	8800      	ldrh	r0, [r0, #0]
c0d01cfa:	b004      	add	sp, #16
c0d01cfc:	bd80      	pop	{r7, pc}
  parameters[1] = (unsigned int)maxlength;
  parameters[2] = (unsigned int)flags;
  retid = SVC_Call(SYSCALL_io_seproxyhal_spi_recv_ID_IN, parameters);
  asm volatile("str r1, %0":"=m"(ret)::"r1");
  if (retid != SYSCALL_io_seproxyhal_spi_recv_ID_OUT) {
    THROW(EXCEPTION_SECURITY);
c0d01cfe:	2004      	movs	r0, #4
c0d01d00:	f7ff fa1a 	bl	c0d01138 <os_longjmp>
c0d01d04:	600070d1 	.word	0x600070d1
c0d01d08:	9000702b 	.word	0x9000702b

c0d01d0c <USBD_LL_Init>:
  * @retval USBD Status
  */
USBD_StatusTypeDef  USBD_LL_Init (USBD_HandleTypeDef *pdev)
{ 
  UNUSED(pdev);
  ep_in_stall = 0;
c0d01d0c:	4902      	ldr	r1, [pc, #8]	; (c0d01d18 <USBD_LL_Init+0xc>)
c0d01d0e:	2000      	movs	r0, #0
c0d01d10:	6008      	str	r0, [r1, #0]
  ep_out_stall = 0;
c0d01d12:	4902      	ldr	r1, [pc, #8]	; (c0d01d1c <USBD_LL_Init+0x10>)
c0d01d14:	6008      	str	r0, [r1, #0]
  return USBD_OK;
c0d01d16:	4770      	bx	lr
c0d01d18:	20001ca8 	.word	0x20001ca8
c0d01d1c:	20001cac 	.word	0x20001cac

c0d01d20 <USBD_LL_DeInit>:
  * @brief  De-Initializes the Low Level portion of the Device driver.
  * @param  pdev: Device handle
  * @retval USBD Status
  */
USBD_StatusTypeDef  USBD_LL_DeInit (USBD_HandleTypeDef *pdev)
{
c0d01d20:	b510      	push	{r4, lr}
  UNUSED(pdev);
  // usb off
  G_io_seproxyhal_spi_buffer[0] = SEPROXYHAL_TAG_USB_CONFIG;
c0d01d22:	4807      	ldr	r0, [pc, #28]	; (c0d01d40 <USBD_LL_DeInit+0x20>)
c0d01d24:	214f      	movs	r1, #79	; 0x4f
c0d01d26:	7001      	strb	r1, [r0, #0]
c0d01d28:	2400      	movs	r4, #0
  G_io_seproxyhal_spi_buffer[1] = 0;
c0d01d2a:	7044      	strb	r4, [r0, #1]
c0d01d2c:	2101      	movs	r1, #1
  G_io_seproxyhal_spi_buffer[2] = 1;
c0d01d2e:	7081      	strb	r1, [r0, #2]
c0d01d30:	2102      	movs	r1, #2
  G_io_seproxyhal_spi_buffer[3] = SEPROXYHAL_TAG_USB_CONFIG_DISCONNECT;
c0d01d32:	70c1      	strb	r1, [r0, #3]
  io_seproxyhal_spi_send(G_io_seproxyhal_spi_buffer, 4);
c0d01d34:	2104      	movs	r1, #4
c0d01d36:	f7ff ffa5 	bl	c0d01c84 <io_seproxyhal_spi_send>

  return USBD_OK; 
c0d01d3a:	4620      	mov	r0, r4
c0d01d3c:	bd10      	pop	{r4, pc}
c0d01d3e:	46c0      	nop			; (mov r8, r8)
c0d01d40:	20001804 	.word	0x20001804

c0d01d44 <USBD_LL_Start>:
  * @brief  Starts the Low Level portion of the Device driver. 
  * @param  pdev: Device handle
  * @retval USBD Status
  */
USBD_StatusTypeDef  USBD_LL_Start(USBD_HandleTypeDef *pdev)
{
c0d01d44:	b570      	push	{r4, r5, r6, lr}
c0d01d46:	b082      	sub	sp, #8
c0d01d48:	466d      	mov	r5, sp
  uint8_t buffer[5];
  UNUSED(pdev);

  // reset address
  buffer[0] = SEPROXYHAL_TAG_USB_CONFIG;
c0d01d4a:	264f      	movs	r6, #79	; 0x4f
c0d01d4c:	702e      	strb	r6, [r5, #0]
c0d01d4e:	2400      	movs	r4, #0
  buffer[1] = 0;
c0d01d50:	706c      	strb	r4, [r5, #1]
c0d01d52:	2002      	movs	r0, #2
  buffer[2] = 2;
c0d01d54:	70a8      	strb	r0, [r5, #2]
c0d01d56:	2003      	movs	r0, #3
  buffer[3] = SEPROXYHAL_TAG_USB_CONFIG_ADDR;
c0d01d58:	70e8      	strb	r0, [r5, #3]
  buffer[4] = 0;
c0d01d5a:	712c      	strb	r4, [r5, #4]
  io_seproxyhal_spi_send(buffer, 5);
c0d01d5c:	2105      	movs	r1, #5
c0d01d5e:	4628      	mov	r0, r5
c0d01d60:	f7ff ff90 	bl	c0d01c84 <io_seproxyhal_spi_send>
  
  // start usb operation
  buffer[0] = SEPROXYHAL_TAG_USB_CONFIG;
c0d01d64:	702e      	strb	r6, [r5, #0]
  buffer[1] = 0;
c0d01d66:	706c      	strb	r4, [r5, #1]
c0d01d68:	2001      	movs	r0, #1
  buffer[2] = 1;
c0d01d6a:	70a8      	strb	r0, [r5, #2]
  buffer[3] = SEPROXYHAL_TAG_USB_CONFIG_CONNECT;
c0d01d6c:	70e8      	strb	r0, [r5, #3]
c0d01d6e:	2104      	movs	r1, #4
  io_seproxyhal_spi_send(buffer, 4);
c0d01d70:	4628      	mov	r0, r5
c0d01d72:	f7ff ff87 	bl	c0d01c84 <io_seproxyhal_spi_send>
  return USBD_OK; 
c0d01d76:	4620      	mov	r0, r4
c0d01d78:	b002      	add	sp, #8
c0d01d7a:	bd70      	pop	{r4, r5, r6, pc}

c0d01d7c <USBD_LL_Stop>:
  * @brief  Stops the Low Level portion of the Device driver.
  * @param  pdev: Device handle
  * @retval USBD Status
  */
USBD_StatusTypeDef  USBD_LL_Stop (USBD_HandleTypeDef *pdev)
{
c0d01d7c:	b510      	push	{r4, lr}
c0d01d7e:	b082      	sub	sp, #8
c0d01d80:	a801      	add	r0, sp, #4
  UNUSED(pdev);
  uint8_t buffer[4];
  buffer[0] = SEPROXYHAL_TAG_USB_CONFIG;
c0d01d82:	214f      	movs	r1, #79	; 0x4f
c0d01d84:	7001      	strb	r1, [r0, #0]
c0d01d86:	2400      	movs	r4, #0
  buffer[1] = 0;
c0d01d88:	7044      	strb	r4, [r0, #1]
c0d01d8a:	2101      	movs	r1, #1
  buffer[2] = 1;
c0d01d8c:	7081      	strb	r1, [r0, #2]
c0d01d8e:	2102      	movs	r1, #2
  buffer[3] = SEPROXYHAL_TAG_USB_CONFIG_DISCONNECT;
c0d01d90:	70c1      	strb	r1, [r0, #3]
  io_seproxyhal_spi_send(buffer, 4);
c0d01d92:	2104      	movs	r1, #4
c0d01d94:	f7ff ff76 	bl	c0d01c84 <io_seproxyhal_spi_send>
  return USBD_OK; 
c0d01d98:	4620      	mov	r0, r4
c0d01d9a:	b002      	add	sp, #8
c0d01d9c:	bd10      	pop	{r4, pc}
	...

c0d01da0 <USBD_LL_OpenEP>:
  */
USBD_StatusTypeDef  USBD_LL_OpenEP  (USBD_HandleTypeDef *pdev, 
                                      uint8_t  ep_addr,                                      
                                      uint8_t  ep_type,
                                      uint16_t ep_mps)
{
c0d01da0:	b5b0      	push	{r4, r5, r7, lr}
c0d01da2:	b082      	sub	sp, #8
  uint8_t buffer[8];
  UNUSED(pdev);

  ep_in_stall = 0;
c0d01da4:	480e      	ldr	r0, [pc, #56]	; (c0d01de0 <USBD_LL_OpenEP+0x40>)
c0d01da6:	2400      	movs	r4, #0
c0d01da8:	6004      	str	r4, [r0, #0]
  ep_out_stall = 0;
c0d01daa:	480e      	ldr	r0, [pc, #56]	; (c0d01de4 <USBD_LL_OpenEP+0x44>)
c0d01dac:	6004      	str	r4, [r0, #0]
c0d01dae:	4668      	mov	r0, sp

  buffer[0] = SEPROXYHAL_TAG_USB_CONFIG;
c0d01db0:	254f      	movs	r5, #79	; 0x4f
c0d01db2:	7005      	strb	r5, [r0, #0]
  buffer[1] = 0;
c0d01db4:	7044      	strb	r4, [r0, #1]
c0d01db6:	2505      	movs	r5, #5
  buffer[2] = 5;
c0d01db8:	7085      	strb	r5, [r0, #2]
c0d01dba:	2504      	movs	r5, #4
  buffer[3] = SEPROXYHAL_TAG_USB_CONFIG_ENDPOINTS;
c0d01dbc:	70c5      	strb	r5, [r0, #3]
c0d01dbe:	2501      	movs	r5, #1
  buffer[4] = 1;
c0d01dc0:	7105      	strb	r5, [r0, #4]
  buffer[5] = ep_addr;
c0d01dc2:	7141      	strb	r1, [r0, #5]
  buffer[6] = 0;
  switch(ep_type) {
c0d01dc4:	2a03      	cmp	r2, #3
c0d01dc6:	d802      	bhi.n	c0d01dce <USBD_LL_OpenEP+0x2e>
c0d01dc8:	00d0      	lsls	r0, r2, #3
c0d01dca:	4c07      	ldr	r4, [pc, #28]	; (c0d01de8 <USBD_LL_OpenEP+0x48>)
c0d01dcc:	40c4      	lsrs	r4, r0
c0d01dce:	4668      	mov	r0, sp
  buffer[1] = 0;
  buffer[2] = 5;
  buffer[3] = SEPROXYHAL_TAG_USB_CONFIG_ENDPOINTS;
  buffer[4] = 1;
  buffer[5] = ep_addr;
  buffer[6] = 0;
c0d01dd0:	7184      	strb	r4, [r0, #6]
      break;
    case USBD_EP_TYPE_INTR:
      buffer[6] = SEPROXYHAL_TAG_USB_CONFIG_TYPE_INTERRUPT;
      break;
  }
  buffer[7] = ep_mps;
c0d01dd2:	71c3      	strb	r3, [r0, #7]
  io_seproxyhal_spi_send(buffer, 8);
c0d01dd4:	2108      	movs	r1, #8
c0d01dd6:	f7ff ff55 	bl	c0d01c84 <io_seproxyhal_spi_send>
c0d01dda:	2000      	movs	r0, #0
  return USBD_OK; 
c0d01ddc:	b002      	add	sp, #8
c0d01dde:	bdb0      	pop	{r4, r5, r7, pc}
c0d01de0:	20001ca8 	.word	0x20001ca8
c0d01de4:	20001cac 	.word	0x20001cac
c0d01de8:	02030401 	.word	0x02030401

c0d01dec <USBD_LL_CloseEP>:
  * @param  pdev: Device handle
  * @param  ep_addr: Endpoint Number
  * @retval USBD Status
  */
USBD_StatusTypeDef  USBD_LL_CloseEP (USBD_HandleTypeDef *pdev, uint8_t ep_addr)   
{
c0d01dec:	b510      	push	{r4, lr}
c0d01dee:	b082      	sub	sp, #8
c0d01df0:	4668      	mov	r0, sp
  UNUSED(pdev);
  uint8_t buffer[8];
  buffer[0] = SEPROXYHAL_TAG_USB_CONFIG;
c0d01df2:	224f      	movs	r2, #79	; 0x4f
c0d01df4:	7002      	strb	r2, [r0, #0]
c0d01df6:	2400      	movs	r4, #0
  buffer[1] = 0;
c0d01df8:	7044      	strb	r4, [r0, #1]
c0d01dfa:	2205      	movs	r2, #5
  buffer[2] = 5;
c0d01dfc:	7082      	strb	r2, [r0, #2]
c0d01dfe:	2204      	movs	r2, #4
  buffer[3] = SEPROXYHAL_TAG_USB_CONFIG_ENDPOINTS;
c0d01e00:	70c2      	strb	r2, [r0, #3]
c0d01e02:	2201      	movs	r2, #1
  buffer[4] = 1;
c0d01e04:	7102      	strb	r2, [r0, #4]
  buffer[5] = ep_addr;
c0d01e06:	7141      	strb	r1, [r0, #5]
  buffer[6] = SEPROXYHAL_TAG_USB_CONFIG_TYPE_DISABLED;
c0d01e08:	7184      	strb	r4, [r0, #6]
  buffer[7] = 0;
c0d01e0a:	71c4      	strb	r4, [r0, #7]
  io_seproxyhal_spi_send(buffer, 8);
c0d01e0c:	2108      	movs	r1, #8
c0d01e0e:	f7ff ff39 	bl	c0d01c84 <io_seproxyhal_spi_send>
  return USBD_OK; 
c0d01e12:	4620      	mov	r0, r4
c0d01e14:	b002      	add	sp, #8
c0d01e16:	bd10      	pop	{r4, pc}

c0d01e18 <USBD_LL_StallEP>:
  * @param  pdev: Device handle
  * @param  ep_addr: Endpoint Number
  * @retval USBD Status
  */
USBD_StatusTypeDef  USBD_LL_StallEP (USBD_HandleTypeDef *pdev, uint8_t ep_addr)   
{ 
c0d01e18:	b5b0      	push	{r4, r5, r7, lr}
c0d01e1a:	b082      	sub	sp, #8
c0d01e1c:	460d      	mov	r5, r1
c0d01e1e:	4668      	mov	r0, sp
  UNUSED(pdev);
  uint8_t buffer[6];
  buffer[0] = SEPROXYHAL_TAG_USB_EP_PREPARE;
c0d01e20:	2150      	movs	r1, #80	; 0x50
c0d01e22:	7001      	strb	r1, [r0, #0]
c0d01e24:	2400      	movs	r4, #0
  buffer[1] = 0;
c0d01e26:	7044      	strb	r4, [r0, #1]
c0d01e28:	2103      	movs	r1, #3
  buffer[2] = 3;
c0d01e2a:	7081      	strb	r1, [r0, #2]
  buffer[3] = ep_addr;
c0d01e2c:	70c5      	strb	r5, [r0, #3]
  buffer[4] = SEPROXYHAL_TAG_USB_EP_PREPARE_DIR_STALL;
c0d01e2e:	2140      	movs	r1, #64	; 0x40
c0d01e30:	7101      	strb	r1, [r0, #4]
  buffer[5] = 0;
c0d01e32:	7144      	strb	r4, [r0, #5]
  io_seproxyhal_spi_send(buffer, 6);
c0d01e34:	2106      	movs	r1, #6
c0d01e36:	f7ff ff25 	bl	c0d01c84 <io_seproxyhal_spi_send>
  if (ep_addr & 0x80) {
c0d01e3a:	2080      	movs	r0, #128	; 0x80
c0d01e3c:	4205      	tst	r5, r0
c0d01e3e:	d101      	bne.n	c0d01e44 <USBD_LL_StallEP+0x2c>
c0d01e40:	4807      	ldr	r0, [pc, #28]	; (c0d01e60 <USBD_LL_StallEP+0x48>)
c0d01e42:	e000      	b.n	c0d01e46 <USBD_LL_StallEP+0x2e>
c0d01e44:	4805      	ldr	r0, [pc, #20]	; (c0d01e5c <USBD_LL_StallEP+0x44>)
c0d01e46:	6801      	ldr	r1, [r0, #0]
c0d01e48:	227f      	movs	r2, #127	; 0x7f
c0d01e4a:	4015      	ands	r5, r2
c0d01e4c:	2201      	movs	r2, #1
c0d01e4e:	40aa      	lsls	r2, r5
c0d01e50:	430a      	orrs	r2, r1
c0d01e52:	6002      	str	r2, [r0, #0]
    ep_in_stall |= (1<<(ep_addr&0x7F));
  }
  else {
    ep_out_stall |= (1<<(ep_addr&0x7F)); 
  }
  return USBD_OK; 
c0d01e54:	4620      	mov	r0, r4
c0d01e56:	b002      	add	sp, #8
c0d01e58:	bdb0      	pop	{r4, r5, r7, pc}
c0d01e5a:	46c0      	nop			; (mov r8, r8)
c0d01e5c:	20001ca8 	.word	0x20001ca8
c0d01e60:	20001cac 	.word	0x20001cac

c0d01e64 <USBD_LL_ClearStallEP>:
  * @param  pdev: Device handle
  * @param  ep_addr: Endpoint Number
  * @retval USBD Status
  */
USBD_StatusTypeDef  USBD_LL_ClearStallEP (USBD_HandleTypeDef *pdev, uint8_t ep_addr)   
{
c0d01e64:	b570      	push	{r4, r5, r6, lr}
c0d01e66:	b082      	sub	sp, #8
c0d01e68:	460d      	mov	r5, r1
c0d01e6a:	4668      	mov	r0, sp
  UNUSED(pdev);
  uint8_t buffer[6];
  buffer[0] = SEPROXYHAL_TAG_USB_EP_PREPARE;
c0d01e6c:	2150      	movs	r1, #80	; 0x50
c0d01e6e:	7001      	strb	r1, [r0, #0]
c0d01e70:	2400      	movs	r4, #0
  buffer[1] = 0;
c0d01e72:	7044      	strb	r4, [r0, #1]
c0d01e74:	2103      	movs	r1, #3
  buffer[2] = 3;
c0d01e76:	7081      	strb	r1, [r0, #2]
  buffer[3] = ep_addr;
c0d01e78:	70c5      	strb	r5, [r0, #3]
c0d01e7a:	2680      	movs	r6, #128	; 0x80
  buffer[4] = SEPROXYHAL_TAG_USB_EP_PREPARE_DIR_UNSTALL;
c0d01e7c:	7106      	strb	r6, [r0, #4]
  buffer[5] = 0;
c0d01e7e:	7144      	strb	r4, [r0, #5]
  io_seproxyhal_spi_send(buffer, 6);
c0d01e80:	2106      	movs	r1, #6
c0d01e82:	f7ff feff 	bl	c0d01c84 <io_seproxyhal_spi_send>
  if (ep_addr & 0x80) {
c0d01e86:	4235      	tst	r5, r6
c0d01e88:	d101      	bne.n	c0d01e8e <USBD_LL_ClearStallEP+0x2a>
c0d01e8a:	4807      	ldr	r0, [pc, #28]	; (c0d01ea8 <USBD_LL_ClearStallEP+0x44>)
c0d01e8c:	e000      	b.n	c0d01e90 <USBD_LL_ClearStallEP+0x2c>
c0d01e8e:	4805      	ldr	r0, [pc, #20]	; (c0d01ea4 <USBD_LL_ClearStallEP+0x40>)
c0d01e90:	6801      	ldr	r1, [r0, #0]
c0d01e92:	227f      	movs	r2, #127	; 0x7f
c0d01e94:	4015      	ands	r5, r2
c0d01e96:	2201      	movs	r2, #1
c0d01e98:	40aa      	lsls	r2, r5
c0d01e9a:	4391      	bics	r1, r2
c0d01e9c:	6001      	str	r1, [r0, #0]
    ep_in_stall &= ~(1<<(ep_addr&0x7F));
  }
  else {
    ep_out_stall &= ~(1<<(ep_addr&0x7F)); 
  }
  return USBD_OK; 
c0d01e9e:	4620      	mov	r0, r4
c0d01ea0:	b002      	add	sp, #8
c0d01ea2:	bd70      	pop	{r4, r5, r6, pc}
c0d01ea4:	20001ca8 	.word	0x20001ca8
c0d01ea8:	20001cac 	.word	0x20001cac

c0d01eac <USBD_LL_IsStallEP>:
  * @retval Stall (1: Yes, 0: No)
  */
uint8_t USBD_LL_IsStallEP (USBD_HandleTypeDef *pdev, uint8_t ep_addr)   
{
  UNUSED(pdev);
  if((ep_addr & 0x80) == 0x80)
c0d01eac:	2080      	movs	r0, #128	; 0x80
c0d01eae:	4201      	tst	r1, r0
c0d01eb0:	d001      	beq.n	c0d01eb6 <USBD_LL_IsStallEP+0xa>
c0d01eb2:	4806      	ldr	r0, [pc, #24]	; (c0d01ecc <USBD_LL_IsStallEP+0x20>)
c0d01eb4:	e000      	b.n	c0d01eb8 <USBD_LL_IsStallEP+0xc>
c0d01eb6:	4804      	ldr	r0, [pc, #16]	; (c0d01ec8 <USBD_LL_IsStallEP+0x1c>)
c0d01eb8:	6800      	ldr	r0, [r0, #0]
c0d01eba:	227f      	movs	r2, #127	; 0x7f
c0d01ebc:	4011      	ands	r1, r2
c0d01ebe:	2201      	movs	r2, #1
c0d01ec0:	408a      	lsls	r2, r1
c0d01ec2:	4002      	ands	r2, r0
  }
  else
  {
    return ep_out_stall & (1<<(ep_addr&0x7F));
  }
}
c0d01ec4:	b2d0      	uxtb	r0, r2
c0d01ec6:	4770      	bx	lr
c0d01ec8:	20001cac 	.word	0x20001cac
c0d01ecc:	20001ca8 	.word	0x20001ca8

c0d01ed0 <USBD_LL_SetUSBAddress>:
  * @param  pdev: Device handle
  * @param  ep_addr: Endpoint Number
  * @retval USBD Status
  */
USBD_StatusTypeDef  USBD_LL_SetUSBAddress (USBD_HandleTypeDef *pdev, uint8_t dev_addr)   
{
c0d01ed0:	b510      	push	{r4, lr}
c0d01ed2:	b082      	sub	sp, #8
c0d01ed4:	4668      	mov	r0, sp
  UNUSED(pdev);
  uint8_t buffer[5];
  buffer[0] = SEPROXYHAL_TAG_USB_CONFIG;
c0d01ed6:	224f      	movs	r2, #79	; 0x4f
c0d01ed8:	7002      	strb	r2, [r0, #0]
c0d01eda:	2400      	movs	r4, #0
  buffer[1] = 0;
c0d01edc:	7044      	strb	r4, [r0, #1]
c0d01ede:	2202      	movs	r2, #2
  buffer[2] = 2;
c0d01ee0:	7082      	strb	r2, [r0, #2]
c0d01ee2:	2203      	movs	r2, #3
  buffer[3] = SEPROXYHAL_TAG_USB_CONFIG_ADDR;
c0d01ee4:	70c2      	strb	r2, [r0, #3]
  buffer[4] = dev_addr;
c0d01ee6:	7101      	strb	r1, [r0, #4]
  io_seproxyhal_spi_send(buffer, 5);
c0d01ee8:	2105      	movs	r1, #5
c0d01eea:	f7ff fecb 	bl	c0d01c84 <io_seproxyhal_spi_send>
  return USBD_OK; 
c0d01eee:	4620      	mov	r0, r4
c0d01ef0:	b002      	add	sp, #8
c0d01ef2:	bd10      	pop	{r4, pc}

c0d01ef4 <USBD_LL_Transmit>:
  */
USBD_StatusTypeDef  USBD_LL_Transmit (USBD_HandleTypeDef *pdev, 
                                      uint8_t  ep_addr,                                      
                                      uint8_t  *pbuf,
                                      uint16_t  size)
{
c0d01ef4:	b5b0      	push	{r4, r5, r7, lr}
c0d01ef6:	b082      	sub	sp, #8
c0d01ef8:	461c      	mov	r4, r3
c0d01efa:	4615      	mov	r5, r2
c0d01efc:	4668      	mov	r0, sp
  UNUSED(pdev);
  uint8_t buffer[6];
  buffer[0] = SEPROXYHAL_TAG_USB_EP_PREPARE;
c0d01efe:	2250      	movs	r2, #80	; 0x50
c0d01f00:	7002      	strb	r2, [r0, #0]
  buffer[1] = (3+size)>>8;
c0d01f02:	1ce2      	adds	r2, r4, #3
c0d01f04:	0a13      	lsrs	r3, r2, #8
c0d01f06:	7043      	strb	r3, [r0, #1]
  buffer[2] = (3+size);
c0d01f08:	7082      	strb	r2, [r0, #2]
  buffer[3] = ep_addr;
c0d01f0a:	70c1      	strb	r1, [r0, #3]
  buffer[4] = SEPROXYHAL_TAG_USB_EP_PREPARE_DIR_IN;
c0d01f0c:	2120      	movs	r1, #32
c0d01f0e:	7101      	strb	r1, [r0, #4]
  buffer[5] = size;
c0d01f10:	7144      	strb	r4, [r0, #5]
  io_seproxyhal_spi_send(buffer, 6);
c0d01f12:	2106      	movs	r1, #6
c0d01f14:	f7ff feb6 	bl	c0d01c84 <io_seproxyhal_spi_send>
  io_seproxyhal_spi_send(pbuf, size);
c0d01f18:	4628      	mov	r0, r5
c0d01f1a:	4621      	mov	r1, r4
c0d01f1c:	f7ff feb2 	bl	c0d01c84 <io_seproxyhal_spi_send>
c0d01f20:	2000      	movs	r0, #0
  return USBD_OK;   
c0d01f22:	b002      	add	sp, #8
c0d01f24:	bdb0      	pop	{r4, r5, r7, pc}

c0d01f26 <USBD_LL_PrepareReceive>:
  * @retval USBD Status
  */
USBD_StatusTypeDef  USBD_LL_PrepareReceive(USBD_HandleTypeDef *pdev, 
                                           uint8_t  ep_addr,
                                           uint16_t  size)
{
c0d01f26:	b510      	push	{r4, lr}
c0d01f28:	b082      	sub	sp, #8
c0d01f2a:	4668      	mov	r0, sp
  UNUSED(pdev);
  uint8_t buffer[6];
  buffer[0] = SEPROXYHAL_TAG_USB_EP_PREPARE;
c0d01f2c:	2350      	movs	r3, #80	; 0x50
c0d01f2e:	7003      	strb	r3, [r0, #0]
c0d01f30:	2400      	movs	r4, #0
  buffer[1] = (3/*+size*/)>>8;
c0d01f32:	7044      	strb	r4, [r0, #1]
c0d01f34:	2303      	movs	r3, #3
  buffer[2] = (3/*+size*/);
c0d01f36:	7083      	strb	r3, [r0, #2]
  buffer[3] = ep_addr;
c0d01f38:	70c1      	strb	r1, [r0, #3]
  buffer[4] = SEPROXYHAL_TAG_USB_EP_PREPARE_DIR_OUT;
c0d01f3a:	2130      	movs	r1, #48	; 0x30
c0d01f3c:	7101      	strb	r1, [r0, #4]
  buffer[5] = size; // expected size, not transmitted here !
c0d01f3e:	7142      	strb	r2, [r0, #5]
  io_seproxyhal_spi_send(buffer, 6);
c0d01f40:	2106      	movs	r1, #6
c0d01f42:	f7ff fe9f 	bl	c0d01c84 <io_seproxyhal_spi_send>
  return USBD_OK;   
c0d01f46:	4620      	mov	r0, r4
c0d01f48:	b002      	add	sp, #8
c0d01f4a:	bd10      	pop	{r4, pc}

c0d01f4c <USBD_Init>:
* @param  pdesc: Descriptor structure address
* @param  id: Low level core index
* @retval None
*/
USBD_StatusTypeDef USBD_Init(USBD_HandleTypeDef *pdev, USBD_DescriptorsTypeDef *pdesc, uint8_t id)
{
c0d01f4c:	b570      	push	{r4, r5, r6, lr}
c0d01f4e:	4615      	mov	r5, r2
c0d01f50:	460e      	mov	r6, r1
c0d01f52:	4604      	mov	r4, r0
c0d01f54:	2002      	movs	r0, #2
  /* Check whether the USB Host handle is valid */
  if(pdev == NULL)
c0d01f56:	2c00      	cmp	r4, #0
c0d01f58:	d011      	beq.n	c0d01f7e <USBD_Init+0x32>
  {
    USBD_ErrLog("Invalid Device handle");
    return USBD_FAIL; 
  }

  memset(pdev, 0, sizeof(USBD_HandleTypeDef));
c0d01f5a:	204d      	movs	r0, #77	; 0x4d
c0d01f5c:	0081      	lsls	r1, r0, #2
c0d01f5e:	4620      	mov	r0, r4
c0d01f60:	f000 fe44 	bl	c0d02bec <__aeabi_memclr>
  
  /* Assign USBD Descriptors */
  if(pdesc != NULL)
c0d01f64:	2e00      	cmp	r6, #0
c0d01f66:	d002      	beq.n	c0d01f6e <USBD_Init+0x22>
  {
    pdev->pDesc = pdesc;
c0d01f68:	2011      	movs	r0, #17
c0d01f6a:	0100      	lsls	r0, r0, #4
c0d01f6c:	5026      	str	r6, [r4, r0]
  }
  
  /* Set Device initial State */
  pdev->dev_state  = USBD_STATE_DEFAULT;
c0d01f6e:	20fc      	movs	r0, #252	; 0xfc
c0d01f70:	2101      	movs	r1, #1
c0d01f72:	5421      	strb	r1, [r4, r0]
  pdev->id = id;
c0d01f74:	7025      	strb	r5, [r4, #0]
  /* Initialize low level driver */
  USBD_LL_Init(pdev);
c0d01f76:	4620      	mov	r0, r4
c0d01f78:	f7ff fec8 	bl	c0d01d0c <USBD_LL_Init>
c0d01f7c:	2000      	movs	r0, #0
  
  return USBD_OK; 
}
c0d01f7e:	b2c0      	uxtb	r0, r0
c0d01f80:	bd70      	pop	{r4, r5, r6, pc}

c0d01f82 <USBD_DeInit>:
*         Re-Initialize th device library
* @param  pdev: device instance
* @retval status: status
*/
USBD_StatusTypeDef USBD_DeInit(USBD_HandleTypeDef *pdev)
{
c0d01f82:	b570      	push	{r4, r5, r6, lr}
c0d01f84:	4604      	mov	r4, r0
  /* Set Default State */
  pdev->dev_state  = USBD_STATE_DEFAULT;
c0d01f86:	20fc      	movs	r0, #252	; 0xfc
c0d01f88:	2101      	movs	r1, #1
c0d01f8a:	5421      	strb	r1, [r4, r0]
  
  /* Free Class Resources */
  uint8_t intf;
  for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d01f8c:	2045      	movs	r0, #69	; 0x45
c0d01f8e:	0080      	lsls	r0, r0, #2
c0d01f90:	1825      	adds	r5, r4, r0
c0d01f92:	2600      	movs	r6, #0
    if(pdev->interfacesClass[intf].pClass != NULL) {
c0d01f94:	00f0      	lsls	r0, r6, #3
c0d01f96:	5828      	ldr	r0, [r5, r0]
c0d01f98:	2800      	cmp	r0, #0
c0d01f9a:	d006      	beq.n	c0d01faa <USBD_DeInit+0x28>
      ((DeInit_t)PIC(pdev->interfacesClass[intf].pClass->DeInit))(pdev, pdev->dev_config);  
c0d01f9c:	6840      	ldr	r0, [r0, #4]
c0d01f9e:	f7ff fd25 	bl	c0d019ec <pic>
c0d01fa2:	4602      	mov	r2, r0
c0d01fa4:	7921      	ldrb	r1, [r4, #4]
c0d01fa6:	4620      	mov	r0, r4
c0d01fa8:	4790      	blx	r2
  /* Set Default State */
  pdev->dev_state  = USBD_STATE_DEFAULT;
  
  /* Free Class Resources */
  uint8_t intf;
  for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d01faa:	1c76      	adds	r6, r6, #1
c0d01fac:	2e03      	cmp	r6, #3
c0d01fae:	d1f1      	bne.n	c0d01f94 <USBD_DeInit+0x12>
      ((DeInit_t)PIC(pdev->interfacesClass[intf].pClass->DeInit))(pdev, pdev->dev_config);  
    }
  }
  
    /* Stop the low level driver  */
  USBD_LL_Stop(pdev); 
c0d01fb0:	4620      	mov	r0, r4
c0d01fb2:	f7ff fee3 	bl	c0d01d7c <USBD_LL_Stop>
  
  /* Initialize low level driver */
  USBD_LL_DeInit(pdev);
c0d01fb6:	4620      	mov	r0, r4
c0d01fb8:	f7ff feb2 	bl	c0d01d20 <USBD_LL_DeInit>
  
  return USBD_OK;
c0d01fbc:	2000      	movs	r0, #0
c0d01fbe:	bd70      	pop	{r4, r5, r6, pc}

c0d01fc0 <USBD_RegisterClassForInterface>:
  * @param  pDevice : Device Handle
  * @param  pclass: Class handle
  * @retval USBD Status
  */
USBD_StatusTypeDef USBD_RegisterClassForInterface(uint8_t interfaceidx, USBD_HandleTypeDef *pdev, USBD_ClassTypeDef *pclass)
{
c0d01fc0:	2302      	movs	r3, #2
  USBD_StatusTypeDef   status = USBD_OK;
  if(pclass != 0)
c0d01fc2:	2a00      	cmp	r2, #0
c0d01fc4:	d007      	beq.n	c0d01fd6 <USBD_RegisterClassForInterface+0x16>
c0d01fc6:	2300      	movs	r3, #0
  {
    if (interfaceidx < USBD_MAX_NUM_INTERFACES) {
c0d01fc8:	2802      	cmp	r0, #2
c0d01fca:	d804      	bhi.n	c0d01fd6 <USBD_RegisterClassForInterface+0x16>
      /* link the class to the USB Device handle */
      pdev->interfacesClass[interfaceidx].pClass = pclass;
c0d01fcc:	00c0      	lsls	r0, r0, #3
c0d01fce:	1808      	adds	r0, r1, r0
c0d01fd0:	2145      	movs	r1, #69	; 0x45
c0d01fd2:	0089      	lsls	r1, r1, #2
c0d01fd4:	5042      	str	r2, [r0, r1]
  {
    USBD_ErrLog("Invalid Class handle");
    status = USBD_FAIL; 
  }
  
  return status;
c0d01fd6:	b2d8      	uxtb	r0, r3
c0d01fd8:	4770      	bx	lr

c0d01fda <USBD_Start>:
  *         Start the USB Device Core.
  * @param  pdev: Device Handle
  * @retval USBD Status
  */
USBD_StatusTypeDef  USBD_Start  (USBD_HandleTypeDef *pdev)
{
c0d01fda:	b580      	push	{r7, lr}
  
  /* Start the low level driver  */
  USBD_LL_Start(pdev); 
c0d01fdc:	f7ff feb2 	bl	c0d01d44 <USBD_LL_Start>
  
  return USBD_OK;  
c0d01fe0:	2000      	movs	r0, #0
c0d01fe2:	bd80      	pop	{r7, pc}

c0d01fe4 <USBD_SetClassConfig>:
* @param  cfgidx: configuration index
* @retval status
*/

USBD_StatusTypeDef USBD_SetClassConfig(USBD_HandleTypeDef  *pdev, uint8_t cfgidx)
{
c0d01fe4:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d01fe6:	b081      	sub	sp, #4
c0d01fe8:	460c      	mov	r4, r1
c0d01fea:	4605      	mov	r5, r0
  /* Set configuration  and Start the Class*/
  uint8_t intf;
  for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d01fec:	2045      	movs	r0, #69	; 0x45
c0d01fee:	0080      	lsls	r0, r0, #2
c0d01ff0:	182f      	adds	r7, r5, r0
c0d01ff2:	2600      	movs	r6, #0
    if(usbd_is_valid_intf(pdev, intf)) {
c0d01ff4:	4628      	mov	r0, r5
c0d01ff6:	4631      	mov	r1, r6
c0d01ff8:	f000 f97c 	bl	c0d022f4 <usbd_is_valid_intf>
c0d01ffc:	2800      	cmp	r0, #0
c0d01ffe:	d008      	beq.n	c0d02012 <USBD_SetClassConfig+0x2e>
      ((Init_t)PIC(pdev->interfacesClass[intf].pClass->Init))(pdev, cfgidx);
c0d02000:	00f0      	lsls	r0, r6, #3
c0d02002:	5838      	ldr	r0, [r7, r0]
c0d02004:	6800      	ldr	r0, [r0, #0]
c0d02006:	f7ff fcf1 	bl	c0d019ec <pic>
c0d0200a:	4602      	mov	r2, r0
c0d0200c:	4628      	mov	r0, r5
c0d0200e:	4621      	mov	r1, r4
c0d02010:	4790      	blx	r2

USBD_StatusTypeDef USBD_SetClassConfig(USBD_HandleTypeDef  *pdev, uint8_t cfgidx)
{
  /* Set configuration  and Start the Class*/
  uint8_t intf;
  for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d02012:	1c76      	adds	r6, r6, #1
c0d02014:	2e03      	cmp	r6, #3
c0d02016:	d1ed      	bne.n	c0d01ff4 <USBD_SetClassConfig+0x10>
    if(usbd_is_valid_intf(pdev, intf)) {
      ((Init_t)PIC(pdev->interfacesClass[intf].pClass->Init))(pdev, cfgidx);
    }
  }

  return USBD_OK; 
c0d02018:	2000      	movs	r0, #0
c0d0201a:	b001      	add	sp, #4
c0d0201c:	bdf0      	pop	{r4, r5, r6, r7, pc}

c0d0201e <USBD_ClrClassConfig>:
* @param  pdev: device instance
* @param  cfgidx: configuration index
* @retval status: USBD_StatusTypeDef
*/
USBD_StatusTypeDef USBD_ClrClassConfig(USBD_HandleTypeDef  *pdev, uint8_t cfgidx)
{
c0d0201e:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d02020:	b081      	sub	sp, #4
c0d02022:	460c      	mov	r4, r1
c0d02024:	4605      	mov	r5, r0
  /* Clear configuration  and De-initialize the Class process*/
  uint8_t intf;
  for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d02026:	2045      	movs	r0, #69	; 0x45
c0d02028:	0080      	lsls	r0, r0, #2
c0d0202a:	182f      	adds	r7, r5, r0
c0d0202c:	2600      	movs	r6, #0
    if(usbd_is_valid_intf(pdev, intf)) {
c0d0202e:	4628      	mov	r0, r5
c0d02030:	4631      	mov	r1, r6
c0d02032:	f000 f95f 	bl	c0d022f4 <usbd_is_valid_intf>
c0d02036:	2800      	cmp	r0, #0
c0d02038:	d008      	beq.n	c0d0204c <USBD_ClrClassConfig+0x2e>
      ((DeInit_t)PIC(pdev->interfacesClass[intf].pClass->DeInit))(pdev, cfgidx);  
c0d0203a:	00f0      	lsls	r0, r6, #3
c0d0203c:	5838      	ldr	r0, [r7, r0]
c0d0203e:	6840      	ldr	r0, [r0, #4]
c0d02040:	f7ff fcd4 	bl	c0d019ec <pic>
c0d02044:	4602      	mov	r2, r0
c0d02046:	4628      	mov	r0, r5
c0d02048:	4621      	mov	r1, r4
c0d0204a:	4790      	blx	r2
*/
USBD_StatusTypeDef USBD_ClrClassConfig(USBD_HandleTypeDef  *pdev, uint8_t cfgidx)
{
  /* Clear configuration  and De-initialize the Class process*/
  uint8_t intf;
  for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d0204c:	1c76      	adds	r6, r6, #1
c0d0204e:	2e03      	cmp	r6, #3
c0d02050:	d1ed      	bne.n	c0d0202e <USBD_ClrClassConfig+0x10>
    if(usbd_is_valid_intf(pdev, intf)) {
      ((DeInit_t)PIC(pdev->interfacesClass[intf].pClass->DeInit))(pdev, cfgidx);  
    }
  }
  return USBD_OK;
c0d02052:	2000      	movs	r0, #0
c0d02054:	b001      	add	sp, #4
c0d02056:	bdf0      	pop	{r4, r5, r6, r7, pc}

c0d02058 <USBD_LL_SetupStage>:
*         Handle the setup stage
* @param  pdev: device instance
* @retval status
*/
USBD_StatusTypeDef USBD_LL_SetupStage(USBD_HandleTypeDef *pdev, uint8_t *psetup)
{
c0d02058:	b570      	push	{r4, r5, r6, lr}
c0d0205a:	4604      	mov	r4, r0
c0d0205c:	2021      	movs	r0, #33	; 0x21
c0d0205e:	00c6      	lsls	r6, r0, #3
  USBD_ParseSetupRequest(&pdev->request, psetup);
c0d02060:	19a5      	adds	r5, r4, r6
c0d02062:	4628      	mov	r0, r5
c0d02064:	f000 fba7 	bl	c0d027b6 <USBD_ParseSetupRequest>
  
  pdev->ep0_state = USBD_EP0_SETUP;
c0d02068:	20f4      	movs	r0, #244	; 0xf4
c0d0206a:	2101      	movs	r1, #1
c0d0206c:	5021      	str	r1, [r4, r0]
  pdev->ep0_data_len = pdev->request.wLength;
c0d0206e:	2087      	movs	r0, #135	; 0x87
c0d02070:	0040      	lsls	r0, r0, #1
c0d02072:	5a20      	ldrh	r0, [r4, r0]
c0d02074:	21f8      	movs	r1, #248	; 0xf8
c0d02076:	5060      	str	r0, [r4, r1]
  
  switch (pdev->request.bmRequest & 0x1F) 
c0d02078:	5da1      	ldrb	r1, [r4, r6]
c0d0207a:	201f      	movs	r0, #31
c0d0207c:	4008      	ands	r0, r1
c0d0207e:	2802      	cmp	r0, #2
c0d02080:	d008      	beq.n	c0d02094 <USBD_LL_SetupStage+0x3c>
c0d02082:	2801      	cmp	r0, #1
c0d02084:	d00b      	beq.n	c0d0209e <USBD_LL_SetupStage+0x46>
c0d02086:	2800      	cmp	r0, #0
c0d02088:	d10e      	bne.n	c0d020a8 <USBD_LL_SetupStage+0x50>
  {
  case USB_REQ_RECIPIENT_DEVICE:   
    USBD_StdDevReq (pdev, &pdev->request);
c0d0208a:	4620      	mov	r0, r4
c0d0208c:	4629      	mov	r1, r5
c0d0208e:	f000 f93f 	bl	c0d02310 <USBD_StdDevReq>
c0d02092:	e00e      	b.n	c0d020b2 <USBD_LL_SetupStage+0x5a>
  case USB_REQ_RECIPIENT_INTERFACE:     
    USBD_StdItfReq(pdev, &pdev->request);
    break;
    
  case USB_REQ_RECIPIENT_ENDPOINT:        
    USBD_StdEPReq(pdev, &pdev->request);   
c0d02094:	4620      	mov	r0, r4
c0d02096:	4629      	mov	r1, r5
c0d02098:	f000 fb02 	bl	c0d026a0 <USBD_StdEPReq>
c0d0209c:	e009      	b.n	c0d020b2 <USBD_LL_SetupStage+0x5a>
  case USB_REQ_RECIPIENT_DEVICE:   
    USBD_StdDevReq (pdev, &pdev->request);
    break;
    
  case USB_REQ_RECIPIENT_INTERFACE:     
    USBD_StdItfReq(pdev, &pdev->request);
c0d0209e:	4620      	mov	r0, r4
c0d020a0:	4629      	mov	r1, r5
c0d020a2:	f000 fad8 	bl	c0d02656 <USBD_StdItfReq>
c0d020a6:	e004      	b.n	c0d020b2 <USBD_LL_SetupStage+0x5a>
  case USB_REQ_RECIPIENT_ENDPOINT:        
    USBD_StdEPReq(pdev, &pdev->request);   
    break;
    
  default:           
    USBD_LL_StallEP(pdev , pdev->request.bmRequest & 0x80);
c0d020a8:	2080      	movs	r0, #128	; 0x80
c0d020aa:	4001      	ands	r1, r0
c0d020ac:	4620      	mov	r0, r4
c0d020ae:	f7ff feb3 	bl	c0d01e18 <USBD_LL_StallEP>
    break;
  }  
  return USBD_OK;  
c0d020b2:	2000      	movs	r0, #0
c0d020b4:	bd70      	pop	{r4, r5, r6, pc}

c0d020b6 <USBD_LL_DataOutStage>:
* @param  pdev: device instance
* @param  epnum: endpoint index
* @retval status
*/
USBD_StatusTypeDef USBD_LL_DataOutStage(USBD_HandleTypeDef *pdev , uint8_t epnum, uint8_t *pdata)
{
c0d020b6:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d020b8:	b083      	sub	sp, #12
c0d020ba:	9202      	str	r2, [sp, #8]
c0d020bc:	4604      	mov	r4, r0
c0d020be:	9101      	str	r1, [sp, #4]
  USBD_EndpointTypeDef    *pep;
  
  if(epnum == 0) 
c0d020c0:	2900      	cmp	r1, #0
c0d020c2:	d01e      	beq.n	c0d02102 <USBD_LL_DataOutStage+0x4c>
    }
  }
  else {

    uint8_t intf;
    for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d020c4:	2045      	movs	r0, #69	; 0x45
c0d020c6:	0080      	lsls	r0, r0, #2
c0d020c8:	1825      	adds	r5, r4, r0
c0d020ca:	4626      	mov	r6, r4
c0d020cc:	36fc      	adds	r6, #252	; 0xfc
c0d020ce:	2700      	movs	r7, #0
      if( usbd_is_valid_intf(pdev, intf) &&  (pdev->interfacesClass[intf].pClass->DataOut != NULL)&&
c0d020d0:	4620      	mov	r0, r4
c0d020d2:	4639      	mov	r1, r7
c0d020d4:	f000 f90e 	bl	c0d022f4 <usbd_is_valid_intf>
c0d020d8:	2800      	cmp	r0, #0
c0d020da:	d00e      	beq.n	c0d020fa <USBD_LL_DataOutStage+0x44>
c0d020dc:	00f8      	lsls	r0, r7, #3
c0d020de:	5828      	ldr	r0, [r5, r0]
c0d020e0:	6980      	ldr	r0, [r0, #24]
c0d020e2:	2800      	cmp	r0, #0
c0d020e4:	d009      	beq.n	c0d020fa <USBD_LL_DataOutStage+0x44>
         (pdev->dev_state == USBD_STATE_CONFIGURED))
c0d020e6:	7831      	ldrb	r1, [r6, #0]
  }
  else {

    uint8_t intf;
    for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
      if( usbd_is_valid_intf(pdev, intf) &&  (pdev->interfacesClass[intf].pClass->DataOut != NULL)&&
c0d020e8:	2903      	cmp	r1, #3
c0d020ea:	d106      	bne.n	c0d020fa <USBD_LL_DataOutStage+0x44>
         (pdev->dev_state == USBD_STATE_CONFIGURED))
      {
        ((DataOut_t)PIC(pdev->interfacesClass[intf].pClass->DataOut))(pdev, epnum, pdata); 
c0d020ec:	f7ff fc7e 	bl	c0d019ec <pic>
c0d020f0:	4603      	mov	r3, r0
c0d020f2:	4620      	mov	r0, r4
c0d020f4:	9901      	ldr	r1, [sp, #4]
c0d020f6:	9a02      	ldr	r2, [sp, #8]
c0d020f8:	4798      	blx	r3
    }
  }
  else {

    uint8_t intf;
    for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d020fa:	1c7f      	adds	r7, r7, #1
c0d020fc:	2f03      	cmp	r7, #3
c0d020fe:	d1e7      	bne.n	c0d020d0 <USBD_LL_DataOutStage+0x1a>
c0d02100:	e035      	b.n	c0d0216e <USBD_LL_DataOutStage+0xb8>
  
  if(epnum == 0) 
  {
    pep = &pdev->ep_out[0];
    
    if ( pdev->ep0_state == USBD_EP0_DATA_OUT)
c0d02102:	20f4      	movs	r0, #244	; 0xf4
c0d02104:	5820      	ldr	r0, [r4, r0]
c0d02106:	2803      	cmp	r0, #3
c0d02108:	d131      	bne.n	c0d0216e <USBD_LL_DataOutStage+0xb8>
    {
      if(pep->rem_length > pep->maxpacket)
c0d0210a:	2090      	movs	r0, #144	; 0x90
c0d0210c:	5820      	ldr	r0, [r4, r0]
c0d0210e:	218c      	movs	r1, #140	; 0x8c
c0d02110:	5861      	ldr	r1, [r4, r1]
c0d02112:	4622      	mov	r2, r4
c0d02114:	328c      	adds	r2, #140	; 0x8c
c0d02116:	4281      	cmp	r1, r0
c0d02118:	d90a      	bls.n	c0d02130 <USBD_LL_DataOutStage+0x7a>
      {
        pep->rem_length -=  pep->maxpacket;
c0d0211a:	1a09      	subs	r1, r1, r0
c0d0211c:	6011      	str	r1, [r2, #0]
c0d0211e:	4281      	cmp	r1, r0
c0d02120:	d300      	bcc.n	c0d02124 <USBD_LL_DataOutStage+0x6e>
c0d02122:	4601      	mov	r1, r0
       
        USBD_CtlContinueRx (pdev, 
c0d02124:	b28a      	uxth	r2, r1
c0d02126:	4620      	mov	r0, r4
c0d02128:	9902      	ldr	r1, [sp, #8]
c0d0212a:	f000 fcb5 	bl	c0d02a98 <USBD_CtlContinueRx>
c0d0212e:	e01e      	b.n	c0d0216e <USBD_LL_DataOutStage+0xb8>
                            MIN(pep->rem_length ,pep->maxpacket));
      }
      else
      {
        uint8_t intf;
        for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d02130:	2045      	movs	r0, #69	; 0x45
c0d02132:	0080      	lsls	r0, r0, #2
c0d02134:	1826      	adds	r6, r4, r0
c0d02136:	4627      	mov	r7, r4
c0d02138:	37fc      	adds	r7, #252	; 0xfc
c0d0213a:	2500      	movs	r5, #0
          if(usbd_is_valid_intf(pdev, intf) &&  (pdev->interfacesClass[intf].pClass->EP0_RxReady != NULL)&&
c0d0213c:	4620      	mov	r0, r4
c0d0213e:	4629      	mov	r1, r5
c0d02140:	f000 f8d8 	bl	c0d022f4 <usbd_is_valid_intf>
c0d02144:	2800      	cmp	r0, #0
c0d02146:	d00c      	beq.n	c0d02162 <USBD_LL_DataOutStage+0xac>
c0d02148:	00e8      	lsls	r0, r5, #3
c0d0214a:	5830      	ldr	r0, [r6, r0]
c0d0214c:	6900      	ldr	r0, [r0, #16]
c0d0214e:	2800      	cmp	r0, #0
c0d02150:	d007      	beq.n	c0d02162 <USBD_LL_DataOutStage+0xac>
             (pdev->dev_state == USBD_STATE_CONFIGURED))
c0d02152:	7839      	ldrb	r1, [r7, #0]
      }
      else
      {
        uint8_t intf;
        for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
          if(usbd_is_valid_intf(pdev, intf) &&  (pdev->interfacesClass[intf].pClass->EP0_RxReady != NULL)&&
c0d02154:	2903      	cmp	r1, #3
c0d02156:	d104      	bne.n	c0d02162 <USBD_LL_DataOutStage+0xac>
             (pdev->dev_state == USBD_STATE_CONFIGURED))
          {
            ((EP0_RxReady_t)PIC(pdev->interfacesClass[intf].pClass->EP0_RxReady))(pdev); 
c0d02158:	f7ff fc48 	bl	c0d019ec <pic>
c0d0215c:	4601      	mov	r1, r0
c0d0215e:	4620      	mov	r0, r4
c0d02160:	4788      	blx	r1
                            MIN(pep->rem_length ,pep->maxpacket));
      }
      else
      {
        uint8_t intf;
        for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d02162:	1c6d      	adds	r5, r5, #1
c0d02164:	2d03      	cmp	r5, #3
c0d02166:	d1e9      	bne.n	c0d0213c <USBD_LL_DataOutStage+0x86>
             (pdev->dev_state == USBD_STATE_CONFIGURED))
          {
            ((EP0_RxReady_t)PIC(pdev->interfacesClass[intf].pClass->EP0_RxReady))(pdev); 
          }
        }
        USBD_CtlSendStatus(pdev);
c0d02168:	4620      	mov	r0, r4
c0d0216a:	f000 fc9c 	bl	c0d02aa6 <USBD_CtlSendStatus>
      {
        ((DataOut_t)PIC(pdev->interfacesClass[intf].pClass->DataOut))(pdev, epnum, pdata); 
      }
    }
  }  
  return USBD_OK;
c0d0216e:	2000      	movs	r0, #0
c0d02170:	b003      	add	sp, #12
c0d02172:	bdf0      	pop	{r4, r5, r6, r7, pc}

c0d02174 <USBD_LL_DataInStage>:
* @param  pdev: device instance
* @param  epnum: endpoint index
* @retval status
*/
USBD_StatusTypeDef USBD_LL_DataInStage(USBD_HandleTypeDef *pdev ,uint8_t epnum, uint8_t *pdata)
{
c0d02174:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d02176:	b081      	sub	sp, #4
c0d02178:	4604      	mov	r4, r0
c0d0217a:	9100      	str	r1, [sp, #0]
  USBD_EndpointTypeDef    *pep;
  UNUSED(pdata);
    
  if(epnum == 0) 
c0d0217c:	2900      	cmp	r1, #0
c0d0217e:	d01d      	beq.n	c0d021bc <USBD_LL_DataInStage+0x48>
      pdev->dev_test_mode = 0;
    }
  }
  else {
    uint8_t intf;
    for (intf = 0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d02180:	2045      	movs	r0, #69	; 0x45
c0d02182:	0080      	lsls	r0, r0, #2
c0d02184:	1827      	adds	r7, r4, r0
c0d02186:	4625      	mov	r5, r4
c0d02188:	35fc      	adds	r5, #252	; 0xfc
c0d0218a:	2600      	movs	r6, #0
      if( usbd_is_valid_intf(pdev, intf) && (pdev->interfacesClass[intf].pClass->DataIn != NULL)&&
c0d0218c:	4620      	mov	r0, r4
c0d0218e:	4631      	mov	r1, r6
c0d02190:	f000 f8b0 	bl	c0d022f4 <usbd_is_valid_intf>
c0d02194:	2800      	cmp	r0, #0
c0d02196:	d00d      	beq.n	c0d021b4 <USBD_LL_DataInStage+0x40>
c0d02198:	00f0      	lsls	r0, r6, #3
c0d0219a:	5838      	ldr	r0, [r7, r0]
c0d0219c:	6940      	ldr	r0, [r0, #20]
c0d0219e:	2800      	cmp	r0, #0
c0d021a0:	d008      	beq.n	c0d021b4 <USBD_LL_DataInStage+0x40>
         (pdev->dev_state == USBD_STATE_CONFIGURED))
c0d021a2:	7829      	ldrb	r1, [r5, #0]
    }
  }
  else {
    uint8_t intf;
    for (intf = 0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
      if( usbd_is_valid_intf(pdev, intf) && (pdev->interfacesClass[intf].pClass->DataIn != NULL)&&
c0d021a4:	2903      	cmp	r1, #3
c0d021a6:	d105      	bne.n	c0d021b4 <USBD_LL_DataInStage+0x40>
         (pdev->dev_state == USBD_STATE_CONFIGURED))
      {
        ((DataIn_t)PIC(pdev->interfacesClass[intf].pClass->DataIn))(pdev, epnum); 
c0d021a8:	f7ff fc20 	bl	c0d019ec <pic>
c0d021ac:	4602      	mov	r2, r0
c0d021ae:	4620      	mov	r0, r4
c0d021b0:	9900      	ldr	r1, [sp, #0]
c0d021b2:	4790      	blx	r2
      pdev->dev_test_mode = 0;
    }
  }
  else {
    uint8_t intf;
    for (intf = 0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d021b4:	1c76      	adds	r6, r6, #1
c0d021b6:	2e03      	cmp	r6, #3
c0d021b8:	d1e8      	bne.n	c0d0218c <USBD_LL_DataInStage+0x18>
c0d021ba:	e051      	b.n	c0d02260 <USBD_LL_DataInStage+0xec>
    
  if(epnum == 0) 
  {
    pep = &pdev->ep_in[0];
    
    if ( pdev->ep0_state == USBD_EP0_DATA_IN)
c0d021bc:	20f4      	movs	r0, #244	; 0xf4
c0d021be:	5820      	ldr	r0, [r4, r0]
c0d021c0:	2802      	cmp	r0, #2
c0d021c2:	d145      	bne.n	c0d02250 <USBD_LL_DataInStage+0xdc>
    {
      if(pep->rem_length > pep->maxpacket)
c0d021c4:	69e0      	ldr	r0, [r4, #28]
c0d021c6:	6a25      	ldr	r5, [r4, #32]
c0d021c8:	42a8      	cmp	r0, r5
c0d021ca:	d90b      	bls.n	c0d021e4 <USBD_LL_DataInStage+0x70>
      {
        pep->rem_length -=  pep->maxpacket;
c0d021cc:	1b40      	subs	r0, r0, r5
c0d021ce:	61e0      	str	r0, [r4, #28]
        pdev->pData += pep->maxpacket;
c0d021d0:	2113      	movs	r1, #19
c0d021d2:	010a      	lsls	r2, r1, #4
c0d021d4:	58a1      	ldr	r1, [r4, r2]
c0d021d6:	1949      	adds	r1, r1, r5
c0d021d8:	50a1      	str	r1, [r4, r2]
        USBD_LL_PrepareReceive (pdev,
                                0,
                                0);  
        */
        
        USBD_CtlContinueSendData (pdev, 
c0d021da:	b282      	uxth	r2, r0
c0d021dc:	4620      	mov	r0, r4
c0d021de:	f000 fc4d 	bl	c0d02a7c <USBD_CtlContinueSendData>
c0d021e2:	e035      	b.n	c0d02250 <USBD_LL_DataInStage+0xdc>
                                  pep->rem_length);
        
      }
      else
      { /* last packet is MPS multiple, so send ZLP packet */
        if((pep->total_length % pep->maxpacket == 0) &&
c0d021e4:	69a6      	ldr	r6, [r4, #24]
c0d021e6:	4630      	mov	r0, r6
c0d021e8:	4629      	mov	r1, r5
c0d021ea:	f000 fcf9 	bl	c0d02be0 <__aeabi_uidivmod>
c0d021ee:	42ae      	cmp	r6, r5
c0d021f0:	d30f      	bcc.n	c0d02212 <USBD_LL_DataInStage+0x9e>
c0d021f2:	2900      	cmp	r1, #0
c0d021f4:	d10d      	bne.n	c0d02212 <USBD_LL_DataInStage+0x9e>
           (pep->total_length >= pep->maxpacket) &&
             (pep->total_length < pdev->ep0_data_len ))
c0d021f6:	20f8      	movs	r0, #248	; 0xf8
c0d021f8:	5820      	ldr	r0, [r4, r0]
c0d021fa:	4627      	mov	r7, r4
c0d021fc:	37f8      	adds	r7, #248	; 0xf8
                                  pep->rem_length);
        
      }
      else
      { /* last packet is MPS multiple, so send ZLP packet */
        if((pep->total_length % pep->maxpacket == 0) &&
c0d021fe:	4286      	cmp	r6, r0
c0d02200:	d207      	bcs.n	c0d02212 <USBD_LL_DataInStage+0x9e>
c0d02202:	2500      	movs	r5, #0
          USBD_LL_PrepareReceive (pdev,
                                  0,
                                  0);
          */

          USBD_CtlContinueSendData(pdev , NULL, 0);
c0d02204:	4620      	mov	r0, r4
c0d02206:	4629      	mov	r1, r5
c0d02208:	462a      	mov	r2, r5
c0d0220a:	f000 fc37 	bl	c0d02a7c <USBD_CtlContinueSendData>
          pdev->ep0_data_len = 0;
c0d0220e:	603d      	str	r5, [r7, #0]
c0d02210:	e01e      	b.n	c0d02250 <USBD_LL_DataInStage+0xdc>
          
        }
        else
        {
          uint8_t intf;
          for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d02212:	2045      	movs	r0, #69	; 0x45
c0d02214:	0080      	lsls	r0, r0, #2
c0d02216:	1826      	adds	r6, r4, r0
c0d02218:	4627      	mov	r7, r4
c0d0221a:	37fc      	adds	r7, #252	; 0xfc
c0d0221c:	2500      	movs	r5, #0
            if(usbd_is_valid_intf(pdev, intf) && (pdev->interfacesClass[intf].pClass->EP0_TxSent != NULL)&&
c0d0221e:	4620      	mov	r0, r4
c0d02220:	4629      	mov	r1, r5
c0d02222:	f000 f867 	bl	c0d022f4 <usbd_is_valid_intf>
c0d02226:	2800      	cmp	r0, #0
c0d02228:	d00c      	beq.n	c0d02244 <USBD_LL_DataInStage+0xd0>
c0d0222a:	00e8      	lsls	r0, r5, #3
c0d0222c:	5830      	ldr	r0, [r6, r0]
c0d0222e:	68c0      	ldr	r0, [r0, #12]
c0d02230:	2800      	cmp	r0, #0
c0d02232:	d007      	beq.n	c0d02244 <USBD_LL_DataInStage+0xd0>
               (pdev->dev_state == USBD_STATE_CONFIGURED))
c0d02234:	7839      	ldrb	r1, [r7, #0]
        }
        else
        {
          uint8_t intf;
          for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
            if(usbd_is_valid_intf(pdev, intf) && (pdev->interfacesClass[intf].pClass->EP0_TxSent != NULL)&&
c0d02236:	2903      	cmp	r1, #3
c0d02238:	d104      	bne.n	c0d02244 <USBD_LL_DataInStage+0xd0>
               (pdev->dev_state == USBD_STATE_CONFIGURED))
            {
              ((EP0_RxReady_t)PIC(pdev->interfacesClass[intf].pClass->EP0_TxSent))(pdev); 
c0d0223a:	f7ff fbd7 	bl	c0d019ec <pic>
c0d0223e:	4601      	mov	r1, r0
c0d02240:	4620      	mov	r0, r4
c0d02242:	4788      	blx	r1
          
        }
        else
        {
          uint8_t intf;
          for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d02244:	1c6d      	adds	r5, r5, #1
c0d02246:	2d03      	cmp	r5, #3
c0d02248:	d1e9      	bne.n	c0d0221e <USBD_LL_DataInStage+0xaa>
               (pdev->dev_state == USBD_STATE_CONFIGURED))
            {
              ((EP0_RxReady_t)PIC(pdev->interfacesClass[intf].pClass->EP0_TxSent))(pdev); 
            }
          }
          USBD_CtlReceiveStatus(pdev);
c0d0224a:	4620      	mov	r0, r4
c0d0224c:	f000 fc37 	bl	c0d02abe <USBD_CtlReceiveStatus>
        }
      }
    }
    if (pdev->dev_test_mode == 1)
c0d02250:	2001      	movs	r0, #1
c0d02252:	0201      	lsls	r1, r0, #8
c0d02254:	1860      	adds	r0, r4, r1
c0d02256:	5c61      	ldrb	r1, [r4, r1]
c0d02258:	2901      	cmp	r1, #1
c0d0225a:	d101      	bne.n	c0d02260 <USBD_LL_DataInStage+0xec>
    {
      USBD_RunTestMode(pdev); 
      pdev->dev_test_mode = 0;
c0d0225c:	2100      	movs	r1, #0
c0d0225e:	7001      	strb	r1, [r0, #0]
      {
        ((DataIn_t)PIC(pdev->interfacesClass[intf].pClass->DataIn))(pdev, epnum); 
      }
    }
  }
  return USBD_OK;
c0d02260:	2000      	movs	r0, #0
c0d02262:	b001      	add	sp, #4
c0d02264:	bdf0      	pop	{r4, r5, r6, r7, pc}

c0d02266 <USBD_LL_Reset>:
* @param  pdev: device instance
* @retval status
*/

USBD_StatusTypeDef USBD_LL_Reset(USBD_HandleTypeDef  *pdev)
{
c0d02266:	b570      	push	{r4, r5, r6, lr}
c0d02268:	4604      	mov	r4, r0
  pdev->ep_out[0].maxpacket = USB_MAX_EP0_SIZE;
c0d0226a:	2090      	movs	r0, #144	; 0x90
c0d0226c:	2140      	movs	r1, #64	; 0x40
c0d0226e:	5021      	str	r1, [r4, r0]
  

  pdev->ep_in[0].maxpacket = USB_MAX_EP0_SIZE;
c0d02270:	6221      	str	r1, [r4, #32]
  /* Upon Reset call user call back */
  pdev->dev_state = USBD_STATE_DEFAULT;
c0d02272:	20fc      	movs	r0, #252	; 0xfc
c0d02274:	2101      	movs	r1, #1
c0d02276:	5421      	strb	r1, [r4, r0]
 
  uint8_t intf;
  for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d02278:	2045      	movs	r0, #69	; 0x45
c0d0227a:	0080      	lsls	r0, r0, #2
c0d0227c:	1826      	adds	r6, r4, r0
c0d0227e:	2500      	movs	r5, #0
    if( usbd_is_valid_intf(pdev, intf))
c0d02280:	4620      	mov	r0, r4
c0d02282:	4629      	mov	r1, r5
c0d02284:	f000 f836 	bl	c0d022f4 <usbd_is_valid_intf>
c0d02288:	2800      	cmp	r0, #0
c0d0228a:	d008      	beq.n	c0d0229e <USBD_LL_Reset+0x38>
    {
      ((DeInit_t)PIC(pdev->interfacesClass[intf].pClass->DeInit))(pdev, pdev->dev_config); 
c0d0228c:	00e8      	lsls	r0, r5, #3
c0d0228e:	5830      	ldr	r0, [r6, r0]
c0d02290:	6840      	ldr	r0, [r0, #4]
c0d02292:	f7ff fbab 	bl	c0d019ec <pic>
c0d02296:	4602      	mov	r2, r0
c0d02298:	7921      	ldrb	r1, [r4, #4]
c0d0229a:	4620      	mov	r0, r4
c0d0229c:	4790      	blx	r2
  pdev->ep_in[0].maxpacket = USB_MAX_EP0_SIZE;
  /* Upon Reset call user call back */
  pdev->dev_state = USBD_STATE_DEFAULT;
 
  uint8_t intf;
  for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d0229e:	1c6d      	adds	r5, r5, #1
c0d022a0:	2d03      	cmp	r5, #3
c0d022a2:	d1ed      	bne.n	c0d02280 <USBD_LL_Reset+0x1a>
    {
      ((DeInit_t)PIC(pdev->interfacesClass[intf].pClass->DeInit))(pdev, pdev->dev_config); 
    }
  }
  
  return USBD_OK;
c0d022a4:	2000      	movs	r0, #0
c0d022a6:	bd70      	pop	{r4, r5, r6, pc}

c0d022a8 <USBD_LL_SetSpeed>:
* @param  pdev: device instance
* @retval status
*/
USBD_StatusTypeDef USBD_LL_SetSpeed(USBD_HandleTypeDef  *pdev, USBD_SpeedTypeDef speed)
{
  pdev->dev_speed = speed;
c0d022a8:	7401      	strb	r1, [r0, #16]
c0d022aa:	2000      	movs	r0, #0
  return USBD_OK;
c0d022ac:	4770      	bx	lr

c0d022ae <USBD_LL_Suspend>:
{
  UNUSED(pdev);
  // Ignored, gently
  //pdev->dev_old_state =  pdev->dev_state;
  //pdev->dev_state  = USBD_STATE_SUSPENDED;
  return USBD_OK;
c0d022ae:	2000      	movs	r0, #0
c0d022b0:	4770      	bx	lr

c0d022b2 <USBD_LL_Resume>:
USBD_StatusTypeDef USBD_LL_Resume(USBD_HandleTypeDef  *pdev)
{
  UNUSED(pdev);
  // Ignored, gently
  //pdev->dev_state = pdev->dev_old_state;  
  return USBD_OK;
c0d022b2:	2000      	movs	r0, #0
c0d022b4:	4770      	bx	lr

c0d022b6 <USBD_LL_SOF>:
* @param  pdev: device instance
* @retval status
*/

USBD_StatusTypeDef USBD_LL_SOF(USBD_HandleTypeDef  *pdev)
{
c0d022b6:	b570      	push	{r4, r5, r6, lr}
c0d022b8:	4604      	mov	r4, r0
  if(pdev->dev_state == USBD_STATE_CONFIGURED)
c0d022ba:	20fc      	movs	r0, #252	; 0xfc
c0d022bc:	5c20      	ldrb	r0, [r4, r0]
c0d022be:	2803      	cmp	r0, #3
c0d022c0:	d116      	bne.n	c0d022f0 <USBD_LL_SOF+0x3a>
  {
    uint8_t intf;
    for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
      if( usbd_is_valid_intf(pdev, intf) && pdev->interfacesClass[intf].pClass->SOF != NULL)
c0d022c2:	2045      	movs	r0, #69	; 0x45
c0d022c4:	0080      	lsls	r0, r0, #2
c0d022c6:	1826      	adds	r6, r4, r0
c0d022c8:	2500      	movs	r5, #0
c0d022ca:	4620      	mov	r0, r4
c0d022cc:	4629      	mov	r1, r5
c0d022ce:	f000 f811 	bl	c0d022f4 <usbd_is_valid_intf>
c0d022d2:	2800      	cmp	r0, #0
c0d022d4:	d009      	beq.n	c0d022ea <USBD_LL_SOF+0x34>
c0d022d6:	00e8      	lsls	r0, r5, #3
c0d022d8:	5830      	ldr	r0, [r6, r0]
c0d022da:	69c0      	ldr	r0, [r0, #28]
c0d022dc:	2800      	cmp	r0, #0
c0d022de:	d004      	beq.n	c0d022ea <USBD_LL_SOF+0x34>
      {
        ((SOF_t)PIC(pdev->interfacesClass[intf].pClass->SOF))(pdev); 
c0d022e0:	f7ff fb84 	bl	c0d019ec <pic>
c0d022e4:	4601      	mov	r1, r0
c0d022e6:	4620      	mov	r0, r4
c0d022e8:	4788      	blx	r1
USBD_StatusTypeDef USBD_LL_SOF(USBD_HandleTypeDef  *pdev)
{
  if(pdev->dev_state == USBD_STATE_CONFIGURED)
  {
    uint8_t intf;
    for (intf =0; intf < USBD_MAX_NUM_INTERFACES; intf++) {
c0d022ea:	1c6d      	adds	r5, r5, #1
c0d022ec:	2d03      	cmp	r5, #3
c0d022ee:	d1ec      	bne.n	c0d022ca <USBD_LL_SOF+0x14>
      {
        ((SOF_t)PIC(pdev->interfacesClass[intf].pClass->SOF))(pdev); 
      }
    }
  }
  return USBD_OK;
c0d022f0:	2000      	movs	r0, #0
c0d022f2:	bd70      	pop	{r4, r5, r6, pc}

c0d022f4 <usbd_is_valid_intf>:

/** @defgroup USBD_REQ_Private_Functions
  * @{
  */ 

unsigned int usbd_is_valid_intf(USBD_HandleTypeDef *pdev , unsigned int intf) {
c0d022f4:	4602      	mov	r2, r0
c0d022f6:	2000      	movs	r0, #0
  return intf < USBD_MAX_NUM_INTERFACES && pdev->interfacesClass[intf].pClass != NULL;
c0d022f8:	2902      	cmp	r1, #2
c0d022fa:	d808      	bhi.n	c0d0230e <usbd_is_valid_intf+0x1a>
c0d022fc:	00c8      	lsls	r0, r1, #3
c0d022fe:	1810      	adds	r0, r2, r0
c0d02300:	2145      	movs	r1, #69	; 0x45
c0d02302:	0089      	lsls	r1, r1, #2
c0d02304:	5841      	ldr	r1, [r0, r1]
c0d02306:	2001      	movs	r0, #1
c0d02308:	2900      	cmp	r1, #0
c0d0230a:	d100      	bne.n	c0d0230e <usbd_is_valid_intf+0x1a>
c0d0230c:	4608      	mov	r0, r1
c0d0230e:	4770      	bx	lr

c0d02310 <USBD_StdDevReq>:
* @param  pdev: device instance
* @param  req: usb request
* @retval status
*/
USBD_StatusTypeDef  USBD_StdDevReq (USBD_HandleTypeDef *pdev , USBD_SetupReqTypedef  *req)
{
c0d02310:	b580      	push	{r7, lr}
c0d02312:	784a      	ldrb	r2, [r1, #1]
  USBD_StatusTypeDef ret = USBD_OK;  
  
  switch (req->bRequest) 
c0d02314:	2a04      	cmp	r2, #4
c0d02316:	dd08      	ble.n	c0d0232a <USBD_StdDevReq+0x1a>
c0d02318:	2a07      	cmp	r2, #7
c0d0231a:	dc0f      	bgt.n	c0d0233c <USBD_StdDevReq+0x2c>
c0d0231c:	2a05      	cmp	r2, #5
c0d0231e:	d014      	beq.n	c0d0234a <USBD_StdDevReq+0x3a>
c0d02320:	2a06      	cmp	r2, #6
c0d02322:	d11b      	bne.n	c0d0235c <USBD_StdDevReq+0x4c>
  {
  case USB_REQ_GET_DESCRIPTOR: 
    
    USBD_GetDescriptor (pdev, req) ;
c0d02324:	f000 f821 	bl	c0d0236a <USBD_GetDescriptor>
c0d02328:	e01d      	b.n	c0d02366 <USBD_StdDevReq+0x56>
c0d0232a:	2a00      	cmp	r2, #0
c0d0232c:	d010      	beq.n	c0d02350 <USBD_StdDevReq+0x40>
c0d0232e:	2a01      	cmp	r2, #1
c0d02330:	d017      	beq.n	c0d02362 <USBD_StdDevReq+0x52>
c0d02332:	2a03      	cmp	r2, #3
c0d02334:	d112      	bne.n	c0d0235c <USBD_StdDevReq+0x4c>
    USBD_GetStatus (pdev , req);
    break;
    
    
  case USB_REQ_SET_FEATURE:   
    USBD_SetFeature (pdev , req);    
c0d02336:	f000 f93b 	bl	c0d025b0 <USBD_SetFeature>
c0d0233a:	e014      	b.n	c0d02366 <USBD_StdDevReq+0x56>
c0d0233c:	2a08      	cmp	r2, #8
c0d0233e:	d00a      	beq.n	c0d02356 <USBD_StdDevReq+0x46>
c0d02340:	2a09      	cmp	r2, #9
c0d02342:	d10b      	bne.n	c0d0235c <USBD_StdDevReq+0x4c>
  case USB_REQ_SET_ADDRESS:                      
    USBD_SetAddress(pdev, req);
    break;
    
  case USB_REQ_SET_CONFIGURATION:                    
    USBD_SetConfig (pdev , req);
c0d02344:	f000 f8c3 	bl	c0d024ce <USBD_SetConfig>
c0d02348:	e00d      	b.n	c0d02366 <USBD_StdDevReq+0x56>
    
    USBD_GetDescriptor (pdev, req) ;
    break;
    
  case USB_REQ_SET_ADDRESS:                      
    USBD_SetAddress(pdev, req);
c0d0234a:	f000 f89b 	bl	c0d02484 <USBD_SetAddress>
c0d0234e:	e00a      	b.n	c0d02366 <USBD_StdDevReq+0x56>
  case USB_REQ_GET_CONFIGURATION:                 
    USBD_GetConfig (pdev , req);
    break;
    
  case USB_REQ_GET_STATUS:                                  
    USBD_GetStatus (pdev , req);
c0d02350:	f000 f90b 	bl	c0d0256a <USBD_GetStatus>
c0d02354:	e007      	b.n	c0d02366 <USBD_StdDevReq+0x56>
  case USB_REQ_SET_CONFIGURATION:                    
    USBD_SetConfig (pdev , req);
    break;
    
  case USB_REQ_GET_CONFIGURATION:                 
    USBD_GetConfig (pdev , req);
c0d02356:	f000 f8f1 	bl	c0d0253c <USBD_GetConfig>
c0d0235a:	e004      	b.n	c0d02366 <USBD_StdDevReq+0x56>
  case USB_REQ_CLEAR_FEATURE:                                   
    USBD_ClrFeature (pdev , req);
    break;
    
  default:  
    USBD_CtlError(pdev , req);
c0d0235c:	f000 f971 	bl	c0d02642 <USBD_CtlError>
c0d02360:	e001      	b.n	c0d02366 <USBD_StdDevReq+0x56>
  case USB_REQ_SET_FEATURE:   
    USBD_SetFeature (pdev , req);    
    break;
    
  case USB_REQ_CLEAR_FEATURE:                                   
    USBD_ClrFeature (pdev , req);
c0d02362:	f000 f944 	bl	c0d025ee <USBD_ClrFeature>
  default:  
    USBD_CtlError(pdev , req);
    break;
  }
  
  return ret;
c0d02366:	2000      	movs	r0, #0
c0d02368:	bd80      	pop	{r7, pc}

c0d0236a <USBD_GetDescriptor>:
* @param  req: usb request
* @retval status
*/
void USBD_GetDescriptor(USBD_HandleTypeDef *pdev , 
                               USBD_SetupReqTypedef *req)
{
c0d0236a:	b5b0      	push	{r4, r5, r7, lr}
c0d0236c:	b082      	sub	sp, #8
c0d0236e:	460d      	mov	r5, r1
c0d02370:	4604      	mov	r4, r0
  uint16_t len;
  uint8_t *pbuf;
  
    
  switch (req->wValue >> 8)
c0d02372:	8869      	ldrh	r1, [r5, #2]
c0d02374:	0a08      	lsrs	r0, r1, #8
c0d02376:	2805      	cmp	r0, #5
c0d02378:	dc13      	bgt.n	c0d023a2 <USBD_GetDescriptor+0x38>
c0d0237a:	2801      	cmp	r0, #1
c0d0237c:	d01c      	beq.n	c0d023b8 <USBD_GetDescriptor+0x4e>
c0d0237e:	2802      	cmp	r0, #2
c0d02380:	d025      	beq.n	c0d023ce <USBD_GetDescriptor+0x64>
c0d02382:	2803      	cmp	r0, #3
c0d02384:	d13a      	bne.n	c0d023fc <USBD_GetDescriptor+0x92>
c0d02386:	b2c8      	uxtb	r0, r1
      }
    }
    break;
    
  case USB_DESC_TYPE_STRING:
    switch ((uint8_t)(req->wValue))
c0d02388:	2802      	cmp	r0, #2
c0d0238a:	dc3c      	bgt.n	c0d02406 <USBD_GetDescriptor+0x9c>
c0d0238c:	2800      	cmp	r0, #0
c0d0238e:	d065      	beq.n	c0d0245c <USBD_GetDescriptor+0xf2>
c0d02390:	2801      	cmp	r0, #1
c0d02392:	d06d      	beq.n	c0d02470 <USBD_GetDescriptor+0x106>
c0d02394:	2802      	cmp	r0, #2
c0d02396:	d131      	bne.n	c0d023fc <USBD_GetDescriptor+0x92>
    case USBD_IDX_MFC_STR:
      pbuf = ((GetManufacturerStrDescriptor_t)PIC(pdev->pDesc->GetManufacturerStrDescriptor))(pdev->dev_speed, &len);
      break;
      
    case USBD_IDX_PRODUCT_STR:
      pbuf = ((GetProductStrDescriptor_t)PIC(pdev->pDesc->GetProductStrDescriptor))(pdev->dev_speed, &len);
c0d02398:	2011      	movs	r0, #17
c0d0239a:	0100      	lsls	r0, r0, #4
c0d0239c:	5820      	ldr	r0, [r4, r0]
c0d0239e:	68c0      	ldr	r0, [r0, #12]
c0d023a0:	e00e      	b.n	c0d023c0 <USBD_GetDescriptor+0x56>
c0d023a2:	2806      	cmp	r0, #6
c0d023a4:	d01d      	beq.n	c0d023e2 <USBD_GetDescriptor+0x78>
c0d023a6:	2807      	cmp	r0, #7
c0d023a8:	d025      	beq.n	c0d023f6 <USBD_GetDescriptor+0x8c>
c0d023aa:	280f      	cmp	r0, #15
c0d023ac:	d126      	bne.n	c0d023fc <USBD_GetDescriptor+0x92>
    
  switch (req->wValue >> 8)
  { 
#if (USBD_LPM_ENABLED == 1)
  case USB_DESC_TYPE_BOS:
    pbuf = ((GetBOSDescriptor_t)PIC(pdev->pDesc->GetBOSDescriptor))(pdev->dev_speed, &len);
c0d023ae:	2011      	movs	r0, #17
c0d023b0:	0100      	lsls	r0, r0, #4
c0d023b2:	5820      	ldr	r0, [r4, r0]
c0d023b4:	69c0      	ldr	r0, [r0, #28]
c0d023b6:	e003      	b.n	c0d023c0 <USBD_GetDescriptor+0x56>
    break;
#endif    
  case USB_DESC_TYPE_DEVICE:
    pbuf = ((GetDeviceDescriptor_t)PIC(pdev->pDesc->GetDeviceDescriptor))(pdev->dev_speed, &len);
c0d023b8:	2011      	movs	r0, #17
c0d023ba:	0100      	lsls	r0, r0, #4
c0d023bc:	5820      	ldr	r0, [r4, r0]
c0d023be:	6800      	ldr	r0, [r0, #0]
c0d023c0:	f7ff fb14 	bl	c0d019ec <pic>
c0d023c4:	4602      	mov	r2, r0
c0d023c6:	7c20      	ldrb	r0, [r4, #16]
c0d023c8:	a901      	add	r1, sp, #4
c0d023ca:	4790      	blx	r2
c0d023cc:	e034      	b.n	c0d02438 <USBD_GetDescriptor+0xce>
    break;
    
  case USB_DESC_TYPE_CONFIGURATION:     
    if(pdev->interfacesClass[0].pClass != NULL) {
c0d023ce:	2045      	movs	r0, #69	; 0x45
c0d023d0:	0080      	lsls	r0, r0, #2
c0d023d2:	5820      	ldr	r0, [r4, r0]
c0d023d4:	2800      	cmp	r0, #0
c0d023d6:	d021      	beq.n	c0d0241c <USBD_GetDescriptor+0xb2>
      if(pdev->dev_speed == USBD_SPEED_HIGH )   
c0d023d8:	7c21      	ldrb	r1, [r4, #16]
c0d023da:	2900      	cmp	r1, #0
c0d023dc:	d026      	beq.n	c0d0242c <USBD_GetDescriptor+0xc2>
        pbuf   = (uint8_t *)((GetHSConfigDescriptor_t)PIC(pdev->interfacesClass[0].pClass->GetHSConfigDescriptor))(&len);
        //pbuf[1] = USB_DESC_TYPE_CONFIGURATION; CONST BUFFER KTHX
      }
      else
      {
        pbuf   = (uint8_t *)((GetFSConfigDescriptor_t)PIC(pdev->interfacesClass[0].pClass->GetFSConfigDescriptor))(&len);
c0d023de:	6ac0      	ldr	r0, [r0, #44]	; 0x2c
c0d023e0:	e025      	b.n	c0d0242e <USBD_GetDescriptor+0xc4>
#endif   
    }
    break;
  case USB_DESC_TYPE_DEVICE_QUALIFIER:                   

    if(pdev->dev_speed == USBD_SPEED_HIGH && pdev->interfacesClass[0].pClass != NULL )   
c0d023e2:	7c20      	ldrb	r0, [r4, #16]
c0d023e4:	2800      	cmp	r0, #0
c0d023e6:	d109      	bne.n	c0d023fc <USBD_GetDescriptor+0x92>
c0d023e8:	2045      	movs	r0, #69	; 0x45
c0d023ea:	0080      	lsls	r0, r0, #2
c0d023ec:	5820      	ldr	r0, [r4, r0]
c0d023ee:	2800      	cmp	r0, #0
c0d023f0:	d004      	beq.n	c0d023fc <USBD_GetDescriptor+0x92>
    {
      pbuf   = (uint8_t *)((GetDeviceQualifierDescriptor_t)PIC(pdev->interfacesClass[0].pClass->GetDeviceQualifierDescriptor))(&len);
c0d023f2:	6b40      	ldr	r0, [r0, #52]	; 0x34
c0d023f4:	e01b      	b.n	c0d0242e <USBD_GetDescriptor+0xc4>
      USBD_CtlError(pdev , req);
      return;
    } 

  case USB_DESC_TYPE_OTHER_SPEED_CONFIGURATION:
    if(pdev->dev_speed == USBD_SPEED_HIGH && pdev->interfacesClass[0].pClass != NULL)   
c0d023f6:	7c20      	ldrb	r0, [r4, #16]
c0d023f8:	2800      	cmp	r0, #0
c0d023fa:	d010      	beq.n	c0d0241e <USBD_GetDescriptor+0xb4>
c0d023fc:	4620      	mov	r0, r4
c0d023fe:	4629      	mov	r1, r5
c0d02400:	f000 f91f 	bl	c0d02642 <USBD_CtlError>
c0d02404:	e028      	b.n	c0d02458 <USBD_GetDescriptor+0xee>
c0d02406:	2803      	cmp	r0, #3
c0d02408:	d02d      	beq.n	c0d02466 <USBD_GetDescriptor+0xfc>
c0d0240a:	2804      	cmp	r0, #4
c0d0240c:	d035      	beq.n	c0d0247a <USBD_GetDescriptor+0x110>
c0d0240e:	2805      	cmp	r0, #5
c0d02410:	d1f4      	bne.n	c0d023fc <USBD_GetDescriptor+0x92>
    case USBD_IDX_CONFIG_STR:
      pbuf = ((GetConfigurationStrDescriptor_t)PIC(pdev->pDesc->GetConfigurationStrDescriptor))(pdev->dev_speed, &len);
      break;
      
    case USBD_IDX_INTERFACE_STR:
      pbuf = ((GetInterfaceStrDescriptor_t)PIC(pdev->pDesc->GetInterfaceStrDescriptor))(pdev->dev_speed, &len);
c0d02412:	2011      	movs	r0, #17
c0d02414:	0100      	lsls	r0, r0, #4
c0d02416:	5820      	ldr	r0, [r4, r0]
c0d02418:	6980      	ldr	r0, [r0, #24]
c0d0241a:	e7d1      	b.n	c0d023c0 <USBD_GetDescriptor+0x56>
c0d0241c:	e00d      	b.n	c0d0243a <USBD_GetDescriptor+0xd0>
      USBD_CtlError(pdev , req);
      return;
    } 

  case USB_DESC_TYPE_OTHER_SPEED_CONFIGURATION:
    if(pdev->dev_speed == USBD_SPEED_HIGH && pdev->interfacesClass[0].pClass != NULL)   
c0d0241e:	2045      	movs	r0, #69	; 0x45
c0d02420:	0080      	lsls	r0, r0, #2
c0d02422:	5820      	ldr	r0, [r4, r0]
c0d02424:	2800      	cmp	r0, #0
c0d02426:	d0e9      	beq.n	c0d023fc <USBD_GetDescriptor+0x92>
    {
      pbuf   = (uint8_t *)((GetOtherSpeedConfigDescriptor_t)PIC(pdev->interfacesClass[0].pClass->GetOtherSpeedConfigDescriptor))(&len);
c0d02428:	6b00      	ldr	r0, [r0, #48]	; 0x30
c0d0242a:	e000      	b.n	c0d0242e <USBD_GetDescriptor+0xc4>
    
  case USB_DESC_TYPE_CONFIGURATION:     
    if(pdev->interfacesClass[0].pClass != NULL) {
      if(pdev->dev_speed == USBD_SPEED_HIGH )   
      {
        pbuf   = (uint8_t *)((GetHSConfigDescriptor_t)PIC(pdev->interfacesClass[0].pClass->GetHSConfigDescriptor))(&len);
c0d0242c:	6a80      	ldr	r0, [r0, #40]	; 0x28
c0d0242e:	f7ff fadd 	bl	c0d019ec <pic>
c0d02432:	4601      	mov	r1, r0
c0d02434:	a801      	add	r0, sp, #4
c0d02436:	4788      	blx	r1
c0d02438:	4601      	mov	r1, r0
c0d0243a:	a801      	add	r0, sp, #4
  default: 
     USBD_CtlError(pdev , req);
    return;
  }
  
  if((len != 0)&& (req->wLength != 0))
c0d0243c:	8802      	ldrh	r2, [r0, #0]
c0d0243e:	2a00      	cmp	r2, #0
c0d02440:	d00a      	beq.n	c0d02458 <USBD_GetDescriptor+0xee>
c0d02442:	88e8      	ldrh	r0, [r5, #6]
c0d02444:	2800      	cmp	r0, #0
c0d02446:	d007      	beq.n	c0d02458 <USBD_GetDescriptor+0xee>
  {
    
    len = MIN(len , req->wLength);
c0d02448:	4282      	cmp	r2, r0
c0d0244a:	d300      	bcc.n	c0d0244e <USBD_GetDescriptor+0xe4>
c0d0244c:	4602      	mov	r2, r0
c0d0244e:	a801      	add	r0, sp, #4
c0d02450:	8002      	strh	r2, [r0, #0]
    
    // prepare abort if host does not read the whole data
    //USBD_CtlReceiveStatus(pdev);

    // start transfer
    USBD_CtlSendData (pdev, 
c0d02452:	4620      	mov	r0, r4
c0d02454:	f000 fafc 	bl	c0d02a50 <USBD_CtlSendData>
                      pbuf,
                      len);
  }
  
}
c0d02458:	b002      	add	sp, #8
c0d0245a:	bdb0      	pop	{r4, r5, r7, pc}
    
  case USB_DESC_TYPE_STRING:
    switch ((uint8_t)(req->wValue))
    {
    case USBD_IDX_LANGID_STR:
     pbuf = ((GetLangIDStrDescriptor_t)PIC(pdev->pDesc->GetLangIDStrDescriptor))(pdev->dev_speed, &len);        
c0d0245c:	2011      	movs	r0, #17
c0d0245e:	0100      	lsls	r0, r0, #4
c0d02460:	5820      	ldr	r0, [r4, r0]
c0d02462:	6840      	ldr	r0, [r0, #4]
c0d02464:	e7ac      	b.n	c0d023c0 <USBD_GetDescriptor+0x56>
    case USBD_IDX_PRODUCT_STR:
      pbuf = ((GetProductStrDescriptor_t)PIC(pdev->pDesc->GetProductStrDescriptor))(pdev->dev_speed, &len);
      break;
      
    case USBD_IDX_SERIAL_STR:
      pbuf = ((GetSerialStrDescriptor_t)PIC(pdev->pDesc->GetSerialStrDescriptor))(pdev->dev_speed, &len);
c0d02466:	2011      	movs	r0, #17
c0d02468:	0100      	lsls	r0, r0, #4
c0d0246a:	5820      	ldr	r0, [r4, r0]
c0d0246c:	6900      	ldr	r0, [r0, #16]
c0d0246e:	e7a7      	b.n	c0d023c0 <USBD_GetDescriptor+0x56>
    case USBD_IDX_LANGID_STR:
     pbuf = ((GetLangIDStrDescriptor_t)PIC(pdev->pDesc->GetLangIDStrDescriptor))(pdev->dev_speed, &len);        
      break;
      
    case USBD_IDX_MFC_STR:
      pbuf = ((GetManufacturerStrDescriptor_t)PIC(pdev->pDesc->GetManufacturerStrDescriptor))(pdev->dev_speed, &len);
c0d02470:	2011      	movs	r0, #17
c0d02472:	0100      	lsls	r0, r0, #4
c0d02474:	5820      	ldr	r0, [r4, r0]
c0d02476:	6880      	ldr	r0, [r0, #8]
c0d02478:	e7a2      	b.n	c0d023c0 <USBD_GetDescriptor+0x56>
    case USBD_IDX_SERIAL_STR:
      pbuf = ((GetSerialStrDescriptor_t)PIC(pdev->pDesc->GetSerialStrDescriptor))(pdev->dev_speed, &len);
      break;
      
    case USBD_IDX_CONFIG_STR:
      pbuf = ((GetConfigurationStrDescriptor_t)PIC(pdev->pDesc->GetConfigurationStrDescriptor))(pdev->dev_speed, &len);
c0d0247a:	2011      	movs	r0, #17
c0d0247c:	0100      	lsls	r0, r0, #4
c0d0247e:	5820      	ldr	r0, [r4, r0]
c0d02480:	6940      	ldr	r0, [r0, #20]
c0d02482:	e79d      	b.n	c0d023c0 <USBD_GetDescriptor+0x56>

c0d02484 <USBD_SetAddress>:
* @param  req: usb request
* @retval status
*/
void USBD_SetAddress(USBD_HandleTypeDef *pdev , 
                            USBD_SetupReqTypedef *req)
{
c0d02484:	b570      	push	{r4, r5, r6, lr}
c0d02486:	4604      	mov	r4, r0
  uint8_t  dev_addr; 
  
  if ((req->wIndex == 0) && (req->wLength == 0)) 
c0d02488:	8888      	ldrh	r0, [r1, #4]
c0d0248a:	2800      	cmp	r0, #0
c0d0248c:	d10b      	bne.n	c0d024a6 <USBD_SetAddress+0x22>
c0d0248e:	88c8      	ldrh	r0, [r1, #6]
c0d02490:	2800      	cmp	r0, #0
c0d02492:	d108      	bne.n	c0d024a6 <USBD_SetAddress+0x22>
  {
    dev_addr = (uint8_t)(req->wValue) & 0x7F;     
c0d02494:	8848      	ldrh	r0, [r1, #2]
c0d02496:	267f      	movs	r6, #127	; 0x7f
c0d02498:	4006      	ands	r6, r0
    
    if (pdev->dev_state == USBD_STATE_CONFIGURED) 
c0d0249a:	20fc      	movs	r0, #252	; 0xfc
c0d0249c:	5c20      	ldrb	r0, [r4, r0]
c0d0249e:	4625      	mov	r5, r4
c0d024a0:	35fc      	adds	r5, #252	; 0xfc
c0d024a2:	2803      	cmp	r0, #3
c0d024a4:	d103      	bne.n	c0d024ae <USBD_SetAddress+0x2a>
c0d024a6:	4620      	mov	r0, r4
c0d024a8:	f000 f8cb 	bl	c0d02642 <USBD_CtlError>
  } 
  else 
  {
     USBD_CtlError(pdev , req);                        
  } 
}
c0d024ac:	bd70      	pop	{r4, r5, r6, pc}
    {
      USBD_CtlError(pdev , req);
    } 
    else 
    {
      pdev->dev_address = dev_addr;
c0d024ae:	20fe      	movs	r0, #254	; 0xfe
c0d024b0:	5426      	strb	r6, [r4, r0]
      USBD_LL_SetUSBAddress(pdev, dev_addr);               
c0d024b2:	b2f1      	uxtb	r1, r6
c0d024b4:	4620      	mov	r0, r4
c0d024b6:	f7ff fd0b 	bl	c0d01ed0 <USBD_LL_SetUSBAddress>
      USBD_CtlSendStatus(pdev);                         
c0d024ba:	4620      	mov	r0, r4
c0d024bc:	f000 faf3 	bl	c0d02aa6 <USBD_CtlSendStatus>
      
      if (dev_addr != 0) 
c0d024c0:	2002      	movs	r0, #2
c0d024c2:	2101      	movs	r1, #1
c0d024c4:	2e00      	cmp	r6, #0
c0d024c6:	d100      	bne.n	c0d024ca <USBD_SetAddress+0x46>
c0d024c8:	4608      	mov	r0, r1
c0d024ca:	7028      	strb	r0, [r5, #0]
  } 
  else 
  {
     USBD_CtlError(pdev , req);                        
  } 
}
c0d024cc:	bd70      	pop	{r4, r5, r6, pc}

c0d024ce <USBD_SetConfig>:
* @param  req: usb request
* @retval status
*/
void USBD_SetConfig(USBD_HandleTypeDef *pdev , 
                           USBD_SetupReqTypedef *req)
{
c0d024ce:	b570      	push	{r4, r5, r6, lr}
c0d024d0:	460d      	mov	r5, r1
c0d024d2:	4604      	mov	r4, r0
  
  uint8_t  cfgidx;
  
  cfgidx = (uint8_t)(req->wValue);                 
c0d024d4:	78ae      	ldrb	r6, [r5, #2]
  
  if (cfgidx > USBD_MAX_NUM_CONFIGURATION ) 
c0d024d6:	2e02      	cmp	r6, #2
c0d024d8:	d21d      	bcs.n	c0d02516 <USBD_SetConfig+0x48>
  {            
     USBD_CtlError(pdev , req);                              
  } 
  else 
  {
    switch (pdev->dev_state) 
c0d024da:	20fc      	movs	r0, #252	; 0xfc
c0d024dc:	5c21      	ldrb	r1, [r4, r0]
c0d024de:	4620      	mov	r0, r4
c0d024e0:	30fc      	adds	r0, #252	; 0xfc
c0d024e2:	2903      	cmp	r1, #3
c0d024e4:	d007      	beq.n	c0d024f6 <USBD_SetConfig+0x28>
c0d024e6:	2902      	cmp	r1, #2
c0d024e8:	d115      	bne.n	c0d02516 <USBD_SetConfig+0x48>
    {
    case USBD_STATE_ADDRESSED:
      if (cfgidx) 
c0d024ea:	2e00      	cmp	r6, #0
c0d024ec:	d022      	beq.n	c0d02534 <USBD_SetConfig+0x66>
      {                                			   							   							   				
        pdev->dev_config = cfgidx;
c0d024ee:	6066      	str	r6, [r4, #4]
        pdev->dev_state = USBD_STATE_CONFIGURED;
c0d024f0:	2103      	movs	r1, #3
c0d024f2:	7001      	strb	r1, [r0, #0]
c0d024f4:	e009      	b.n	c0d0250a <USBD_SetConfig+0x3c>
      }
      USBD_CtlSendStatus(pdev);
      break;
      
    case USBD_STATE_CONFIGURED:
      if (cfgidx == 0) 
c0d024f6:	2e00      	cmp	r6, #0
c0d024f8:	d012      	beq.n	c0d02520 <USBD_SetConfig+0x52>
        pdev->dev_state = USBD_STATE_ADDRESSED;
        pdev->dev_config = cfgidx;          
        USBD_ClrClassConfig(pdev , cfgidx);
        USBD_CtlSendStatus(pdev);
      } 
      else  if (cfgidx != pdev->dev_config) 
c0d024fa:	6860      	ldr	r0, [r4, #4]
c0d024fc:	4286      	cmp	r6, r0
c0d024fe:	d019      	beq.n	c0d02534 <USBD_SetConfig+0x66>
      {
        /* Clear old configuration */
        USBD_ClrClassConfig(pdev , pdev->dev_config);
c0d02500:	b2c1      	uxtb	r1, r0
c0d02502:	4620      	mov	r0, r4
c0d02504:	f7ff fd8b 	bl	c0d0201e <USBD_ClrClassConfig>
        
        /* set new configuration */
        pdev->dev_config = cfgidx;
c0d02508:	6066      	str	r6, [r4, #4]
c0d0250a:	4620      	mov	r0, r4
c0d0250c:	4631      	mov	r1, r6
c0d0250e:	f7ff fd69 	bl	c0d01fe4 <USBD_SetClassConfig>
c0d02512:	2802      	cmp	r0, #2
c0d02514:	d10e      	bne.n	c0d02534 <USBD_SetConfig+0x66>
c0d02516:	4620      	mov	r0, r4
c0d02518:	4629      	mov	r1, r5
c0d0251a:	f000 f892 	bl	c0d02642 <USBD_CtlError>
    default:					
       USBD_CtlError(pdev , req);                     
      break;
    }
  }
}
c0d0251e:	bd70      	pop	{r4, r5, r6, pc}
      break;
      
    case USBD_STATE_CONFIGURED:
      if (cfgidx == 0) 
      {                           
        pdev->dev_state = USBD_STATE_ADDRESSED;
c0d02520:	2102      	movs	r1, #2
c0d02522:	7001      	strb	r1, [r0, #0]
        pdev->dev_config = cfgidx;          
c0d02524:	6066      	str	r6, [r4, #4]
        USBD_ClrClassConfig(pdev , cfgidx);
c0d02526:	4620      	mov	r0, r4
c0d02528:	4631      	mov	r1, r6
c0d0252a:	f7ff fd78 	bl	c0d0201e <USBD_ClrClassConfig>
        USBD_CtlSendStatus(pdev);
c0d0252e:	4620      	mov	r0, r4
c0d02530:	f000 fab9 	bl	c0d02aa6 <USBD_CtlSendStatus>
c0d02534:	4620      	mov	r0, r4
c0d02536:	f000 fab6 	bl	c0d02aa6 <USBD_CtlSendStatus>
    default:					
       USBD_CtlError(pdev , req);                     
      break;
    }
  }
}
c0d0253a:	bd70      	pop	{r4, r5, r6, pc}

c0d0253c <USBD_GetConfig>:
* @param  req: usb request
* @retval status
*/
void USBD_GetConfig(USBD_HandleTypeDef *pdev , 
                           USBD_SetupReqTypedef *req)
{
c0d0253c:	b580      	push	{r7, lr}

  if (req->wLength != 1) 
c0d0253e:	88ca      	ldrh	r2, [r1, #6]
c0d02540:	2a01      	cmp	r2, #1
c0d02542:	d10a      	bne.n	c0d0255a <USBD_GetConfig+0x1e>
  {                   
     USBD_CtlError(pdev , req);
  }
  else 
  {
    switch (pdev->dev_state )  
c0d02544:	22fc      	movs	r2, #252	; 0xfc
c0d02546:	5c82      	ldrb	r2, [r0, r2]
c0d02548:	2a03      	cmp	r2, #3
c0d0254a:	d009      	beq.n	c0d02560 <USBD_GetConfig+0x24>
c0d0254c:	2a02      	cmp	r2, #2
c0d0254e:	d104      	bne.n	c0d0255a <USBD_GetConfig+0x1e>
    {
    case USBD_STATE_ADDRESSED:                     
      pdev->dev_default_config = 0;
c0d02550:	2100      	movs	r1, #0
c0d02552:	6081      	str	r1, [r0, #8]
c0d02554:	4601      	mov	r1, r0
c0d02556:	3108      	adds	r1, #8
c0d02558:	e003      	b.n	c0d02562 <USBD_GetConfig+0x26>
c0d0255a:	f000 f872 	bl	c0d02642 <USBD_CtlError>
    default:
       USBD_CtlError(pdev , req);
      break;
    }
  }
}
c0d0255e:	bd80      	pop	{r7, pc}
                        1);
      break;
      
    case USBD_STATE_CONFIGURED:   
      USBD_CtlSendData (pdev, 
                        (uint8_t *)&pdev->dev_config,
c0d02560:	1d01      	adds	r1, r0, #4
c0d02562:	2201      	movs	r2, #1
c0d02564:	f000 fa74 	bl	c0d02a50 <USBD_CtlSendData>
    default:
       USBD_CtlError(pdev , req);
      break;
    }
  }
}
c0d02568:	bd80      	pop	{r7, pc}

c0d0256a <USBD_GetStatus>:
* @param  req: usb request
* @retval status
*/
void USBD_GetStatus(USBD_HandleTypeDef *pdev , 
                           USBD_SetupReqTypedef *req)
{
c0d0256a:	b5b0      	push	{r4, r5, r7, lr}
c0d0256c:	4604      	mov	r4, r0
  
    
  switch (pdev->dev_state) 
c0d0256e:	20fc      	movs	r0, #252	; 0xfc
c0d02570:	5c20      	ldrb	r0, [r4, r0]
c0d02572:	22fe      	movs	r2, #254	; 0xfe
c0d02574:	4002      	ands	r2, r0
c0d02576:	2a02      	cmp	r2, #2
c0d02578:	d116      	bne.n	c0d025a8 <USBD_GetStatus+0x3e>
  {
  case USBD_STATE_ADDRESSED:
  case USBD_STATE_CONFIGURED:
    
#if ( USBD_SELF_POWERED == 1)
    pdev->dev_config_status = USB_CONFIG_SELF_POWERED;                                  
c0d0257a:	2001      	movs	r0, #1
c0d0257c:	60e0      	str	r0, [r4, #12]
#else
    pdev->dev_config_status = 0;                                   
#endif
                      
    if (pdev->dev_remote_wakeup) USBD_CtlReceiveStatus(pdev);
c0d0257e:	2041      	movs	r0, #65	; 0x41
c0d02580:	0080      	lsls	r0, r0, #2
c0d02582:	5821      	ldr	r1, [r4, r0]
  {
  case USBD_STATE_ADDRESSED:
  case USBD_STATE_CONFIGURED:
    
#if ( USBD_SELF_POWERED == 1)
    pdev->dev_config_status = USB_CONFIG_SELF_POWERED;                                  
c0d02584:	4625      	mov	r5, r4
c0d02586:	350c      	adds	r5, #12
c0d02588:	2003      	movs	r0, #3
#else
    pdev->dev_config_status = 0;                                   
#endif
                      
    if (pdev->dev_remote_wakeup) USBD_CtlReceiveStatus(pdev);
c0d0258a:	2900      	cmp	r1, #0
c0d0258c:	d005      	beq.n	c0d0259a <USBD_GetStatus+0x30>
c0d0258e:	4620      	mov	r0, r4
c0d02590:	f000 fa95 	bl	c0d02abe <USBD_CtlReceiveStatus>
c0d02594:	68e1      	ldr	r1, [r4, #12]
c0d02596:	2002      	movs	r0, #2
c0d02598:	4308      	orrs	r0, r1
    {
       pdev->dev_config_status |= USB_CONFIG_REMOTE_WAKEUP;                                
c0d0259a:	60e0      	str	r0, [r4, #12]
    }
    
    USBD_CtlSendData (pdev, 
c0d0259c:	2202      	movs	r2, #2
c0d0259e:	4620      	mov	r0, r4
c0d025a0:	4629      	mov	r1, r5
c0d025a2:	f000 fa55 	bl	c0d02a50 <USBD_CtlSendData>
    
  default :
    USBD_CtlError(pdev , req);                        
    break;
  }
}
c0d025a6:	bdb0      	pop	{r4, r5, r7, pc}
                      (uint8_t *)& pdev->dev_config_status,
                      2);
    break;
    
  default :
    USBD_CtlError(pdev , req);                        
c0d025a8:	4620      	mov	r0, r4
c0d025aa:	f000 f84a 	bl	c0d02642 <USBD_CtlError>
    break;
  }
}
c0d025ae:	bdb0      	pop	{r4, r5, r7, pc}

c0d025b0 <USBD_SetFeature>:
* @param  req: usb request
* @retval status
*/
void USBD_SetFeature(USBD_HandleTypeDef *pdev , 
                            USBD_SetupReqTypedef *req)
{
c0d025b0:	b5b0      	push	{r4, r5, r7, lr}
c0d025b2:	460d      	mov	r5, r1
c0d025b4:	4604      	mov	r4, r0

  if (req->wValue == USB_FEATURE_REMOTE_WAKEUP)
c0d025b6:	8868      	ldrh	r0, [r5, #2]
c0d025b8:	2801      	cmp	r0, #1
c0d025ba:	d117      	bne.n	c0d025ec <USBD_SetFeature+0x3c>
  {
    pdev->dev_remote_wakeup = 1;  
c0d025bc:	2041      	movs	r0, #65	; 0x41
c0d025be:	0080      	lsls	r0, r0, #2
c0d025c0:	2101      	movs	r1, #1
c0d025c2:	5021      	str	r1, [r4, r0]
    if(usbd_is_valid_intf(pdev, LOBYTE(req->wIndex))) {
c0d025c4:	7928      	ldrb	r0, [r5, #4]
/** @defgroup USBD_REQ_Private_Functions
  * @{
  */ 

unsigned int usbd_is_valid_intf(USBD_HandleTypeDef *pdev , unsigned int intf) {
  return intf < USBD_MAX_NUM_INTERFACES && pdev->interfacesClass[intf].pClass != NULL;
c0d025c6:	2802      	cmp	r0, #2
c0d025c8:	d80d      	bhi.n	c0d025e6 <USBD_SetFeature+0x36>
c0d025ca:	00c0      	lsls	r0, r0, #3
c0d025cc:	1820      	adds	r0, r4, r0
c0d025ce:	2145      	movs	r1, #69	; 0x45
c0d025d0:	0089      	lsls	r1, r1, #2
c0d025d2:	5840      	ldr	r0, [r0, r1]
{

  if (req->wValue == USB_FEATURE_REMOTE_WAKEUP)
  {
    pdev->dev_remote_wakeup = 1;  
    if(usbd_is_valid_intf(pdev, LOBYTE(req->wIndex))) {
c0d025d4:	2800      	cmp	r0, #0
c0d025d6:	d006      	beq.n	c0d025e6 <USBD_SetFeature+0x36>
      ((Setup_t)PIC(pdev->interfacesClass[LOBYTE(req->wIndex)].pClass->Setup)) (pdev, req);   
c0d025d8:	6880      	ldr	r0, [r0, #8]
c0d025da:	f7ff fa07 	bl	c0d019ec <pic>
c0d025de:	4602      	mov	r2, r0
c0d025e0:	4620      	mov	r0, r4
c0d025e2:	4629      	mov	r1, r5
c0d025e4:	4790      	blx	r2
    }
    USBD_CtlSendStatus(pdev);
c0d025e6:	4620      	mov	r0, r4
c0d025e8:	f000 fa5d 	bl	c0d02aa6 <USBD_CtlSendStatus>
  }

}
c0d025ec:	bdb0      	pop	{r4, r5, r7, pc}

c0d025ee <USBD_ClrFeature>:
* @param  req: usb request
* @retval status
*/
void USBD_ClrFeature(USBD_HandleTypeDef *pdev , 
                            USBD_SetupReqTypedef *req)
{
c0d025ee:	b5b0      	push	{r4, r5, r7, lr}
c0d025f0:	460d      	mov	r5, r1
c0d025f2:	4604      	mov	r4, r0
  switch (pdev->dev_state)
c0d025f4:	20fc      	movs	r0, #252	; 0xfc
c0d025f6:	5c20      	ldrb	r0, [r4, r0]
c0d025f8:	21fe      	movs	r1, #254	; 0xfe
c0d025fa:	4001      	ands	r1, r0
c0d025fc:	2902      	cmp	r1, #2
c0d025fe:	d11b      	bne.n	c0d02638 <USBD_ClrFeature+0x4a>
  {
  case USBD_STATE_ADDRESSED:
  case USBD_STATE_CONFIGURED:
    if (req->wValue == USB_FEATURE_REMOTE_WAKEUP) 
c0d02600:	8868      	ldrh	r0, [r5, #2]
c0d02602:	2801      	cmp	r0, #1
c0d02604:	d11c      	bne.n	c0d02640 <USBD_ClrFeature+0x52>
    {
      pdev->dev_remote_wakeup = 0; 
c0d02606:	2041      	movs	r0, #65	; 0x41
c0d02608:	0080      	lsls	r0, r0, #2
c0d0260a:	2100      	movs	r1, #0
c0d0260c:	5021      	str	r1, [r4, r0]
      if(usbd_is_valid_intf(pdev, LOBYTE(req->wIndex))) {
c0d0260e:	7928      	ldrb	r0, [r5, #4]
/** @defgroup USBD_REQ_Private_Functions
  * @{
  */ 

unsigned int usbd_is_valid_intf(USBD_HandleTypeDef *pdev , unsigned int intf) {
  return intf < USBD_MAX_NUM_INTERFACES && pdev->interfacesClass[intf].pClass != NULL;
c0d02610:	2802      	cmp	r0, #2
c0d02612:	d80d      	bhi.n	c0d02630 <USBD_ClrFeature+0x42>
c0d02614:	00c0      	lsls	r0, r0, #3
c0d02616:	1820      	adds	r0, r4, r0
c0d02618:	2145      	movs	r1, #69	; 0x45
c0d0261a:	0089      	lsls	r1, r1, #2
c0d0261c:	5840      	ldr	r0, [r0, r1]
  case USBD_STATE_ADDRESSED:
  case USBD_STATE_CONFIGURED:
    if (req->wValue == USB_FEATURE_REMOTE_WAKEUP) 
    {
      pdev->dev_remote_wakeup = 0; 
      if(usbd_is_valid_intf(pdev, LOBYTE(req->wIndex))) {
c0d0261e:	2800      	cmp	r0, #0
c0d02620:	d006      	beq.n	c0d02630 <USBD_ClrFeature+0x42>
        ((Setup_t)PIC(pdev->interfacesClass[LOBYTE(req->wIndex)].pClass->Setup)) (pdev, req);   
c0d02622:	6880      	ldr	r0, [r0, #8]
c0d02624:	f7ff f9e2 	bl	c0d019ec <pic>
c0d02628:	4602      	mov	r2, r0
c0d0262a:	4620      	mov	r0, r4
c0d0262c:	4629      	mov	r1, r5
c0d0262e:	4790      	blx	r2
      }
      USBD_CtlSendStatus(pdev);
c0d02630:	4620      	mov	r0, r4
c0d02632:	f000 fa38 	bl	c0d02aa6 <USBD_CtlSendStatus>
    
  default :
     USBD_CtlError(pdev , req);
    break;
  }
}
c0d02636:	bdb0      	pop	{r4, r5, r7, pc}
      USBD_CtlSendStatus(pdev);
    }
    break;
    
  default :
     USBD_CtlError(pdev , req);
c0d02638:	4620      	mov	r0, r4
c0d0263a:	4629      	mov	r1, r5
c0d0263c:	f000 f801 	bl	c0d02642 <USBD_CtlError>
    break;
  }
}
c0d02640:	bdb0      	pop	{r4, r5, r7, pc}

c0d02642 <USBD_CtlError>:
  USBD_LL_StallEP(pdev , 0);
}

__weak void USBD_CtlError( USBD_HandleTypeDef *pdev ,
                            USBD_SetupReqTypedef *req)
{
c0d02642:	b510      	push	{r4, lr}
c0d02644:	4604      	mov	r4, r0
* @param  req: usb request
* @retval None
*/
void USBD_CtlStall( USBD_HandleTypeDef *pdev)
{
  USBD_LL_StallEP(pdev , 0x80);
c0d02646:	2180      	movs	r1, #128	; 0x80
c0d02648:	f7ff fbe6 	bl	c0d01e18 <USBD_LL_StallEP>
  USBD_LL_StallEP(pdev , 0);
c0d0264c:	2100      	movs	r1, #0
c0d0264e:	4620      	mov	r0, r4
c0d02650:	f7ff fbe2 	bl	c0d01e18 <USBD_LL_StallEP>

__weak void USBD_CtlError( USBD_HandleTypeDef *pdev ,
                            USBD_SetupReqTypedef *req)
{
  USBD_CtlStall(pdev);
}
c0d02654:	bd10      	pop	{r4, pc}

c0d02656 <USBD_StdItfReq>:
* @param  pdev: device instance
* @param  req: usb request
* @retval status
*/
USBD_StatusTypeDef  USBD_StdItfReq (USBD_HandleTypeDef *pdev , USBD_SetupReqTypedef  *req)
{
c0d02656:	b5b0      	push	{r4, r5, r7, lr}
c0d02658:	460d      	mov	r5, r1
c0d0265a:	4604      	mov	r4, r0
  USBD_StatusTypeDef ret = USBD_OK; 
  
  switch (pdev->dev_state) 
c0d0265c:	20fc      	movs	r0, #252	; 0xfc
c0d0265e:	5c20      	ldrb	r0, [r4, r0]
c0d02660:	2803      	cmp	r0, #3
c0d02662:	d117      	bne.n	c0d02694 <USBD_StdItfReq+0x3e>
  {
  case USBD_STATE_CONFIGURED:
    
    if (usbd_is_valid_intf(pdev, LOBYTE(req->wIndex))) 
c0d02664:	7928      	ldrb	r0, [r5, #4]
/** @defgroup USBD_REQ_Private_Functions
  * @{
  */ 

unsigned int usbd_is_valid_intf(USBD_HandleTypeDef *pdev , unsigned int intf) {
  return intf < USBD_MAX_NUM_INTERFACES && pdev->interfacesClass[intf].pClass != NULL;
c0d02666:	2802      	cmp	r0, #2
c0d02668:	d814      	bhi.n	c0d02694 <USBD_StdItfReq+0x3e>
c0d0266a:	00c0      	lsls	r0, r0, #3
c0d0266c:	1820      	adds	r0, r4, r0
c0d0266e:	2145      	movs	r1, #69	; 0x45
c0d02670:	0089      	lsls	r1, r1, #2
c0d02672:	5840      	ldr	r0, [r0, r1]
  
  switch (pdev->dev_state) 
  {
  case USBD_STATE_CONFIGURED:
    
    if (usbd_is_valid_intf(pdev, LOBYTE(req->wIndex))) 
c0d02674:	2800      	cmp	r0, #0
c0d02676:	d00d      	beq.n	c0d02694 <USBD_StdItfReq+0x3e>
    {
      ((Setup_t)PIC(pdev->interfacesClass[LOBYTE(req->wIndex)].pClass->Setup)) (pdev, req);
c0d02678:	6880      	ldr	r0, [r0, #8]
c0d0267a:	f7ff f9b7 	bl	c0d019ec <pic>
c0d0267e:	4602      	mov	r2, r0
c0d02680:	4620      	mov	r0, r4
c0d02682:	4629      	mov	r1, r5
c0d02684:	4790      	blx	r2
      
      if((req->wLength == 0)&& (ret == USBD_OK))
c0d02686:	88e8      	ldrh	r0, [r5, #6]
c0d02688:	2800      	cmp	r0, #0
c0d0268a:	d107      	bne.n	c0d0269c <USBD_StdItfReq+0x46>
      {
         USBD_CtlSendStatus(pdev);
c0d0268c:	4620      	mov	r0, r4
c0d0268e:	f000 fa0a 	bl	c0d02aa6 <USBD_CtlSendStatus>
c0d02692:	e003      	b.n	c0d0269c <USBD_StdItfReq+0x46>
c0d02694:	4620      	mov	r0, r4
c0d02696:	4629      	mov	r1, r5
c0d02698:	f7ff ffd3 	bl	c0d02642 <USBD_CtlError>
    
  default:
     USBD_CtlError(pdev , req);
    break;
  }
  return USBD_OK;
c0d0269c:	2000      	movs	r0, #0
c0d0269e:	bdb0      	pop	{r4, r5, r7, pc}

c0d026a0 <USBD_StdEPReq>:
* @param  pdev: device instance
* @param  req: usb request
* @retval status
*/
USBD_StatusTypeDef  USBD_StdEPReq (USBD_HandleTypeDef *pdev , USBD_SetupReqTypedef  *req)
{
c0d026a0:	b570      	push	{r4, r5, r6, lr}
c0d026a2:	460d      	mov	r5, r1
c0d026a4:	4604      	mov	r4, r0
  USBD_StatusTypeDef ret = USBD_OK; 
  USBD_EndpointTypeDef   *pep;
  ep_addr  = LOBYTE(req->wIndex);   
  
  /* Check if it is a class request */
  if ((req->bmRequest & 0x60) == 0x20 && usbd_is_valid_intf(pdev, LOBYTE(req->wIndex)))
c0d026a6:	7828      	ldrb	r0, [r5, #0]
c0d026a8:	2160      	movs	r1, #96	; 0x60
c0d026aa:	4001      	ands	r1, r0
{
  
  uint8_t   ep_addr;
  USBD_StatusTypeDef ret = USBD_OK; 
  USBD_EndpointTypeDef   *pep;
  ep_addr  = LOBYTE(req->wIndex);   
c0d026ac:	792e      	ldrb	r6, [r5, #4]
  
  /* Check if it is a class request */
  if ((req->bmRequest & 0x60) == 0x20 && usbd_is_valid_intf(pdev, LOBYTE(req->wIndex)))
c0d026ae:	2920      	cmp	r1, #32
c0d026b0:	d110      	bne.n	c0d026d4 <USBD_StdEPReq+0x34>
/** @defgroup USBD_REQ_Private_Functions
  * @{
  */ 

unsigned int usbd_is_valid_intf(USBD_HandleTypeDef *pdev , unsigned int intf) {
  return intf < USBD_MAX_NUM_INTERFACES && pdev->interfacesClass[intf].pClass != NULL;
c0d026b2:	2e02      	cmp	r6, #2
c0d026b4:	d80e      	bhi.n	c0d026d4 <USBD_StdEPReq+0x34>
c0d026b6:	00f0      	lsls	r0, r6, #3
c0d026b8:	1820      	adds	r0, r4, r0
c0d026ba:	2145      	movs	r1, #69	; 0x45
c0d026bc:	0089      	lsls	r1, r1, #2
c0d026be:	5840      	ldr	r0, [r0, r1]
  USBD_StatusTypeDef ret = USBD_OK; 
  USBD_EndpointTypeDef   *pep;
  ep_addr  = LOBYTE(req->wIndex);   
  
  /* Check if it is a class request */
  if ((req->bmRequest & 0x60) == 0x20 && usbd_is_valid_intf(pdev, LOBYTE(req->wIndex)))
c0d026c0:	2800      	cmp	r0, #0
c0d026c2:	d007      	beq.n	c0d026d4 <USBD_StdEPReq+0x34>
  {
    ((Setup_t)PIC(pdev->interfacesClass[LOBYTE(req->wIndex)].pClass->Setup)) (pdev, req);
c0d026c4:	6880      	ldr	r0, [r0, #8]
c0d026c6:	f7ff f991 	bl	c0d019ec <pic>
c0d026ca:	4602      	mov	r2, r0
c0d026cc:	4620      	mov	r0, r4
c0d026ce:	4629      	mov	r1, r5
c0d026d0:	4790      	blx	r2
c0d026d2:	e06e      	b.n	c0d027b2 <USBD_StdEPReq+0x112>
    
    return USBD_OK;
  }
  
  switch (req->bRequest) 
c0d026d4:	7868      	ldrb	r0, [r5, #1]
c0d026d6:	2800      	cmp	r0, #0
c0d026d8:	d017      	beq.n	c0d0270a <USBD_StdEPReq+0x6a>
c0d026da:	2801      	cmp	r0, #1
c0d026dc:	d01e      	beq.n	c0d0271c <USBD_StdEPReq+0x7c>
c0d026de:	2803      	cmp	r0, #3
c0d026e0:	d167      	bne.n	c0d027b2 <USBD_StdEPReq+0x112>
  {
    
  case USB_REQ_SET_FEATURE :
    
    switch (pdev->dev_state) 
c0d026e2:	20fc      	movs	r0, #252	; 0xfc
c0d026e4:	5c20      	ldrb	r0, [r4, r0]
c0d026e6:	2803      	cmp	r0, #3
c0d026e8:	d11c      	bne.n	c0d02724 <USBD_StdEPReq+0x84>
        USBD_LL_StallEP(pdev , ep_addr);
      }
      break;	
      
    case USBD_STATE_CONFIGURED:   
      if (req->wValue == USB_FEATURE_EP_HALT)
c0d026ea:	8868      	ldrh	r0, [r5, #2]
c0d026ec:	2800      	cmp	r0, #0
c0d026ee:	d108      	bne.n	c0d02702 <USBD_StdEPReq+0x62>
      {
        if ((ep_addr != 0x00) && (ep_addr != 0x80)) 
c0d026f0:	2080      	movs	r0, #128	; 0x80
c0d026f2:	4330      	orrs	r0, r6
c0d026f4:	2880      	cmp	r0, #128	; 0x80
c0d026f6:	d004      	beq.n	c0d02702 <USBD_StdEPReq+0x62>
        { 
          USBD_LL_StallEP(pdev , ep_addr);
c0d026f8:	4620      	mov	r0, r4
c0d026fa:	4631      	mov	r1, r6
c0d026fc:	f7ff fb8c 	bl	c0d01e18 <USBD_LL_StallEP>
          
        }
c0d02700:	792e      	ldrb	r6, [r5, #4]
/** @defgroup USBD_REQ_Private_Functions
  * @{
  */ 

unsigned int usbd_is_valid_intf(USBD_HandleTypeDef *pdev , unsigned int intf) {
  return intf < USBD_MAX_NUM_INTERFACES && pdev->interfacesClass[intf].pClass != NULL;
c0d02702:	2e02      	cmp	r6, #2
c0d02704:	d852      	bhi.n	c0d027ac <USBD_StdEPReq+0x10c>
c0d02706:	00f0      	lsls	r0, r6, #3
c0d02708:	e043      	b.n	c0d02792 <USBD_StdEPReq+0xf2>
      break;    
    }
    break;
    
  case USB_REQ_GET_STATUS:                  
    switch (pdev->dev_state) 
c0d0270a:	20fc      	movs	r0, #252	; 0xfc
c0d0270c:	5c20      	ldrb	r0, [r4, r0]
c0d0270e:	2803      	cmp	r0, #3
c0d02710:	d018      	beq.n	c0d02744 <USBD_StdEPReq+0xa4>
c0d02712:	2802      	cmp	r0, #2
c0d02714:	d111      	bne.n	c0d0273a <USBD_StdEPReq+0x9a>
    {
    case USBD_STATE_ADDRESSED:          
      if ((ep_addr & 0x7F) != 0x00) 
c0d02716:	0670      	lsls	r0, r6, #25
c0d02718:	d10a      	bne.n	c0d02730 <USBD_StdEPReq+0x90>
c0d0271a:	e04a      	b.n	c0d027b2 <USBD_StdEPReq+0x112>
    }
    break;
    
  case USB_REQ_CLEAR_FEATURE :
    
    switch (pdev->dev_state) 
c0d0271c:	20fc      	movs	r0, #252	; 0xfc
c0d0271e:	5c20      	ldrb	r0, [r4, r0]
c0d02720:	2803      	cmp	r0, #3
c0d02722:	d029      	beq.n	c0d02778 <USBD_StdEPReq+0xd8>
c0d02724:	2802      	cmp	r0, #2
c0d02726:	d108      	bne.n	c0d0273a <USBD_StdEPReq+0x9a>
c0d02728:	2080      	movs	r0, #128	; 0x80
c0d0272a:	4330      	orrs	r0, r6
c0d0272c:	2880      	cmp	r0, #128	; 0x80
c0d0272e:	d040      	beq.n	c0d027b2 <USBD_StdEPReq+0x112>
c0d02730:	4620      	mov	r0, r4
c0d02732:	4631      	mov	r1, r6
c0d02734:	f7ff fb70 	bl	c0d01e18 <USBD_LL_StallEP>
c0d02738:	e03b      	b.n	c0d027b2 <USBD_StdEPReq+0x112>
c0d0273a:	4620      	mov	r0, r4
c0d0273c:	4629      	mov	r1, r5
c0d0273e:	f7ff ff80 	bl	c0d02642 <USBD_CtlError>
c0d02742:	e036      	b.n	c0d027b2 <USBD_StdEPReq+0x112>
        USBD_LL_StallEP(pdev , ep_addr);
      }
      break;	
      
    case USBD_STATE_CONFIGURED:
      pep = ((ep_addr & 0x80) == 0x80) ? &pdev->ep_in[ep_addr & 0x7F]:\
c0d02744:	4625      	mov	r5, r4
c0d02746:	3514      	adds	r5, #20
                                         &pdev->ep_out[ep_addr & 0x7F];
c0d02748:	4620      	mov	r0, r4
c0d0274a:	3084      	adds	r0, #132	; 0x84
        USBD_LL_StallEP(pdev , ep_addr);
      }
      break;	
      
    case USBD_STATE_CONFIGURED:
      pep = ((ep_addr & 0x80) == 0x80) ? &pdev->ep_in[ep_addr & 0x7F]:\
c0d0274c:	2180      	movs	r1, #128	; 0x80
c0d0274e:	420e      	tst	r6, r1
c0d02750:	d100      	bne.n	c0d02754 <USBD_StdEPReq+0xb4>
c0d02752:	4605      	mov	r5, r0
                                         &pdev->ep_out[ep_addr & 0x7F];
      if(USBD_LL_IsStallEP(pdev, ep_addr))
c0d02754:	4620      	mov	r0, r4
c0d02756:	4631      	mov	r1, r6
c0d02758:	f7ff fba8 	bl	c0d01eac <USBD_LL_IsStallEP>
c0d0275c:	2101      	movs	r1, #1
c0d0275e:	2800      	cmp	r0, #0
c0d02760:	d100      	bne.n	c0d02764 <USBD_StdEPReq+0xc4>
c0d02762:	4601      	mov	r1, r0
c0d02764:	207f      	movs	r0, #127	; 0x7f
c0d02766:	4006      	ands	r6, r0
c0d02768:	0130      	lsls	r0, r6, #4
c0d0276a:	5029      	str	r1, [r5, r0]
c0d0276c:	1829      	adds	r1, r5, r0
      else
      {
        pep->status = 0x0000;  
      }
      
      USBD_CtlSendData (pdev,
c0d0276e:	2202      	movs	r2, #2
c0d02770:	4620      	mov	r0, r4
c0d02772:	f000 f96d 	bl	c0d02a50 <USBD_CtlSendData>
c0d02776:	e01c      	b.n	c0d027b2 <USBD_StdEPReq+0x112>
        USBD_LL_StallEP(pdev , ep_addr);
      }
      break;	
      
    case USBD_STATE_CONFIGURED:   
      if (req->wValue == USB_FEATURE_EP_HALT)
c0d02778:	8868      	ldrh	r0, [r5, #2]
c0d0277a:	2800      	cmp	r0, #0
c0d0277c:	d119      	bne.n	c0d027b2 <USBD_StdEPReq+0x112>
      {
        if ((ep_addr & 0x7F) != 0x00) 
c0d0277e:	0670      	lsls	r0, r6, #25
c0d02780:	d014      	beq.n	c0d027ac <USBD_StdEPReq+0x10c>
        {        
          USBD_LL_ClearStallEP(pdev , ep_addr);
c0d02782:	4620      	mov	r0, r4
c0d02784:	4631      	mov	r1, r6
c0d02786:	f7ff fb6d 	bl	c0d01e64 <USBD_LL_ClearStallEP>
          if(usbd_is_valid_intf(pdev, LOBYTE(req->wIndex))) {
c0d0278a:	7928      	ldrb	r0, [r5, #4]
/** @defgroup USBD_REQ_Private_Functions
  * @{
  */ 

unsigned int usbd_is_valid_intf(USBD_HandleTypeDef *pdev , unsigned int intf) {
  return intf < USBD_MAX_NUM_INTERFACES && pdev->interfacesClass[intf].pClass != NULL;
c0d0278c:	2802      	cmp	r0, #2
c0d0278e:	d80d      	bhi.n	c0d027ac <USBD_StdEPReq+0x10c>
c0d02790:	00c0      	lsls	r0, r0, #3
c0d02792:	1820      	adds	r0, r4, r0
c0d02794:	2145      	movs	r1, #69	; 0x45
c0d02796:	0089      	lsls	r1, r1, #2
c0d02798:	5840      	ldr	r0, [r0, r1]
c0d0279a:	2800      	cmp	r0, #0
c0d0279c:	d006      	beq.n	c0d027ac <USBD_StdEPReq+0x10c>
c0d0279e:	6880      	ldr	r0, [r0, #8]
c0d027a0:	f7ff f924 	bl	c0d019ec <pic>
c0d027a4:	4602      	mov	r2, r0
c0d027a6:	4620      	mov	r0, r4
c0d027a8:	4629      	mov	r1, r5
c0d027aa:	4790      	blx	r2
c0d027ac:	4620      	mov	r0, r4
c0d027ae:	f000 f97a 	bl	c0d02aa6 <USBD_CtlSendStatus>
    
  default:
    break;
  }
  return ret;
}
c0d027b2:	2000      	movs	r0, #0
c0d027b4:	bd70      	pop	{r4, r5, r6, pc}

c0d027b6 <USBD_ParseSetupRequest>:
* @retval None
*/

void USBD_ParseSetupRequest(USBD_SetupReqTypedef *req, uint8_t *pdata)
{
  req->bmRequest     = *(uint8_t *)  (pdata);
c0d027b6:	780a      	ldrb	r2, [r1, #0]
c0d027b8:	7002      	strb	r2, [r0, #0]
  req->bRequest      = *(uint8_t *)  (pdata +  1);
c0d027ba:	784a      	ldrb	r2, [r1, #1]
c0d027bc:	7042      	strb	r2, [r0, #1]
  req->wValue        = SWAPBYTE      (pdata +  2);
c0d027be:	788a      	ldrb	r2, [r1, #2]
c0d027c0:	78cb      	ldrb	r3, [r1, #3]
c0d027c2:	021b      	lsls	r3, r3, #8
c0d027c4:	4313      	orrs	r3, r2
c0d027c6:	8043      	strh	r3, [r0, #2]
  req->wIndex        = SWAPBYTE      (pdata +  4);
c0d027c8:	790a      	ldrb	r2, [r1, #4]
c0d027ca:	794b      	ldrb	r3, [r1, #5]
c0d027cc:	021b      	lsls	r3, r3, #8
c0d027ce:	4313      	orrs	r3, r2
c0d027d0:	8083      	strh	r3, [r0, #4]
  req->wLength       = SWAPBYTE      (pdata +  6);
c0d027d2:	798a      	ldrb	r2, [r1, #6]
c0d027d4:	79c9      	ldrb	r1, [r1, #7]
c0d027d6:	0209      	lsls	r1, r1, #8
c0d027d8:	4311      	orrs	r1, r2
c0d027da:	80c1      	strh	r1, [r0, #6]

}
c0d027dc:	4770      	bx	lr

c0d027de <USBD_HID_Setup>:
  * @param  req: usb requests
  * @retval status
  */
uint8_t  USBD_HID_Setup (USBD_HandleTypeDef *pdev, 
                                USBD_SetupReqTypedef *req)
{
c0d027de:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d027e0:	b083      	sub	sp, #12
c0d027e2:	460d      	mov	r5, r1
c0d027e4:	4604      	mov	r4, r0
c0d027e6:	a802      	add	r0, sp, #8
c0d027e8:	2700      	movs	r7, #0
  uint16_t len = 0;
c0d027ea:	8007      	strh	r7, [r0, #0]
c0d027ec:	a801      	add	r0, sp, #4
  uint8_t  *pbuf = NULL;

  uint8_t val = 0;
c0d027ee:	7007      	strb	r7, [r0, #0]

  switch (req->bmRequest & USB_REQ_TYPE_MASK)
c0d027f0:	7829      	ldrb	r1, [r5, #0]
c0d027f2:	2060      	movs	r0, #96	; 0x60
c0d027f4:	4008      	ands	r0, r1
c0d027f6:	2800      	cmp	r0, #0
c0d027f8:	d010      	beq.n	c0d0281c <USBD_HID_Setup+0x3e>
c0d027fa:	2820      	cmp	r0, #32
c0d027fc:	d138      	bne.n	c0d02870 <USBD_HID_Setup+0x92>
c0d027fe:	7868      	ldrb	r0, [r5, #1]
  {
  case USB_REQ_TYPE_CLASS :  
    switch (req->bRequest)
c0d02800:	4601      	mov	r1, r0
c0d02802:	390a      	subs	r1, #10
c0d02804:	2902      	cmp	r1, #2
c0d02806:	d333      	bcc.n	c0d02870 <USBD_HID_Setup+0x92>
c0d02808:	2802      	cmp	r0, #2
c0d0280a:	d01c      	beq.n	c0d02846 <USBD_HID_Setup+0x68>
c0d0280c:	2803      	cmp	r0, #3
c0d0280e:	d01a      	beq.n	c0d02846 <USBD_HID_Setup+0x68>
                        (uint8_t *)&val,
                        1);      
      break;      
      
    default:
      USBD_CtlError (pdev, req);
c0d02810:	4620      	mov	r0, r4
c0d02812:	4629      	mov	r1, r5
c0d02814:	f7ff ff15 	bl	c0d02642 <USBD_CtlError>
c0d02818:	2702      	movs	r7, #2
c0d0281a:	e029      	b.n	c0d02870 <USBD_HID_Setup+0x92>
      return USBD_FAIL; 
    }
    break;
    
  case USB_REQ_TYPE_STANDARD:
    switch (req->bRequest)
c0d0281c:	7868      	ldrb	r0, [r5, #1]
c0d0281e:	280b      	cmp	r0, #11
c0d02820:	d014      	beq.n	c0d0284c <USBD_HID_Setup+0x6e>
c0d02822:	280a      	cmp	r0, #10
c0d02824:	d00f      	beq.n	c0d02846 <USBD_HID_Setup+0x68>
c0d02826:	2806      	cmp	r0, #6
c0d02828:	d122      	bne.n	c0d02870 <USBD_HID_Setup+0x92>
    {
    case USB_REQ_GET_DESCRIPTOR: 
      // 0x22
      if( req->wValue >> 8 == HID_REPORT_DESC)
c0d0282a:	8868      	ldrh	r0, [r5, #2]
c0d0282c:	0a00      	lsrs	r0, r0, #8
c0d0282e:	2700      	movs	r7, #0
c0d02830:	2821      	cmp	r0, #33	; 0x21
c0d02832:	d00f      	beq.n	c0d02854 <USBD_HID_Setup+0x76>
c0d02834:	2822      	cmp	r0, #34	; 0x22
      
      //USBD_CtlReceiveStatus(pdev);
      
      USBD_CtlSendData (pdev, 
                        pbuf,
                        len);
c0d02836:	463a      	mov	r2, r7
c0d02838:	4639      	mov	r1, r7
c0d0283a:	d116      	bne.n	c0d0286a <USBD_HID_Setup+0x8c>
c0d0283c:	ae02      	add	r6, sp, #8
    {
    case USB_REQ_GET_DESCRIPTOR: 
      // 0x22
      if( req->wValue >> 8 == HID_REPORT_DESC)
      {
        pbuf =  USBD_HID_GetReportDescriptor_impl(&len);
c0d0283e:	4630      	mov	r0, r6
c0d02840:	f000 f852 	bl	c0d028e8 <USBD_HID_GetReportDescriptor_impl>
c0d02844:	e00a      	b.n	c0d0285c <USBD_HID_Setup+0x7e>
c0d02846:	a901      	add	r1, sp, #4
c0d02848:	2201      	movs	r2, #1
c0d0284a:	e00e      	b.n	c0d0286a <USBD_HID_Setup+0x8c>
                        len);
      break;

    case USB_REQ_SET_INTERFACE :
      //hhid->AltSetting = (uint8_t)(req->wValue);
      USBD_CtlSendStatus(pdev);
c0d0284c:	4620      	mov	r0, r4
c0d0284e:	f000 f92a 	bl	c0d02aa6 <USBD_CtlSendStatus>
c0d02852:	e00d      	b.n	c0d02870 <USBD_HID_Setup+0x92>
c0d02854:	ae02      	add	r6, sp, #8
        len = MIN(len , req->wLength);
      }
      // 0x21
      else if( req->wValue >> 8 == HID_DESCRIPTOR_TYPE)
      {
        pbuf = USBD_HID_GetHidDescriptor_impl(&len);
c0d02856:	4630      	mov	r0, r6
c0d02858:	f000 f832 	bl	c0d028c0 <USBD_HID_GetHidDescriptor_impl>
c0d0285c:	4601      	mov	r1, r0
c0d0285e:	8832      	ldrh	r2, [r6, #0]
c0d02860:	88e8      	ldrh	r0, [r5, #6]
c0d02862:	4282      	cmp	r2, r0
c0d02864:	d300      	bcc.n	c0d02868 <USBD_HID_Setup+0x8a>
c0d02866:	4602      	mov	r2, r0
c0d02868:	8032      	strh	r2, [r6, #0]
c0d0286a:	4620      	mov	r0, r4
c0d0286c:	f000 f8f0 	bl	c0d02a50 <USBD_CtlSendData>
      
    }
  }

  return USBD_OK;
}
c0d02870:	b2f8      	uxtb	r0, r7
c0d02872:	b003      	add	sp, #12
c0d02874:	bdf0      	pop	{r4, r5, r6, r7, pc}

c0d02876 <USBD_HID_Init>:
  * @param  cfgidx: Configuration index
  * @retval status
  */
uint8_t  USBD_HID_Init (USBD_HandleTypeDef *pdev, 
                               uint8_t cfgidx)
{
c0d02876:	b5f0      	push	{r4, r5, r6, r7, lr}
c0d02878:	b081      	sub	sp, #4
c0d0287a:	4604      	mov	r4, r0
  UNUSED(cfgidx);

  /* Open EP IN */
  USBD_LL_OpenEP(pdev,
c0d0287c:	2182      	movs	r1, #130	; 0x82
c0d0287e:	2603      	movs	r6, #3
c0d02880:	2540      	movs	r5, #64	; 0x40
c0d02882:	4632      	mov	r2, r6
c0d02884:	462b      	mov	r3, r5
c0d02886:	f7ff fa8b 	bl	c0d01da0 <USBD_LL_OpenEP>
c0d0288a:	2702      	movs	r7, #2
                 HID_EPIN_ADDR,
                 USBD_EP_TYPE_INTR,
                 HID_EPIN_SIZE);
  
  /* Open EP OUT */
  USBD_LL_OpenEP(pdev,
c0d0288c:	4620      	mov	r0, r4
c0d0288e:	4639      	mov	r1, r7
c0d02890:	4632      	mov	r2, r6
c0d02892:	462b      	mov	r3, r5
c0d02894:	f7ff fa84 	bl	c0d01da0 <USBD_LL_OpenEP>
                 HID_EPOUT_ADDR,
                 USBD_EP_TYPE_INTR,
                 HID_EPOUT_SIZE);

        /* Prepare Out endpoint to receive 1st packet */ 
  USBD_LL_PrepareReceive(pdev, HID_EPOUT_ADDR, HID_EPOUT_SIZE);
c0d02898:	4620      	mov	r0, r4
c0d0289a:	4639      	mov	r1, r7
c0d0289c:	462a      	mov	r2, r5
c0d0289e:	f7ff fb42 	bl	c0d01f26 <USBD_LL_PrepareReceive>
  USBD_LL_Transmit (pdev, 
                    HID_EPIN_ADDR,                                      
                    NULL,
                    0);
  */
  return USBD_OK;
c0d028a2:	2000      	movs	r0, #0
c0d028a4:	b001      	add	sp, #4
c0d028a6:	bdf0      	pop	{r4, r5, r6, r7, pc}

c0d028a8 <USBD_HID_DeInit>:
  * @param  cfgidx: Configuration index
  * @retval status
  */
uint8_t  USBD_HID_DeInit (USBD_HandleTypeDef *pdev, 
                                 uint8_t cfgidx)
{
c0d028a8:	b510      	push	{r4, lr}
c0d028aa:	4604      	mov	r4, r0
  UNUSED(cfgidx);
  /* Close HID EP IN */
  USBD_LL_CloseEP(pdev,
c0d028ac:	2182      	movs	r1, #130	; 0x82
c0d028ae:	f7ff fa9d 	bl	c0d01dec <USBD_LL_CloseEP>
                  HID_EPIN_ADDR);
  
  /* Close HID EP OUT */
  USBD_LL_CloseEP(pdev,
c0d028b2:	2102      	movs	r1, #2
c0d028b4:	4620      	mov	r0, r4
c0d028b6:	f7ff fa99 	bl	c0d01dec <USBD_LL_CloseEP>
                  HID_EPOUT_ADDR);
  
  return USBD_OK;
c0d028ba:	2000      	movs	r0, #0
c0d028bc:	bd10      	pop	{r4, pc}
	...

c0d028c0 <USBD_HID_GetHidDescriptor_impl>:
{
  *length = sizeof (USBD_CfgDesc);
  return (uint8_t*)USBD_CfgDesc;
}

uint8_t* USBD_HID_GetHidDescriptor_impl(uint16_t* len) {
c0d028c0:	4601      	mov	r1, r0
  switch (USBD_Device.request.wIndex&0xFF) {
c0d028c2:	2043      	movs	r0, #67	; 0x43
c0d028c4:	0080      	lsls	r0, r0, #2
c0d028c6:	4a06      	ldr	r2, [pc, #24]	; (c0d028e0 <USBD_HID_GetHidDescriptor_impl+0x20>)
c0d028c8:	5c12      	ldrb	r2, [r2, r0]
      *len = sizeof(USBD_HID_Desc_fido);
      return (uint8_t*)USBD_HID_Desc_fido; 
#endif // HAVE_IO_U2F
    case HID_INTF:
      *len = sizeof(USBD_HID_Desc);
      return (uint8_t*)USBD_HID_Desc; 
c0d028ca:	2309      	movs	r3, #9
c0d028cc:	2000      	movs	r0, #0
c0d028ce:	2a00      	cmp	r2, #0
c0d028d0:	d000      	beq.n	c0d028d4 <USBD_HID_GetHidDescriptor_impl+0x14>
c0d028d2:	4603      	mov	r3, r0
    case U2F_INTF:
      *len = sizeof(USBD_HID_Desc_fido);
      return (uint8_t*)USBD_HID_Desc_fido; 
#endif // HAVE_IO_U2F
    case HID_INTF:
      *len = sizeof(USBD_HID_Desc);
c0d028d4:	800b      	strh	r3, [r1, #0]
      return (uint8_t*)USBD_HID_Desc; 
c0d028d6:	2a00      	cmp	r2, #0
c0d028d8:	d101      	bne.n	c0d028de <USBD_HID_GetHidDescriptor_impl+0x1e>
c0d028da:	4802      	ldr	r0, [pc, #8]	; (c0d028e4 <USBD_HID_GetHidDescriptor_impl+0x24>)
c0d028dc:	4478      	add	r0, pc
  }
  *len = 0;
  return 0;
}
c0d028de:	4770      	bx	lr
c0d028e0:	20001cb0 	.word	0x20001cb0
c0d028e4:	000006d4 	.word	0x000006d4

c0d028e8 <USBD_HID_GetReportDescriptor_impl>:

uint8_t* USBD_HID_GetReportDescriptor_impl(uint16_t* len) {
c0d028e8:	4601      	mov	r1, r0
  switch (USBD_Device.request.wIndex&0xFF) {
c0d028ea:	2043      	movs	r0, #67	; 0x43
c0d028ec:	0080      	lsls	r0, r0, #2
c0d028ee:	4a06      	ldr	r2, [pc, #24]	; (c0d02908 <USBD_HID_GetReportDescriptor_impl+0x20>)
c0d028f0:	5c12      	ldrb	r2, [r2, r0]
    *len = sizeof(HID_ReportDesc_fido);
    return (uint8_t*)HID_ReportDesc_fido;
#endif // HAVE_IO_U2F
  case HID_INTF:
    *len = sizeof(HID_ReportDesc);
    return (uint8_t*)HID_ReportDesc;
c0d028f2:	2322      	movs	r3, #34	; 0x22
c0d028f4:	2000      	movs	r0, #0
c0d028f6:	2a00      	cmp	r2, #0
c0d028f8:	d000      	beq.n	c0d028fc <USBD_HID_GetReportDescriptor_impl+0x14>
c0d028fa:	4603      	mov	r3, r0

    *len = sizeof(HID_ReportDesc_fido);
    return (uint8_t*)HID_ReportDesc_fido;
#endif // HAVE_IO_U2F
  case HID_INTF:
    *len = sizeof(HID_ReportDesc);
c0d028fc:	800b      	strh	r3, [r1, #0]
    return (uint8_t*)HID_ReportDesc;
c0d028fe:	2a00      	cmp	r2, #0
c0d02900:	d101      	bne.n	c0d02906 <USBD_HID_GetReportDescriptor_impl+0x1e>
c0d02902:	4802      	ldr	r0, [pc, #8]	; (c0d0290c <USBD_HID_GetReportDescriptor_impl+0x24>)
c0d02904:	4478      	add	r0, pc
  }
  *len = 0;
  return 0;
}
c0d02906:	4770      	bx	lr
c0d02908:	20001cb0 	.word	0x20001cb0
c0d0290c:	000006b5 	.word	0x000006b5

c0d02910 <USBD_HID_DataOut_impl>:
}
#endif // HAVE_IO_U2F

uint8_t  USBD_HID_DataOut_impl (USBD_HandleTypeDef *pdev, 
                              uint8_t epnum, uint8_t* buffer)
{
c0d02910:	b5b0      	push	{r4, r5, r7, lr}
c0d02912:	4614      	mov	r4, r2
  // only the data hid endpoint will receive data
  switch (epnum) {
c0d02914:	2902      	cmp	r1, #2
c0d02916:	d11b      	bne.n	c0d02950 <USBD_HID_DataOut_impl+0x40>

  // HID gen endpoint
  case (HID_EPOUT_ADDR&0x7F):
    // prepare receiving the next chunk (masked time)
    USBD_LL_PrepareReceive(pdev, HID_EPOUT_ADDR , HID_EPOUT_SIZE);
c0d02918:	2102      	movs	r1, #2
c0d0291a:	2240      	movs	r2, #64	; 0x40
c0d0291c:	f7ff fb03 	bl	c0d01f26 <USBD_LL_PrepareReceive>

    // avoid troubles when an apdu has not been replied yet
    if (G_io_apdu_media == IO_APDU_MEDIA_NONE) {      
c0d02920:	4d0c      	ldr	r5, [pc, #48]	; (c0d02954 <USBD_HID_DataOut_impl+0x44>)
c0d02922:	7828      	ldrb	r0, [r5, #0]
c0d02924:	2800      	cmp	r0, #0
c0d02926:	d113      	bne.n	c0d02950 <USBD_HID_DataOut_impl+0x40>
      // add to the hid transport
      switch(io_usb_hid_receive(io_usb_send_apdu_data, buffer, io_seproxyhal_get_ep_rx_size(HID_EPOUT_ADDR))) {
c0d02928:	2002      	movs	r0, #2
c0d0292a:	f7fe fc55 	bl	c0d011d8 <io_seproxyhal_get_ep_rx_size>
c0d0292e:	4602      	mov	r2, r0
c0d02930:	480c      	ldr	r0, [pc, #48]	; (c0d02964 <USBD_HID_DataOut_impl+0x54>)
c0d02932:	4478      	add	r0, pc
c0d02934:	4621      	mov	r1, r4
c0d02936:	f7fe faaf 	bl	c0d00e98 <io_usb_hid_receive>
c0d0293a:	2802      	cmp	r0, #2
c0d0293c:	d108      	bne.n	c0d02950 <USBD_HID_DataOut_impl+0x40>
        default:
          break;

        case IO_USB_APDU_RECEIVED:
          G_io_apdu_media = IO_APDU_MEDIA_USB_HID; // for application code
c0d0293e:	2001      	movs	r0, #1
c0d02940:	7028      	strb	r0, [r5, #0]
          G_io_apdu_state = APDU_USB_HID; // for next call to io_exchange
c0d02942:	4805      	ldr	r0, [pc, #20]	; (c0d02958 <USBD_HID_DataOut_impl+0x48>)
c0d02944:	2107      	movs	r1, #7
c0d02946:	7001      	strb	r1, [r0, #0]
          G_io_apdu_length = G_io_usb_hid_total_length;
c0d02948:	4804      	ldr	r0, [pc, #16]	; (c0d0295c <USBD_HID_DataOut_impl+0x4c>)
c0d0294a:	6800      	ldr	r0, [r0, #0]
c0d0294c:	4904      	ldr	r1, [pc, #16]	; (c0d02960 <USBD_HID_DataOut_impl+0x50>)
c0d0294e:	8008      	strh	r0, [r1, #0]
      }
    }
    break;
  }

  return USBD_OK;
c0d02950:	2000      	movs	r0, #0
c0d02952:	bdb0      	pop	{r4, r5, r7, pc}
c0d02954:	20001c4c 	.word	0x20001c4c
c0d02958:	20001c54 	.word	0x20001c54
c0d0295c:	20001b30 	.word	0x20001b30
c0d02960:	20001c56 	.word	0x20001c56
c0d02964:	ffffea1b 	.word	0xffffea1b

c0d02968 <USBD_DeviceDescriptor>:
  * @retval Pointer to descriptor buffer
  */
static uint8_t *USBD_DeviceDescriptor(USBD_SpeedTypeDef speed, uint16_t *length)
{
  UNUSED(speed);
  *length = sizeof(USBD_DeviceDesc);
c0d02968:	2012      	movs	r0, #18
c0d0296a:	8008      	strh	r0, [r1, #0]
  return (uint8_t*)USBD_DeviceDesc;
c0d0296c:	4801      	ldr	r0, [pc, #4]	; (c0d02974 <USBD_DeviceDescriptor+0xc>)
c0d0296e:	4478      	add	r0, pc
c0d02970:	4770      	bx	lr
c0d02972:	46c0      	nop			; (mov r8, r8)
c0d02974:	000006c6 	.word	0x000006c6

c0d02978 <USBD_LangIDStrDescriptor>:
  * @retval Pointer to descriptor buffer
  */
static uint8_t *USBD_LangIDStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length)
{
  UNUSED(speed);
  *length = sizeof(USBD_LangIDDesc);  
c0d02978:	2004      	movs	r0, #4
c0d0297a:	8008      	strh	r0, [r1, #0]
  return (uint8_t*)USBD_LangIDDesc;
c0d0297c:	4801      	ldr	r0, [pc, #4]	; (c0d02984 <USBD_LangIDStrDescriptor+0xc>)
c0d0297e:	4478      	add	r0, pc
c0d02980:	4770      	bx	lr
c0d02982:	46c0      	nop			; (mov r8, r8)
c0d02984:	000006c8 	.word	0x000006c8

c0d02988 <USBD_ManufacturerStrDescriptor>:
  * @retval Pointer to descriptor buffer
  */
static uint8_t *USBD_ManufacturerStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length)
{
  UNUSED(speed);
  *length = sizeof(USBD_MANUFACTURER_STRING);
c0d02988:	200e      	movs	r0, #14
c0d0298a:	8008      	strh	r0, [r1, #0]
  return (uint8_t*)USBD_MANUFACTURER_STRING;
c0d0298c:	4801      	ldr	r0, [pc, #4]	; (c0d02994 <USBD_ManufacturerStrDescriptor+0xc>)
c0d0298e:	4478      	add	r0, pc
c0d02990:	4770      	bx	lr
c0d02992:	46c0      	nop			; (mov r8, r8)
c0d02994:	000006bc 	.word	0x000006bc

c0d02998 <USBD_ProductStrDescriptor>:
  * @retval Pointer to descriptor buffer
  */
static uint8_t *USBD_ProductStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length)
{
  UNUSED(speed);
  *length = sizeof(USBD_PRODUCT_FS_STRING);
c0d02998:	200e      	movs	r0, #14
c0d0299a:	8008      	strh	r0, [r1, #0]
  return (uint8_t*)USBD_PRODUCT_FS_STRING;
c0d0299c:	4801      	ldr	r0, [pc, #4]	; (c0d029a4 <USBD_ProductStrDescriptor+0xc>)
c0d0299e:	4478      	add	r0, pc
c0d029a0:	4770      	bx	lr
c0d029a2:	46c0      	nop			; (mov r8, r8)
c0d029a4:	000006ba 	.word	0x000006ba

c0d029a8 <USBD_SerialStrDescriptor>:
  * @retval Pointer to descriptor buffer
  */
static uint8_t *USBD_SerialStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length)
{
  UNUSED(speed);
  *length = sizeof(USB_SERIAL_STRING);
c0d029a8:	200a      	movs	r0, #10
c0d029aa:	8008      	strh	r0, [r1, #0]
  return (uint8_t*)USB_SERIAL_STRING;
c0d029ac:	4801      	ldr	r0, [pc, #4]	; (c0d029b4 <USBD_SerialStrDescriptor+0xc>)
c0d029ae:	4478      	add	r0, pc
c0d029b0:	4770      	bx	lr
c0d029b2:	46c0      	nop			; (mov r8, r8)
c0d029b4:	000006b8 	.word	0x000006b8

c0d029b8 <USBD_ConfigStrDescriptor>:
  * @retval Pointer to descriptor buffer
  */
static uint8_t *USBD_ConfigStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length)
{
  UNUSED(speed);
  *length = sizeof(USBD_CONFIGURATION_FS_STRING);
c0d029b8:	200e      	movs	r0, #14
c0d029ba:	8008      	strh	r0, [r1, #0]
  return (uint8_t*)USBD_CONFIGURATION_FS_STRING;
c0d029bc:	4801      	ldr	r0, [pc, #4]	; (c0d029c4 <USBD_ConfigStrDescriptor+0xc>)
c0d029be:	4478      	add	r0, pc
c0d029c0:	4770      	bx	lr
c0d029c2:	46c0      	nop			; (mov r8, r8)
c0d029c4:	0000069a 	.word	0x0000069a

c0d029c8 <USBD_InterfaceStrDescriptor>:
  * @retval Pointer to descriptor buffer
  */
static uint8_t *USBD_InterfaceStrDescriptor(USBD_SpeedTypeDef speed, uint16_t *length)
{
  UNUSED(speed);
  *length = sizeof(USBD_INTERFACE_FS_STRING);
c0d029c8:	200e      	movs	r0, #14
c0d029ca:	8008      	strh	r0, [r1, #0]
  return (uint8_t*)USBD_INTERFACE_FS_STRING;
c0d029cc:	4801      	ldr	r0, [pc, #4]	; (c0d029d4 <USBD_InterfaceStrDescriptor+0xc>)
c0d029ce:	4478      	add	r0, pc
c0d029d0:	4770      	bx	lr
c0d029d2:	46c0      	nop			; (mov r8, r8)
c0d029d4:	0000068a 	.word	0x0000068a

c0d029d8 <USB_power>:
  // nothing to do ?
  return 0;
}
#endif // HAVE_USB_CLASS_CCID

void USB_power(unsigned char enabled) {
c0d029d8:	b570      	push	{r4, r5, r6, lr}
c0d029da:	4604      	mov	r4, r0
c0d029dc:	204d      	movs	r0, #77	; 0x4d
c0d029de:	0085      	lsls	r5, r0, #2
  os_memset(&USBD_Device, 0, sizeof(USBD_Device));
c0d029e0:	4810      	ldr	r0, [pc, #64]	; (c0d02a24 <USB_power+0x4c>)
c0d029e2:	2100      	movs	r1, #0
c0d029e4:	462a      	mov	r2, r5
c0d029e6:	f7fe fb01 	bl	c0d00fec <os_memset>

  if (enabled) {
c0d029ea:	2c00      	cmp	r4, #0
c0d029ec:	d016      	beq.n	c0d02a1c <USB_power+0x44>
    os_memset(&USBD_Device, 0, sizeof(USBD_Device));
c0d029ee:	4c0d      	ldr	r4, [pc, #52]	; (c0d02a24 <USB_power+0x4c>)
c0d029f0:	2600      	movs	r6, #0
c0d029f2:	4620      	mov	r0, r4
c0d029f4:	4631      	mov	r1, r6
c0d029f6:	462a      	mov	r2, r5
c0d029f8:	f7fe faf8 	bl	c0d00fec <os_memset>
    /* Init Device Library */
    USBD_Init(&USBD_Device, (USBD_DescriptorsTypeDef*)&HID_Desc, 0);
c0d029fc:	490a      	ldr	r1, [pc, #40]	; (c0d02a28 <USB_power+0x50>)
c0d029fe:	4479      	add	r1, pc
c0d02a00:	4620      	mov	r0, r4
c0d02a02:	4632      	mov	r2, r6
c0d02a04:	f7ff faa2 	bl	c0d01f4c <USBD_Init>
    
    /* Register the HID class */
    USBD_RegisterClassForInterface(HID_INTF,  &USBD_Device, (USBD_ClassTypeDef*)&USBD_HID);
c0d02a08:	4a08      	ldr	r2, [pc, #32]	; (c0d02a2c <USB_power+0x54>)
c0d02a0a:	447a      	add	r2, pc
c0d02a0c:	4630      	mov	r0, r6
c0d02a0e:	4621      	mov	r1, r4
c0d02a10:	f7ff fad6 	bl	c0d01fc0 <USBD_RegisterClassForInterface>
    USBD_RegisterClassForInterface(CCID_INTF, &USBD_Device, (USBD_ClassTypeDef*)&USBD_CCID);
#endif // HAVE_USB_CLASS_CCID


    /* Start Device Process */
    USBD_Start(&USBD_Device);
c0d02a14:	4620      	mov	r0, r4
c0d02a16:	f7ff fae0 	bl	c0d01fda <USBD_Start>
  }
  else {
    USBD_DeInit(&USBD_Device);
  }
}
c0d02a1a:	bd70      	pop	{r4, r5, r6, pc}

    /* Start Device Process */
    USBD_Start(&USBD_Device);
  }
  else {
    USBD_DeInit(&USBD_Device);
c0d02a1c:	4801      	ldr	r0, [pc, #4]	; (c0d02a24 <USB_power+0x4c>)
c0d02a1e:	f7ff fab0 	bl	c0d01f82 <USBD_DeInit>
  }
}
c0d02a22:	bd70      	pop	{r4, r5, r6, pc}
c0d02a24:	20001cb0 	.word	0x20001cb0
c0d02a28:	000005de 	.word	0x000005de
c0d02a2c:	000005f2 	.word	0x000005f2

c0d02a30 <USBD_GetCfgDesc_impl>:
  * @param  length : pointer data length
  * @retval pointer to descriptor buffer
  */
static uint8_t  *USBD_GetCfgDesc_impl (uint16_t *length)
{
  *length = sizeof (USBD_CfgDesc);
c0d02a30:	2129      	movs	r1, #41	; 0x29
c0d02a32:	8001      	strh	r1, [r0, #0]
  return (uint8_t*)USBD_CfgDesc;
c0d02a34:	4801      	ldr	r0, [pc, #4]	; (c0d02a3c <USBD_GetCfgDesc_impl+0xc>)
c0d02a36:	4478      	add	r0, pc
c0d02a38:	4770      	bx	lr
c0d02a3a:	46c0      	nop			; (mov r8, r8)
c0d02a3c:	0000063a 	.word	0x0000063a

c0d02a40 <USBD_GetDeviceQualifierDesc_impl>:
* @param  length : pointer data length
* @retval pointer to descriptor buffer
*/
static uint8_t  *USBD_GetDeviceQualifierDesc_impl (uint16_t *length)
{
  *length = sizeof (USBD_DeviceQualifierDesc);
c0d02a40:	210a      	movs	r1, #10
c0d02a42:	8001      	strh	r1, [r0, #0]
  return (uint8_t*)USBD_DeviceQualifierDesc;
c0d02a44:	4801      	ldr	r0, [pc, #4]	; (c0d02a4c <USBD_GetDeviceQualifierDesc_impl+0xc>)
c0d02a46:	4478      	add	r0, pc
c0d02a48:	4770      	bx	lr
c0d02a4a:	46c0      	nop			; (mov r8, r8)
c0d02a4c:	00000656 	.word	0x00000656

c0d02a50 <USBD_CtlSendData>:
* @retval status
*/
USBD_StatusTypeDef  USBD_CtlSendData (USBD_HandleTypeDef  *pdev, 
                               uint8_t *pbuf,
                               uint16_t len)
{
c0d02a50:	b5b0      	push	{r4, r5, r7, lr}
c0d02a52:	460c      	mov	r4, r1
  /* Set EP0 State */
  pdev->ep0_state          = USBD_EP0_DATA_IN;                                      
c0d02a54:	21f4      	movs	r1, #244	; 0xf4
c0d02a56:	2302      	movs	r3, #2
c0d02a58:	5043      	str	r3, [r0, r1]
  pdev->ep_in[0].total_length = len;
c0d02a5a:	6182      	str	r2, [r0, #24]
  pdev->ep_in[0].rem_length   = len;
c0d02a5c:	61c2      	str	r2, [r0, #28]
  // store the continuation data if needed
  pdev->pData = pbuf;
c0d02a5e:	2113      	movs	r1, #19
c0d02a60:	0109      	lsls	r1, r1, #4
c0d02a62:	5044      	str	r4, [r0, r1]
 /* Start the transfer */
  USBD_LL_Transmit (pdev, 0x00, pbuf, MIN(len, pdev->ep_in[0].maxpacket));  
c0d02a64:	6a01      	ldr	r1, [r0, #32]
c0d02a66:	428a      	cmp	r2, r1
c0d02a68:	d300      	bcc.n	c0d02a6c <USBD_CtlSendData+0x1c>
c0d02a6a:	460a      	mov	r2, r1
c0d02a6c:	b293      	uxth	r3, r2
c0d02a6e:	2500      	movs	r5, #0
c0d02a70:	4629      	mov	r1, r5
c0d02a72:	4622      	mov	r2, r4
c0d02a74:	f7ff fa3e 	bl	c0d01ef4 <USBD_LL_Transmit>
  
  return USBD_OK;
c0d02a78:	4628      	mov	r0, r5
c0d02a7a:	bdb0      	pop	{r4, r5, r7, pc}

c0d02a7c <USBD_CtlContinueSendData>:
* @retval status
*/
USBD_StatusTypeDef  USBD_CtlContinueSendData (USBD_HandleTypeDef  *pdev, 
                                       uint8_t *pbuf,
                                       uint16_t len)
{
c0d02a7c:	b5b0      	push	{r4, r5, r7, lr}
c0d02a7e:	460c      	mov	r4, r1
 /* Start the next transfer */
  USBD_LL_Transmit (pdev, 0x00, pbuf, MIN(len, pdev->ep_in[0].maxpacket));   
c0d02a80:	6a01      	ldr	r1, [r0, #32]
c0d02a82:	428a      	cmp	r2, r1
c0d02a84:	d300      	bcc.n	c0d02a88 <USBD_CtlContinueSendData+0xc>
c0d02a86:	460a      	mov	r2, r1
c0d02a88:	b293      	uxth	r3, r2
c0d02a8a:	2500      	movs	r5, #0
c0d02a8c:	4629      	mov	r1, r5
c0d02a8e:	4622      	mov	r2, r4
c0d02a90:	f7ff fa30 	bl	c0d01ef4 <USBD_LL_Transmit>
  return USBD_OK;
c0d02a94:	4628      	mov	r0, r5
c0d02a96:	bdb0      	pop	{r4, r5, r7, pc}

c0d02a98 <USBD_CtlContinueRx>:
* @retval status
*/
USBD_StatusTypeDef  USBD_CtlContinueRx (USBD_HandleTypeDef  *pdev, 
                                          uint8_t *pbuf,                                          
                                          uint16_t len)
{
c0d02a98:	b510      	push	{r4, lr}
c0d02a9a:	2400      	movs	r4, #0
  UNUSED(pbuf);
  USBD_LL_PrepareReceive (pdev,
c0d02a9c:	4621      	mov	r1, r4
c0d02a9e:	f7ff fa42 	bl	c0d01f26 <USBD_LL_PrepareReceive>
                          0,                                            
                          len);
  return USBD_OK;
c0d02aa2:	4620      	mov	r0, r4
c0d02aa4:	bd10      	pop	{r4, pc}

c0d02aa6 <USBD_CtlSendStatus>:
*         send zero lzngth packet on the ctl pipe
* @param  pdev: device instance
* @retval status
*/
USBD_StatusTypeDef  USBD_CtlSendStatus (USBD_HandleTypeDef  *pdev)
{
c0d02aa6:	b510      	push	{r4, lr}

  /* Set EP0 State */
  pdev->ep0_state = USBD_EP0_STATUS_IN;
c0d02aa8:	21f4      	movs	r1, #244	; 0xf4
c0d02aaa:	2204      	movs	r2, #4
c0d02aac:	5042      	str	r2, [r0, r1]
c0d02aae:	2400      	movs	r4, #0
  
 /* Start the transfer */
  USBD_LL_Transmit (pdev, 0x00, NULL, 0);   
c0d02ab0:	4621      	mov	r1, r4
c0d02ab2:	4622      	mov	r2, r4
c0d02ab4:	4623      	mov	r3, r4
c0d02ab6:	f7ff fa1d 	bl	c0d01ef4 <USBD_LL_Transmit>
  
  return USBD_OK;
c0d02aba:	4620      	mov	r0, r4
c0d02abc:	bd10      	pop	{r4, pc}

c0d02abe <USBD_CtlReceiveStatus>:
*         receive zero lzngth packet on the ctl pipe
* @param  pdev: device instance
* @retval status
*/
USBD_StatusTypeDef  USBD_CtlReceiveStatus (USBD_HandleTypeDef  *pdev)
{
c0d02abe:	b510      	push	{r4, lr}
  /* Set EP0 State */
  pdev->ep0_state = USBD_EP0_STATUS_OUT; 
c0d02ac0:	21f4      	movs	r1, #244	; 0xf4
c0d02ac2:	2205      	movs	r2, #5
c0d02ac4:	5042      	str	r2, [r0, r1]
c0d02ac6:	2400      	movs	r4, #0
  
 /* Start the transfer */  
  USBD_LL_PrepareReceive ( pdev,
c0d02ac8:	4621      	mov	r1, r4
c0d02aca:	4622      	mov	r2, r4
c0d02acc:	f7ff fa2b 	bl	c0d01f26 <USBD_LL_PrepareReceive>
                    0,
                    0);  

  return USBD_OK;
c0d02ad0:	4620      	mov	r0, r4
c0d02ad2:	bd10      	pop	{r4, pc}

c0d02ad4 <__aeabi_uidiv>:
c0d02ad4:	2200      	movs	r2, #0
c0d02ad6:	0843      	lsrs	r3, r0, #1
c0d02ad8:	428b      	cmp	r3, r1
c0d02ada:	d374      	bcc.n	c0d02bc6 <__aeabi_uidiv+0xf2>
c0d02adc:	0903      	lsrs	r3, r0, #4
c0d02ade:	428b      	cmp	r3, r1
c0d02ae0:	d35f      	bcc.n	c0d02ba2 <__aeabi_uidiv+0xce>
c0d02ae2:	0a03      	lsrs	r3, r0, #8
c0d02ae4:	428b      	cmp	r3, r1
c0d02ae6:	d344      	bcc.n	c0d02b72 <__aeabi_uidiv+0x9e>
c0d02ae8:	0b03      	lsrs	r3, r0, #12
c0d02aea:	428b      	cmp	r3, r1
c0d02aec:	d328      	bcc.n	c0d02b40 <__aeabi_uidiv+0x6c>
c0d02aee:	0c03      	lsrs	r3, r0, #16
c0d02af0:	428b      	cmp	r3, r1
c0d02af2:	d30d      	bcc.n	c0d02b10 <__aeabi_uidiv+0x3c>
c0d02af4:	22ff      	movs	r2, #255	; 0xff
c0d02af6:	0209      	lsls	r1, r1, #8
c0d02af8:	ba12      	rev	r2, r2
c0d02afa:	0c03      	lsrs	r3, r0, #16
c0d02afc:	428b      	cmp	r3, r1
c0d02afe:	d302      	bcc.n	c0d02b06 <__aeabi_uidiv+0x32>
c0d02b00:	1212      	asrs	r2, r2, #8
c0d02b02:	0209      	lsls	r1, r1, #8
c0d02b04:	d065      	beq.n	c0d02bd2 <__aeabi_uidiv+0xfe>
c0d02b06:	0b03      	lsrs	r3, r0, #12
c0d02b08:	428b      	cmp	r3, r1
c0d02b0a:	d319      	bcc.n	c0d02b40 <__aeabi_uidiv+0x6c>
c0d02b0c:	e000      	b.n	c0d02b10 <__aeabi_uidiv+0x3c>
c0d02b0e:	0a09      	lsrs	r1, r1, #8
c0d02b10:	0bc3      	lsrs	r3, r0, #15
c0d02b12:	428b      	cmp	r3, r1
c0d02b14:	d301      	bcc.n	c0d02b1a <__aeabi_uidiv+0x46>
c0d02b16:	03cb      	lsls	r3, r1, #15
c0d02b18:	1ac0      	subs	r0, r0, r3
c0d02b1a:	4152      	adcs	r2, r2
c0d02b1c:	0b83      	lsrs	r3, r0, #14
c0d02b1e:	428b      	cmp	r3, r1
c0d02b20:	d301      	bcc.n	c0d02b26 <__aeabi_uidiv+0x52>
c0d02b22:	038b      	lsls	r3, r1, #14
c0d02b24:	1ac0      	subs	r0, r0, r3
c0d02b26:	4152      	adcs	r2, r2
c0d02b28:	0b43      	lsrs	r3, r0, #13
c0d02b2a:	428b      	cmp	r3, r1
c0d02b2c:	d301      	bcc.n	c0d02b32 <__aeabi_uidiv+0x5e>
c0d02b2e:	034b      	lsls	r3, r1, #13
c0d02b30:	1ac0      	subs	r0, r0, r3
c0d02b32:	4152      	adcs	r2, r2
c0d02b34:	0b03      	lsrs	r3, r0, #12
c0d02b36:	428b      	cmp	r3, r1
c0d02b38:	d301      	bcc.n	c0d02b3e <__aeabi_uidiv+0x6a>
c0d02b3a:	030b      	lsls	r3, r1, #12
c0d02b3c:	1ac0      	subs	r0, r0, r3
c0d02b3e:	4152      	adcs	r2, r2
c0d02b40:	0ac3      	lsrs	r3, r0, #11
c0d02b42:	428b      	cmp	r3, r1
c0d02b44:	d301      	bcc.n	c0d02b4a <__aeabi_uidiv+0x76>
c0d02b46:	02cb      	lsls	r3, r1, #11
c0d02b48:	1ac0      	subs	r0, r0, r3
c0d02b4a:	4152      	adcs	r2, r2
c0d02b4c:	0a83      	lsrs	r3, r0, #10
c0d02b4e:	428b      	cmp	r3, r1
c0d02b50:	d301      	bcc.n	c0d02b56 <__aeabi_uidiv+0x82>
c0d02b52:	028b      	lsls	r3, r1, #10
c0d02b54:	1ac0      	subs	r0, r0, r3
c0d02b56:	4152      	adcs	r2, r2
c0d02b58:	0a43      	lsrs	r3, r0, #9
c0d02b5a:	428b      	cmp	r3, r1
c0d02b5c:	d301      	bcc.n	c0d02b62 <__aeabi_uidiv+0x8e>
c0d02b5e:	024b      	lsls	r3, r1, #9
c0d02b60:	1ac0      	subs	r0, r0, r3
c0d02b62:	4152      	adcs	r2, r2
c0d02b64:	0a03      	lsrs	r3, r0, #8
c0d02b66:	428b      	cmp	r3, r1
c0d02b68:	d301      	bcc.n	c0d02b6e <__aeabi_uidiv+0x9a>
c0d02b6a:	020b      	lsls	r3, r1, #8
c0d02b6c:	1ac0      	subs	r0, r0, r3
c0d02b6e:	4152      	adcs	r2, r2
c0d02b70:	d2cd      	bcs.n	c0d02b0e <__aeabi_uidiv+0x3a>
c0d02b72:	09c3      	lsrs	r3, r0, #7
c0d02b74:	428b      	cmp	r3, r1
c0d02b76:	d301      	bcc.n	c0d02b7c <__aeabi_uidiv+0xa8>
c0d02b78:	01cb      	lsls	r3, r1, #7
c0d02b7a:	1ac0      	subs	r0, r0, r3
c0d02b7c:	4152      	adcs	r2, r2
c0d02b7e:	0983      	lsrs	r3, r0, #6
c0d02b80:	428b      	cmp	r3, r1
c0d02b82:	d301      	bcc.n	c0d02b88 <__aeabi_uidiv+0xb4>
c0d02b84:	018b      	lsls	r3, r1, #6
c0d02b86:	1ac0      	subs	r0, r0, r3
c0d02b88:	4152      	adcs	r2, r2
c0d02b8a:	0943      	lsrs	r3, r0, #5
c0d02b8c:	428b      	cmp	r3, r1
c0d02b8e:	d301      	bcc.n	c0d02b94 <__aeabi_uidiv+0xc0>
c0d02b90:	014b      	lsls	r3, r1, #5
c0d02b92:	1ac0      	subs	r0, r0, r3
c0d02b94:	4152      	adcs	r2, r2
c0d02b96:	0903      	lsrs	r3, r0, #4
c0d02b98:	428b      	cmp	r3, r1
c0d02b9a:	d301      	bcc.n	c0d02ba0 <__aeabi_uidiv+0xcc>
c0d02b9c:	010b      	lsls	r3, r1, #4
c0d02b9e:	1ac0      	subs	r0, r0, r3
c0d02ba0:	4152      	adcs	r2, r2
c0d02ba2:	08c3      	lsrs	r3, r0, #3
c0d02ba4:	428b      	cmp	r3, r1
c0d02ba6:	d301      	bcc.n	c0d02bac <__aeabi_uidiv+0xd8>
c0d02ba8:	00cb      	lsls	r3, r1, #3
c0d02baa:	1ac0      	subs	r0, r0, r3
c0d02bac:	4152      	adcs	r2, r2
c0d02bae:	0883      	lsrs	r3, r0, #2
c0d02bb0:	428b      	cmp	r3, r1
c0d02bb2:	d301      	bcc.n	c0d02bb8 <__aeabi_uidiv+0xe4>
c0d02bb4:	008b      	lsls	r3, r1, #2
c0d02bb6:	1ac0      	subs	r0, r0, r3
c0d02bb8:	4152      	adcs	r2, r2
c0d02bba:	0843      	lsrs	r3, r0, #1
c0d02bbc:	428b      	cmp	r3, r1
c0d02bbe:	d301      	bcc.n	c0d02bc4 <__aeabi_uidiv+0xf0>
c0d02bc0:	004b      	lsls	r3, r1, #1
c0d02bc2:	1ac0      	subs	r0, r0, r3
c0d02bc4:	4152      	adcs	r2, r2
c0d02bc6:	1a41      	subs	r1, r0, r1
c0d02bc8:	d200      	bcs.n	c0d02bcc <__aeabi_uidiv+0xf8>
c0d02bca:	4601      	mov	r1, r0
c0d02bcc:	4152      	adcs	r2, r2
c0d02bce:	4610      	mov	r0, r2
c0d02bd0:	4770      	bx	lr
c0d02bd2:	e7ff      	b.n	c0d02bd4 <__aeabi_uidiv+0x100>
c0d02bd4:	b501      	push	{r0, lr}
c0d02bd6:	2000      	movs	r0, #0
c0d02bd8:	f000 f806 	bl	c0d02be8 <__aeabi_idiv0>
c0d02bdc:	bd02      	pop	{r1, pc}
c0d02bde:	46c0      	nop			; (mov r8, r8)

c0d02be0 <__aeabi_uidivmod>:
c0d02be0:	2900      	cmp	r1, #0
c0d02be2:	d0f7      	beq.n	c0d02bd4 <__aeabi_uidiv+0x100>
c0d02be4:	e776      	b.n	c0d02ad4 <__aeabi_uidiv>
c0d02be6:	4770      	bx	lr

c0d02be8 <__aeabi_idiv0>:
c0d02be8:	4770      	bx	lr
c0d02bea:	46c0      	nop			; (mov r8, r8)

c0d02bec <__aeabi_memclr>:
c0d02bec:	b510      	push	{r4, lr}
c0d02bee:	2200      	movs	r2, #0
c0d02bf0:	f000 f802 	bl	c0d02bf8 <__aeabi_memset>
c0d02bf4:	bd10      	pop	{r4, pc}
c0d02bf6:	46c0      	nop			; (mov r8, r8)

c0d02bf8 <__aeabi_memset>:
c0d02bf8:	0013      	movs	r3, r2
c0d02bfa:	b510      	push	{r4, lr}
c0d02bfc:	000a      	movs	r2, r1
c0d02bfe:	0019      	movs	r1, r3
c0d02c00:	f000 f802 	bl	c0d02c08 <memset>
c0d02c04:	bd10      	pop	{r4, pc}
c0d02c06:	46c0      	nop			; (mov r8, r8)

c0d02c08 <memset>:
c0d02c08:	b570      	push	{r4, r5, r6, lr}
c0d02c0a:	0783      	lsls	r3, r0, #30
c0d02c0c:	d03f      	beq.n	c0d02c8e <memset+0x86>
c0d02c0e:	1e54      	subs	r4, r2, #1
c0d02c10:	2a00      	cmp	r2, #0
c0d02c12:	d03b      	beq.n	c0d02c8c <memset+0x84>
c0d02c14:	b2ce      	uxtb	r6, r1
c0d02c16:	0003      	movs	r3, r0
c0d02c18:	2503      	movs	r5, #3
c0d02c1a:	e003      	b.n	c0d02c24 <memset+0x1c>
c0d02c1c:	1e62      	subs	r2, r4, #1
c0d02c1e:	2c00      	cmp	r4, #0
c0d02c20:	d034      	beq.n	c0d02c8c <memset+0x84>
c0d02c22:	0014      	movs	r4, r2
c0d02c24:	3301      	adds	r3, #1
c0d02c26:	1e5a      	subs	r2, r3, #1
c0d02c28:	7016      	strb	r6, [r2, #0]
c0d02c2a:	422b      	tst	r3, r5
c0d02c2c:	d1f6      	bne.n	c0d02c1c <memset+0x14>
c0d02c2e:	2c03      	cmp	r4, #3
c0d02c30:	d924      	bls.n	c0d02c7c <memset+0x74>
c0d02c32:	25ff      	movs	r5, #255	; 0xff
c0d02c34:	400d      	ands	r5, r1
c0d02c36:	022a      	lsls	r2, r5, #8
c0d02c38:	4315      	orrs	r5, r2
c0d02c3a:	042a      	lsls	r2, r5, #16
c0d02c3c:	4315      	orrs	r5, r2
c0d02c3e:	2c0f      	cmp	r4, #15
c0d02c40:	d911      	bls.n	c0d02c66 <memset+0x5e>
c0d02c42:	0026      	movs	r6, r4
c0d02c44:	3e10      	subs	r6, #16
c0d02c46:	0936      	lsrs	r6, r6, #4
c0d02c48:	3601      	adds	r6, #1
c0d02c4a:	0136      	lsls	r6, r6, #4
c0d02c4c:	001a      	movs	r2, r3
c0d02c4e:	199b      	adds	r3, r3, r6
c0d02c50:	6015      	str	r5, [r2, #0]
c0d02c52:	6055      	str	r5, [r2, #4]
c0d02c54:	6095      	str	r5, [r2, #8]
c0d02c56:	60d5      	str	r5, [r2, #12]
c0d02c58:	3210      	adds	r2, #16
c0d02c5a:	4293      	cmp	r3, r2
c0d02c5c:	d1f8      	bne.n	c0d02c50 <memset+0x48>
c0d02c5e:	220f      	movs	r2, #15
c0d02c60:	4014      	ands	r4, r2
c0d02c62:	2c03      	cmp	r4, #3
c0d02c64:	d90a      	bls.n	c0d02c7c <memset+0x74>
c0d02c66:	1f26      	subs	r6, r4, #4
c0d02c68:	08b6      	lsrs	r6, r6, #2
c0d02c6a:	3601      	adds	r6, #1
c0d02c6c:	00b6      	lsls	r6, r6, #2
c0d02c6e:	001a      	movs	r2, r3
c0d02c70:	199b      	adds	r3, r3, r6
c0d02c72:	c220      	stmia	r2!, {r5}
c0d02c74:	4293      	cmp	r3, r2
c0d02c76:	d1fc      	bne.n	c0d02c72 <memset+0x6a>
c0d02c78:	2203      	movs	r2, #3
c0d02c7a:	4014      	ands	r4, r2
c0d02c7c:	2c00      	cmp	r4, #0
c0d02c7e:	d005      	beq.n	c0d02c8c <memset+0x84>
c0d02c80:	b2c9      	uxtb	r1, r1
c0d02c82:	191c      	adds	r4, r3, r4
c0d02c84:	7019      	strb	r1, [r3, #0]
c0d02c86:	3301      	adds	r3, #1
c0d02c88:	429c      	cmp	r4, r3
c0d02c8a:	d1fb      	bne.n	c0d02c84 <memset+0x7c>
c0d02c8c:	bd70      	pop	{r4, r5, r6, pc}
c0d02c8e:	0014      	movs	r4, r2
c0d02c90:	0003      	movs	r3, r0
c0d02c92:	e7cc      	b.n	c0d02c2e <memset+0x26>

c0d02c94 <setjmp>:
c0d02c94:	c0f0      	stmia	r0!, {r4, r5, r6, r7}
c0d02c96:	4641      	mov	r1, r8
c0d02c98:	464a      	mov	r2, r9
c0d02c9a:	4653      	mov	r3, sl
c0d02c9c:	465c      	mov	r4, fp
c0d02c9e:	466d      	mov	r5, sp
c0d02ca0:	4676      	mov	r6, lr
c0d02ca2:	c07e      	stmia	r0!, {r1, r2, r3, r4, r5, r6}
c0d02ca4:	3828      	subs	r0, #40	; 0x28
c0d02ca6:	c8f0      	ldmia	r0!, {r4, r5, r6, r7}
c0d02ca8:	2000      	movs	r0, #0
c0d02caa:	4770      	bx	lr

c0d02cac <longjmp>:
c0d02cac:	3010      	adds	r0, #16
c0d02cae:	c87c      	ldmia	r0!, {r2, r3, r4, r5, r6}
c0d02cb0:	4690      	mov	r8, r2
c0d02cb2:	4699      	mov	r9, r3
c0d02cb4:	46a2      	mov	sl, r4
c0d02cb6:	46ab      	mov	fp, r5
c0d02cb8:	46b5      	mov	sp, r6
c0d02cba:	c808      	ldmia	r0!, {r3}
c0d02cbc:	3828      	subs	r0, #40	; 0x28
c0d02cbe:	c8f0      	ldmia	r0!, {r4, r5, r6, r7}
c0d02cc0:	1c08      	adds	r0, r1, #0
c0d02cc2:	d100      	bne.n	c0d02cc6 <longjmp+0x1a>
c0d02cc4:	2001      	movs	r0, #1
c0d02cc6:	4718      	bx	r3

c0d02cc8 <strlen>:
c0d02cc8:	b510      	push	{r4, lr}
c0d02cca:	0783      	lsls	r3, r0, #30
c0d02ccc:	d027      	beq.n	c0d02d1e <strlen+0x56>
c0d02cce:	7803      	ldrb	r3, [r0, #0]
c0d02cd0:	2b00      	cmp	r3, #0
c0d02cd2:	d026      	beq.n	c0d02d22 <strlen+0x5a>
c0d02cd4:	0003      	movs	r3, r0
c0d02cd6:	2103      	movs	r1, #3
c0d02cd8:	e002      	b.n	c0d02ce0 <strlen+0x18>
c0d02cda:	781a      	ldrb	r2, [r3, #0]
c0d02cdc:	2a00      	cmp	r2, #0
c0d02cde:	d01c      	beq.n	c0d02d1a <strlen+0x52>
c0d02ce0:	3301      	adds	r3, #1
c0d02ce2:	420b      	tst	r3, r1
c0d02ce4:	d1f9      	bne.n	c0d02cda <strlen+0x12>
c0d02ce6:	6819      	ldr	r1, [r3, #0]
c0d02ce8:	4a0f      	ldr	r2, [pc, #60]	; (c0d02d28 <strlen+0x60>)
c0d02cea:	4c10      	ldr	r4, [pc, #64]	; (c0d02d2c <strlen+0x64>)
c0d02cec:	188a      	adds	r2, r1, r2
c0d02cee:	438a      	bics	r2, r1
c0d02cf0:	4222      	tst	r2, r4
c0d02cf2:	d10f      	bne.n	c0d02d14 <strlen+0x4c>
c0d02cf4:	3304      	adds	r3, #4
c0d02cf6:	6819      	ldr	r1, [r3, #0]
c0d02cf8:	4a0b      	ldr	r2, [pc, #44]	; (c0d02d28 <strlen+0x60>)
c0d02cfa:	188a      	adds	r2, r1, r2
c0d02cfc:	438a      	bics	r2, r1
c0d02cfe:	4222      	tst	r2, r4
c0d02d00:	d108      	bne.n	c0d02d14 <strlen+0x4c>
c0d02d02:	3304      	adds	r3, #4
c0d02d04:	6819      	ldr	r1, [r3, #0]
c0d02d06:	4a08      	ldr	r2, [pc, #32]	; (c0d02d28 <strlen+0x60>)
c0d02d08:	188a      	adds	r2, r1, r2
c0d02d0a:	438a      	bics	r2, r1
c0d02d0c:	4222      	tst	r2, r4
c0d02d0e:	d0f1      	beq.n	c0d02cf4 <strlen+0x2c>
c0d02d10:	e000      	b.n	c0d02d14 <strlen+0x4c>
c0d02d12:	3301      	adds	r3, #1
c0d02d14:	781a      	ldrb	r2, [r3, #0]
c0d02d16:	2a00      	cmp	r2, #0
c0d02d18:	d1fb      	bne.n	c0d02d12 <strlen+0x4a>
c0d02d1a:	1a18      	subs	r0, r3, r0
c0d02d1c:	bd10      	pop	{r4, pc}
c0d02d1e:	0003      	movs	r3, r0
c0d02d20:	e7e1      	b.n	c0d02ce6 <strlen+0x1e>
c0d02d22:	2000      	movs	r0, #0
c0d02d24:	e7fa      	b.n	c0d02d1c <strlen+0x54>
c0d02d26:	46c0      	nop			; (mov r8, r8)
c0d02d28:	fefefeff 	.word	0xfefefeff
c0d02d2c:	80808080 	.word	0x80808080

c0d02d30 <welcome_to_str>:
c0d02d30:	636c6557 20656d6f 42006f74                       Welcome to.

c0d02d3b <blocknet_str>:
c0d02d3b:	636f6c42 74656e6b 00000300                       Blocknet.

c0d02d44 <bagl_ui_sample_nanos>:
c0d02d44:	00000003 00800000 00000020 00000001     ........ .......
c0d02d54:	00000000 00ffffff 00000000 00000000     ................
	...
c0d02d7c:	00000207 0080000c 0000000b 00000000     ................
c0d02d8c:	00ffffff 00000000 0000800a 20001934     ............4.. 
	...
c0d02db4:	00170207 0052001a 008a000b 00000000     ......R.........
c0d02dc4:	00ffffff 00000000 001a8008 200019ac     ............... 
	...
c0d02dec:	00030005 0007000c 00000007 00000000     ................
c0d02dfc:	00ffffff 00000000 00070000 00000000     ................
	...
c0d02e24:	00750005 0008000d 00000006 00000000     ..u.............
c0d02e34:	00ffffff 00000000 00060000 00000000     ................
	...

c0d02e5c <status_balance_str>:
c0d02e5c:	616c6142 3a65636e 20202000                       Balance:.

c0d02e65 <TXT_BLANK>:
c0d02e65:	20202020 20202020 20202020 20202020                     
c0d02e75:	03000020                                          ..

c0d02e78 <bagl_ui_approval_nanos>:
c0d02e78:	00000003 00800000 00000020 00000001     ........ .......
c0d02e88:	00000000 00ffffff 00000000 00000000     ................
	...
c0d02eb0:	00000207 0080000c 0000000b 00000000     ................
c0d02ec0:	00ffffff 00000000 0000800a c0d02f90     ............./..
	...
c0d02ee8:	00170207 0052001a 008a000b 00000000     ......R.........
c0d02ef8:	00ffffff 00000000 001a8008 c0d02f98     ............./..
	...
c0d02f20:	00030005 0007000c 00000007 00000000     ................
c0d02f30:	00ffffff 00000000 00070000 00000000     ................
	...
c0d02f58:	00750005 0008000d 00000006 00000000     ..u.............
c0d02f68:	00ffffff 00000000 00060000 00000000     ................
	...
c0d02f90:	666e6f43 006d7269 6e617274 74636173     Confirm.transact
c0d02fa0:	206e6f69 7453003f                                ion ?.

c0d02fa6 <status_str>:
c0d02fa6:	74617453 003a7375                       Status:.

c0d02fae <status_ready_str>:
c0d02fae:	64616552 21090079                                Ready.

c0d02fb4 <USBD_HID_Desc>:
c0d02fb4:	01112109 22220100 ffa00600                       .!...."".

c0d02fbd <HID_ReportDesc>:
c0d02fbd:	09ffa006 0901a101 26001503 087500ff     ...........&..u.
c0d02fcd:	08814095 00150409 7500ff26 91409508     .@......&..u..@.
c0d02fdd:	6900c008                                         ...

c0d02fe0 <HID_Desc>:
c0d02fe0:	c0d02969 c0d02979 c0d02989 c0d02999     i)..y)...)...)..
c0d02ff0:	c0d029a9 c0d029b9 c0d029c9 00000000     .)...)...)......

c0d03000 <USBD_HID>:
c0d03000:	c0d02877 c0d028a9 c0d027df 00000000     w(...(...'......
	...
c0d03018:	c0d02911 00000000 00000000 00000000     .)..............
c0d03028:	c0d02a31 c0d02a31 c0d02a31 c0d02a41     1*..1*..1*..A*..

c0d03038 <USBD_DeviceDesc>:
c0d03038:	02000112 40000000 00012c97 02010200     .......@.,......
c0d03048:	03040103                                         ..

c0d0304a <USBD_LangIDDesc>:
c0d0304a:	04090304                                ....

c0d0304e <USBD_MANUFACTURER_STRING>:
c0d0304e:	004c030e 00640065 00650067 030e0072              ..L.e.d.g.e.r.

c0d0305c <USBD_PRODUCT_FS_STRING>:
c0d0305c:	004e030e 006e0061 0020006f 030a0053              ..N.a.n.o. .S.

c0d0306a <USB_SERIAL_STRING>:
c0d0306a:	0030030a 00300030 02090031                       ..0.0.0.1.

c0d03074 <USBD_CfgDesc>:
c0d03074:	00290209 c0020101 00040932 00030200     ..).....2.......
c0d03084:	21090200 01000111 07002222 40038205     ...!...."".....@
c0d03094:	05070100 00400302 00000001              ......@.....

c0d030a0 <USBD_DeviceQualifierDesc>:
c0d030a0:	0200060a 40000000 6f4e0001                       .......@..

c0d030aa <NOT_AVAILABLE>:
c0d030aa:	20746f4e 69617661 6c62616c 00000065              Not available.

c0d030b8 <_etext>:
	...
