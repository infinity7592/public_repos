
// import "./index.css";
import React, { Component } from "react";
import ReactDOM from "react-dom";
import TransportU2F from "@ledgerhq/hw-transport-u2f";   // For browser support

/*
  ReferenceError: regeneratorRuntime is not defined
*/
import "babel-polyfill";

class App extends Component {
    
    constructor(props) {
      super(props);

      this.state = {
        result: "",
        address: null,
        balance: 0,
        error: null,
      };

      this.onTestButton1Click = this.onTestButton1Click.bind(this);
      this.onTestButton2Click = this.onTestButton2Click.bind(this);
      this.onTestButton3Click = this.onTestButton3Click.bind(this);
      
    }
    
    // Ledger communication test : getVersion
    async onTestButton1Click() {
      
      try {
        const transport = await TransportU2F.create();
        transport.setScrambleKey('BLOCK');
        transport.setDebugMode(true);

        let apduBufferTest = Buffer.from("80050000", "hex");
        const response = await transport.exchange(apduBufferTest);
                  
        console.log(response);
        this.setState({ result: response });
                  
      } catch (error) {
        this.setState({ result: "ERROR" });
        this.setState({ error });
      }

    };

    // Ledger communication test : getVersion
    async onTestButton2Click() {
      
      try {
        const transport = await TransportU2F.create();
        transport.setScrambleKey('BLOCK');
        transport.setDebugMode(true);
        const response = await transport.send(0x80, 0x05, 0x00, 0x00);
                  
        console.log(response);
        this.setState({ result: response });
                  
      } catch (error) {
        this.setState({ result: "ERROR" });
        this.setState({ error });
      }

    };

    // Ledger communication test
    async onTestButton3Click() {
      
      try {
        const transport = await TransportU2F.create();
        transport.setScrambleKey('BLOCK');
        transport.setDebugMode(true);

        // get Public Key for "44'/0'/0'/0/0"
        let apduTest = Buffer.from("148000002c80000000800000000000000000000000", "hex");
        let response = await transport.send(0x80, 0x06, 0x00, 0x00, apduTest);
                  
        console.log(response);
        this.setState({ result: response });
        
      } catch (error) {
        this.setState({ result: error });
      }

    };
    
    render() {
      const { address, error } = this.state;

      return (
        <div>

          <p>
            <button onClick={this.onTestButton1Click}>
              Test wrapAPDU TransportU2F             
            </button>
          </p>

          <p>
            <button onClick={this.onTestButton2Click}>
              Get Version APDU test             
            </button>
          </p>

          <p>
            <button onClick={this.onTestButton3Click}>
              Get Public Key APDU test             
            </button>
          </p>

          <p> output={this.state.result} </p>
        </div>
      );
    }
  }
  

  ReactDOM.render(<App />, document.getElementById("root"));

