import numpy as np
import lib_utils

class Error(object):
    def __init__(self, msg):
        self.text = msg

    def __str__(self):
        # print("Error - %s" % self.text)
        return self.text

"""
    - STAT
"""


def getRegression(x_arr, y_arr):
    """ Returns the intercept and first degree coefficient of the regression equation.

    - Example:
    get_regression([0.0, 1.0, 2.0, 3.0,  4.0,  5.0], [0.0, 0.8, 0.9, 0.1, -0.8, -1.0])

    - Returns:
    [-0.30285714  0.75714286]

    """
    try:
        if not isinstance(x_arr, list) or not isinstance(y_arr, list):
            return []
        x = np.array(x_arr)
        y = np.array(y_arr)
        z = np.polyfit(x, y, 1)
        return z
    except TypeError:
        return []


def getVariance(x_arr, sample=False):
    """
    Returns the average of the squared deviations from the mean.

    Parameters:
    - [x_arr]: required, the array of data to compute the variance from.
    - sample: optional. By default, False.

    True: provides an unbiased estimator of the variance of a hypothetical infinite population.
    False: provides a maximum likelihood estimate of the variance for normally distributed variables.

    - Example:
    get_variance([0.0, 1.0, 2.0, 3.0, 4.0, 5.0])

    - Returns:
    2.91666666667
    """
    try:
        if not isinstance(x_arr, list):
            return []
        x = np.array(x_arr)
        if sample:
            z = np.var(x, dtype=np.float64, ddof=1)
        else:
            z = np.var(x, dtype=np.float64, ddof=0)
        return z
    except TypeError:
        return []


def getMean(x_arr):
    """
    Returns the average of an array.

    Parameters:
    - x_arr: required, the array of data to compute the mean from.

    - Example:
    get_mean([0.0, 1.0, 2.0, 3.0, 4.0, 5.0])

    - Returns 2.5
    """
    try:
        if not isinstance(x_arr, list):
            return []
        x = np.array(x_arr)
        z = np.mean(x)
        return z
    except TypeError:
        return []


def getQuantile(x_arr, percentile, interpolation="nearest"):
    """ Returns the specified quantile related to an array.

    Parameters:
    - x_arr: required, the array of data to compute the mean from.

    - percentile: float in range of [0,100] (or sequence of floats).
    Percentile to compute, which must be between 0 and 100 inclusive.

    - interpolation : {‘linear’, ‘lower’, ‘higher’, ‘midpoint’, ‘nearest’}
    This optional parameter specifies the interpolation method to use when the desired quantile lies between two data points i < j:
    linear: i + (j - i) * fraction, where fraction is the fractional part of the index surrounded by i and j.
    lower: i.
    higher: j.
    nearest: i or j, whichever is nearest.
    midpoint: (i + j) / 2.

    - Example:
    get_quantile([0.5, 1.0, 2.0, 3.0, 4.0, 5.0])

    test_arr = [0.5, 1.0, 2.0, 3.0, 4.0, 5.0]
    print("quantile: %s" % get_quantile(test_arr, 75))
    """
    try:
        if not isinstance(x_arr, list):
            return Error("invalid data")
        if not lib_utils.isNumber(percentile):
            return Error("invalid percentile parameter")
        if percentile > 100 or percentile < 0:
            return Error("invalid percentile parameter")
        if interpolation not in ("linear", "lower", "higher", "midpoint", "nearest"):
            return Error("invalid interpolation parameter")
        x = np.array(x_arr)
        z = np.percentile(x, percentile, interpolation=interpolation)
        return z
    except TypeError as e:
        return Error(e)
