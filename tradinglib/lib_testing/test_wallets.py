
import sys, os
lib_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "prod")
sys.path.append(lib_path)

from tradinglib_obj import BlockDX, MarketOrder, LimitOrder, Error

confFilePath = "C:\\Users\\kbentahmed\\Dropbox\\__ALTCOINS\\blockdx_explorer\\architecture\\xbridge236.conf"

DX = BlockDX(ip="34.236.125.224", port="41414", rpcuser="user", rpcpass="pass", conf=confFilePath)

print(DX.unlockAll())
print(DX.relockAll())


def test_DX_getDust(maker_param):
    print("------------")
    print("DX.getDustAmount: [valid]")
    print(DX.getDustAmount(maker_param))
    print("------------")
    print("DX.getDustAmount: [invalid=X]")
    print(DX.getDustAmount("XXX"))


def test_DX_inputs(maker_param):
    print("------------")
    print("DX.getInputs: %s" % maker_param)
    print(DX.getInputs("LTC"))


def test_Wallet_unlock():
    print("------------")
    print("DX.isWalletUnlocked:")
    print(DX.isWalletUnlocked("LTC"))
    print(DX.isWalletUnlocked("BLOCK"))
    print(DX.isWalletUnlocked("XYZ"))


# test_DX_getDust()

