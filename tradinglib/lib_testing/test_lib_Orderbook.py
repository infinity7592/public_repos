import sys, os, pprint as pp
lib_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "prod")
sys.path.append(lib_path)

from blockdx import BlockDX, MarketOrder, LimitOrder, Error

confFilePath = "C:\\Users\\kbentahmed\\Dropbox\\__ALTCOINS\\___REPOS\\Bitbucket\\private\\tradinglib\\xbridge236.conf"
DX = BlockDX(ip="34.236.125.224", port="41414", rpcuser="user", rpcpass="pass", conf=confFilePath)


def cancelAll():
    print("------------")
    print("DX.cancelAll")
    print(DX.cancelAll())


def postOrders(maker_param: str, taker_param: str):
    print("------------")
    print("LimitOrder(maker_size=0.05, maker=%s, taker_size=50, taker=%s)" % (maker_param, taker_param))
    newOrder = LimitOrder(maker_size=0.05, maker=maker_param, taker_size=50, taker=taker_param)
    print(DX.postLimitOrder(newOrder))
    print("------------")
    print("DX.getOpenOrders:")
    print(DX.getOpenOrders())


def prelimSteps(maker_param: str, taker_param: str):
    cancelAll()
    postOrders(maker_param, taker_param)


def test_Orderbook(maker_param: str, taker_param: str):

    print("------------")
    print("DX.getOrderbook: %s <=> %s" % (maker_param, taker_param))
    pp.pprint(DX.getOrderbook(maker=maker_param, taker=taker_param))


def test_Asks(maker_param: str, taker_param: str):
    print("------------")
    print("DX.getAsks: %s <=> %s" % (maker_param, taker_param))
    pp.pprint(DX.getAsks(maker="LTC", taker="SYS", VERBOSE=True))
    print("------------")
    print("DX.getAsks: %s <=> %s" % (maker_param, taker_param))
    pp.pprint(DX.getAsks(maker="LTC", taker="SYS", vol_limit=0.001, VERBOSE=True))
    print("------------")
    print("DX.getAsks: %s <=> %s" % (maker_param, taker_param))
    pp.pprint(DX.getAsks(maker="LTC", taker="SYS", vol_limit=1, VERBOSE=True))
    print("------------")
    print("DX.getAsks: %s <=> %s" % (maker_param, taker_param))
    pp.pprint(DX.getAsks(maker="LTC", taker="SYS", vol_floor=0.001, VERBOSE=True))
    print("------------")
    print("DX.getAsks: %s <=> %s" % (maker_param, taker_param))
    pp.pprint(DX.getAsks(maker="LTC", taker="SYS", vol_floor=1, VERBOSE=True))
    print("------------")
    print("DX.getAsks: %s <=> %s" % (maker_param, taker_param))
    pp.pprint(DX.getAsks(maker="LTC", taker="SYS", max_price=1, VERBOSE=True))
    print("------------")
    print("DX.getAsks: %s <=> %s" % (maker_param, taker_param))
    pp.pprint(DX.getAsks(maker="LTC", taker="SYS", max_price=1000, VERBOSE=True))


def test_Bids(maker_param: str, taker_param: str):
    print("------------")
    print("DX.getBids: %s <=> %s" % (maker_param, taker_param))
    pp.pprint(DX.getBids(maker="LTC", taker="SYS"))


maker = "LTC"
taker = "SYS"

print(DX.getPrice(maker, taker))
print(DX.getPrice(taker, maker))

"""
prelimSteps(maker, taker)
test_Orderbook(maker, taker)
test_Asks(maker, taker)
"""
