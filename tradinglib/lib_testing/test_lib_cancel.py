import sys, os
lib_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "prod")
sys.path.append(lib_path)

from tradinglib_obj import BlockDX, MarketOrder, LimitOrder, Error

confFilePath = "C:\\Users\\kbentahmed\\Dropbox\\__ALTCOINS\\blockdx_explorer\\architecture\\xbridge236.conf"

DX = BlockDX(ip="34.236.125.224", port="41414", rpcuser="user", rpcpass="pass", conf=confFilePath)


def postOrders(maker_param="MONA", taker_param="LTC"):
    DX.cancelAll()
    for itm in [[0.3, 0.1], [0.2, 0.1], [0.15, 0.1]]:
        """ 2 MONA => 1 LTC """
        postRst = DX.postLimitOrder(LimitOrder(maker_size=itm[0], maker=maker_param, taker_size=itm[1], taker=taker_param))
        # print(postRst)


def test_cancel_lst():
    print("------------")
    print("DX.cancelAll")
    cancelled = DX.cancelOrders(maker="MONA", VERBOSE=True)
    print(cancelled)


postOrders()
test_cancel_lst()


