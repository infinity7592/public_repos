
import sys, os
lib_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "prod")
sys.path.append(lib_path)

import blockdx


print("--------------------------")
print(blockdx.getQuantile([1, 2, 3, 4, 5, 6], 75))

print("--------------------------")
print(blockdx.getVariance([1, 2, 3, 4, 5]))

