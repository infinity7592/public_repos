
import sys, os
lib_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "prod")
sys.path.append(lib_path)

from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
from tradinglib_obj import BlockDX, MarketOrder, LimitOrder, Error

confFilePath = "C:\\Users\\kbentahmed\\Dropbox\\__ALTCOINS\\blockdx_explorer\\architecture\\xbridge236.conf"

DX = BlockDX(ip="34.236.125.224", port="41414", rpcuser="user", rpcpass="pass", conf=confFilePath)


def test_DX_balance(maker_param):
    print("------------")
    print("DX.getWalletBalance:")
    print(DX.getWalletBalance(maker_param))
    print("-----------")
    print("DX.getWalletBalance - XXX")
    print(DX.getWalletBalance("XXX"))
    print("------------")
    print("DX.getBalance:")
    print(DX.getBalance("LTC", "LZNVpRrcgwdE2k8j1sAN3o8sH8PqYhi3wS"))
    print(DX.getBalance("LTC", "LKMxk14QggeDwa3vXzZeLL7pULSEHc4m6h"))
    """ {'Wallet': '9.687454', 'LTC': '0.155521', 'MONA': '4.676331', 'VIA': '45.913419', 'BLOCK': '9.687454'} """
    print("------------")
    print("DX.dxGetTokenBalances:")
    print(DX.dxGetTokenBalances())

