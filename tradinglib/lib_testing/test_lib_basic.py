
import sys, os
lib_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "prod")
sys.path.append(lib_path)
import pprint as pp

from blockdx import BlockDX, MarketOrder, LimitOrder, Error
import blockdx

confFilePath = "C:\\Users\\kbentahmed\\Dropbox\\__ALTCOINS\\___REPOS\\Bitbucket\\private\\tradinglib\\xbridge236.conf"

DX = BlockDX(ip="34.236.125.224", port="41414", rpcuser="user", rpcpass="pass", conf=confFilePath)

print("------------")
print("DX obj:")
print(DX)


def test_DX_Tokens():
    print("------------")
    print("DX.getLocalTokens:")
    print(DX.getLocalTokens())
    print("------------")
    print("DX.getNetworkTokens:")
    print(DX.getNetworkTokens())
    print("------------")
    print("DX.getConnectedCoins():")
    print(DX.getConnectedCoins())
    print("------------")
    print("DX.getTradeableCoins():")
    print(DX.getTradeableCoins())


def postOrders(maker_param="MONA", taker_param="LTC"):
    DX.cancelAll()
    for itm in [[0.3, 0.1], [0.2, 0.1], [0.15, 0.1]]:
        """ 2 MONA => 1 LTC """
        postRst = DX.postLimitOrder(LimitOrder(maker_size=itm[0], maker=maker_param, taker_size=itm[1], taker=taker_param))
        # print(postRst)


def test_DX_Orders():
    # postOrders()
    print("------------")
    print("DX.getClosedOrders:")
    pp.pprint(DX.getClosedOrders(VERBOSE=True))
    print("------------")
    print("DX.getClosedOrders:")
    pp.pprint(DX.getClosedOrders(taker="VIA", VERBOSE=True))
    print("------------")
    print("DX.getClosedOrders:")
    pp.pprint(DX.getClosedOrders(maker="DOGE", VERBOSE=True))
    print("------------")
    print("DX.getOpenOrders:")
    pp.pprint(DX.getOpenOrders(VERBOSE=True))
    print("------------")
    print("DX.getOpenOrders:")
    pp.pprint(DX.getOpenOrders(taker="LTC", VERBOSE=True))
    print("------------")
    print("DX.getOpenOrders:")
    pp.pprint(DX.getOpenOrders(maker="MONA", VERBOSE=True))


"""
    {'Wallet': '2.054815', 'SYS': '40.820723', 'LTC': '0.102697', 'DGB': '39.276828', 'DOGE': '668.571089', 'MONA': '5.276331', 'VIA': '43.204451', 'BLOCK': '2.054815'}
"""

# print(DX.getInfo(txid="ec223683761db838d71351945e633a6ee771d91e3f52b2b00083455a7da96ea6"))
test_DX_Tokens()
# test_DX_Orders()

