
import sys, os
lib_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "prod")
sys.path.append(lib_path)

from tradinglib_obj import BlockDX, MarketOrder, LimitOrder, Error

DX = BlockDX(ip="34.236.125.224", port="41414", rpcuser="user", rpcpass="pass", conf="C:\\Users\\kbentahmed\\Dropbox\\__ALTCOINS\\blockdx_explorer\\architecture\\xbridge236.conf")


def test_precheck():
    print("-----------")
    print("DX.executePrecheck")
    print(DX.executePrecheck(MarketOrder(maker="LTC", taker="MONA", taker_size=0.05, VERBOSE=True)), VERBOSE=True)

    print("-----------")
    print("DX.execute_precheck")
    print(DX.executePrecheck(MarketOrder(maker="LTC", taker="MONA", maker_size=0.05, VERBOSE=True)), VERBOSE=True)
    print("-----------")
    print("DX.execute_precheck")
    print(DX.executePrecheck(MarketOrder(maker="LTC", taker="MONA", maker_size=5, VERBOSE=True)), VERBOSE=True)
    print("-----------")
    print("DX.execute_precheck - XXX")
    print(DX.executePrecheck(MarketOrder(maker="XXX", taker="MONA", maker_size=0.05, VERBOSE=True)), VERBOSE=True)
    print("-----------")
    print("DX.execute_precheck - XXX")
    print(DX.executePrecheck(MarketOrder(maker="LTC", taker="XXX", maker_size=0.05, VERBOSE=True)), VERBOSE=True)

