import sys, os
lib_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "prod")
sys.path.append(lib_path)

import random

from blockdx import BlockDX, MarketOrder, LimitOrder, Error

confFilePath = "C:\\Users\\kbentahmed\\Dropbox\\__ALTCOINS\\___REPOS\\Bitbucket\\private\\tradinglib\\xbridge236.conf"

DX = BlockDX(ip="34.236.125.224", port="41414", rpcuser="user", rpcpass="pass", conf=confFilePath)


def test_DX_address(maker_param: str):
    print("------------")
    print("DX.getAddressList: %s" % maker_param)
    print(DX.getAddressList(maker_param))
    print("------------")
    print("DX.getTopAddress: %s" % maker_param)
    print(DX.getTopAddress(maker_param))
    print("-----------")
    print("DX.getTopAddress - XXX")
    print(DX.getTopAddress("XXX"))
    print("------------")
    val1=0.1
    print("DX.getAddressForSize: %s %s" % (maker_param, val1))
    print(DX.getAddressForSize(maker_param, val1))
    print("------------")
    print("DX.getAddressForSize: %s %s" % (maker_param, 0.01))
    print(DX.getAddressForSize(maker_param, 0.01))
    print("------------")
    randInt1 = random.randint(1, 3)
    print("DX.getAddressForSize: %s %s" % (maker_param, randInt1))
    print(DX.getAddressForSize(maker_param, randInt1))
    print("------------")
    print("DX.validateAddress:")
    print(DX.validateAddress("LTC", "cccc"))
    print("------------")
    print("DX.validateAddress:")
    print(DX.validateAddress("LTC", "LZNVpRrcgwdE2k8j1sAN3o8sH8PqYhi3wS"))


test_DX_address("MONA")

