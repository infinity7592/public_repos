
import sys, os
lib_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "prod")
sys.path.append(lib_path)

import pprint as pp

from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
from tradinglib_obj import BlockDX, MarketOrder, LimitOrder, Error

confFilePath = "C:\\Users\\kbentahmed\\Dropbox\\__ALTCOINS\\blockdx_explorer\\architecture\\xbridge236.conf"

DX = BlockDX(ip="34.236.125.224", port="41414", rpcuser="user", rpcpass="pass", conf=confFilePath)


def cancelAll():
    print("------------")
    print("DX.cancelAll")
    print(DX.cancelAll())


def test_limit_matching_engine(maker_param: str, taker_param: str):
    cancelAll()
    for itm in [[0.3, 0.1], [0.2, 0.1], [0.15, 0.1]]:
        """ 2 MONA => 1 LTC """
        postRst = DX.postLimitOrder(LimitOrder(maker_size=itm[0], maker=maker_param, taker_size=itm[1], taker=taker_param))
        print(postRst)
    # exit(0)
    for itm in [[0.3, 0.1], [0.2, 0.1], [0.15, 0.1]]:
        """ 1 LTC => x MONA """
        newLimitOrder = LimitOrder(maker_size=itm[1], maker=taker_param, taker_size=itm[0], taker=maker_param, dryrun=True)
        print(newLimitOrder)
        print(DX.execute(newLimitOrder))
    pp.pprint(DX.getPostedOrders())


def test_buy_market_matching_engine(maker_param: str, taker_param: str):
    cancelAll()
    for itm in [[0.3, 0.1], [0.2, 0.1], [0.15, 0.1]]:
        """ 2 MONA => 1 LTC """
        postRst = DX.postLimitOrder(LimitOrder(maker_size=itm[0], maker=maker_param, taker_size=itm[1], taker=taker_param))
        print(postRst)
    # exit(0)
    for itm in [[0.3, 0.1], [0.2, 0.1], [0.15, 0.1]]:
        """ 1 LTC => x MONA """
        newBuyMarketOrder = MarketOrder(maker=taker_param, taker_size=itm[0], taker=maker_param, dryrun=True)
        print(newBuyMarketOrder)
        print(DX.execute(newBuyMarketOrder))
    pp.pprint(DX.getPostedOrders())

import time

def test_sell_market_matching_engine(maker_param: str, taker_param: str):
    cancelAll()
    for itm in [[0.3, 0.03], [0.2, 0.03], [0.15, 0.03]]:
        """ 2 MONA => 1 LTC """
        postRst = DX.postLimitOrder(LimitOrder(maker_size=itm[0], maker=maker_param, taker_size=itm[1], taker=taker_param))
        time.sleep(1)
        """ 1 LTC => x MONA """
        newSellMarketOrder = MarketOrder(maker=taker_param, maker_size=0.1, taker=maker_param, dryrun=True)
        print(newSellMarketOrder)
        print(DX.execute(newSellMarketOrder))
        # print(postRst)
    """
    for itm in [[0.3, 0.1], [0.2, 0.1], [0.15, 0.1]]:
        newSellMarketOrder = MarketOrder(maker=taker_param, maker_size=itm[1], taker=maker_param, dryrun=True)
        print(newSellMarketOrder)
        print(DX.execute(newSellMarketOrder))
    """
    pp.pprint(DX.getPostedOrders())


# test_limit_matching_engine("MONA", "LTC")
# test_buy_market_matching_engine("MONA", "LTC")
# test_sell_market_matching_engine("MONA", "LTC")
