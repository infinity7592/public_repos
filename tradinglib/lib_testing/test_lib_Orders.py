import sys, os
lib_path = os.path.join(os.path.dirname(os.path.dirname(__file__)), "prod")
sys.path.append(lib_path)

from tradinglib_obj import BlockDX, MarketOrder, LimitOrder, Error

confFilePath = "C:\\Users\\kbentahmed\\Dropbox\\__ALTCOINS\\blockdx_explorer\\architecture\\xbridge236.conf"

DX = BlockDX(ip="34.236.125.224", port="41414", rpcuser="user", rpcpass="pass", conf=confFilePath)


def cancelAll():
    print("------------")
    print("DX.cancelAll")
    print(DX.cancelAll())


def test_DX_buyMarketOrder(maker_param: str, taker_param: str):
    marketOrder = MarketOrder(maker=maker_param, taker=taker_param, taker_size=0.05, dryrun=True, VERBOSE=True)
    print("------------")
    print(marketOrder)
    print(DX.execute(marketOrder))


def test_DX_sellMarketOrder(maker_param: str, taker_param: str):
    marketOrder2 = MarketOrder(maker=maker_param, taker=taker_param, maker_size=0.1, dryrun=True)
    print("------------")
    print(marketOrder2)
    print(DX.execute(marketOrder2))


def test_DX_postOrder(maker_param: str, taker_param: str):
    cancelAll()
    newOrder = LimitOrder(maker_size=0.05, maker=maker_param, taker_size=50, taker=taker_param, dryrun=True)
    print(DX.postLimitOrder(newOrder))


def test_DX_limitOrder(maker_param: str, taker_param: str):
    cancelAll()
    print("------------")
    print("LimitOrder(maker_size=0.05, maker=%s, taker_size=100, taker=%s)" % (maker_param, taker_param))
    newOrder = LimitOrder(maker_size=0.05, maker=maker_param, taker_size=100, taker=taker_param, dryrun=True, VERBOSE=True)
    print(DX.execute(newOrder))


def test_DX_price_limitOrder(maker_param: str, taker_param: str):
    cancelAll()
    newOrder = LimitOrder(price=2, maker=maker_param, taker_size=1, taker=taker_param, dryrun=True,
                          VERBOSE=True)
    print("------------")
    print(newOrder)
    print(DX.execute(newOrder))


def test_invalid_maker_order():
    newOrder = LimitOrder(price=2, maker="XYZ", taker_size=1, taker="MONA", dryrun=True,
                          VERBOSE=True)
    print("------------")
    print(newOrder)
    print(DX.execute(newOrder))


def test_invalid_taker_order():
    newOrder = LimitOrder(price=2, maker="MONA", taker_size=1, taker="XYZ", dryrun=True,
                          VERBOSE=True)
    print("------------")
    print(newOrder)
    print(DX.execute(newOrder))


# test_DX_postOrder("LTC", "SYS")
# test_DX_postOrder("LTC", "MONA")
# test_DX_limitOrder("LTC", "MONA")

# test_DX_price_limitOrder("MONA", "LTC")

# test_DX_buyMarketOrder("MONA", "LTC")
# test_DX_sellMarketOrder("LTC", "MONA")

# test_invalid_maker_order()
# test_invalid_taker_order()
