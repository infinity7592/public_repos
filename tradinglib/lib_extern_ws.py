import time, requests
from decimal import *

D = Decimal

"""
    Contains interfaces to external web services
"""

class Error(object):
    def __init__(self, msg):
        self.text = msg

    def __str__(self):
        return self.text


CMC_prices = []
last_CMC_update = time.time()

cachedCmcUSDPrices = {}
def getCmcUSDPrices():
    global cachedCmcUSDPrices
    try:
        URL = "https://api.coinmarketcap.com/v2/ticker/"
        r = requests.get(URL)
        if r.status_code == 200:
            rst = r.json()
            rst = rst.get("data", {})
            for k in rst.keys():
                cachedCmcUSDPrices[rst[k]["symbol"]] = rst[k]["quotes"]["USD"]["price"]
            return cachedCmcUSDPrices
        else:
            return Error("[coinmarketcap.com] Unavailable data")
    except (requests.exceptions.Timeout, requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout,
            requests.exceptions.ConnectionError, TypeError) as err:
        return Error("[coinmarketcap.com] unavailable data")


def getCmcUSDPrice(coin: str):
    if cachedCmcUSDPrices == {}:
        getCmcUSDPrices()
    return cachedCmcUSDPrices.get(coin, -1)


coinGeckoSupportedList = []
coinGeckoMappings = {}
def getCoinGeckoList():
    global coinGeckoSupportedList
    global coinGeckoMappings
    try:
        r = requests.get("https://api.coingecko.com/api/v3/coins/list")
        if r.status_code == 200:
            rst = r.json()
            for coin in rst:
                coinGeckoMappings[coin.get("symbol").upper()] = coin.get("id")
                coinGeckoSupportedList.append(coin.get("symbol").upper())
            return coinGeckoSupportedList
        else:
            return Error("[CoinGecko] Unavailable data")
    except (requests.exceptions.Timeout, requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout,
            requests.exceptions.ConnectionError, TypeError) as err:
        return Error("[CoinGecko] Unavailable data")


getCoinGeckoList()


def getGeckoUSDPrice(coin: str, VERBOSE=False):
    """ Returns prices from CoinGecko web service """
    if coinGeckoMappings == {}:
        getCoinGeckoList()
    geckoId = coinGeckoMappings.get(coin, None)
    if geckoId is None:
        print("[getCoinGeckoPrice] geckoId: %s" % str(geckoId))
        return -1
    requestStr = "https://api.coingecko.com/api/v3/coins/"
    requestStr += geckoId
    if VERBOSE:
        print("[getCoinGeckoPrice] request_str: %s" % requestStr)
    try:
        r = requests.get(requestStr)
        if r.status_code == 200:
            rst = r.json()
            return rst.get("market_data", {}).get("current_price", {}).get("usd", -1)
        else:
            return Error("[CoinGecko] Unavailable data")
    except (requests.exceptions.Timeout, requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout,
        requests.exceptions.ConnectionError, TypeError) as err:
        return Error("[CoinGecko] Unavailable data: %s" % str(err))


def getBtcRecommendedFees(**kwargs):
    """ Returns bitcoin related recommended fees
    - Data source: https://bitcoinfees.earn.com/api
    - Sample returned value:
    {"fastestFee":500,"halfHourFee":490,"hourFee":480}
    """
    try:
        r = requests.get("https://bitcoinfees.earn.com/api/v1/fees/recommended")
        if r.status_code == 200:
            rst = r.json()
            if kwargs is None:
                assert isinstance(rst, dict) is True
                return rst
            if kwargs.get("type") == "fast":
                rst.get("fastestFee")
            if kwargs.get("type") == "30m":
                rst.get("halfHourFee")
            if kwargs.get("type") == "1h":
                rst.get("hourFee")
            assert isinstance(rst, dict) is True
            return rst
        else:
            return Error("[bitcoinfees.earn.com] Unavailable data")
    except (requests.exceptions.Timeout, requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout,
            requests.exceptions.ConnectionError, TypeError) as err:
        return Error("unavailable data")


def getBtcFeeList():
    """ Returns a list of Fee objects that contain predictions about fees in the given range from minFee to maxFee in satoshis/byte.

    - Data source:
        https://bitcoinfees.earn.com/api

    - Sample return value:
        {"fees":[{"minFee":0,"maxFee":0,"dayCount":14,"memCount":0,"minDelay":10000,"maxDelay":10000,"minMinutes":9960,"maxMinutes":10000},{"minFee":1,"maxFee":10,"dayCount":21685,"memCount":21642,"minDelay":26,"maxDelay":10000,"minMinutes":180,"maxMinutes":10000},{"minFee":11,"maxFee":20,"dayCount":23053,"memCount":22885,"minDelay":18,"maxDelay":10000,"minMinutes":120,"maxMinutes":10000},{"minFee":21,"maxFee":30,"dayCount":19033,"memCount":18889,"minDelay":5,"maxDelay":10000,"minMinutes":30,"maxMinutes":10000},{"minFee":31,"maxFee":40,"dayCount":10350,"memCount":10199,"minDelay":4,"maxDelay":10000,"minMinutes":30,"maxMinutes":10000},{"minFee":41,"maxFee":50,"dayCount":14726,"memCount":14541,"minDelay":4,"maxDelay":10000,"minMinutes":30,"maxMinutes":10000},{"minFee":51,"maxFee":60,"dayCount":2580,"memCount":2520,"minDelay":4,"maxDelay":10000,"minMinutes":30,"maxMinutes":10000},{"minFee":61,"maxFee":70,"dayCount":1701,"memCount":1642,"minDelay":4,"maxDelay":10000,"minMinutes":25,"maxMinutes":10000},{"minFee":71,"maxFee":80,"dayCount":1512,"memCount":1474,"minDelay":4,"maxDelay":10000,"minMinutes":25,"maxMinutes":10000},{"minFee":81,"maxFee":90,"dayCount":1498,"memCount":1456,"minDelay":4,"maxDelay":10000,"minMinutes":25,"maxMinutes":10000},{"minFee":91,"maxFee":100,"dayCount":3749,"memCount":3524,"minDelay":4,"maxDelay":10000,"minMinutes":25,"maxMinutes":10000},{"minFee":101,"maxFee":110,"dayCount":4157,"memCount":3993,"minDelay":4,"maxDelay":10000,"minMinutes":25,"maxMinutes":10000},{"minFee":111,"maxFee":120,"dayCount":3273,"memCount":3131,"minDelay":4,"maxDelay":10000,"minMinutes":25,"maxMinutes":10000},{"minFee":121,"maxFee":130,"dayCount":2728,"memCount":2622,"minDelay":4,"maxDelay":10000,"minMinutes":25,"maxMinutes":10000},{"minFee":131,"maxFee":140,"dayCount":1993,"memCount":1912,"minDelay":4,"maxDelay":10000,"minMinutes":25,"maxMinutes":10000},{"minFee":141,"maxFee":150,"dayCount":2708,"memCount":2617,"minDelay":4,"maxDelay":10000,"minMinutes":25,"maxMinutes":10000},{"minFee":151,"maxFee":160,"dayCount":1985,"memCount":1833,"minDelay":4,"maxDelay":143,"minMinutes":25,"maxMinutes":1320},{"minFee":161,"maxFee":170,"dayCount":2250,"memCount":2123,"minDelay":4,"maxDelay":140,"minMinutes":25,"maxMinutes":1200},{"minFee":171,"maxFee":180,"dayCount":3257,"memCount":3059,"minDelay":4,"maxDelay":140,"minMinutes":25,"maxMinutes":1200},{"minFee":181,"maxFee":190,"dayCount":2022,"memCount":1554,"minDelay":4,"maxDelay":137,"minMinutes":25,"maxMinutes":1200},{"minFee":191,"maxFee":200,"dayCount":2644,"memCount":1467,"minDelay":4,"maxDelay":137,"minMinutes":25,"maxMinutes":1200},{"minFee":201,"maxFee":210,"dayCount":1721,"memCount":1032,"minDelay":4,"maxDelay":134,"minMinutes":25,"maxMinutes":1200},{"minFee":211,"maxFee":220,"dayCount":1000,"memCount":485,"minDelay":4,"maxDelay":134,"minMinutes":25,"maxMinutes":1200},{"minFee":221,"maxFee":230,"dayCount":2024,"memCount":1127,"minDelay":4,"maxDelay":134,"minMinutes":25,"maxMinutes":1200},{"minFee":231,"maxFee":240,"dayCount":1379,"memCount":649,"minDelay":4,"maxDelay":122,"minMinutes":25,"maxMinutes":1140},{"minFee":241,"maxFee":250,"dayCount":1257,"memCount":747,"minDelay":4,"maxDelay":122,"minMinutes":25,"maxMinutes":1140},{"minFee":251,"maxFee":260,"dayCount":1906,"memCount":1188,"minDelay":4,"maxDelay":122,"minMinutes":25,"maxMinutes":1140},{"minFee":261,"maxFee":270,"dayCount":3187,"memCount":2043,"minDelay":4,"maxDelay":114,"minMinutes":25,"maxMinutes":1140},{"minFee":271,"maxFee":280,"dayCount":2756,"memCount":1319,"minDelay":4,"maxDelay":106,"minMinutes":20,"maxMinutes":1080},{"minFee":281,"maxFee":290,"dayCount":4088,"memCount":1523,"minDelay":4,"maxDelay":88,"minMinutes":20,"maxMinutes":840},{"minFee":291,"maxFee":300,"dayCount":4906,"memCount":690,"minDelay":4,"maxDelay":88,"minMinutes":20,"maxMinutes":840},{"minFee":301,"maxFee":310,"dayCount":7663,"memCount":1117,"minDelay":4,"maxDelay":70,"minMinutes":15,"maxMinutes":660},{"minFee":311,"maxFee":320,"dayCount":7002,"memCount":1255,"minDelay":4,"maxDelay":48,"minMinutes":10,"maxMinutes":480},{"minFee":321,"maxFee":330,"dayCount":4211,"memCount":744,"minDelay":4,"maxDelay":40,"minMinutes":10,"maxMinutes":420},{"minFee":331,"maxFee":340,"dayCount":4359,"memCount":823,"minDelay":3,"maxDelay":36,"minMinutes":10,"maxMinutes":360},{"minFee":341,"maxFee":350,"dayCount":6483,"memCount":428,"minDelay":3,"maxDelay":30,"minMinutes":5,"maxMinutes":300},{"minFee":351,"maxFee":360,"dayCount":7797,"memCount":528,"minDelay":3,"maxDelay":28,"minMinutes":5,"maxMinutes":300},{"minFee":361,"maxFee":370,"dayCount":5960,"memCount":332,"minDelay":3,"maxDelay":26,"minMinutes":5,"maxMinutes":300},{"minFee":371,"maxFee":380,"dayCount":5909,"memCount":419,"minDelay":2,"maxDelay":22,"minMinutes":5,"maxMinutes":300},{"minFee":381,"maxFee":390,"dayCount":3955,"memCount":375,"minDelay":2,"maxDelay":21,"minMinutes":5,"maxMinutes":240},{"minFee":391,"maxFee":400,"dayCount":27231,"memCount":1149,"minDelay":2,"maxDelay":18,"minMinutes":5,"maxMinutes":240},{"minFee":401,"maxFee":410,"dayCount":9741,"memCount":602,"minDelay":1,"maxDelay":17,"minMinutes":0,"maxMinutes":240},{"minFee":411,"maxFee":420,"dayCount":13849,"memCount":445,"minDelay":1,"maxDelay":15,"minMinutes":0,"maxMinutes":180},{"minFee":421,"maxFee":430,"dayCount":8948,"memCount":224,"minDelay":1,"maxDelay":13,"minMinutes":0,"maxMinutes":180},{"minFee":431,"maxFee":440,"dayCount":5583,"memCount":175,"minDelay":1,"maxDelay":12,"minMinutes":0,"maxMinutes":180},{"minFee":441,"maxFee":450,"dayCount":10814,"memCount":180,"minDelay":1,"maxDelay":11,"minMinutes":0,"maxMinutes":180},{"minFee":451,"maxFee":460,"dayCount":37788,"memCount":121,"minDelay":1,"maxDelay":10,"minMinutes":0,"maxMinutes":180},{"minFee":461,"maxFee":470,"dayCount":149908,"memCount":188,"minDelay":1,"maxDelay":6,"minMinutes":0,"maxMinutes":90},{"minFee":471,"maxFee":480,"dayCount":23048,"memCount":2866,"minDelay":0,"maxDelay":1,"minMinutes":0,"maxMinutes":40},{"minFee":481,"maxFee":490,"dayCount":6284,"memCount":186,"minDelay":0,"maxDelay":1,"minMinutes":0,"maxMinutes":30},{"minFee":491,"maxFee":500,"dayCount":6836,"memCount":104,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25},{"minFee":501,"maxFee":510,"dayCount":5677,"memCount":134,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25},{"minFee":511,"maxFee":520,"dayCount":2854,"memCount":44,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25},{"minFee":521,"maxFee":530,"dayCount":3037,"memCount":79,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25},{"minFee":531,"maxFee":540,"dayCount":1883,"memCount":51,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25},{"minFee":541,"maxFee":550,"dayCount":2336,"memCount":21,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25},{"minFee":551,"maxFee":560,"dayCount":2719,"memCount":34,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25},{"minFee":561,"maxFee":570,"dayCount":3326,"memCount":28,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25},{"minFee":571,"maxFee":580,"dayCount":2832,"memCount":53,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25},{"minFee":581,"maxFee":590,"dayCount":1396,"memCount":56,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25},{"minFee":591,"maxFee":600,"dayCount":2077,"memCount":51,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25},{"minFee":601,"maxFee":610,"dayCount":4570,"memCount":54,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25},{"minFee":611,"maxFee":620,"dayCount":1024,"memCount":16,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25},{"minFee":621,"maxFee":29600,"dayCount":27913,"memCount":528,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25}]}

    The Fee objects have the following properties (aside from the minFee-maxFee range they refer to):
        - dayCount: Number of confirmed transactions with this fee in the last 24 hours.
        - memCount: Number of unconfirmed transactions with this fee.
        - minDelay: Estimated minimum delay (in blocks) until transaction is confirmed (90% confidence interval).
        - maxDelay: Estimated maximum delay (in blocks) until transaction is confirmed (90% confidence interval).
        - minMinutes: Estimated minimum time (in minutes) until transaction is confirmed (90% confidence interval).
        - maxMinutes: Estimated maximum time (in minutes) until transaction is confirmed (90% confidence interval).
    """
    try:
        URL = "https://bitcoinfees.earn.com/api/v1/fees/list"
        r = requests.get(URL)
        if r.status_code == 200:
            rst = r.json()
            assert isinstance(rst, dict) is True
            return rst
        else:
            return Error("[bitcoinfees.earn.com] Unavailable data")
    except (requests.exceptions.Timeout, requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout,
            requests.exceptions.ConnectionError, TypeError) as err:
        return Error("unavailable data")


def getUnconfirmedTxCount(currency):
    """ Returns the number of unconfirmed transactions on the network of specified currency, as an integer.

    - The information is:
        - retrieved from chain.so WebService.
        - only available for BTC, LTC, DASH and DOGE.

    Returns an JSON error object if it fails.

    - Usages
        This information can be used to know:
        1) whether it is appropriate to trade bitcoin now.
        2) whether fees have to be adjusted higher to get priority.

    - Additional remark:
        In case the chain.so service is temporarily unavailable, get_btc_network_unconfirmed_tx_count can be used
        to retrieve the same information, only for bitcoin.
    """
    time.sleep(0.5)
    try:
        if currency not in ["BTC", "LTC", "DASH", "DOGE"]:
            return Error("unsupported coin")
        URL = "https://chain.so/api/v2/get_info/"
        r = requests.get(URL + currency)
        if r.status_code == 200:
            rst = r.json()
            assert isinstance(rst, dict) is True
            return rst.get("data", {}).get("unconfirmed_txs")
        else:
            return Error("[chain.so] Unavailable data")
    except (requests.exceptions.Timeout, requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout,
            requests.exceptions.ConnectionError, TypeError) as err:
        return Error(err)


def getBtcUnconfirmedTxCount():
    """ Returns the number of unconfirmed transactions on the bitcoin network, as an integer.

    - The information is:
        - retrieved from blockchain.info WebService.
        - only available for BTC, LTC, DASH and DOGE.

    Returns an JSON error object if it fails.

    In case the chain.so service is temporarily unavailable, get_btc_network_unconfirmed_tx_count can be used
    to retrieve the same information, only for bitcoin.
    """
    time.sleep(0.5)
    try:
        URL = "https://blockchain.info/q/unconfirmedcount"
        r = requests.get(URL)
        if r.status_code == 200:
            rst = r.json()
            return rst
            # return rst.get("data", {}).get("unconfirmed_txs")
        else:
            return Error("[blockchain.info] Unavailable data")
    except (requests.exceptions.Timeout, requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout,
            requests.exceptions.ConnectionError, TypeError) as err:
        return Error(err)


def getBtcActualFee(txid: str):
    """ Returns the fee paid for a specific bitcoin txid

    - Example:
        https://blockchain.info/q/txfee/42817953a636ae2a94034fba56840af08d306549a37f59406ea3d01e4fd69dc7
        getBtcActualFee("42817953a636ae2a94034fba56840af08d306549a37f59406ea3d01e4fd69dc7")

    ==> Returns 104381

    - Remark:
        the returned integer represent the fees paid in satoshis.
    """
    time.sleep(0.5)
    try:
        URL = "https://blockchain.info/q/txfee/"
        r = requests.get(URL + txid)
        if r.status_code == 200:
            return r.json()
        else:
            return Error("[blockchain.info] Unavailable data")
    except (requests.exceptions.Timeout, requests.exceptions.ReadTimeout, requests.exceptions.ConnectTimeout,
            requests.exceptions.ConnectionError, TypeError) as err:
        return Error(err)

