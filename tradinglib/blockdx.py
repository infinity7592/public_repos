
from decimal import *
D = Decimal

from bitcoinrpc.authproxy import AuthServiceProxy, JSONRPCException
import configparser
import os, time
import http, socket
from http import client
from itertools import chain

import pprint
pp = pprint.PrettyPrinter(indent=4)

import lib_utils
import lib_extern_ws
import lib_stats
import lib_utils

DX_MAX_VALUE = 100000000
BLOCK_FEE = D(0.005)


def isoDateStrToUnix(d):
    """ Converts an ISO 8601 date string to a unix timestamp

        Parameters:
            - ISO 8601 string [required: string]

        Returns:
            Unix timestamp
    """
    return lib_utils.IsodateStrToUnix(d)


def toBtc(sat):
    """ Converts satoshis to btc """
    return (sat / 100000000)


def toSat(btc):
    """ Converts btc to satoshis """
    return (btc * 100000000)


def getMean(x_arr):
    return lib_stats.getMean(x_arr)


def getVariance(x_arr, sample=False):
    return lib_stats.getVariance(x_arr, sample)


def getRegression(x_arr, y_arr):
    return lib_stats.getRegression(x_arr, y_arr)


def getQuantile(x_arr, percentile, interpolation="nearest"):
    return lib_stats.getQuantile(x_arr, percentile, interpolation)


def getCmcUSDPrices():
    return lib_extern_ws.getCmcUSDPrices()


def getCmcUSDPrice(coin: str):
    """ Returns the dollar value of a specified coin.
        Returns -1 if the coin is not valid or the information is unavailable.
        Provider: Coinmarketcap

        Parameters:
             - coin [required]
    """
    return lib_extern_ws.getCmcUSDPrice(coin)


def getGeckoUSDPrice(coin: str, VERBOSE=False):
    """ Returns the dollar value of a specified coin.
        Returns -1 if the coin is not valid or the information is unavailable.
        Provider: CoinGecko

        Parameters:
             - coin [required]
    """
    return lib_extern_ws.getGeckoUSDPrice(coin, VERBOSE)


def getGeckoCryptoPrice(coin: str, base: str, VERBOSE=False):
    """ Returns the price of a coin with a base coin as the base currency
        Returns -1 if the coin is not valid or the information is unavailable.
        Provider: CoinGecko

        Parameters:
             - coin [required]
             - base [required]
    """
    if VERBOSE:
        print("[getCryptoPrice] params - coin: %s - base: %s" % (coin, base))
    coinPrice = getGeckoUSDPrice(coin)
    basePrice = getGeckoUSDPrice(base)
    if basePrice <= 0 or coinPrice <= 0:
        if VERBOSE:
            print("[getCryptoPrice] coinPrice: %s - basePrice: %s" % (coinPrice, basePrice))
        return -1
    return coinPrice / basePrice


def getUnconfirmedTxCount(coin):
    return lib_extern_ws.getUnconfirmedTxCount(coin)


def getBtcUnconfirmedTxCount():
    return lib_extern_ws.getBtcUnconfirmedTxCount()


def getBtcRecommendedFees(**kwargs):
    return lib_extern_ws.getBtcRecommendedFees(**kwargs)


def getBtcFeeList():
    return lib_extern_ws.getBtcFeeList()


def getBtcActualFee(txid: str):
    return lib_extern_ws.getBtcActualFee(txid)


def xprint(msg_str, newLine=False):
    if newLine:
        print("")
    pp.pprint(msg_str)


def normalizeArray(arr):
    lst = list(chain.from_iterable(arr))
    return lst


class Error(object):
    def __init__(self, msg):
        self.text = msg

    def __str__(self):
        return self.text


def verifyOrder(order):
    if not isinstance(order, Order):
        return Error("Invalid type")
    if order.maker == order.taker:
        return Error("Inconsistent maker and taker params")
    order.taker_size = str(lib_utils.formatted_nb(order.taker_size))
    order.maker_size = str(lib_utils.formatted_nb(order.maker_size))
    return order


class Order(object):
    def __init__(self, **kwargs):
        self.VERBOSE = True
        
        self.maker = kwargs.get("maker", None)
        self.taker = kwargs.get("taker", None)
        self.maker_size = kwargs.get("maker_size", None)
        self.taker_size = kwargs.get("taker_size", None)
        self.maker_address = kwargs.get("maker_address", None)
        self.taker_address = kwargs.get("taker_address", None)

        if self.maker_size is not None:
            self.maker_size = lib_utils.formatted_nb(self.maker_size)

        if self.taker_size is not None:
            self.taker_size = lib_utils.formatted_nb(self.taker_size)

        self.dryrun = kwargs.get("dryrun", False)
        self.txid = None
        self.errors = []
        self.accepted = []
        self.posted = []

    def __str__(self):
        log_str = "\n---------------------------------------------\n"
        log_str += "[{0}] {1} {2} <=> {3} {4}\n".format(self.__class__.__qualname__, self.maker_size,
                                                       self.maker,
                                                       self.taker_size, self.taker)
        log_str += "\n---------------------------------------------\n"
        return log_str


class MarketOrder(Order):
    def __init__(self, **kwargs):
        Order.__init__(self, **kwargs)


class LimitOrder(Order):
    def __init__(self, **kwargs):
        Order.__init__(self, **kwargs)
        self.price = kwargs.get("price", None)
        if self.price is not None and self.maker_size is None:
            self.maker_size = float(self.price) * float(self.taker_size)
            self.maker_size = round(float(self.maker_size), 6)


def checkId(txid):
    if len(txid) != 64:
        return False
    return True


""" Decorator """
def rpc(func):
    def function_wrapper(*args, **kwargs):
        try:
            call_rst = func(*args, **kwargs)
            if isinstance(call_rst, dict):
                if call_rst.get("error", None):
                    return Error(call_rst.get("error"))
            return call_rst
        except http.client.CannotSendRequest:
            return Error("[%s] CannotSendRequest" % func.__name__)
        except ConnectionRefusedError:
            return Error("[%s] ConnectionRefusedError" % func.__name__)
        except ConnectionAbortedError:
            return Error("[%s] ConnectionAbortedError" % func.__name__)
        except JSONRPCException as e:
            return Error("[%s] JSONRPCException: %s" % (func.__name__, e))
        except TimeoutError as e:
            return Error("[%s] TimeoutError: %s" % (func.__name__, e))
        except socket.timeout as e:
            return Error("[%s] TimeoutError: %s" % (func.__name__, e))
        except OSError as e:
            return Error("[%s] OSError: %s" % (func.__name__, e))
    return function_wrapper
        

class RPConnector(object):
    def __init__(self, **kwargs):
        self.coin = ""
        self.rpcIP = kwargs.get("ip", None)
        self.rpcPort = kwargs.get("port", None)        
        self.rpcUsername = kwargs.get("rpcuser", None)
        self.rpcPass = kwargs.get("rpcpass", None)
        self.connector = None
        self.connect()
        self.dustAmount = -1

    def connect(self, VERBOSE=True):
        if VERBOSE:
            print("[RPConnector/connect] Trying to connect to http://%s:%s@%s:%s" % (self.rpcUsername, self.rpcPass, self.rpcIP, self.rpcPort))
        if int(self.rpcPort) > 65535 or int(self.rpcPort) < 1000:
            print("[RPConnector/connect] Invalid port: %s", self.rpcPort)
            self.connector = None
            return Error("Invalid port")
        self.connector = AuthServiceProxy("http://%s:%s@%s:%s" % (self.rpcUsername, self.rpcPass, self.rpcIP, self.rpcPort))
        return True

    def isConnectionAlive(self):
        if isinstance(self.getWalletBalance(), Error):
            return False
        return True

    @rpc
    def getTransaction(self, txid):
        rst = self.connector.gettransaction(txid)
        return rst

    @rpc
    def isSynced(self):
        rst = self.connector.getblockchaininfo()
        if not isinstance(rst, dict):
            return False
        return float(rst.get("verificationprogress", 0)) >= 0.98

    @rpc
    def getWalletBalance(self):
        return self.connector.getbalance()

    @rpc
    def getBalance(self, address=None):
        if address is None:
            return self.getWalletBalance()
        if len(address) < 30:
            return self.getWalletBalance()
        rst = self.connector.listaddressgroupings()
        
        if rst in ([], [[]]):
            return []

        normalized_rst = normalizeArray(rst)
        for itm in normalized_rst:
            if itm[0] == address:
                return round(float(itm[1]), 6)
        return 0

    @rpc
    def getAddressList(self):
        rst = self.connector.listaddressgroupings()
        normalized_array = normalizeArray(rst)
        return normalized_array

    @rpc
    def getTopAddress(self):
        addressList = self.getAddressList()
        if len(addressList) == 0:
            return Error("No address found")
        sortedAddressLst = sorted(addressList, key=lambda x: x[1], reverse=True)
        topAddress = sortedAddressLst.pop(0)
        return topAddress[0]

    @rpc
    def getAddressForSize(self, size):
        addressList = self.getAddressList()
        if len(addressList) == 0 or size == 0:
            return Error("No address found")
        sortedAddressLst = sorted(addressList, key=lambda x: x[1], reverse=False)
        for address in sortedAddressLst:
            if address[1] > size:
                return address[0]
        return Error("No address found")

    @rpc
    def getInputs(self):
        rst = self.connector.listunspent()
        return rst

    @rpc
    def getDustAmount(self):
        if self.dustAmount > 0:
            return self.dustAmount
        network_rst = self.connector.getnetworkinfo()
        if isinstance(network_rst, Error):
            return network_rst
        minRelayFee = network_rst.get("relayfee", 0)
        self.dustAmount = 182 * (minRelayFee) / 1000 * 3
        return self.dustAmount

    @rpc
    def validateAddress(self, address: str):
        return self.connector.validateaddress(address)

    @rpc
    def isWalletUnlocked(self):
        """ Returns True if the wallet is currently unlocked.
            False otherwise.
        """
        rst = self.connector.getwalletinfo()
        if rst.get("unlocked_until", 0) == 0:
            return False
        return True

    @rpc
    def unlock(self, pwd="mypwd", delay=99999):
        """ Unlocks the wallet with the provided password """
        if not self.isWalletUnlocked():
            self.connector.walletpassphrase(pwd, delay)

    @rpc
    def relock(self):
        """ Relocks the wallet """
        if self.isWalletUnlocked():
            self.connector.walletlock()


"""
    Adds block specific API connectors
"""
class BlockConnector(RPConnector):
    def __init__(self, **kwargs):
        RPConnector.__init__(self, **kwargs)

    @rpc
    def dxMakeOrder(self, **kwargs):
        """
            dxMakeOrder (maker) (maker size) (maker address) (taker) (taker size) (taker address) (type) (dryrun)[optional]
        """
        maker = kwargs.get("maker", None)
        maker_address = kwargs.get("maker_address", None)
        maker_size = kwargs.get("maker_size", None)
        taker = kwargs.get("taker", None)
        taker_address = kwargs.get("taker_address", None)
        taker_size = kwargs.get("taker_size", None)
        if any(x is None for x in [maker, taker, maker_size, taker_size, maker_address, taker_address]):
            return Error("[dxMakeOrder] Invalid parameter")
        if any(isinstance(x, Error) for x in [maker, taker, maker_size, taker_size, maker_address, taker_address]):
            return Error("[dxMakeOrder] Invalid return value")
        maker_size = round(maker_size, 6)
        taker_size = round(taker_size, 6)
        make_order_rst = self.connector.dxMakeOrder(maker, str(maker_size), maker_address, taker, str(taker_size), taker_address, "exact")
        if make_order_rst.get("error", None) is not None:
            return Error(make_order_rst.get("error"))
        return make_order_rst

    @rpc
    def dxTakeOrder(self, **kwargs):
        txid = kwargs.get("txid", None)
        maker_address = kwargs.get("maker_address", None)
        taker_address = kwargs.get("taker_address", None)
        dryrun = kwargs.get("dryrun", False)

        if txid is None or maker_address is None or taker_address is None:
            return Error("[dxTakeOrder] Invalid parameter")

        if dryrun:
            take_order_rst = self.connector.dxTakeOrder(txid, maker_address, taker_address, "dryrun")
        else:
            take_order_rst = self.connector.dxTakeOrder(txid, maker_address, taker_address)

        if take_order_rst.get("error", None) is not None:
            return Error(take_order_rst.get("error"))

        return take_order_rst

    @rpc
    def cancelId(self, txid: str):
        """ Cancels a transaction with the supplied txid.

        - If it succeeds, returns a JSON object { "id": ""}
        - If it fails, returns an error object

        Parameter:
            - txid [required: string]
        """
        if not checkId(txid):
            return Error("[cancel] Invalid txid param: %s" % txid)
        cancel_rst = self.connector.dxCancelOrder(txid)
        if cancel_rst.get("error", None) is not None:
            print("")
            pp.pprint(cancel_rst)
        time.sleep(0.2)
        return cancel_rst

    def dxGetTokenBalances(self):
        rst = self.connector.dxGetTokenBalances()
        return rst

    @rpc
    def getInfo(self, txid):
        if not checkId(txid):
            return Error("[getInfo] Invalid txid param: %s" % txid)

        info_rst = self.connector.dxGetOrder(txid)
        return info_rst

    @rpc
    def getOrderBook(self, **kwargs):
        maker = kwargs.get("maker", None)
        taker = kwargs.get("taker", None)
        detailled = kwargs.get("detailled", True)

        if maker is None or taker is None:
            return {'asks': [], 'bids': []}

        if detailled:
            orderbook = self.connector.dxGetOrderBook(3, maker, taker, 10000)
        else:
            orderbook = self.connector.dxGetOrderBook(2, maker, taker, 10000)
        return orderbook

    def getBidsAsks(self, bidAsk="bids", **kwargs):
        if kwargs is None:
            return []

        maker_param = kwargs.get("maker", None)
        taker_param = kwargs.get("taker", None)

        if maker_param is None or taker_param is None:
            return []

        orderbook = self.getOrderBook(maker=maker_param, taker=taker_param)

        if isinstance(orderbook, Error):
            return orderbook

        vol_limit = kwargs.get("size_limit", D(DX_MAX_VALUE))
        max_price = kwargs.get("max_price", D(DX_MAX_VALUE))
        cost_limit = kwargs.get("cost_limit", D(DX_MAX_VALUE))
        vol_floor = kwargs.get("size_floor", 0)
        top_ask_only = kwargs.get("top_ask_only", False)
        sort = kwargs.get("sort", "desc")
        VERBOSE = kwargs.get("VERBOSE", True)

        asks = orderbook.get(bidAsk, [])

        if asks == []:
            return []

        top_ask_price = asks[0][0]
        filtered_asks = []
        for ask in asks:
            if lib_utils.isNumber(float(max_price)):
                if float(ask[0]) > float(max_price):
                    continue
            if lib_utils.isNumber(float(vol_limit)):
                if float(ask[1]) > float(vol_limit):
                    continue
            if lib_utils.isNumber(float(vol_floor)):
                if float(ask[1]) < float(vol_floor):
                    continue
            if lib_utils.isNumber(cost_limit):
                if float(ask[0]) * float(ask[1]) > float(cost_limit):
                    continue
            if top_ask_only and float(ask[0]) != float(top_ask_price):
                continue
            filtered_asks.append(ask)
        if sort == "desc":
            filtered_asks = sorted(filtered_asks, key=lambda x: float(x[0]), reverse=True)
        if sort == "asc":
            filtered_asks = sorted(filtered_asks, key=lambda x: float(x[0]), reverse=False)
        if VERBOSE:
            pp.pprint("[getAsks] params: %s" % str(kwargs))
        return filtered_asks

    def getBids(self, **kwargs):
        return self.getBidsAsks(bidAsk="bids", **kwargs)

    def getAsks(self, **kwargs):
        return self.getBidsAsks(bidAsk="asks", **kwargs)

    @rpc
    def dxGetMyOrders(self, **kwargs):
        maker = kwargs.get("maker", None)
        taker = kwargs.get("taker", None)
        filter_str = kwargs.get("filter_str", "")

        rst = self.connector.dxGetMyOrders()

        if rst == [] or not isinstance(rst, list):
            return []

        rst = [x for x in rst if x.get("status", "") == filter_str]

        if maker is not None:
            rst = [x for x in rst if x.get("maker", "") == maker]

        if taker is not None:
            rst = [x for x in rst if x.get("taker", "") == taker]

        if rst is None:
            rst = []

        return rst

    @rpc
    def getOpenOrders(self, **kwargs):
        """ Returns the traders' open orders for an optional specific pair. With filtering capabilities.

        Usage examples:
            - getOpenOrders(maker="BTC", taker="LTC")
            - getOpenOrders(maker="BTC")
            - getOpenOrders(taker="LTC")
            - getOpenOrders()
        """
        return self.dxGetMyOrders(filter_str="open", **kwargs)

    @rpc
    def getClosedOrders(self, **kwargs):
        """
            dxGetOrderFills [maker] [taker] [combined](optional)
        """
        return self.dxGetMyOrders(filter_str="finished", **kwargs)

    @rpc
    def getLocalTokens(self):
        """ Return the locally set up coins """
        return self.connector.dxGetLocalTokens()
    
    @rpc
    def getNetworkTokens(self):
        """ Return the network supported coins """
        rst = self.connector.dxGetNetworkTokens()
        return rst


class Coin(object):
    def __init__(self, **kwargs):
        self.name = kwargs.get("name", None)
        self.title = kwargs.get("title", None)

        """ Coin technical data """
        self.blockTime = kwargs.get("blocktime", None)
        self.dustAmount = kwargs.get("dustamount", None)
        self.feePerByte = kwargs.get("feeperbyte", None)

        """ CoinConnector object """
        self.rpcIP = kwargs.get("ip", None)
        self.rpcPort = kwargs.get("port", None)
        self.rpcUsername = kwargs.get("rpcuser", None)
        self.rpcPass = kwargs.get("rpcpass", None)
        self.connector = RPConnector(ip=self.rpcIP, port=self.rpcPort, rpcuser=self.rpcUsername, rpcpass=self.rpcPass)

    def getDustAmount(self):
        return self.connector.getDustAmount()

    def relock(self):
        return self.connector.relock()

    def unlock(self, pwd):
        return self.connector.unlock(pwd)

    def getWalletBalance(self):
        rst = self.connector.getWalletBalance()
        if isinstance(rst, Error):
            return 0
        return rst

    def getBalance(self, address=None):
        if address is None:
            return self.getWalletBalance()
        rst = self.connector.getBalance(address)
        if isinstance(rst, Error):
            return 0
        return rst

    def getInputs(self, **kwargs):
        """ Returns the list of inputs

        Possible filters:
            - ignoreDust
            - spendableOnly
        """
        inputLst = self.connector.getInputs()
        if isinstance(inputLst, Error):
            return []
        if kwargs is None:
            return inputLst
        ignoreDust = kwargs.get("ignoreDust", False)
        if ignoreDust:
            dust = self.getDustAmount()
            finalInputLst = [x for x in inputLst if x.get("amount", 0) > dust]
        spendableOnly = kwargs.get("spendableOnly", True)
        if spendableOnly:
            finalInputLst = [x for x in inputLst if x.get("spendable", 0) is True]
        return finalInputLst

    def getTransaction(self, txid):
        return self.connector.getTransaction(txid)

    def getAddressList(self, **kwargs):
        ignoreEmptyAddresses = kwargs.get("ignoreEmptyAddresses", False)
        addressList = self.connector.getAddressList()

        if kwargs is None or ignoreEmptyAddresses is False:
            return addressList

        tmp = []
        dust = self.getDustAmount()
        for address in addressList:
            if address[1] > dust:
                tmp.append(address)

        return tmp

    def getAddressForSize(self, size):
        return self.connector.getAddressForSize(size)

    def getTopAddress(self):
        return self.connector.getTopAddress()

    def validateAddress(self, address: str):
        return self.connector.validateAddress(address)

    def isWalletUnlocked(self):
        return self.connector.isWalletUnlocked()

    def isConnectionAlive(self):
        return self.connector.isConnectionAlive()

    def isSynced(self):
        return self.connector.isSynced()

    def __str__(self):
        repr_str = "Coin=[%s]\n" % (self.name)
        return repr_str

    def __repr__(self):
        repr_str = "Coin=[%s]\n" % (self.name)
        return repr_str


class BlockConfParser(object):
    def __init__(self, confPath="", **kwargs):
        self.status = None
        self.config = configparser.ConfigParser()
        self.result = {}
        self.coins = []
        self.coinList = []
        self.confFilePath = confPath
        self.ip = kwargs.get("ip", None)
        if not os.path.exists(self.confFilePath):
            xprint("[BlockConfParser] xbridge.conf not found: %s" % self.confFilePath)
        else:
            self.read()
            self.processAll()

    def __str__(self):
        log_str = "--------- BlockConfParser ---------\n"
        log_str += "\t dict(self.config.items()): %s \n" % repr(dict(self.config.items()))
        log_str += "\t self.result: %s\n" % self.result
        log_str += "\t self.coinList: %s\n" % self.coinList
        log_str += "----------------------"
        return log_str

    def read(self):
        try:
            self.config.read(self.confFilePath)
            self.result = dict(self.config.items())
        except configparser.DuplicateOptionError as ex:
            print("[BlockConfParser] configparser.DuplicateOptionError: %s" % ex)

        self.coinList = [x for x in self.config.sections() if x not in ("Main", "RPC")]

        if self.coinList == [] or self.coinList is None:
            print("[BlockConfParser] No wallet info found in the conf file")
            exit(1)

    def processAll(self):
        for coin in self.coinList:
            if coin[:1] == "t":
                continue
            coin_rst = self.process(coin)
            if coin_rst is None:
                return
            self.coins.append(coin_rst)

    def process(self, coin):
        try:
            wallet_obj = dict(self.config.items(coin))
            if wallet_obj is None:
                xprint("[connect_to_wallets] Duplicate config information found for %s wallet in %s" % (
                    coin, self.confFilePath))
            if any(x in (None, "") for x in
                   [wallet_obj.get("username", ""), wallet_obj.get("password", ""), wallet_obj.get("port", "")]):
                xprint("[connect_to_wallets] Required key missing in xbridge.conf file for %s wallet: %s" % (
                    coin, self.confFilePath))
            if int(wallet_obj.get("port", -1)) < 0 or int(wallet_obj.get("port", -1)) > 65535:
                xprint("Invalid port (%s) for %s wallet in %s" % (
                    wallet_obj.get("port", -1), coin, self.confFilePath))
                Error("Invalid port (%s) for %s wallet in %s" % (
                    wallet_obj.get("port", -1), coin, self.confFilePath))
            if self.ip is not None:
                retainedIp = self.ip
            else:
                retainedIp = wallet_obj.get("ip", -1)
            newCoin = Coin(ip=retainedIp,
                           port=wallet_obj.get("port", -1),
                           name=coin,
                           title=wallet_obj.get("title", -1),
                           rpcuser=wallet_obj.get("username", ""),
                           rpcpass=wallet_obj.get("password", ""),
                           dustamount=wallet_obj.get("dustamount", -1),
                           blocktime=wallet_obj.get("blocktime", -1),
                           feeperbyte=wallet_obj.get("feeperbyte", -1)
                           )
            return newCoin
        except configparser.NoSectionError:
            print("NoSectionError")


class BlockDX(object):
    def __init__(self, **kwargs):
        self.allowOrderSplit = True
        # self.ip = kwargs.get("ip", "127.0.0.1")
        self.ip = kwargs.get("ip")
        self.port = kwargs.get("port")
        self.rpcUsername = kwargs.get("rpcuser", None)
        self.rpcPass = kwargs.get("rpcpass", None)
        self.connector = BlockConnector(ip=self.ip, port=self.port, rpcuser=self.rpcUsername,
                                        rpcpass=self.rpcPass)

        """ CoinConnector object """
        self.confFilePath = kwargs.get("conf", None)
        self.coins = []

        self.connectedCoins = []
        self.tradeableCoins = []
        self.failedRpcConnections = []

        if self.confFilePath is not None:
            self.blockConfParserObj = BlockConfParser(self.confFilePath, ip=self.ip)
            self.coins = self.blockConfParserObj.coins
            self.connectedCoins = self.getConnectedTokens()

        """ User-defined """
        self.setBlacklistedCoins = []
        self.setWhiteListedCoins = []

        """ Orders """
        self.postedOrders = []
        self.acceptedOrders = []
        self.setLogLevel = 1

    def __str__(self):
        log_str = "--------- DX --------- \n"
        log_str += "\t IP: %s\n" % self.ip
        log_str += "\t port: %s\n" % self.port
        log_str += "\t confFilePath: %s\n" % self.confFilePath
        for coin in self.coins:
            log_str += "\t%s" % repr(coin)
        log_str += "----------------------"
        return log_str

    def getConnectedTokens(self):
        """ Returns the list of wallets connected to the DX

            No expected parameters
         """
        if len(self.connectedCoins) > 0:
            return self.connectedCoins
        rst = self.dxGetTokenBalances()
        rst = list(rst.keys())
        rst = [x for x in rst if x != "Wallet"]
        self.connectedCoins = rst
        return rst

    def getTradeableTokens(self):
        """ Returns the list of coins with a balance that is non dust

            No expected parameters
         """
        tmp = []
        rst = self.dxGetTokenBalances()
        rst = list(rst.keys())
        rst = [x for x in rst if x != "Wallet"]
        for x in rst:
            coinBal = self.getWalletBalance(x)
            dustAmount = self.getDustAmount(x)
            if isinstance(coinBal, Error) or isinstance(dustAmount, Error):
                print("[getTradeableCoins] Error - coinBal => %s" % coinBal)
                print("[getTradeableCoins] Error - dustAmount => %s" % dustAmount)
                continue
            if coinBal > dustAmount:
                tmp.append(x)
        return tmp

    def getPostedOrders(self):
        """ Returns the sent orders by the trading library to the DX

            No expected parameters
         """
        print("\n---------- POSTED ORDERS ----------\n")
        for postedOrder in self.postedOrders:
            print(str(postedOrder))

    def dxGetTokenBalances(self):
        """ Returns the balances of the coins connected to the DX

         No expected parameters.
         """
        return self.connector.dxGetTokenBalances()

    def getCoin(self, name: str):
        """ Returns the Coin object to a specific wallet

         Parameters:
             - coin [required: string]
         """
        for coin in self.coins:
            if coin.name == name:
                return coin
        return None

    def getDustAmount(self, coin):
        """ Returns the dust amount related to a specific coin

         Parameters:
             - coin [required]
         """
        coinObj = self.getCoin(coin)
        if coinObj is None:
            return Error("%s coin not found" % coin)
        return coinObj.getDustAmount()

    def getWalletBalance(self, coin):
        """ Returns the wallet total balance related to a specific coin

         Parameters:
             - coin [required]
         """
        coinObj = self.getCoin(coin)
        if coinObj is None:
            return 0
        return coinObj.getWalletBalance()

    def getAddressList(self, coin, **kwargs):
        """ Returns the list of all addresses from the specified wallet

         Parameters:
             - coin [required]
             - ignoreEmptyAddresses=True/False [optional]
         """
        coinObj = self.getCoin(coin)
        if coinObj is None:
            return Error("%s coin not found" % coin)
        return coinObj.getAddressList(**kwargs)

    def getTopAddress(self, coin):
        """ Returns the address with the highest balance from a specific wallet

         Parameters:
             - coin [required]
         """
        coinObj = self.getCoin(coin)
        if coinObj is None:
            return Error("%s coin not found" % coin)
        return coinObj.getTopAddress()

    def getAddressForSize(self, coin, size):
        """ Returns the address with a balance that contains at least the 'size' parameter

         Parameters:
             - coin [required]
             - size [required]
         """
        coinObj = self.getCoin(coin)
        if coinObj is None:
            return Error("%s coin not found" % coin)
        return coinObj.getAddressForSize(size)

    def getBalance(self, coin, address=None):
        """ Returns the address of the specified address for a specific coin

         Parameters:
             - coin [required]
             - address [optional]: if set, returns the balance only of the set address
         """
        if address is None:
            return self.getWalletBalance(coin)
        coinObj = self.getCoin(coin)
        if coinObj is None:
            return Error("%s coin not found" % coin)
        return coinObj.getBalance(address)

    def validateAddress(self, coin: str, address: str):
        """ Verify that an address is valid

         Parameters:
             - coin [required]
             - address [required]
         """
        coinObj = self.getCoin(coin)
        if coinObj is None:
            return Error("%s coin not found" % coin)
        return coinObj.validateAddress(address)

    def isWalletUnlocked(self, coin: str):
        """ Returns a boolean indicating whether a wallet is unlocked

         Parameters:
             - coin [required]
         """
        coinObj = self.getCoin(coin)
        if coinObj is None:
            return Error("%s coin not found" % coin)
        return coinObj.isWalletUnlocked()

    def isSynced(self, coin: str):
        """ Returns a boolean indicating whether a wallet is synced

         Parameters:
             - coin [required]
         """
        coinObj = self.getCoin(coin)
        if coinObj is None:
            return Error("%s coin not found" % coin)
        return coinObj.isSynced()

    def getFeePerByte(self, coin):
        """ Returns the feePerByte specified in the *xbridge.conf* file for the specified coin.

         Parameters:
             - coin [required]
         """
        coinObj = self.getCoin(coin)
        if coinObj is None:
            return Error("%s coin not found" % coin)
        return coinObj.feePerByte

    def getTransaction(self, coin, txid):
        """ Returns a JSON object of the details of a transaction from the specified wallet.

        Parameters:
            - coin [required: string]
            - txid [required: string]
         """
        coinObj = self.getCoin(coin)
        if coinObj is None:
            return Error("%s coin not found" % coin)
        return coinObj.getTransaction(txid)

    def getInputs(self, coin, **kwargs):
        """ Returns the list of all inputs from the specified wallet

         Parameters:
             - coin [required]
             - ignoreDust=True/False [optional]: filter out dust inputs
             - spendable=True/False [optional]: filter out non spendable inputs
         """
        coinObj = self.getCoin(coin)
        if coinObj is None:
            return Error("%s coin not found" % coin)
        return coinObj.getInputs(**kwargs)

    def getPrice(self, coin: str, base: str):
        orderbook = self.getOrderbook(maker=coin, taker=base)
        rst = -1
        bid = None
        ask = None

        if orderbook.get("bids", None) not in ([], None):
            bid = orderbook["bids"][0][0]

        if orderbook.get("asks", None) not in ([], None):
            ask = orderbook["asks"][0][0]

        if bid is None and ask is None:
            return rst

        if bid is not None and ask is not None:
            return (bid + ask) / 2

        if bid is not None and ask is None:
            return bid

        if bid is None and ask is not None:
            return ask


    def getOrderbook(self, **kwargs):
        """ Returns a detailed view the order book data with the txids

                    Parameters:
                        - maker [required]
                        - taker [required]
                        - detailled=True/False [optional, by default = True]

                    Usage example:
                        - DX.getOrderBook(maker="LTC", taker="SYS")
                        - DX.getOrderBook(maker="LTC", taker="SYS", detailled=False)

                    Sample returned data:
                        { 'asks': [
                                    [price, size, 'txid'],
                                    [0.6, 2, 'ask_id_2'],
                                    ...
                                   ],
                        'bids': [
                                    [price, size, 'txid'],
                                    [0.6, 2, 'ask_id_2'],
                                    ...
                                   ]
                        }
        """

        return self.connector.getOrderBook(**kwargs)

    def getAsks(self, **kwargs):
        """ Returns a detailed view of the asks.
            Filtering is enabled via the optional parameters

            Parameters:
                - maker [required]
                - taker [required]
                - max_price=X [optional, numeric]
                - size_limit=X [optional, numeric]
                - size_floor=X [optional, numeric]
                - cost_limit=X [optional, numeric]
                - sort="asc"/"desc" [optional, by default "desc"]

                Sample returned data:
                [
                    [price, size, txid],
                    [0.6, 4, 'ask_id_1'],
                    ...
                ]

                Usage Examples:
                    - getAsks("BTC", "LTC")
                    - getAsks("BTC", "LTC", max_price=5, vol_limit=1)
                    - getAsks("BTC", "LTC", sort="desc")

        """
        return self.connector.getAsks(**kwargs)

    def getBids(self, **kwargs):
        """ Returns a detailed view of the bids.
            Filtering is enabled via the optional parameters

            Parameters:
                - maker [required]
                - taker [required]
                - max_price=X [optional, numeric]
                - size_limit=X [optional, numeric]
                - size_floor=X [optional, numeric]
                - cost_limit=X [optional, numeric]
                - sort="asc"/"desc" [optional, by default "desc"]

                Sample returned data:
                [
                    [price, size, txid],
                    [0.6, 4, 'bid_id_1'],
                    ...
                ]

                Usage Examples:
                    - getBids("BTC", "LTC")
                    - getBids("BTC", "LTC", max_price=5, size_limit=1)
                    - getBids("BTC", "LTC", sort="desc")

        """
        return self.connector.getBids(**kwargs)

    def getOpenOrders(self, **kwargs):
        """ Returns the list of open orders

         Parameters:
            - maker [optional]
            - taker [optional]
         """
        VERBOSE = kwargs.get("VERBOSE", False)
        if VERBOSE:
            print("[getOpenOrders] params: %s" % str(kwargs))
        return self.connector.getOpenOrders(**kwargs)

    def getClosedOrders(self, **kwargs):
        """ Returns the list of finished orders

         Parameters:
            - maker [optional]
            - taker [optional]
         """
        VERBOSE = kwargs.get("VERBOSE", False)
        if VERBOSE:
            print("[getClosedOrders] params: %s" % str(kwargs))
        return self.connector.getClosedOrders(**kwargs)

    def getLocalTokens(self):
        """ Returns the list of locally supported wallets

            No expected parameter(s)
         """
        return self.connector.getLocalTokens()

    def getNetworkTokens(self):
        """ Returns the list of supported coins on the network

            No expected parameter(s)
         """
        return self.connector.getNetworkTokens()

    def cancelId(self, txid: str):
        """ Cancel an order with the supplied txid

            Parameter:
                - txid [required: string]
         """
        return self.connector.cancelId(txid)

    def getInfo(self, txid: str):
        """ Returns information about the provided txid

            Parameter:
                - txid [required]
         """
        return self.connector.getInfo(txid)

    def cancelAll(self, VERBOSE=False):
        """ Cancel all open orders and returns the list of cancelled orders

            No expected parameter(s)
         """
        cancelled = []
        failures = []
        rst = {"cancelled": [], "failures": []}
        openOrders = self.getOpenOrders()
        for openOrder in openOrders:
            cancelRst = self.connector.cancelId(openOrder.get("id", ""))
            if isinstance(cancelRst, Error):
                failures.append(str(cancelRst))
                if VERBOSE:
                    print("[cancelAll] Error: %s" % str(cancelRst))
            cancelled.append(cancelRst)
        rst["cancelled"] = cancelled
        rst["failures"] = failures
        if VERBOSE:
            print("[] cancelled %s orders" % len(cancelled))
        return rst

    def cancelOrders(self, **kwargs):
        """ Cancel orders with some filtering capabilities

            Parameters:
                - maker [optional: string]
                - taker [optional: string]

            Usage examples:
                - DX.cancelOrders(maker="BTC")
                - DX.cancelOrders(taker="BTC")
                - DX.cancelOrders(maker="BTC", taker="LTC")
        """
        maker = kwargs.get("maker", None)
        taker = kwargs.get("taker", None)
        VERBOSE = kwargs.get("VERBOSE", False)

        if VERBOSE:
            print("[cancelOrders] params: %s" % str(kwargs))

        if maker is None and taker is None:
            self.cancelAll()

        openOrders = self.getOpenOrders(**kwargs)
        cancelledList = []
        if len(openOrders) == 0:
            return cancelledList

        filter_str = ("open", "Open")

        if len(openOrders) > 0 and maker is not None:
            openOrders = [d for d in openOrders if d['status'] in filter_str and d['maker'] == maker]

        if len(openOrders) > 0 and taker is not None:
            openOrders = [d for d in openOrders if d['status'] in filter_str and d['maker'] == taker]

        if len(openOrders) == 0:
            return cancelledList

        for openOrder in openOrders:
            cancelRst = self.cancelId(openOrder.get("id", ""))
            if isinstance(cancelRst, Error):
                print(str(cancelRst))
            cancelledList.append(cancelRst)

        return cancelledList

    def fillAddresses(self, **kwargs):
        maker_address = kwargs.get("maker_address", None)
        taker_address = kwargs.get("taker_address", None)

        if maker_address is None:
            maker_address = self.getTopAddress(kwargs.get("maker", None))

        if taker_address is None:
            taker_address = self.getTopAddress(kwargs.get("taker", None))

        return maker_address, taker_address

    def dxMakeOrder(self, **kwargs):
        """ Posts an order to the DX without trying to match any order from the orderbook

            Parameters:
                - maker [required: string]
                - taker [required: string]
                - maker_size [required: numeric]
                - taker_size [required: numeric]
                - maker_address [optional: string]
                - taker_address [optional: string]

            Usage Example:
                - DX.dxMakeOrder(maker="LTC", maker_size=1, taker="SYS", taker_size=10)
        """
        maker_address, taker_address = self.fillAddresses(**kwargs)
        dxMakeOrderRst = self.connector.dxMakeOrder(txid=kwargs.get("txid", None),
                                          maker=kwargs.get("maker", None),
                                          taker=kwargs.get("taker", None),
                                          maker_size=kwargs.get("maker_size", None),
                                          taker_size=kwargs.get("taker_size", None),
                                          maker_address=maker_address,
                                          taker_address=taker_address)
        time.sleep(1/4)
        return dxMakeOrderRst

    def dxTakeOrder(self, **kwargs):
        """
            Parameters:
                - txid [required: string]
                - maker_address [optional: string]
                - taker_address [optional: string]
        """
        maker_address, taker_address = self.fillAddresses(**kwargs)
        dryrun = kwargs.get("dryrun", False)
        if dryrun:
            return self.connector.dxTakeOrder(txid=kwargs.get("txid", ""), maker_address=maker_address, taker_address=taker_address, dryrun=dryrun)
            # return {"txid": "0000"}
        else:
            dxTakeOrderRst = self.connector.dxTakeOrder(txid=kwargs.get("txid", ""), maker_address=maker_address, taker_address=taker_address)
            time.sleep(1/3)
            return dxTakeOrderRst

    def postLimitOrder(self, limitOrder: LimitOrder):
        """ Posts a Limit Order without looking at the orderbook without trying to match it.

            Parameter:
                - LimitOrder [required]

            Returns:
                 - a JSON object in case of success.
                 - an Error object in case of failure.

            The function checks that the balance is sufficient before posting.
         """
        assert limitOrder.maker_size > 0, print(limitOrder)
        assert limitOrder.taker_size > 0, print(limitOrder)

        walletBalance = self.getWalletBalance(limitOrder.maker)

        if isinstance(walletBalance, Error):
            return Error("[postLimitOrder] Balance check error: %s" % (walletBalance))

        if walletBalance < float(limitOrder.maker_size):
            return Error("[postLimitOrder] Insufficient balance: %s vs maker_size=%s" % (walletBalance, limitOrder.maker))

        makeOrderRst = self.dxMakeOrder(maker=limitOrder.maker, maker_size=limitOrder.maker_size,
                                taker=limitOrder.taker, taker_size=limitOrder.taker_size)

        if isinstance(makeOrderRst, Error):
            limitOrder.results.errors.append(makeOrderRst)
            return makeOrderRst

        limitOrder.results.posted.append(makeOrderRst)
        self.postedOrders.append(limitOrder)
        return limitOrder

    def fillLimitOrder(self, limitOrder: LimitOrder):
        matchable = 0
        remaining = round(float(limitOrder.taker_size), 6)
        limitOrderMinPrice = round(float(limitOrder.taker_size) / float(limitOrder.maker_size), 6)

        time.sleep(1 / 3)
        """ Example: 1 LTC => 2 SYS """
        """ taker is the base currency, so get the bids in SYS """
        openOrders = self.getBids(maker=limitOrder.maker, taker=limitOrder.taker)

        if limitOrder.VERBOSE:
            print("[fillLimitOrder] initial taker_size: %s" % remaining)
            print("[fillLimitOrder] getBids %s %s: %s" % (limitOrder.maker, limitOrder.taker, openOrders))

        walletBalance = -1
        for openOrder in openOrders:
            """ 1. check if price (bids of taker in taker coin) is enough """
            """ openOrder[0] is the nb of offered of taker for one unit of maker """
            if round(float(openOrder[0]), 6) < limitOrderMinPrice:
                if limitOrder.VERBOSE:
                    print("[fillLimitOrder] skipping insufficient bid: orderbook=%s vs requested=%s" % (
                    openOrder[0], round(limitOrder.taker_size / limitOrder.maker_size, 6)))
                continue

            """ 2. check if quantity is enough """
            """ compare limitOrder.maker_size with openOrder[1] = taker_size for counterparty """
            """ openOrder[1] = size in maker=LTC """
            if round(float(openOrder[0]) * float(openOrder[1]), 6) > remaining:
                print("[fillLimitOrder] skipping too large bid: %s vs remaining=%s" % (
                round(float(openOrder[0]) * float(openOrder[1]), 6), remaining))
                continue

            if -1 < walletBalance < float(openOrder[1]):
                if limitOrder.VERBOSE:
                    print("[fillLimitOrder] Insufficient balance: %s - order book size: %s" % (walletBalance, openOrder[1]))
                continue

            matchable += 1
            print("[fillLimitOrder] matchable => bid: %s" % str(openOrder[0]))
            take_order_rst = self.dxTakeOrder(txid=openOrder[2], maker=limitOrder.maker, taker=limitOrder.taker,
                                              dryrun=limitOrder.dryrun)

            if isinstance(take_order_rst, Error):
                limitOrder.errors.append(take_order_rst)
                if limitOrder.VERBOSE:
                    print("[fillLimitOrder] [Error]: %s" % str(take_order_rst))
                continue

            limitOrder.accepted.append(take_order_rst)
            time.sleep(1/2)
            walletBalance = self.getWalletBalance(limitOrder.maker)

            remaining -= round(float(openOrder[0]) * float(openOrder[1]), 6)
            if remaining <= 0:
                break

            remaining = min(remaining, walletBalance)

            if limitOrder.VERBOSE:
                print("[fillLimitOrder] %s matched orders" % matchable)
                print("[fillLimitOrder] remaining unmatched qty : %s" % remaining)

            if remaining <= 0:
                return limitOrder

        newPostedOrder = limitOrder
        newPostedOrder.taker_size = round(remaining, 6)
        newPostedOrder.maker_size = remaining * round(limitOrder.maker_size, 6) / round(limitOrder.taker_size, 6)
        newPostedOrder = self.postLimitOrder(newPostedOrder)
        limitOrder.posted.append(newPostedOrder)
        # self.postedOrders.append(limitOrder)
        return limitOrder

    """ Fill the orders, whatever the price, until the requested taker size is reached """
    def executeBuyMarketOrder(self, buyMarketOrder: MarketOrder):
        remaining = buyMarketOrder.taker_size
        matchable = 0

        openOrders = self.getBids(maker=buyMarketOrder.maker, taker=buyMarketOrder.taker)
        if buyMarketOrder.VERBOSE:
            print("[executeBuyMarketOrder] initial taker_size: %s" % remaining)
            print("[executeBuyMarketOrder] getAsks %s %s: %s" % (buyMarketOrder.maker, buyMarketOrder.taker, openOrders))

        walletBalance = -1
        for openOrder in openOrders:
            """ 1. Order size check """
            """ compare limitOrder.maker_size with openOrder[1] = taker_size for counterparty """
            """ openOrder[1] = size in maker=LTC """
            if round(float(openOrder[0]) * float(openOrder[1]), 6) > remaining:
                print("[executeBuyMarketOrder] skipping too large order: %s vs remaining=%s" % (
                    round(float(openOrder[0]) * float(openOrder[1]), 6), remaining))
                continue

            """ 2. Balance check """
            if -1 < walletBalance < round(float(openOrder[1]), 6):
                if buyMarketOrder.VERBOSE:
                    print("[executeBuyMarketOrder] Insufficient balance - balance: %s - cost: %s" % (walletBalance, openOrder[1]))
                continue

            matchable += 1
            if buyMarketOrder.VERBOSE:
                print("[executeBuyMarketOrder] matchable bid: %s" % str(openOrder[0]))

            take_order_rst = self.dxTakeOrder(txid=openOrder[2],
                                              maker=buyMarketOrder.maker,
                                              taker=buyMarketOrder.taker,
                                              dryrun=buyMarketOrder.dryrun)

            if isinstance(take_order_rst, Error):
                if buyMarketOrder.VERBOSE:
                    print("[executeBuyMarketOrder] Error: %s" % str(take_order_rst))
                buyMarketOrder.errors.append(take_order_rst)
                continue

            buyMarketOrder.accepted.append(take_order_rst)
            time.sleep(1/3)
            walletBalance = self.getWalletBalance(buyMarketOrder.maker)
            remaining -= round(float(openOrder[0]) * float(openOrder[1]), 6)

            if buyMarketOrder.VERBOSE:
                print("[executeBuyMarketOrder] updated remaining taker_size: %s" % (remaining))

            if remaining <= 0:
                break

        if buyMarketOrder.VERBOSE:
            print("[executeBuyMarketOrder] %s matchable orders" % matchable)
            print("[executeBuyMarketOrder] unmatched taker_size: %s" % remaining)
            print("[executeBuyMarketOrder] ------ ")

        return buyMarketOrder

    def executeSellMarketOrder(self, sellMarketOrder: MarketOrder):
        remaining = sellMarketOrder.maker_size
        matched = 0

        """ Inverted """
        openOrders = self.getAsks(maker=sellMarketOrder.taker, taker=sellMarketOrder.maker)

        if sellMarketOrder.VERBOSE:
            print("[executeSellMarketOrder] initial maker_size: %s" % remaining)
            print("[executeSellMarketOrder] getAsks %s %s: %s" % (sellMarketOrder.taker, sellMarketOrder.maker, openOrders))

        walletBalance = -1
        for openOrder in openOrders:

            makerCost = round(float(openOrder[0]) * float(openOrder[1]), 6)

            if makerCost > remaining:
                print("[executeSellMarketOrder] skipping - orderbook maker cost: %s > remaining: %s" % (makerCost, remaining))
                continue

            if -1 < walletBalance < makerCost:
                if sellMarketOrder.VERBOSE:
                    print("[executeSellMarketOrder] Insufficient balance - balance: %s - cost: %s" % (
                            walletBalance, makerCost))
                continue

            matched += 1
            print("[executeSellMarketOrder] matchable => bid: %s" % str(openOrder[0]))

            take_order_rst = self.dxTakeOrder(txid=openOrder[2],
                                              maker=sellMarketOrder.maker,
                                              taker=sellMarketOrder.taker,
                                              dryrun=sellMarketOrder.dryrun)

            if isinstance(take_order_rst, Error):
                if sellMarketOrder.VERBOSE:
                    print("[executeSellMarketOrder] Error: %s" % str(take_order_rst))
                sellMarketOrder.accepted.append(take_order_rst)
                continue

            sellMarketOrder.accepted.append(take_order_rst)
            time.sleep(1/3)
            walletBalance = self.getWalletBalance(sellMarketOrder.maker)

            remaining -= round(float(openOrder[0]) * float(openOrder[1]), 6)

            if not isinstance(walletBalance, Error):
                remaining = min(remaining, walletBalance)

            if sellMarketOrder.VERBOSE:
                print("[executeSellMarketOrder] updated remaining maker_size: %s" % (remaining))

            if remaining <= 0:
                break

        if sellMarketOrder.VERBOSE:
            print("[executeSellMarketOrder] %s matched orders" % matched)
            print("[executeSellMarketOrder] unmatched maker_size: %s" % round(remaining, 6))

        return sellMarketOrder

    def executeMarketOrder(self, marketOrder):
        if marketOrder.maker_size is not None and marketOrder.taker_size is not None:
            return Error("LimitOrder")
        if marketOrder.maker_size is None and marketOrder.taker_size is not None:
            return self.executeBuyMarketOrder(marketOrder)
        if marketOrder.maker_size is not None and marketOrder.taker_size is None:
            return self.executeSellMarketOrder(marketOrder)

    def verifyCoins(self, order):
        """ Internal function """
        if order.maker not in self.getConnectedCoins():
            return Error("[verifyCoins] Invalid maker coin: %s" % order.maker)
        if order.taker not in self.getConnectedCoins():
            return Error("[verifyCoins] Invalid taker coin: %s" % order.taker)
        return True

    def executeMarketOrderPrecheck(self, order):
        """ Internal function """
        verifyCoinsRst = self.verifyCoins(order)
        if isinstance(verifyCoinsRst, Error):
            return verifyCoinsRst
        if order.maker_size is not None:
            makerBalance = self.getWalletBalance(order.maker)
            if isinstance(makerBalance, Error):
                return makerBalance
            if makerBalance < order.maker_size:
                return Error("[execute] Insufficient balance")
        return True

    def executeLimitOrderPrecheck(self, order):
        verifyCoinsRst = self.verifyCoins(order)
        if isinstance(verifyCoinsRst, Error):
            return verifyCoinsRst
        if order.maker_size is None:
            return Error("[execute] Invalid maker size parameter")
        makerBalance = self.getWalletBalance(order.maker)
        if isinstance(makerBalance, Error):
            return makerBalance
        if makerBalance < order.maker_size:
            return Error("[execute] Insufficient balance")
        takerAddress = self.getTopAddress(order.taker)
        if isinstance(takerAddress, Error):
            return takerAddress
        return True

    def executePrecheck(self, order, VERBOSE=True):
        if VERBOSE:
            print("[executePrecheck] params: %s" % str(order))
        if isinstance(order, MarketOrder):
            return self.executeMarketOrderPrecheck(order)
        if isinstance(order, LimitOrder):
            return self.executeLimitOrderPrecheck(order)

    def execute(self, order):
        """ Executes a Limit or a Market Order

            Parameters:
                - LimitOrder/MarketOrder [required]

            Usage examples:
                limitOrder1 = LimitOrder(maker_size=1, maker="LTC", taker_size=2, taker="SYS")
                limitOrder2 = LimitOrder(price=0.5, maker="LTC", taker_size=2, taker="SYS")
                marketOrder1 = MarketOrder(maker="LTC", taker_size=2, taker="SYS")
                marketOrder2 = MarketOrder(maker="LTC", maker_size=0.5, taker="SYS")
                DX.execute(limitOrder1)
                DX.execute(limitOrder2)
                DX.execute(marketOrder1)
                DX.execute(marketOrder2)
        """
        precheckRst = self.executePrecheck(order)
        if isinstance(precheckRst, Error):
            return precheckRst
        if isinstance(order, LimitOrder):
            return self.fillLimitOrder(order)
        if isinstance(order, MarketOrder):
            return self.executeMarketOrder(order)

    def unlock(self, coin: str, pwd: str):
        """ Unlock a wallet with the provided pass

            Parameters:
                - coin [required: string]
                - pwd [required: string]
        """
        coinObj = self.getCoin(coin)
        if coinObj is None:
            return Error("%s coin not found" % coin)
        return coinObj.unlock(pwd)

    def relock(self, coin: str):
        """ Relock a wallet

            Parameters:
                - coin [required: string]
        """
        coinObj = self.getCoin(coin)
        if coinObj is None:
            return Error("%s coin not found" % coin)
        return coinObj.relock()

    def unlockAll(self, pwd: str):
        """ Unlock all wallets with the provided pass

            Parameters:
                - pwd [required: string]
         """
        if not isinstance(pwd, str):
            return Error("[unlockAll] Invalid parameter type: %s instead of string" % type(pwd))
        for coin in self.coins:
            coin.connector.unlock(pwd)

    def relockAll(self):
        """ Relock all wallets

            No expected parameters
        """
        for coin in self.coins:
            coin.connector.relock()