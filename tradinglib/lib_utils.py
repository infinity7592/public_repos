import time, dateutil, datetime


class Error(object):
    def __init__(self, msg):
        self.text = msg

    def __str__(self):
        # print("Error - %s" % self.text)
        return self.text


def toBtc(sat):
    """ Converts satoshis to btc """
    return (sat / 100000000)


def toSat(btc):
    """ Converts btc to satoshis """
    return (btc * 100000000)


def isAlphanumeric(str_param):
    """ Checks whether a variable is made only of alphanumeric items.

    - Required parameters:
        random variable

    - Optional parameters:
        (None)

    - Returned data:
        True if the supplied variable is made only of alphanumeric items. False, otherwise.
    """
    temp = all(i.isalnum() for i in str_param)
    return temp


def isNumber(s):
    """ Checks that a specific variable is a number

    - Required parameters:
        Number

    - Optional parameters:
        (None)

    - Returned data:
        True if a specific variable is a number. False, otherwise.
    """
    try:
        complex(s)
        return True
    except ValueError:
        return False
    except TypeError:
        return False


def getDatetimeFromIso8601(iso8601_date):
    """ Converts an ISO 8601 date to a Python datetime object """
    datetime_obj = dateutil.parser.parse(iso8601_date)
    assert isinstance(datetime_obj, datetime) is True
    return datetime_obj


def IsodateToUnix(d):
    """ Converts an ISO 8601 date to a unix timestamp

    - Required parameters:
        ISO 8601 date

    - Optional parameters:
        (None)

    - Returned data:
        Unix timestamp

    - Example:
        d = datetime.date(2015,1,5)
        print(convert_isodate_to_unix(d))
    """
    unixtime = time.mktime(d.timetuple())
    return int(unixtime)


def IsodateStrToUnix(d):
    """ Converts an ISO 8601 date string to a unix timestamp

    Parameters:
        - ISO 8601 string [required: string]

    Returns:
        Unix timestamp
    """
    datetime_obj = dateutil.parser.parse(d)
    unixtime = time.mktime(datetime_obj.timetuple())
    return unixtime


def formatted_nb(float_number, precision=6, width=5):
    """ Number formatting utility

    - Required parameters:
        A number

    - Optional parameters:
        precision, by default set to 6
        width, by default set to 5

    - Returned data:
        Formatted number

    """
    # formatted_rst = '{:{width}.{prec}f}'.format(float_number, width=width, prec=precision)
    formatted_rst = round(float(float_number), 6)
    return formatted_rst


def isValidAddress(address_lst):
    """ Basic sanity checks for a list of addresses

    - Required parameters:
        list of addresses to check

    - Optional parameters:
        (None)

    - Returned data:
        True if all addresses pass the sanity checks
        False if at least one of them fails
    """
    if isinstance(address_lst, str):
        if address_lst is None or len(address_lst) < 30 or not isAlphanumeric(address_lst):
            return False
    if isinstance(address_lst, list):
        for address in address_lst:
            if address is None or len(address) != 34 or not isAlphanumeric(address):
                return False
    return True

