
## Python Blocknet Trading Library

Requires Python **3.5+**


======

#### Expected benefits for the end-user of this library

======

- Place orders by price, instead of specifying explicitly the **maker_size** and **taker_size** parameters.
- Cancel by specifying tokens instead of tokens.
- Filter the orderbook by price or bid/ask size.
- Automatic address retrieval when placing or accepting an order.
- Automatically perform balance check before trying to post or accept an order, when appropriate.

### Useful links

======

Configuration:

- https://github.com/BlocknetDX/blocknet-docs/blob/master/blocknetDXtrader-setup.md

- https://github.com/BlocknetDX/blocknet-docs/blob/master/walletsCONF.md

- https://github.com/BlocknetDX/blocknet-docs/blob/master/xbridgeCONF.md


<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Getting Started](#markdown-header-getting-started)
- [Recommended](#markdown-header-recommended)
- [DX - Market actions](#markdown-header-dx-market-actions)
- [DX - Tokens](#markdown-header-dx-tokens)
- [DX - Cancel actions](#markdown-header-dx-cancel-actions)
- [DX - Order information](#markdown-header-dx-order-information)
- [DX - Orderbook](#markdown-header-dx-orderbook)
- [DX/Wallets - Balances](#markdown-header-dxwallets-balances)
- [DX/Wallets - Addresses](#markdown-header-dxwallets-addresses)
- [DX/Wallets - Unlock and relock](#markdown-header-dxwallets-unlock-and-relock)
- [Price information [External web services]](#markdown-header-price-information-external-web-services)
- [BTC specific information [External web services]](#markdown-header-btc-specific-information-external-web-services)
- [Statistics](#markdown-header-statistics)
- [Other utilities](#markdown-header-other-utilities)
- [DX object initialization](#markdown-header-dx-object-initialization)
- [DX.execute(LimitOrder/MarketOrder)](#markdown-header-dxexecutelimitordermarketorder)
- [Creating a new LimitOrder object](#markdown-header-creating-a-new-limitorder-object)
- [Creating a new MarketOrder object](#markdown-header-creating-a-new-marketorder-object)
- [Difference between a LimitOrder and a MarketOrder](#markdown-header-difference-between-a-limitorder-and-a-marketorder)
- [DX.dxMakeOrder(**kwargs)](#markdown-header-dxdxmakeorderkwargs)
- [DX.dxTakeOrder(**kwargs)](#markdown-header-dxdxtakeorderkwargs)
- [DX.getConnectedTokens()](#markdown-header-dxgetconnectedtokens)
- [DX.getTradeableTokens()](#markdown-header-dxgettradeabletokens)
- [DX.getNetworkTokens()](#markdown-header-dxgetnetworktokens)
- [DX.cancelId(txid)](#markdown-header-dxcancelidtxid)
- [DX.cancelOrders(**kwargs)](#markdown-header-dxcancelorderskwargs)
- [DX.cancelAll()](#markdown-header-dxcancelall)
- [DX.getOpenOrders(**kwargs)](#markdown-header-dxgetopenorderskwargs)
- [DX.getClosedOrders(**kwargs)](#markdown-header-dxgetclosedorderskwargs)
- [DX.getInfo(txid)](#markdown-header-dxgetinfotxid)
- [DX.getOrderbook(**kwargs):](#markdown-header-dxgetorderbookkwargs)
- [DX.getAsks(**kwargs)](#markdown-header-dxgetaskskwargs)
- [DX.getBids(**kwargs)](#markdown-header-dxgetbidskwargs)
- [DX.getPrice(token, base)](#markdown-header-dxgetpricetoken-base)
- [DX.getInputs(token)](#markdown-header-dxgetinputstoken)
- [DX.getDustAmount(token)](#markdown-header-dxgetdustamounttoken)
- [DX.dxGetTokenBalances()](#markdown-header-dxdxgettokenbalances)
- [DX.getWalletBalance(token)](#markdown-header-dxgetwalletbalancetoken)
- [DX.getBalance(token, address)](#markdown-header-dxgetbalancetoken-address)
- [DX.getAddressList(token, **kwargs)](#markdown-header-dxgetaddresslisttoken-kwargs)
- [DX.getTopAddress(token)](#markdown-header-dxgettopaddresstoken)
- [DX.getAddressForSize(token, size)](#markdown-header-dxgetaddressforsizetoken-size)
- [DX.validateAddress(self, token: str, address: str)](#markdown-header-dxvalidateaddressself-token-str-address-str)
- [DX.unlock(token, passphrase)](#markdown-header-dxunlocktoken-passphrase)
- [DX.unlockAll(passphrase)](#markdown-header-dxunlockallpassphrase)
- [DX.getFeePerByte(token)](#markdown-header-dxgetfeeperbytetoken)
- [DX.getTransaction(token, txid)](#markdown-header-dxgettransactiontoken-txid)
- [getCmcUSDPrices() [External data]](#markdown-header-getcmcusdprices-external-data)
- [getCmcUSDPrice(token: str) [External data]](#markdown-header-getcmcusdpricetoken-str-external-data)
- [getGeckoUSDPrice(token: str) [External data]](#markdown-header-getgeckousdpricetoken-str-external-data)
- [getGeckoCryptoPrice(token: str, base: str) [External data]](#markdown-header-getgeckocryptopricetoken-str-base-str-external-data)
- [getBtcRecommendedFees() [External data]](#markdown-header-getbtcrecommendedfees-external-data)
- [getBtcFeeList() [External data]](#markdown-header-getbtcfeelist-external-data)
- [getUnconfirmedTxCount(token) [External data]](#markdown-header-getunconfirmedtxcounttoken-external-data)
- [getBtcUnconfirmedTxCount() [External data]](#markdown-header-getbtcunconfirmedtxcount-external-data)
- [getBtcActualFee(txid) [External data]](#markdown-header-getbtcactualfeetxid-external-data)
- [getMean(x_arr: list)](#markdown-header-getmeanx_arr-list)
- [getVariance(x_arr)](#markdown-header-getvariancex_arr)
- [getRegression(x_arr: list, y_arr: list)](#markdown-header-getregressionx_arr-list-y_arr-list)
- [getQuantile(x_arr, percentile, interpolation)](#markdown-header-getquantilex_arr-percentile-interpolation)
- [isoDateStrToUnix(date)](#markdown-header-isodatestrtounixdate)
- [toBtc(sat)](#markdown-header-tobtcsat)
- [toSat(btc)](#markdown-header-tosatbtc)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->


### Getting Started

-----------------------

1) Clone the repo and install the requirement modules:

```
pip install -r requirements.txt
```

pip install will be made available down the road.

You may manually install the project or **later** use ```pip```:
```python
pip install blockdx
```

2) Locate the **xbridge.conf** file of your block client.

The path to this conf file will be necessary in the below steps.

3) Import the correct module and objects
```python

from blockdx import BlockDX, MarketOrder, LimitOrder, Error
import blockdx as lib
```

Barebone Sample bot:

```python

from blockdx import BlockDX, MarketOrder, LimitOrder, Error
import blockdx as lib

confFilePath = "..."
DX = BlockDX(ip="...", port="41414", rpcuser="user", rpcpass="pass", conf=confFilePath)

# If the wallets are locked by the same password
DX.unlockAll("mypwd")

# cancel all open transactions
DX.cancelAll()

""" IF YOU TRADE BTC """
btc_halfHourFee = 0
while not btc_halfHourFee > 0:
 recommended_fees = lib.getBtcRecommendedFees()
 btc_halfHourFee = recommended_fees.get("halfHourFee", 0)
 time.sleep(1)

# Check that the BTC network is not congested
IS_BTC_NETWORK_NOT_CONGESTED = lib.getBtcUnconfirmedTxCount("BTC") < 200000
ARE_BTC_RECOMMENDED_FEES_OK = lib.btc_halfHourFee < 1000 
 
IS_LTC_NETWORK_NOT_CONGESTED = lib.get_network_unconfirmed_tx_count("LTC") < 50000

"""
Trading is allowed only all conditions are met, ie the BTC and LTC networks are congested,
BTC fees are not outrageous and the Block network is fine.
"""
isTradingAllowed = all([IS_BTC_NETWORK_NOT_CONGESTED, ARE_BTC_RECOMMENDED_FEES_OK, IS_LTC_NETWORK_NOT_CONGESTED])

if not isTradingAllowed:
 print("BTC or LTC network is congested...")
 exit(1)

# Start trading...

newLimitOrder = LimitOrder(maker="BTC", taker="LTC", taker_size=10)
DX.execute(newLimitOrder)

```

### Recommended

-----------------------

- Do not submit orders with amounts that have a precision of more than 6 digits (not yet supported).
- Make sure the wallets are: 1) always synced to their respective networks when the bot is running. 2) unlocked.

- Make sure your wallet is unlocked before placing trades or accepting orders, and to relock the wallets once trading is done.
- Do not relock said wallets if you have an open order.

- Too heavy polling may result in *Broken Pipe* exceptions.

- Avoid to poll to often functions that pull data from external web services and check the return values to check for errors.
Some web services have rate limits and can sometimes be congested.

- When relying on data from these services, make sure to keep a copy of the last valid information, as a fallback.
It is advised to check for outliers before using these data in your decision making process.

- Inputs or address amounts that are less than dust may be ignored by the trading library and the dex.

- Run your bot with small amounts of cryptos before going in production mode.


### DX - Market actions

-----------------------

| function | Description
|------------|----------------------
| DX.postLimitOrder(...)  | Posts an order, no order matching engine.
| DX.execute(...)  | Executes a **LimitOrder** or a **MarketOrder**.
| DX.makeOrder(...)  | Posts an order, no order matching engine.
| DX.takeOrder(...) | Accepts an existing order from the provided txid.


### DX - Tokens

-----------------------

| function | Description
|------------|----------------------
| DX.getConnectedTokens()  | Retrieve the list of coins the DX is connected to.
| DX.getTradeableTokens()  | Retrieve the list of coins connected to the DX and having a non dust wallet balance.
| DX.getNetworkTokens()  | Retrieve the list of supported coins on the network.
| DX.getLocalTokens()  | Retrieve the list of supported coins by the local BLOCK client.


### DX - Cancel actions

-----------------------

| function | Description
|------------|----------------------
| DX.cancelId(...)  | Cancel a specific open transaction with the provided txid.
| DX.cancelOrders(...)  | Cancel a orders based on a token or group of tokens.
| DX.cancelAll()  | Cancel all open orders.


### DX - Order information

-----------------------

| function | Description
|------------|----------------------
| DX.getInfo(...)  | Retrieve detailed information about a transaction id.
| DX.getOpenOrders(...)  | Retrieve the list of still open transactions.
| DX.getClosedOrders(...)  | Retrieve the list of finished transactions.
| DX.getTransaction(...)  | Return information about a transaction on a specific blockchain.


### DX - Orderbook

-----------------------

| function | Description
|------------|----------------------
| DX.getOrderbook(...)  | Retrieve the real-time order book.
| DX.getAsks(...)  | Retrieve only the asks from the order book, with filtering options.
| DX.getBids(...)  | Retrieve only the bids from the order book, with filtering options.
| DX.getPrice(...)  | Retrieve the price of one crypto in terms of another crypto, as the mid point between the top ask and the top bid.


### DX/Wallets - Balances

-----------------------

| function | Description
|------------|----------------------
| DX.getWalletBalance()  | Get the total wallet balance of the provided token.
| DX.getBalance(...)  | Get the balance of the provided address and token.
| DX.getInputs(...)  | Return the list of all wallet inputs or the inputs of an address provided as parameter.


### DX/Wallets - Addresses

-----------------------

| function | Description
|------------|----------------------
| DX.getAddressForSize(...)  | Get the address from a token name (the one supplied in the **bot.conf** file).
| DX.validateAddress(...)  | Get the address from a token name (the one supplied in the **bot.conf** file).
| DX.getAddressList(...)  | Use this address if you have a wallet that is slown down by the many inputs it contains.
| DX.getInputs(...) | Use this address if you have a wallet that is slown down by the many inputs it contains.


### DX/Wallets - Unlock and relock

-----------------------

| function | Description
|------------|----------------------
| DX.isWalletUnlocked(...)  | Returns whether a wallet is unlocked.
| DX.isSynced()  | Returns whether a wallet is fully synced or not.
| DX.unlock(...)  | Unlocks a specific wallet.
| DX.unlockAll(...)  | Get the unconfirmed balance from a token name.
| DX.relock(...)  | Get the unconfirmed balance from a token name.
| DX.relockAll()  | Relock all connected token wallets.


### Price information [External web services]

-----------------------

| function | Description
|------------|----------------------
| getCmcUSDPrices(...)  | Returns the list of all available token prices in USD from CoinMarketCap.
| getCmcUSDPrice(...)  | Returns the USD price of a specific token from CoinMarketCap.
| getGeckoUSDPrice(...)  | External web service
| getGeckoCryptoPrice(...)  | External web service


### BTC specific information [External web services]

-----------------------

| function | Description
|------------|----------------------
| getBtcUnconfirmedTxCount()  | External web service
| getBtcRecommendedFees()  | External web service
| getBtcFeeList()  | External web service
| getBtcActualFee()  | External web service


### Statistics

-----------------------

Memory-efficient statistics functions based on NumPy.

| function | Description
|------------|----------------------
| getRegression  | Get the regression parameters from a dataset.
| getMean  | Get the mean of a dataset.
| getVariance  | Returns the variance from an array, to build basic volatility indicators.
| getQuantile  | Returns the quantile from an array. Useful to remove outliers from datasets.


### Other utilities

-----------------------

| function | Description
|------------|----------------------
| isodateStrToUnix  | Converts an ISO 8601 date string to a unix timestamp
| toBtc  | Converts satoshis to btc
| toSat  | Converts btc to satoshis


### DX object initialization

-----------------------

DX = BlockDX(ip="...", port="41414", rpcuser="user", rpcpass="pass", conf=confFilePath)

Parameters:
```
- ip [required]
- port [required]
- rpcuser [required]
- rpcpass [required]
- conf [required]
```


### DX.execute(LimitOrder/MarketOrder)

-----------------------

Executes a Limit or a Market Order

Parameters:
```
- LimitOrder/MarketOrder [required]
```

Usage examples:

```
// Limit order setting both maker_size and taker_size
limitOrder1 = LimitOrder(maker_size=1, maker="LTC", taker_size=2, taker="SYS")

// Limit order setting taker_size and price
limitOrder2 = LimitOrder(price=0.5, maker="LTC", taker_size=2, taker="SYS")

DX.execute(limitOrder1)
DX.execute(limitOrder2)

// For a MarketOrder, either maker_size or taker_size is specified, but not both
// Otherwise, the order is converted into a LimitOrder
marketOrder1 = MarketOrder(maker="LTC", taker_size=2, taker="SYS")
marketOrder2 = MarketOrder(maker="LTC", maker_size=0.5, taker="SYS")
                
DX.execute(marketOrder1)
DX.execute(marketOrder2)
```

### Creating a new LimitOrder object

-----------------------

Parameters:
```
- maker_size [required: numeric]
- maker [required: string]
- taker [required: string]
- taker_size [required: string]
- VERBOSE [optional: boolean, False by default]
```

Example #1
```
limitOrder = LimitOrder(maker_size=1, maker="LTC", taker_size=2, taker="MONA", VERBOSE=True)
DX.execute(limitOrder)
```

Example #2
by price:
```
limitOrder = LimitOrder(price=2, maker="XYZ", taker_size=1, taker="MONA", VERBOSE=True)
DX.execute(limitOrder)
```


Return value:

```
{"posted": [], "accepted": [], "errors": []}
```

```
{"accepted": [], "errors": []}
```

A JSON object with:
- the "accepted" key containing an array of the accepted orders.
- the "error" key containing an array of the errors that occured.


### Creating a new MarketOrder object

-----------------------

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| maker | string | required
| taker | string | required
| maker_size | numeric | optional
| taker_size | numeric | optional
| maker_address | string | optional
| taker_address | string | optional
| VERBOSE | boolean | optional | by default=False, set to True only for debugging/testing purposes.


Usage Example:
```
marketOrder = MarketOrder(maker="LTC", taker_size=2, taker="MONA", VERBOSE=True)
DX.execute(marketOrder)
```

### Difference between a LimitOrder and a MarketOrder

-----------------------

A LimitOrder will accept all orders that satisfy the specified taker_size/maker_size ratio or the price parameter.
If there are no or not enough open orders to satisfy, then remaining quantity to buy / sell is posted as an open order.


A MarketOrder will accept all orders until the taker_size is satisfied.
No open orders will be posted.


### DX.dxMakeOrder(**kwargs)

-----------------------

Posts an order to the DX without trying to match any open order.

Parameters:


| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| maker | string | required
| taker | string | required
| maker_size | numeric | required
| taker_size | numeric | required
| maker_address | string | optional
| taker_address | string | optional


Usage Example:
```
- DX.dxMakeOrder(maker="LTC", maker_size=1, taker="SYS", taker_size=10)
```

If the optional *maker_address* or *taker_address* parameters are not specified, the library will search automatically
search for suitable addresses.

It is advised to specify them explicitly, if you insist on trading from/to specific sets of addresses.


### DX.dxTakeOrder(**kwargs)

-----------------------

Accepts an open order with the supplied txid.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| txid | string | required
| maker_address | string | optional
| taker_address | string | optional


If the optional *maker_address* or *taker_address* parameters are not specified, the library will search automatically
search for suitable addresses.

It is advised to specify them explicitly, if you insist on trading from/to specific sets of addresses.


### DX.getConnectedTokens()

-----------------------

Returns the list of wallets connected to the DX.

```
No expected parameter(s).
```


### DX.getTradeableTokens()

-----------------------

Returns the list of tokens with a balance that is non dust.

```
No expected parameter(s).
```


### DX.getNetworkTokens()

-----------------------

Returns the list of supported tokens on the network.

```
No expected parameter(s).
```

### DX.cancelId(txid)

-----------------------

Cancel a specific transaction id.

Parameter:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| txid | string | required


Returns the following JSON if it succeeds:
```
{ "id": "", "time": ""}
```

Returns an error object if it fails:
```
{ "error": "error_description", "code": error_code}
```


### DX.cancelOrders(**kwargs)

-----------------------

Cancel orders with some filtering capabilities.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| maker | string | optional
| taker | string | optional


Usage examples:

```
DX.cancelOrders(maker="BTC")
DX.cancelOrders(taker="BTC")
DX.cancelOrders(maker="BTC", taker="LTC")
```

### DX.cancelAll()

-----------------------

Cancel all open orders and returns the list of cancelled orders.

```
No expected parameter(s).
```

Return value:
```
{"cancelled": 15, "errors": 0}
```

### DX.getOpenOrders(**kwargs)

-----------------------

Returns the list of open orders.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| maker | string | optional
| taker | string | optional


### DX.getClosedOrders(**kwargs)

-----------------------

Returns the list of closed orders.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| maker | string | optional
| taker | string | optional


### DX.getInfo(txid)

-----------------------

Returns detailed information about a transaction id.

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| txid | string | required


Sample returned data:
```
{ "id": "", "from": "BTC", "fromAddress": "btc_address", "fromAmount": 10, "to": "LTC", "toAddress": "ltc_address", "toAmount": 10, "state": "Open"}
```


### DX.getOrderbook(**kwargs):

-----------------------

Returns the live orderbook as a JSON object, with the relevant transaction ids.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| maker | string | required
| taker | string | required
| detailled | boolean | optional | by default = True

Usage example:

```
DX.getOrderBook(maker="LTC", taker="SYS")
DX.getOrderBook(maker="LTC", taker="SYS", detailled=False)
```

Sample returned data:

```
{ 'asks': [
            [price, size, 'txid'],
            [0.6, 2, 'ask_id_1'],
            ...
           ],
'bids': [
            [price, size, 'txid'],
            [0.6, 2, 'ask_id_1'],
            ...
           ]
}
```

### DX.getAsks(**kwargs)

-----------------------

Returns a detailed view of the asks.
Filtering is enabled via the optional parameters

Sample return value:

```
[
    [price, size, txid],
    [0.6, 4, 'ask_id_1'],
    ...
]
```

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| maker | string | required
| taker | string | required
| max_price | numeric | optional
| size_limit | string | optional
| size_floor | string | optional
| cost_limit | string | optional
| sort | string | optional | "asc", "desc", "asc" by default

It is recommended to use the **vol_limit** option and set the optional **sort** parameter to **"desc"** in most cases, 
to filter out irrelevant ask orders from the orderbook.


Examples:
```
// No filter applied, returns the BTC/LTC orderbook with LTC as a base token
DX.getAsks(maker="LTC", taker="BTC")
// limit the size of the returned ask orders up to 0.1 BTC for 1 LTC
DX.getAsks(maker="LTC", taker="BTC", max_price=0.1)
// Retrieve only the ask orders that are between 1 and 10 LTC
DX.getAsks(maker="LTC", taker="BTC", size_floor=1, size_limit=10)
// retrieve only the ask orders that are between 1 and 10 LTC and that would cost no more than 2 BTC
DX.getAsks(maker="LTC", taker="BTC", size_floor=1, size_limit=10, cost_limit=2)
```


### DX.getBids(**kwargs)

-----------------------

Returns a detailed view of the bids.
Filtering is enabled via the optional parameters.

Sample return value:

```
[
    [price, size, txid],
    [0.6, 4, 'bid_id_1'],
    ...
]
```

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| maker | string | required
| taker | string | required
| max_price | numeric | optional
| size_limit | string | optional
| size_floor | string | optional
| cost_limit | string | optional
| sort | string | optional | "asc", "desc", "asc" by default

It is recommended to use the **vol_limit** option and set the optional **sort** parameter to **"desc"** in most cases, 
to filter out irrelevant ask orders from the orderbook.

Examples:
```
// No filter applied, returns the BTC/LTC orderbook with LTC as a base token
DX.getBids(maker="LTC", taker="BTC")
// limit the size of the returned ask orders up to 0.1 BTC for 1 LTC
DX.getBids(maker="LTC", taker="BTC", max_price=0.1)
// Retrieve only the ask orders that are between 1 and 10 LTC
DX.getBids(maker="LTC", taker="BTC", size_floor=1, size_limit=10)
// retrieve only the ask orders that are between 1 and 10 LTC and that would cost no more than 2 BTC
DX.getBids(maker="LTC", taker="BTC", size_floor=1, size_limit=10, cost_limit=2, sort="asc")
```


### DX.getPrice(token, base)

-----------------------

Returns the price of **token** in terms of **base** token as:
1) the mid-point between the highest bid and the lowest ask prices if the orderbook contains both bids and asks.
2) the highest bid if the orderbook does not contain any asks.
3) the lowest ask if the orderbook does not contain any bids.
4) -1 if it fails.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required
| base | string | required


Example:
```
// Returns the LTC price in terms of BTC
DX.getPrice("LTC", "BTC")
```


### DX.getInputs(token)

-----------------------

Returns the list of all inputs from the specified **token** wallet.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required
| ignoreDust | boolean | optional | filter out dust inputs
| spendable | boolean | optional | filter out non spendable inputs


Returns an array with the inputs.


### DX.getDustAmount(token)

-----------------------

Returns the dust amount related to a specific token as a numeric value.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required

The following formula is applied:

```
dustAmount = 182 * (minRelayFee) / 1000 * 3
```
            

### DX.dxGetTokenBalances()

-----------------------

Returns the balances of the coins connected to the DX

```
No expected parameters.
```             
             
### DX.getWalletBalance(token)

-----------------------

Returns the wallet total balance related to a specific token.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required


### DX.getBalance(token, address)

-----------------------

Returns the address of the specified address for a specific token.
If no address is provided, it returns the wallet balance.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required
| address | string | optional | if set, returns the balance only of the set address


### DX.getAddressList(token, **kwargs)

-----------------------

Returns the list of addresses from the specified wallet

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required
| ignoreEmptyAddresses | boolean | optional


Sample return value:

```             
[
    ['Lc92jEzoKxUEhtWxqZUJW4g1NoPtzG8iUU', Decimal('0.05000000'), 'LTC'], 
    ['LL4BAv3LDU8Dtei5E2XH3VehV2rqvTmjFi', Decimal('0.01000000'), ''], 
    ...
]
```             


### DX.getTopAddress(token)

-----------------------

Returns the address with the highest balance from a specific wallet.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required
     

### DX.getAddressForSize(token, size)

-----------------------

Returns the address with a balance that contains at least the 'size' parameter

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required
| size | numeric | required


Returns:
	- an address if successful.
	- an Error object otherwise.


### DX.validateAddress(self, token: str, address: str)

-----------------------

Verify that an address is valid.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required
| address | string | required


Example:

```
// DX.validateAddress("BTC", "some_invalid_address")

==> {'isvalid': False}
```

```
// DX.validateAddress("BTC", "some_valid_address")

==> {'isvalid': True, 
		'address': 'LZNVpRrcgwdE2k8j1sAN3o8sH8PqYhi3wS', 
		'scriptPubKey': '76a9149b2f6e09978faf6ded7f4987f37941d7f0d3304088ac', 
		'ismine': False, 
		'iswatchonly': False, 
		'isscript': False, 
		'iswitness': False}
```


### DX.unlock(token, passphrase)

-----------------------

Unlock the specified wallet with the optional passphrase parameter.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required
| passphrase | string | required


### DX.unlockAll(passphrase)

Unlock all wallets with the provided passphrase.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| passphrase | string | required      


### DX.getFeePerByte(token)

-----------------------

Returns the fee per byte value from the *xbridge.conf* file, as an integer.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required          


### DX.getTransaction(token, txid)

-----------------------

Returns a JSON object of the details of a transaction from the specified **token** wallet.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required    
| txid | string | required    


Example:
```
{
 'amount': Decimal('29.60977131'), 
 'confirmations': 43444, 
 'blockhash': '...', 
 'blockindex': 2, 
 'blocktime': 1514640659, 
 'txid': '...', 
 'walletconflicts': [], 
 'time': 1514640632, 
 'timereceived': 1514640632
}
```

### getCmcUSDPrices() [External data]

-----------------------

Returns the list of dollar prices.

Data Provider: 
```
Coin Market Cap
```

Parameters:
```
No expected parameter(s)
```

### getCmcUSDPrice(token: str) [External data]

-----------------------

Returns the dollar value of a specified token.
Returns -1 if the token is not valid or the information is unavailable.

Provider: Coin Market Cap

Parameters:
```
- token [required]
```


### getGeckoUSDPrice(token: str) [External data]

-----------------------

Returns the dollar value of a specified token from the CoinGecko Web service.

Returns -1 if the token is not valid or the information is unavailable.

```
Data Provider: CoinGecko
```

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required    


### getGeckoCryptoPrice(token: str, base: str) [External data]

-----------------------

- Description

Returns the price of a token with a base token as the base token, from the Gecko Web service.

- Return value

Returns ```-1``` if the token is not valid or the information is unavailable.

- Data Provider:

```
CoinGecko
```

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required    
| base | string | required    


### getBtcRecommendedFees() [External data]

-----------------------

Returns a JSON object containing the recommended BTC fees.

```
No expected parameter(s).
```

- Data source:
```
https://bitcoinfees.earn.com/api
```

- Sample Return Value:
```
{"fastestFee":500,"halfHourFee":490,"hourFee":480}
```

### getBtcFeeList() [External data]

-----------------------

Returns a list of Fee objects that contain predictions about fees in the given range from minFee to maxFee in satoshis/byte.

```
No expected parameter(s).
```

- Data source:
```
https://bitcoinfees.earn.com/api
```

- Sample Return Value:
```
{"fees":[{"minFee":0,"maxFee":0,"dayCount":14,"memCount":0,"minDelay":10000,"maxDelay":10000,"minMinutes":9960,"maxMinutes":10000},{"minFee":1,"maxFee":10,"dayCount":21685,"memCount":21642,"minDelay":26,"maxDelay":10000,"minMinutes":180,"maxMinutes":10000},{"minFee":11,"maxFee":20,"dayCount":23053,"memCount":22885,"minDelay":18,"maxDelay":10000,"minMinutes":120,"maxMinutes":10000},{"minFee":21,"maxFee":30,"dayCount":19033,"memCount":18889,"minDelay":5,"maxDelay":10000,"minMinutes":30,"maxMinutes":10000},{"minFee":31,"maxFee":40,"dayCount":10350,"memCount":10199,"minDelay":4,"maxDelay":10000,"minMinutes":30,"maxMinutes":10000},{"minFee":111,"maxFee":120,"dayCount":3273,"memCount":3131,"minDelay":4,"maxDelay":10000,"minMinutes":25,"maxMinutes":10000},{"minFee":121,"maxFee":130,"dayCount":2728,"memCount":2622,"minDelay":4,"maxDelay":10000,"minMinutes":25,"maxMinutes":10000},{"minFee":131,"maxFee":140,"dayCount":1993,"memCount":1912,"minDelay":4,"maxDelay":10000,"minMinutes":25,"maxMinutes":10000},{"minFee":141,"maxFee":150,"dayCount":2708,"memCount":2617,"minDelay":4,"maxDelay":10000,"minMinutes":25,"maxMinutes":10000},{"minFee":151,"maxFee":160,"dayCount":1985,"memCount":1833,"minDelay":4,"maxDelay":143,"minMinutes":25,"maxMinutes":1320},{"minFee":161,"maxFee":170,"dayCount":2250,"memCount":2123,"minDelay":4,"maxDelay":140,"minMinutes":25,"maxMinutes":1200},{"minFee":171,"maxFee":180,"dayCount":3257,"memCount":3059,"minDelay":4,"maxDelay":140,"minMinutes":25,"maxMinutes":1200},
...
{"minFee":601,"maxFee":610,"dayCount":4570,"memCount":54,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25},{"minFee":611,"maxFee":620,"dayCount":1024,"memCount":16,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25},{"minFee":621,"maxFee":29600,"dayCount":27913,"memCount":528,"minDelay":0,"maxDelay":0,"minMinutes":0,"maxMinutes":25}]}
```

The Fee objects have the following properties (aside from the minFee-maxFee range they refer to):
```
- dayCount: Number of confirmed transactions with this fee in the last 24 hours.
- memCount: Number of unconfirmed transactions with this fee.
- minDelay: Estimated minimum delay (in blocks) until transaction is confirmed (90% confidence interval).
- maxDelay: Estimated maximum delay (in blocks) until transaction is confirmed (90% confidence interval).
- minMinutes: Estimated minimum time (in minutes) until transaction is confirmed (90% confidence interval).
- maxMinutes: Estimated maximum time (in minutes) until transaction is confirmed (90% confidence interval).
```

Remarks:
- An external web service is queried to retrieve the information.
- This is not BlockNet related data.


### getUnconfirmedTxCount(token) [External data]

-----------------------

- Description

Returns the number of unconfirmed transactions on the network of specified token, as an integer.

- Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required    

- Data Provider: 
```
chain.so
```

- The information is available for **BTC**, **LTC**, **DASH** and **DOGE** only.

- Returns:
	- an integer if successful.
	- an JSON error object if it fails.

	
- Remarks:
- An external web service is queried to retrieve the information.
- This is not BlockNet related data.
- In case the *chain.so* service is temporarily unavailable, ```get_btc_network_unconfirmed_tx_count``` can be used
to retrieve the same information, only for bitcoin.


- Use cases:
This information can be used to know:
	- whether it is appropriate to trade bitcoin now.
	- whether fees have to be adjusted higher to get priority.


### getBtcUnconfirmedTxCount() [External data]

-----------------------

Returns the number of unconfirmed transactions on the bitcoin network, as an integer.

```
No expected parameter(s).
```

- Data Provider: 
```
blockchain.info
```

- Returns:
	- an integer if successful.
	- an JSON error object if it fails.

	
Remarks:

- An external web service is queried to retrieve the information.
- This is not BlockNet related data.


### getBtcActualFee(txid) [External data]

-----------------------

- Description:

Returns the fee paid for a specific **bitcoin** txid

- Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| token | string | required  


- Data Provider: 
```
blockchain.info
```

Return value:
The returned integer represent the fees paid in satoshis.

Remarks:
- An external web service is queried to retrieve the information.
- This is not BlockNet related data.


### getMean(x_arr: list)

-----------------------

Returns the average of an array.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| x_arr | array | required 


Example:
```
getMean([1.0, 2.0, 3.0, 4.0, 5.0])
```

### getVariance(x_arr)

-----------------------

Returns the variance of an array.


Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| x_arr | array | required 


Example:
```
getVariance([1.0, 2.0, 3.0, 4.0, 5.0])
==> Result: 2
```


### getRegression(x_arr: list, y_arr: list)

-----------------------

Returns as an array the intercept and first degree coefficient of the regression equation.

Example:
```
getRegression([0.0, 1.0, 2.0, 3.0, 4.0, 5.0], [0.0, 0.8, 0.9, 0.1, -0.8, -1.0])
```

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| x_arr | array | required 
| y_arr | array | required 


Sample return value:
```
[-0.30285714  0.75714286]
```

### getQuantile(x_arr, percentile, interpolation)

-----------------------

Returns the specified quantile related to an array.
Based on the NumPy library.

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| x_arr | array | required 
| percentile | numeric | required | number in range of [0,100] (or sequence of floats)
| interpolation | string | optional | {"linear", "lower", "higher", "midpoint", "nearest" by default}


Additional details:
```
The interpolation parameter specifies the interpolation method to use when the desired quantile lies between two data points i < j:
linear: i + (j - i) * fraction, where fraction is the fractional part of the index surrounded by i and j.
lower: i.
higher: j.
nearest: i or j, whichever is nearest.
midpoint: (i + j) / 2.
```

Example:
```
blockdx.getQuantile([1, 2, 3, 4, 5, 6], 75)
=> Result: 5
```

### isoDateStrToUnix(date)

-----------------------

Converts an ISO 8601 date string to a unix timestamp

Parameters:

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| date | string | required  

Returns a Unix timestamp.

### toBtc(sat)

-----------------------

Converts satoshis to bitcoins.

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| sat | numeric | required  


### toSat(btc)

-----------------------

Converts bitcoins to satoshis.

| Parameter | Type | Required/Optional | Comment
|------------|----------------------|-----|------
| btc | numeric | required  

