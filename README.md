
To run the ledger js client:

1. Install the dependencies

```
npm install
```

2. To run:

```
parcel index.html --https
```

